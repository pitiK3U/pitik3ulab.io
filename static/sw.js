console.log("sw.js loaded");

const cacheName = 'v1';

self.addEventListener('install', function (event) {
  console.log("installed");
  event.waitUntil(
    caches.open(cacheName).then( function (cache) {
      return cache.addAll([
        './elasticlunr.min.js',
        './search_index.en.js',
        './style.css',
        './index.html',
        './manifest.json'
      ]);
    })
  );
});

// https://developers.google.com/web/ilt/pwa/caching-files-with-service-worker
self.addEventListener('fetch', function(event) {
  event.respondWith(
    caches.open(cacheName).then(function(cache) {
      // Try to get network version first
      return fetch(event.request).then(function(networkResponse) {
        // Update the cache
        // response may be used only once
        // we need to save clone to put one copy in cache
        // and serve second one
        cache.put(event.request, networkResponse.clone());
        return networkResponse;
      }) || cache.match(event.request).then(function(cacheResponse) {
        // If we failed to get network version use cached version
        return cacheResponse;
      });
    })
  );
});

/*
// Old cache
self.addEventListener('fetch', event => {
  // Let the browser do its default thing
  // for non-GET requests.
  if (event.request.method != 'GET') return;

  // Prevent the default, and handle the request ourselves.
  event.respondWith(async function() {
    // Try to get the response from a cache.
    const cache = await caches.open('v1');
    const request = new Request(event.request.url + "index.html");
    //request.url += 'index.html';
    const cachedResponse = await cache.match(request);

    if (cachedResponse) {
      // If we found a match in the cache, return it, but also
      // update the entry in the cache in the background.
      event.waitUntil(cache.add(request));
      return cachedResponse;
    }

    // If we didn't find a match in the cache, use the network.
    return fetch(event.request);
  }());
});


self.addEventListener('fetch', function(event) {
  event.respondWith(caches.match(event.request).then(function(response) {
    // caches.match() always resolves
    // but in case of success response will have value
    if (response !== undefined) {
      return response;
    } else {
      return fetch(event.request).then(function (response) {
        // response may be used only once
        // we need to save clone to put one copy in cache
        // and serve second one
        let responseClone = response.clone();

        caches.open('v1').then(function (cache) {
          cache.put(event.request, responseClone);
        });
        return response;
      });
    }
  }));
});
*/
