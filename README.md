# This project is custom repository for my notes from school. 

## Why custom notes?
I used several applications for notes but time showed that I needed something more powerful to fit my needs:
- Cross-platform (Be able to read my notes from my phone)
- Search my notes
- Custom hierarchy structure 
- Light-weight (No long loading times for text...)

I might even use this for blog, cv and personal web.

## Tool I am using
This project is created using the [zola](www.getzola.com) and is needed to run.
I found this tool to suit my needs best. This could be achieved by going from scratch but it took me too much time.

Zola is really power and creates small STATIC website.

## Running project
To run this project locally use: `zola serve` in the root of this repository.
To see the static version use: `zola build` and you'll find the result in the `public/` directory.
