+++
title = "VNV - Summary"
date = 2024-02-01
+++

# Obecný tvar diferenciální rovnice

$$
a_n \frac{d^ny}{dt^n} + a_{n-1} \frac{d^{n_1}y}{dt^{n-1}} + \dots + a_0 y
= b_m \frac{d^m z}{dt^m} + b_{m-1} \frac{d^{m-1} z}{dt^{m-1}} + \dots + b_0z
$$

- $y$ = hledané řešení
- $n$ řád diferenciální rovnice
- $m$ řád vynucující funkce (řád pravé strany)
- $a_i, b_j$ = koeficienty, $i = 0, \dots, n$; $j = 0, \dots, m$

# Laplaceova transformace
- převod do operátorového tvaru, zavádi se nový operátor (symbol) $p \equiv \frac{d}{dt}$
- $y' = \frac{dy}{dt} = p \cdot y$, $y'' = \frac{d^2 y}{dt^2}$, $\dots$
- $\int y \,dt = \frac{1}{p} y$

# Metoda snižování řádu derivace = MSŘD
- $y''' + a_2 y'' + a_1 y' + a_0 y = b_0 z$, pořáteční podmínky $IC \equiv \emptyset$
  **Nejsou derivace vynucující funkce na pravé straně**!
1. Operátorový tvar: $p^3 + a_2 p^2 y + a_1 p y + a_0 y = b_0 z$
2. Osamostatnění nejvyšší derivace: ($p^3 y$)
    - $p^3 y = - (a_2 p^2 y + a_1 p y + a_0 y - b_0 z)$
3. Postupně získání dalších rovnic:
   - $p^2 = \frac{1}{p} \cdot p^3 y$
   - $py = \frac{1}{p} \cdot p^2 y$
   - $y = \frac{1}{p} \cdot p y$

# Metoda snižování řádu derivace se zavedením pomocné proměnné = MSŘD PP
- **Jsou derivace pravé strany** (vynucující funkce)
- Převedeme rovnici $n$-tého řádu na $n$ rovnic $1$. řádu
- Otázka stability výpočtu
  - Lichý počet (invertujících) aktivních prvků ve smyčcce - záporná zpětná vazba
  - **Hurwitzovo kritérium** stability
- $y^{iv} + a_3 y''' + a_2 y'' + a_1 y' + a_0 y = b_4 z^{iv} + b_3 z''' + b_2 z'' + b_1 z' + b_0 z$
- Operátorový tvar: $p^4 y + a_3 p^3 y + a_2 p^2 y + a_1 p y + a_0 y = b_4 p^4 z + b_3 p^3 z + b_2 p^2 z + b_1 p^1 z + b_0 z$
1. Osamostatnění funkcí: $y (p^4 + a_3 p^3 + a_2 p^2 + a_1 p + a_0) = z (b_4 p^4 + b_3 p^3 + b_2 p^2 + b_1 p^1 + b_0)$
2. $\displaystyle y = \frac{z (b_4 p^4 + b_3 p^3 + b_2 p^2 + b_1 p^1 + b_0)}{p^4 + a_3 p^3 + a_2 p^2 + a_1 p + a_0}$
3. Zavedení pomocné proměnné: $\displaystyle v = \frac{z}{p^4 + a_3 p^3 + a_2 p^2 + a_1 p + a_0}$
4. **Výsledná rovnice**: $y = b_4 p^4 v + b_3 p^3 v + b_2 p^2 v + b_1 p^1 v + b_0 v$
5. $p^4 + a_3 p^3 v + a_2 p^2 v + a_1 p v + a_0 = z$ - již zde nejsou derivace vynucující funkce &Rarr; MSŘD
   - $p^4 v = - (a_3 p^3 v + a_2 p^2 v + a_1 p v + a_0 v - z)$
   - $p^3 v = \frac{1}{p} \cdot p^4 v$
   - $p^2 v = \frac{1}{p} \cdot p^3 v$
   - $p v = \frac{1}{p} \cdot p^2 v$
   - $v = \frac{1}{p} \cdot pv$

# Hurwitzův determinant - stabilita
1. $a_i, b_k > 0$
2. $a_i$ **splňují Hurwitzovo kritérium stability**
- Hurwitzův determinant $n$-tého řádu:
$$
H =
\begin{vmatrix}
a_3 & a_1 & 0   & 0   \\\\
a_4 & a_2 & a_0 & 0   \\\\
0   & a_3 & a_1 & 0   \\\\
0   & a_4 & a_2 & a_0
\end{vmatrix}
$$

- ***Všechny subdeterminanty $> 0$***

- $H_1 = a_3 > 0$
- $H_2 = \begin{vmatrix} a_3 & a_1 \\\\ a_4 & a_2 \end{vmatrix} = a_3 a_2 - a_4 a_1 > 0$
- $
H_3 =
\begin{vmatrix}
a_3 & a_1 & 0   \\\\
a_4 & a_2 & a_0 \\\\
0   & a_3 & a_1
\end{vmatrix}
= a_3 a_2 a_1 - a_3^2 a_0 - a_1^2 a_4 > 0$
$
- $H_4 = a_0 H_3 > 0$

# Generování funkcí
- vynucující funkce $z$ - převod na diferenciální rovnici
1. $z = e^t$
2. $z' = e^t$
3. $z' = z$, počáteční podmínky ! **$z(0) = e^0 = 1$
4. Operátorový tvar: $pz = z$
5. Výsledná rovnice $z = \frac{1}{p} z$
- Výsledné zapojení s invertujícími prvky má sudý počet aktivních prvků ve zpětné vazbě &rarr; řešení je nestabilní!

# Metoda Postupné Integrace (MPI)
- Na pravé straně derivace vynucující funkce
- $y^{iv} + a_3 y''' + a_2 y'' + a_1 y' + a_0 y = b_4 z^{iv} + b_3 z''' + b_2 z'' + b_1 z' + b_0 z$
- Operátorový tvar: $p^4 y + a_3 p^3 y + a_2 p^2 y + a_1 p y + a_0 y = b_4 p^4 z + b_3 p^3 z + b_2 p^2 z + b_1 p^1 z + b_0 z$
- $IC = \emptyset$ - pro každou derivaci (tedy 4)
1. **Osamostatnění nejvyšší derivace hledaného řešení**
    - $p^4 y = b_4 p^4 z + p^3 (b_3 z - a_3 y) + p^2 (b_2 z - a_2 y) + p (b_1 z - a_1 y) + (b_0 z - a_0 y)$
2. Vynásobení $\frac{1}{p}$
   - $p^3 y = b_4 p^3 z + p^2 (b_3 z - a_3 y) + p (b_2 z - a_2 y) + (b_1 z - a_1 y) + \frac{1}{p} (b_0 z - a_0 y)$
3. **Zavedení nové proměnné**
   - $V1 = \frac{1}{p} (b_0 z - a_0 y)$
   - $p^3 y = b_4 p^3 z + p^2 (b_3 z - a_3 y) + p (b_2 z - a_2 y) + (b_1 z - a_1 y) + V1$
4. Opakování 2. a 3. kroku, dokud nedostaneme výsledné řešení
   - $p^2 y =b_4 p^2 z + p (b_3 z - a_3 y) + (b_2 z - a_2 y) + \frac{1}{p} (b_1 z - a_1 y + V1)$
   - $V2 = \frac{1}{p} (b_1 z - a_1 y + V1)$
   - $p y = b_4 p z + (b_3 z - a_3 y) + \frac{1}{p} (b_2 z - a_2 y + V2)$
   - $V3 = \frac{1}{p} (b_2 z - a_2 y + V2)$
   - $y = b_4 z + \frac{1}{p} (b_3 z - a_3 y + V3)$

## Kombinovaná metoda MPI a MSŘD
- $n > m$ = řád levé strany je větší jak řád pravé strany (opět derivace vynucující funkce
- můžeme "vyrobit" nejen $y$, ale i $y'$, $y''$, $\dots$, až do řádu $n - m$
- $y'''' + a_3 y''' + a_2 y'' + a_1 y' + a_0 y = b_2 z'' + b_1 z' + b_0 z$
1. Operátorový tvar: $p^4 y + a_3 p^3 y + a_2 p^2 y + a_1 p y + a_0 y = b_2 p^2 z + b_1 p z + b_0 z$
2. MPI:
   - $p^4 y = - a_3 p^3 y + p^2 (b_2 z - a_2 y) + p (b_1 z - a_1 y) + (b_0 z - a_0 y)$
   - $p^3 y = - a_3 p^2 y + p (b_2 z - a_2 y) + (b_1 z - a_1 y) + \frac{1}{p} (b_0 z - a_0 y)$
   - $V1 = \frac{1}{p} (b_0 z - a_0 y)$
   - $p^2 y = - a_3 p y + (b_2 z - a_2 y) + \frac{1}{p} (b_1 z - a_1 y + V1)$
   - $V2 = \frac{1}{p} (b_1 z - a_1 y + V1)$
   - $p^2 y = - a_3 p y + (b_2 z - a_2 y) + V2$
   - Už zde není derivace vynucující funkce
3. Dále podle MSŘD
   - $p y = \frac{1}{p} p^2 y$
   - $y = \frac{1}{p} p y$
