+++
title = "05 - Komunikátory a Topologie"
date = 2024-03-04
+++

# Komunikátory

## Tag
- není unikátní
- unikátní pouze kombinace **komunikátor + tag**

##
- "mřížkový scatter"
    - rekurzivní algoritmy
    - vhodné pro velké NUMA domény - místo síťovky, tak přes L3

## Skupiny procesů
- `MPI_COMM_WORLD` = vzniká při `MPI_Init`
    - neobsahuje dynamicky nastartované procesy!

## Vytváření a úprava komunikátorů
- **Kolektivní operace**!!!
    - musí všichni zavolat

## Pojmenování komunikátoru a reportování chyb
- `MPI_COMM_set_name`!
- komunikátor je pouze číslo, pro reportování je lepší lidský popis
- pro každý komunikátor lze nastavit jak má řešit chyby

## Řádkové komunikátory na 2D mřížce
- **Na zkoušce !** - ale místo řádků, tak sloupce
- slide 11

## Komunikátor bez procesu
- `MPI_UNDEFINED` - do splitu pro vyřazení procesu, proces pak dostane `MPI_COMM_NULL`

# Topologie

## Sousedské kolektivní komunikace
- **Důležité pro projekt**
- slide 28
- pro každý vrchol topologie **allgather**
    - aranžování v rámci topologie
    - pouze 1 komunikace
    - prvně nejdříve negativní
    - není potřeba řešit okrajové podmínky, vyignorují se

## Grafové topologie