+++
title = "08"
date = 2024-03-25
+++

# HPC
- problém = generuje se extrémně velké množství dat

## Strategie I/O
- 3 úrovně hierarchie - ssd, disky (raid 6), pásky
- často = natáhni data ze vzdáleného úložiště na lokální scratch buffer

### Disky
- rychlost zápisu 500 MB/s
- stripe = +- 1MB
- zápis velkých dat = souvislé bloky dat, rozdělené na 1MB

### Serial I/O
- pouze **1** rank může zapisovat, stará se pouze o I/O
- všechny I/O operace jdou přes něj
- 1 síťovka tohoto ranku je úzkým hrdlem!

### Parallel I/O Multi-file
- do jistého okamžiku nejrychlejší způsob!
- simulace, kdy každých n kroků je checkpoint! = nejlepší možnost
- zahlcení FS tvorbou velkého množství souborů, prealokace dat do souboru = další zahlcení

### Parallel I/O with a Single File
- musí se dát pozor, aby se nezapisovalo na stejné části paměti

## POSIX I/O

## MPI I/O
- kolektivní operace!
- pro jednotlivé operace = vytáhnu které chci do vlastního komunikátoru
- `MPI_MODE_SEQUANTIAL` = hodí se pro pásky