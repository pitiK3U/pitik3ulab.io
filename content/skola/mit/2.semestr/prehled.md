+++
title = "Přehled 2. semestr"
date = 2024-02-06 
+++

# FLP
- [ ] Půlsemestrální zkouška z FLP 18. 3. 2024 (písemná část půlsemestrální zkoušky); 20 bodů
- [ ] Funkcionální projekt - Haskell; **min 4** z 12 bodů
- [ ] Logický projekt - Prolog; **min 3** z 8 bodů
- bonusové body cvičení až 6 bodů

## Zápočet
- Potřeba 20 bodů ze 40

## Zkouška
- Min 25 bodů ze 60

# MPC-MAT
1. závěrečná zkouška 70 bodů					
2. písemný test	20 bodů					
3. domácí úkoly 5 bodů					
4. domácí úkoly	5 bodů
- Zápočet získá každý student, který dosáhne ve cvičení nejméně 10 bodů. (20 + 5 + 5)

# PRL
- [ ] Půlsemestrální zkouška 20.3.; 10 bodů
- [ ] 1. projekt MPI; **min 1** z 10 bodů
- [ ] 2. projekt MPI; **min 1** z 10 bodů
- Zápočet **15** bodů z 30

# PPP - Praktické paralelní programování
- [ ] Půlsemestrální test 25.3.; 15 bodů
- [ ] Projekt; **min 1** bod z 25
- Zápočet nutno **20 bodů** ze 40

## Zkouška
- **min 20 bodů** z 60

# VNV
- Počítačová cvičení; 20 bodů
- Půlsemestrální test; 20 bodů

## Zkouška
- **min 29** z 60 bodů