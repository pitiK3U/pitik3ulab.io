+++
title = "Přehled"
date = 2023-09-25
+++

# TIN

## Zápočet
- min. 18 bodů z 40 bodů z projektů a testů
- 2 testy
    - 4\. týden (10 bodů)
    - 9\. týden (15 bodů)
- 3 domácí úkoly, každý za 5 bodů

## Zkouška
- min. 25 bodů z 60 bodů
- 4 části; z každé min. 4 body

# AVS

## Zápočet
- 2 projekty (v rozsahu 14 hodin)
- Získání **20 ze 40 bodů** za projekty a půlsemestrální písemku.
- **Z každého projektu** je nutné získat minimálně **1 bod**.

## Zkouška
- min. 20 bodů z 60

# HSC

## Zápočet
- min. 5 bodů z projektu
- projekt 25 bodů (10 povinných, 15 nepovinných?)
- 20 bodů půlsemestrální zkouška

## Zkouška
- min. 25 bodů z 55 bodů

# MSP

## Zápočet
- **účast na cvičení povinná**, (max 2 absence)
- min. 20 bodů z 40 bodů z (2) testů a (2) projektů
- 2 testy:
    - 1\. test: 10 bodů, 5. týden - Markovské procesy a na základy randomizovaných algoritmů
    - 2\. test: 10 bodů, 10. týden - *pokročilejší statistické metody*
- 2 projekty:
    - 1\. projekt: 8 bodů (2 body minimum) -- Statistika a programování.
    - 2\. projekt: 12 bodů (4 body minimum) -- Pokročilá statistika.

## Zkouška
- min. 25 bodů z 60 bodů

# FIT

# GZN

## Zápočet?
- půlsemestrální test - 9 bodů
- projekt - 40 bodů
- závěrečná zkouška - 51 bodů, min zisk 20 bodů

### Alternativně:
- půlsemestrální test - 19 bodů
- projekt - 30 bodů
- závěrečná zkouška - 51 bodů, min zisk 20 bodů

# THE

## Zápočet
- Vypracování individuálního projektu a získání alespoň poloviny bodů (20 ze 40).

## Zkouška
- min. 20 bodů z 60