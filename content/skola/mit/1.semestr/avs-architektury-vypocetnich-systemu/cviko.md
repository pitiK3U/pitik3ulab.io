+++
title = "cviko"
date = 2023-10-05
+++

- `it4free` - počet hodin na uzlech

- po VNC nastavit `xhost +`, aby jakýkoliv cluster mohl použít Xsession

- `squeue --me -l`

- `a * a + b * a + b * b`
- pouze 1 načtení `a` + 1 načtení `b`
- `5/(2*4)`

- icpc vs icpx
    - radši použít icpc
    - kvůli lepšímu logování

- `OMP_PROC_BIND=close` - vlákna se na cpu vytváří blízko sebe
- `OMP_PLACES=cores`