+++
title = "Summary for exam"
+++

# Fixed points
$$
f(x^*) = x^*
\implies f'(x^*) = 0
$$

## Stability

## Eigenvalues and eigenvectors

# 2D

## nullclines
- fixed pointes in 1D
- $f'(x^*) = 0$ and $g'(y^*) = 0$ independently
- **cross between nullclines = fixed point!!**

## Linear classification of FP
- Jacobian 
$$
J = \begin{pmatrix}
\frac{\partial f}{\partial x} & \frac{\partial f}{\partial y} & \ldots \\
\frac{\partial g}{\partial x} & \frac{\partial g}{\partial y} \\
\vdots & & \ddots
\end{pmatrix}
$$

$$
A = \begin{pmatrix}
a & b \\
c & d 
\end{pmatrix} \\
\text{trace: }\tau = a + d\\
\Delta = \det A = ad - cb
$$

![https://www.researchgate.net/profile/Fabrice-Laussy/publication/301840280/figure/fig9/AS:391132958740558@1470264772604/Classification-of-the-fixed-points-of-the-dissipative-Bosonic-Josephson-Junction-The.png](https://www.researchgate.net/profile/Fabrice-Laussy/publication/301840280/figure/fig9/AS:391132958740558@1470264772604/Classification-of-the-fixed-points-of-the-dissipative-Bosonic-Josephson-Junction-The.png)

## Eigenvalues and eigenvectors
$$
\lambda_{1,2} = \frac{\tau \pm \sqrt{\tau - 4 \Delta}}{2}
$$

- $\lambda_i < 0$ = **stable** (= decays exponentially = arrow towards FP)
- $\lambda_i > 0$ = **unstable** (= grows exponentially = arrow from FP)
- $|\lambda_i| < |\lambda_j|$
    - $\lambda_i$ = slow direction
    - $\lambda_j$ = fast direction

### eigenvector
- rref of $A - \lambda_i * I$

## Transition to polar coordinates
- $x = r \cos \theta$, $y = r \sin \theta$
- $r^2 = x^2 + y^2$
- $r \dot{r} = x \dot{x} + y \dot{y}$
- $\dot{\theta} = \frac{x \dot{y} - \dot{x} y}{r^2}$