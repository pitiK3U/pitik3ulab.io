+++
date = 2025-02-26
title = "07 - Bifurcations in 2D"
+++

# saddle-node bifurcation
- fp appears "out of blue" - no fixed points (ghost/bottleneck) -> 1 point -> 2 points (saddle and stable)

## Example I: Genetic control
- Stable manifold that separates them into 2

## Example III:
- Look at symmetries! ()

# Hopf bifurcation
- can only happen in 2D or higher

## Supercritical Hopf bifurcation
- For exam

## Subcritical hopf bifurcation
- error on slide with $\dot{r} = 0 \dots$
- important is that as $\mu$ gets lower (under 0) it splits into stable center and unstable circle (the other circle doesn't matter)

# Poincare maps

## example
- start from right side
$$
    \int_0^{2\pi} d \theta = 2 \pi
$$