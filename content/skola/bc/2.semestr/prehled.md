+++
title = "Přehled"
date = 2021-03-01
+++
# IB002
Nutnou podmínkou pro účast na závěrečné zkoušce je zisk alespoň 15 bodů za seminární odpovědníky. Celkový počet bodů za seminární odpovědníky je 26 (13 týdnů výuky).

### Závěrečná zkouška

Závěrečná zkouška je složena ze dvou částí (implementační a znalostní).

**Implementační část zkoušky** se bude konat v den zkoušky dopoledne. Úkolem bude naprogramovat řešení zadaného příkladu rozděleného do jednotlivých úloh. Povolený programovací jazyk je stejně jako v domácích úkolech, tj. **Python 3.7**.

Z implementační části lze získat 100 bodů, ke kterým bude připočten bodový zisk za speciální domácí úkol. K úspěšnému absolvování implementační části je potřeba v součtu získat alespoň 50 bodů, tento limit se navyšuje o 1 bod za každý neodevzdaný domácí úkol.  

**Znalostní část zkoušky** se bude konat v den zkoušky odpoledne. Tato část bude písemná.

Ze znalostní části lze získat maximálně 100 bodů. K úspěšnému absolvování znalostní části zkoušky je potřeba získat alespoň 50 bodů.

Neúspěch u závěrečné zkoušky se řeší separátním opakováním neúspěšné části dle [SZŘ](https://is.muni.cz/do/mu/Uredni_deska/Predpisy_MU/Masarykova_univerzita/Vnitrni_predpisy_MU/Studijni_a_zkusebni_rad/), tj. pokud student neuspěl pouze v jedné části, opakuje jen tuto. Neomluvená neúčast na žádném z řádných termínů (tj. i když se nepřihlásíte) znamená hodnocení "-".

## Absolvování a hodnocení
- Pro úspěšné absolvování předmětu je nutné získat alespoň 15 bodů za seminární odpovědníky, alespoň 50 až 60 bodů (dle odevzdaných domácích úkolů) z implementační části (a za spec. DÚ) a alespoň 50 bodů ze znalostní části závěrečné zkoušky.

- Výsledné hodnocení závisí na počtu všech získaných bodů, tj. součtu bodů z odpovědníků, z implementační části závěrečné zkoušky (a spec. DÚ) a ze znalostní části závěrečné zkoušky, a to následujícím způsobem:
```
>= 203 bodů --- hodnocení A
>= 181 bodů --- hodnocení B
>= 159 bodů --- hodnocení C
>= 137 bodů --- hodnocení D
>= 115 bodů --- hodnocení E
 < 115 bodů --- hodnocení F
```

# MB151
- Nutnost získat **10 čárek za účast na cvičení**
	- aktivní účast na online cvičení
	- pasivní účast na online cvičení + odpovednik 30%
	- odpovednik na 60%
- 2 vnitrosemestrální test P1 a P2
- závěrečná zkouška -3 částí = P3, R, O
- Každá část na 20 bodů a cca 45 minut
- Celkem max 100 bodů:

A. 90 - 100 bodů
B. 80 - 89 bodů
C. 70 - 79 bodů
D. 60 - 69 bodů
E. 50 - 59 bodů
F.  0 - 49 bodů

- V testech P1, P2, P3 budou 2 příkady, které se počítají na papír, fotka se následně odevzdá do ISu. Půjde spíše o standardní (algoritmické) příklady. Při opravování se hodnotí správnost postupu i výsledek.
- Test R sestává z cca deseti rychle řešitelných příkladů. Tuto část opravuje IS, který kontroluje pouze správné číselné odpovědi.
- Část O je odpovědník otevřených otázek. Zde se zkouší jednak teorie ataké pochopení probrané látky na nestandardních příkladech. Odpovídá se přímo do ISu pomocí textu, který opravují vyučující.

**Pouze jeden řádný termín!!**

- Při opravném termínu se opravují povinně části O, P3 a R.
- Lze se rozhodnout, jestli opravovat i vnitrosemestrální zisky

# PB071
- https://www.fi.muni.cz/pb071/
- Domácí úkoly - 5 + 1 za semestr (2 týdny na úkol)

## Zápočet 
- https://www.fi.muni.cz/pb071/info/exam.html
- alespoň 60 bodů z odpovědníků + [dú](https://www.fi.muni.cz/pb071/info/homeworks.html)
- Alespoň **4 domácí úkoly** s kladným hodnocením.
- Úspěšná obhajoba vypracování některého domácího úkolu.
- Alespoň jeden průchod zápočtovým drilem s úspěšností nejméně 90 %.

## Zkouška
- https://www.fi.muni.cz/pb071/info/exam.html#zkou%C5%A1ka

# PV004
- http://www.fi.muni.cz/usr/brandejs/unix/ - slajdy
- https://is.muni.cz/auth/el/fi/jaro2021/PV004/op/organizacni_pokyny_PV004.txt

## Milníky
- 3 povinné milníky 
- https://is.muni.cz/auth/edutools/brandejs/pv004lab

## Zkouška

Zkouška proběhne formou samostatného zpracování zkouškového úkolu.
Zkouškový úkol bude studentovi dostupný po splnění čtyř milníků.
Čtvrtý milník nebude omezen konkrétním datem a nebude penalizován.
Čtvrtý milník bude obsahovat závěr probírané látky nepokrytý povinnými
milníky 1 až 3.

Zkouškový úkol musí student splnit nejpozději do konce zkouškového
období.

Student si volí, jaký rozsah úkolu chce zpracovat. Úkol odevzdává v laboratoři
v ISu, který řešení zkontroluje na funkčnost i unikátnost. Nefunkční řešení
nebo řešení podobné dříve odevzdanému řešení kteréhokoli studenta IS nepřijme.
Podle rozsahu splněného úkolu student obdrží hodnocení.

Další podrobnosti v předmětovém diskusním fóru v ISu

# PB152
## Semestr
- potřeba splnil 2/3 vniter, 8 otázek, 15min, max 1 chyba
## Zkouška
- 1\. část - pass/fail, 12 otázek, 20 min, max 1 chyba
- 2\. část - 12 otázek, 90 min, +1/-0.5 za otázku
hodnocení:
- A = 10+
- B = 9+
- C = 8+
- D = 7+
- E = 6+
# PB156
Zkouska pomocí odpovědníku v ISu. 10 otázek na 90 minut. (1 bonusová - chyták nebo něco zajímavého, pokud více než z těch 10 dostane víc bodů)
V době poslední přednášky.

# VB036 
Requirements:
1. **Individual In-class Presentation**
2. **Presentation Handout**
3. **Presentation Style Review**
4. **ROPOTs** (quizzes)
5. **Attendance**
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTU1NjExNjU3MCwtNzI5MDMxMTA4LC0yMj
A5NTE5Myw0MTMyOTgyNDcsMTA3NTYwMjQ3NCwxMTE4NTMwOTU1
LC0xNTQ1NDkwNDg4LDcwMjEwMDE2MywxMDYzNjA2NTg2LC0xMz
MwODU5MjA4LC0yMDA1MjM4NTk1LDE5MzY1ODk3OTgsMTIwMTE5
MzE3MCwxMjM2OTI1MTk0LC0yNzk5MzEzMDldfQ==
-->