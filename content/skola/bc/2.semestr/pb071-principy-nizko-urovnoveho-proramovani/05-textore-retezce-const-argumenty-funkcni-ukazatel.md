+++
title = "05 - Textové řetězce, const, argumenty main, funkční ukazatel"
date = 2021-03-29
+++

# Textové řetězce
- pole `char`ů
- `\0` binární nula - speciální znak na ukončení
- nemusíme udržovat délku řetězce - změna délky pomocí posunu nuly

```c
char myString[100];
char myString2[] = "Hello";
```

- Od C99 lze i široké (wide) znaky řetězce
```c
wchar_t mySideString[100]; // 99 wide znaků + '\0'
```

## Řetězová konstana v C
- uzavřena v `""`
- uložena ve statické sekci programu
- konstanty pro široké (unicode) řetězce mají předřazené `L`
    - `L"Děsně šťavňaťoučké"`
    - `sizeof(L"Hello world") == sizeof("Hello world") * sizeof(wchar_t)`
- Lokalizace se řeší jinak - v kódu unikátní čísla řetězců, samotené řetězce jinde (soubor, res)

## Inicializace řetězců
- inicilizace řetězce pole 
    - `char answers[] = {'a','y','n'};` - nevloží koncovou nulu
    - `char answers[] = "ayn";` - vloží koncovou nulu
- Rozdíl ukazatel vs pole
    - `char *myString = "Hello";` - ukazatel na pozoci řetězcové konstanty
    - `char myString[50] = "Hello";` - nová proměnná typu pole, inilializována na "Hello"
- Řetězcové konstanty nelze měnit
- `strlen()`

## Manipulace
- jako ukazatel
- jako pole - `[]`
- knihovní funkce - `<string.h>`
    - `strcpy`, `strcat`, `strlen`, `strcmp`, `strchr`, `strstr`
- manipulace s širokými znaky `<wchar.h>`
    - `wchar_t *wcscpy(wchar_t *, const wchar_t *);`
- testování znaků v řetězci `<ctype.h>`

## Časté problémy
- nedostatečně velký cílový řetězec
- chybějící koncová nula
    - N+1 problém
- nelvožení koncové nuly - `strncpy`
- délka/velikost řetězce bez místa pro koncovou nulu

## Pole řetězců
- pole ukazatelů na řetězce
- typicky nepravoúhlé
- použití často pro skupiny konstantích řetězců
    - `char *dayNames[] = {"Pondeli","Utery","Streda"};`

## Klíčové slovo `const`
- označení **proměnné, která nesmí být změněná**
- kotrolováno **překladačem**
- zavedeno proti nezáměrným implementačním chybám
- umožňuje optimalizaci 
- proměnné jsou lokální v daném souboru
- kostantní je pouze hodnota označené proměnné
- Není konstantní objekt odkazovaný
    - konstatní proměnnou lze modifikovat přes nekonstatní ukazatel

```c
int value = 0;
// Ukazatel na proměnnou typu const int
const int *pValue = &value;
*pValue = 10; // ERROR
pValue = &value; //OK

// konstatní ukazatel na proměnou typu int
int * const pValue2 = &value;
*pValue2 = 10; // OK
pvalue2 = &value; // ERROR

const int * const pValue3 = &value;
*pValue3 = 10; // ERROR
pvalue3 = &value; // ERROR
```

### Pomůcka
- `const` se aplikuje na typ vlevo, pokud to nejde, tak na typ vpravo

# Argumenty funkce `main()`
- `int main(void);` - bez parametrů
- `int main(int argc, char *argv[]);`
    - parametry předané při spuštení
    - `**argv == *argv[]`
- `int main(int argc, char **argv, char **envp);`
    - navíc proměnné prostředí
- `binary.exe -test -param1 hello "hello world"`
- `argc` - obsahuje počet parametrů
    - pokud `argc > 0`, první je cesta k programu
- `argv[]` - pole řetězců, obsahuje každý jeden parametr
- `envp[]` - je ukončeno pomocí `NULL`
    - ve formátu `NAZEV=hodnota`
    - lze využít funkci `getenv()` v `<stdlib.h>`

# Funkční ukazatel
- callback funkce
- &rarr; Událostmi řízené programování
- funkční ukazatel obsahuje adresu umístění kódu funkce
    - na drese je kód funkce
- ukazatel na funkci pomocí `&`
    - `&Analyze`
- ukazatel na funkci lze uložit do proměnné
    - `návratový typ (*jméno_proměnné) (typy argumentů)`
```c
int (*pAnalyze) (const char *) = &Analyze;
```

## Konvence volání
- kdo bude uklízet zásobník
    - uklízí volající funkce (cdecl) - default pro C/C++ programy
    - uklízí volaná funkce (stdcall) - např. Win32 API
- GCC: `void foo(int a, char b) __attribute__((cdecl));`
- Microsoft, Borland: `void __cdecl foo(int a, char b);`
- pře nekompabitilní kombinace dochází k porušení zásobníku
- https://en.wikipedia.org/wiki/X86_calling_conventions
- http://newty.de/fpt/
- http://newty.de/fpt/zip/e%20fpt.df
