+++
title = "04 - Pole, vícerozměrné pole, ukazatelová aritmetika, typový systém"
date = 2021-03-22
+++

[Slides](https://drive.google.com/file/d/1UE3o6_wZWg9ftUbciY_1DcbWWFgEwoor/view)

# Práce s poli

- C **nehlídá** meze při přístupu: (Ok pro kompilátor, může nastat vyjímka, ale nemusí)
```c
int array[10];
array[100] = 1;
```

## Proměnná délka
- Dynamická alokace
    - proměnná typu ukazatel `int *pArray;`
    - místo na pole alokujeme (a odebíráme) pomocí speciálních funkcí na **haldě** (**heap**)
        - `malloc()`, `free()`
- Deklerace pole s variabilní délkou
    - variable length array (VLA)
    - až od C99
    - alokuje se na **zásobníku**
    - není nutné starat se o uvolnění (lokální proměnná)
    - nedoporučuje se pro přiliš velká pole (použít haldu)
    - nedoporučuje se používat pro hodnotu zadanou uživatelem bez důkladné kontroly (příliš velké)
```c
int arraySize = 20;
scanf("%d", &arraySize);
int arrayLocal[arraySize];
arrayLocal[10] = 9;
```

# Ukazatelová aritmetika
- Aritmetické operátory prováděné nad ukazateli
- využívá se faktu, že `array[X]` je definován jako `*(array + X)`
- Operátor `+` přičítá k adrese na úrovni prvků pole
    - Nikoli na úrovni bajtů!
    - Nař. pokud je `array` je typ `int *`, tak se přičte `X * sizeof(int)`

## Typické problémy
- Zápis do pole bez specifikace místa
    - `int array[10]; array = 1;`
    - proměnnou typu pole nelze naplnit
- Zápis těsně za konec pole, častý "N+1" problém
    - `int array[N]; array[N] = 1;`
    - v C pole se indexuje od 0
- Zápis za konec pole
    - např. důsledek ukazatelové aritmetiky nebo chybného cyklu
    - `int array[10]; array[someVariable + 5] = 1;`
- Zápis před začátek pole
    - méně časté, ukazatelová aritmetika

# Vícerozměné pole
- Pravoúhlé pole NxM
    - stejný počet prvků v každém řádku
    - `int array[N][M];`

## Reprezentace vízerozměrného pole jako 1D
- `array2D[row][col]` &rarr; `array1D[row * NUM_COLS + col];`
- `array3D[x][y][z]` &rarr; `array1D[x*(NUM_Y*NUM_Z) + y*NUM_Z + z];`

## Nepravoúhlé pole
- Pole, které nemá pro všechny "řádky" stejný počet prvků
    - dosahováno typicky pomocí dynamické alokace
    - lze ale i pomocí statické alokace (řádek nebude delší než `x`)
- Ukazatel na pole
- Pole ukazatelů
    - pole položek typu ukazatel
    - `int *pArray1D[4];`

# Typový systém
- jak reprezentovat sekvenci bitů
- dán během výpočtu
- jak měnit typy
- které typy mohou být použité danou operací

## Zajišťuje
- abstrakci - nemusímě přemýšlet na úrovni bitů
- operace nad nečekanými typy - string vs int
- možnost optimalizace

## Konverze typů
- Implictiní konverze proběhne bez dodatečného příkazu programátora - **type corcion**
    - `float realVal = 10;`
- Explicitní konverzi vynucuje programáor
    - `float value = (float) 5 / 9;`

### automatická konverze
1. Vyhodnocování výrazu 
```c
int value = 9.5;
```
2. Předání argumentů funkci
```c
void foo(cloat param);
foo(5);
```
3. Návratové hodnotě funkce
```c
double foo() { return 5; }
```

## Zarovnání v paměti
- Proměnné daného typu můžou vyžadovat zarovnání v paměti (závislé na architektuře)
    - `char` na každé adrese
    - `int` např. na násobcích 4
    - `float` např. na násobních 8
- Pokus o přístup na nezarovnanou paměť dříve způsoboval pád programu
    - nyní jádro OS obslouží za cenu výrazného zpomalení
- Paměťový aliasing
    - na stejné místo v paměti více ukazatelů s různým typem
- Striktnější varianta: Strict aliasing
    - žádné dvě proměnné různého typu neukazují na stejné místo
    - zavedeno z důvodu možnosti optimalizace
    - standard vyžaduje, ale lze vypnout
    - https://en.wikipedia.org/wiki/Aliasing_(computing)

### Způsob provedení konverze
- Buď na **bitové** nebo **sémantické** úrovni
- Bitová úroveň vezme bitovou reprezentaci původní hodnoty a interpretuje ji jako hodnotu nového typu
    - není ztrátová (pokud do paměti nepíšeme)
    - pole `flaot` jako pole `char`
    - `flaot` jako `int`
```c
float fArray[10];
char *cArray = (char *)fArray;
```
- Sémantická úroveň "vyčte" hodnotu původního typu a "uloží" jo do nového typu
    - např. `int` a `float` jsou v paměti realizovány výrazně odlišně (dvojkový doplň. kód vs IEEE 754)
    - `5.4` typu `double` &rarr; `float` je bezztráty vs `5.4` `float` na `int` je ztrátová
        - **16777217** jako `float` 
    - **může** být ztrátová (ale nusí vždy být)
    - výrazný rozdíl oproti bitové konverzi

## Přetypování ukazatelů
- Ukazatele lze přetypovat - bitové přetypování, mění se interpretace, nikoli hodnota

### `void *`
- Některé funkce mohou pracovat i bez znalosti datového typu
    - např. nastavení všech bitů na konkrátní hodnotu
    - např. bitové operace (`^`, `&`, ...)
- Je zbytečné definovat separátní funkci pro každý datový typ
    - namísto specifickéh typu ukazatele se použije `void *`
```c
void *memset( void *ptr, int value, size_t num );
int array[100];
memset( arary, 0, sizeof(array) );
```

## Vhodnost použití přetypování
- Typová kontrola výrazně snižuje riziko chyby
    - nedovolí nám dělat nekorektní operace
    - klíčová vlastnost moderních jazyků
- Explicitní přetypování obchází typový systém
    - programátor musí vuyžít korektně, jinak problém
- Snažet se obejít bez typové konverze (automatické i explicitní)
- Napsát kód zavisející na automatické konverzi
- Preferujeme explicitní konverzi před implicitní (čitelnější a jednoznačnější)
