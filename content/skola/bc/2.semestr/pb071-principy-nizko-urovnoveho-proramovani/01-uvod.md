+++
title = "01 - Úvod"
date = 2021-03-01
+++

# Úvod
- [Benchmarks game](https://benchmarksgame-team.pages.debian.net/benchmarksgame/) - rychlost jazyků
	- záleží na hardware

## Normy
- Kihna The C rpogramming Language 1978
- ANSI X3.159-1989 (ANSI C, Standard C, C89)
- ISO/IEC 9899:1990 (jen převzaté ANSI C, C90)
- ISO/IOC 9899:1999 (C99)
	- gcc -std=c99
- ISO/IEC 9899:2011 (C11)
	- rozšíření vláknam sychronizace
	- ISO/IED 9899:2018 jen opravy

### Nestandardizovaná rozšíření
- užitečné prvky, které nejsou v normě
- Nevýhoda:
	- špatná přenositelnost
	- omezuje dostupnost programu

## Překlad
- V C/C++ překlad do strojového kódu, nutný pro každou platformu
- V Javě, C# překlad do bytecodu, nutná VM pro spuštění
- Skriptovací jazyky Perl, Python: interpretuje se
- Funkcionální: ...

### Přepínače
- povinné
```sh
gcc -std=c99 -pedantic -Wall -o vystup vstup.c
```
- dodatečné
```sh
gcc -std=c99 -Wall -Wextra -Werror
```

## Nástroje
- Centrální repozitář - např. SVN (Subversion)
- Lokální a centrální repozitář - např. git

## Překlad po částech
1. **Prepocessing**
	- `gcc -E hello.c > hello.i`
	- rozvinutí maker, expanze `#include`, odstranění poznámek 
2. **Kompilace**
	- `gcc -S hello.i`
	- syntaktická kontrola kódu, typicky chybová hlášení a varování
	- překlad do assembleru
	- vzniká soubor `*.s`
3. **Sestavení**
	- `as hello.s -o hello.o`
	- assembly do strojového kódu
	- zatím ještě relativní adresy funkcí apod.
4. **Linkování**
	- `gcc hello.o`
	- nahrazení relativních adres absolutními
	- odstranění přebytečných textů apod.
	- objevují se chyby linkování (např. chybějící slíbená implementace)
	- získáme spustitelný program
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTc4MTk3MjE3NV19
-->