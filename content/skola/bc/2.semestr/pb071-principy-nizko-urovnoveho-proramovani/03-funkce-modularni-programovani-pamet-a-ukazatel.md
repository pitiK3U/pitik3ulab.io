+++
title = "03 - Funkce, modulární programování, paměť a ukazatel"
date = 2021-03-15
+++

# Funkce
- umožňuje řešení problému na podproblémy

## Deklarace
```c
float f2c(float fahr);
```
- musí být deklarována před použitím
- předběžná deklarace
- deklarace a definici zároveň
- argumenty se vždy předávají hodnotou
- pořadí předávání argumentů funkci není definováno

# Hlavičkové soubory
- kód, který lze použít i v jiných programech
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTEyMDA3MzgxNywxMjQ2NzY2NTM3LC0xOD
gzNzQ4OTYxXX0=
-->