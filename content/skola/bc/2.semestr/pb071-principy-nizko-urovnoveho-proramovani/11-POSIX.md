+++
title = "10 - POSIX, Make, CMake"
date = 2021-05-10
+++

# POSIX
- standardní knihovna = základní funkce pro práci s prostředky počítače

## Historie
- `POSIX- Portable Operating System Interface`
- Norma pro rozhraní OS založená na UNIXu
- Součástí normy POSIX je knihovna pro jazyk C - `POSIX C library`
- UNIX - musí používat normu POSIX a být certifikovaný podle Single UNIX Specification
- většina UNIX-like (UN*X) systému je dodržuje s odchylkami

## API operačního systému
- Funkce pro interakci s OS.
- Snaha vytvořit API kompatibilní C knihovnu (která je součástí).
- Umožňuje velkou množinnu služeb od jádra:
	- Správa procesů (start, komunikace, ukončení)
	- Přístup k filesystemu
	- Správa s synchronizace vláken
	- Správa virtuální paměti
	- ...
- Umožňuje "přenositelné" programy s větším záběrem než std knihovna.

## Kompatibilita
- UN*X systémy
	- Pokud je certifikován jako UNIX, potom splňuje POSIX.
- Windows
	- Má vlastní API OS `Win32` a `WinRT`
	- Části POSIX, ale né úplně
	- `MinGW` a `Cygwin` implementuji POSIX prostředí pomocí Win32 API.
	- WSL ?
	
## Souborový systém
- **N-ární strom s jedním kořenem.**
- Všechny svazky (jiné disky, síťová úložiště i zařízení) jsou v něm adresovány - `mount`.
- Kořen souborového systému se jmenuje `/`.
- Soubory jsou implementovány pomocí `inode` (i-uzel)
	- `inode` je datová struktura popisující objekt existujícíc v souborovém systému
- Vazba jméno - inode je v implementován adresáři.
- Pro získání informací o souboru: `stat()`, `fstat()`, `lstat()`

### Práce se soubory
- Místo `FILE *` se používá pomocí file descriptor typu `int`
- Funkce: `open()`, `close()`, `read()`, `write()`

### Adresáře
- Silně závislá na filesystému.
- 1. nikdy neotevírat jako soubor -
- Funkce pro práci: `opendir()`, `readdir()` a `closedir()`.
- `struct dirent` - radši nevěřit,

## Procesy
- Proces je jednotka OS pro běh programu s vlastní oddělenou pamětí.
- Práce s novými procesy: `fork()`, `execl()` a `waitpid()`
- Rodina funkcí `exec()`
- Funkce `popen()`
	- Spouští proces asynchronně.
	- Návratová hodnota je roura pro komunikaci s procesem.
	- Může být pouze pro čtení nebo zápis, nikoliv obojí.
	- Po skončení musí být zavřeno `pclose`.

## Vlákna
- Způsob jak proces může vykonávat paralelní činnosti  zároveň.
- Zároveň všechna vlákna mezi sebou sdílí prostředky (např. paměť).
- Mnoho problémů -> souběh, uváznutí, atd.
- Knihovna `pthread`.

# Make, CMake
## Make
- Nástroj na automatizaci sestavení.
- Popis se ukládá do `Makefile`.
- Rekurzivní pravidla pro vystavění stromu závislostí, který následně projde a nad každým cílem vykoná požadovanou akci
- Základním syntax je pravidlo `target: source1 source2`
	- `target` cíl, typicky produkovaný soubor
	- `source1 source2` zdrojové soubory, může jít o soubory vytvořené jiným pravidlem.
- Akce se nad cílem vykoná, pokud:
	- cíl neexistuje a je potřeba jej vytvořit,
	- libovolný ze zdrojových souborů byl upraven po vytvoření cíle.
	- cíl je `phony`, tedy neprodukuje žádný soubor.
- Spuštěním příkazu `make` se vyhledá soubor `Makefile` v lokálním adresáři.
- Pravidlo, které se začne vykonávat je první nalezené. (popř. zadané pravidlo `make clean`)
- Proměnné lze nastavit na jiné hodnoty při spustšní `CC=clang make all`
- `make -j5 all` - Pro zrychlení překladu lze použít přepínač `-jN`, překládá paralelně až `N` překladových jednotek. (**doporučené `N=<počet jader procesoru>+1`**)

### Makefile
- používat pouze tabulátor!
- proměnné
	- `VAR=value`
	- `$(VAR)`
- zobecnění pravidel (v GNU Make):
	Speciální znak `%`.
		- `%.o: %c`
		- Ke každému pravidlu potom náleží speciální proměnné
			- `$@` je jméno cíle.
			- `$<` ke první závislost
			- `$` jsou všechny závislosti
			
## Generáory
- `make` je silný nástroj, ale neřeší všechny problémy.
- Nezbytné závislosti popisovat ručně (popř. pomocí `gcc -MM`)
- Komplikovaná syntax a pro velké projekty nedostačuje.
- Je méně přenositelný a nemá přímou podporu pro ověření vlastností kompilátoru nebo existence knihoven.
- Pro větší projekty se používají nástroje, které umí `Makefile` vygenerovat.

### Autotools
- Jeden z nejrozšířenějších nástrojů v UNIXovém světě.
- Tři nástroje:
	- `Autoconf` - vyhledávání externích závislostí
	- `Automake` - popis překladu (podadresáře, layout projektu)
	- `Libtool` - podpora pro tvorbu knihoven.
- Výsledkem je několik servisních souborů a jeden spustielný skript `configure`.
- Testuje OS pro potřebné závislosti.
- Při úspěchu vygeneruje `Makefile`.

### CMake
- Základem je jeden soubor `CMakeLists.txt`, obsahuje popis projektu.
- Umí generovat `Makefile`, ale i jiné typy souborů, třeba pro VisualStudio, nebo `Ninja` build system.