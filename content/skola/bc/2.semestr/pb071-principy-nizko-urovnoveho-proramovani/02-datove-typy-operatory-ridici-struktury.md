+++
title = "02 - Datové typy, operátory, řídící struktury"
date = 2021-03-08
+++

# Datové typy
- Datový typ objektu:
	- hodnoty, kterých může objekt nabývat
- operace, které lze/nezle nad objektem provádět (řetězec nelze dělit číslem, ...)
- slabě typovaný jazyk - lze přetypovávat nekompatibilní typy
- konverze typů
	- **implicitní** - překladač (coercion)
	- **explicitní** - programátor
- typy
	- `bool` - `#include <stdbool.h>`
	- UNICODE char - `wchar_t`
- `singed`(defaultní) vs `unsigned`
- `short`
- `long`
- `long long`

## Velikost datových typů
- `sizeof()`
- standard předepisuje
	- ```c 
	  sizeof(char) <= sizeof(short) <= sizeof(int) <= sizeof(long)
	  ```
	- ```c
	  sizeof(char) <= sizeof(short) <= sizeof(float) <= sizeof(double)
	  ```
	- ```c
	  sizeof(char) == 1
		```
(nemusí být vždy 8 bitů, např. 16bitů)

## Přesnost primitivních typů
- typy s plovoucí desetinou čárkou mají různou přesnost
- double (64 bitů) IEEE 754
	- double-presicion binary floating-point format
	- přesnost 15-17 desetinných míst
- algoritmus RSA - klíč 256 bajtů
	- Číslo je realizované jako pole, aritmetické operace nad polem

## Big vs. little endian
- problém pořadí bajtů u více bajtových typů (int)
- Big = významnější je na vyšší adrese (využíváno pro přenos dat v sítích - network order)
- Little = méně významný na nižší adrese

## Proměnné
- musí začínat znakem anglické abecedy (a-zA-Z) nebo _
- může obsahovat pouze písmena, cifry a podtržítka
- záleží na velikosti znaků
- nesmí být klíčové slovo jazyka
- konvence pojmenování
	- respektuje existující konvenci v projektu
	- výstižná jména
- jména proměnných začínat malým písmenkem
- nezačínat proměnné __ nebo _X (X libovolné velké písmenko)
	- rezervované, typicky různé platformě závislé makra apod.
	- oddělovat slova v názvu proměnných

## Porovnávání
- `0` je v C False, jiné hodnoty jsou True
```c
if (prom = 1) ... // warning
if (1 = prom) ... // compilation error
```
- Problém s porovnáváním reálných čísel - nemusí být shodné a operátor přesto vrátí TRUE

## Zvýšení a snížení o '1'
- postfixová notace
	- `a++` 
	- `a--`
	- `b = a++;` je zkratka pro `b =a; a = a + 1`
		- `++` je vyhodnoceno a `a` změněno **po** přiřazení
- prefixová notace
	- `++a`
	- `++a`
	- `b = ++a;` je zkratka pro `a = a + 1; b = a;`
		- `++` je vyhodnoceno a `a` změněno **před** přiřazením
- pozor na `a[i] = i++;`
	- není definované, zda bude pozice i před nebo po ++

## Bitové operátory
- `&` - AND
- `|` - OR
- `~` - INVERT
- `^` - XOR
- `<<` - left shift
- `>>` - right shift

## Pořadí vyhodnocení
- `++`, `--`, `()`, `[]` mají nejvyšší prioritu
- `*`,`/`,`%` mají prioritu vyšší než `+` a `-`
- Porovnávací operátory(`==`, `!=`, `<`) mají vyšší prioritu než logické (`&&`,`||`,`!`)
- Operátory přiřazení mají velmi nízkou prioritu

## Testování
- manuální vs automatické
- dle rozsahu kódu
- Unit testing
	- testování elementárních komponent
	- Typicky jednotlivé funkce
- integrační testy
	- test spolupráce několika komponent mezi sebou
	- typicky dodržení definovaného rozhraní
- systémové testy
	- test celého programu v reálném prostředí
	- ověření chování vůči specifikaci

### Tipy
- ponechávejte v testu ladící výstupy i po nalezení chyby
	- obalit do podmínky `if (debug) { /*...*/}`
- soubor pro stdin, diff na výstup (ASSERT_FILE)
- funkce jednoznačně testovatelné (vstup/výstup)

* regresní testy - pro odhalení dříve objevených chyb
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTc3MjQwNTY2MiwtMTM2MjA1NDg1NCwxNT
g2NTI5MDEwLC05OTkxMjUxMzUsLTIwNDkwMzI3OTgsLTIwMTEy
ODc5MDEsLTExMDA4MjUzXX0=
-->