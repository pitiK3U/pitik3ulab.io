+++
title = "07 - Relační a prezenční vrstva"
date = 2021-04-30
+++

# L5 - Relační vrstva
- Hlavní úkoly: správa relací (tzv. **dialogů**)
- L1-L4 orientují na přenos vlastních dat mezi počítači
- vyšší vrstvy se orientují na potřeby *síťových aplikací*
- **relace** (též dialog):
	- spojení mezi dvěma koncovými účastníky na úrovni vyšší, než je vrstva transportní
- každé spojení zajišťováno prostřednictvím jednoho transportního spojení, jedno trans. spojení více po sobě jdoucích relací nebo více trans. spojení jednu relaci

## Služby
- **Řízení dialogu** (které aplikace smí vysílat)
	- 3 možnosti:
		- plně duplexní (*Two-Way-Simultaneous* - TWS)
		- poloduplexní (*Two-Way-Alternate* - TWA)
		- simplexní (*One-Way*)
	- poloduplexní režim řízen pomocí předávání pověření k přenosu dat (**data token**)
- **Synchronizace** (též checkpointing)
	- zaseknutá tiskárna - přišli bychom o data
	- mechanismus **kontrolních bodů** (synchronization points, checkpoints)
		- příjemce si může vyžádat znovu vysílání od daného bodu
		- dva druhy - **hlavní** (major) a **vedlejší** (minor)
			- vysílající si pamatuje data pouze po poslední hlavní bod (k bodům předtím se už nelze vrátit)

## Závěr
- relační vrstva není v TCP/IP modelu uplatněna (aplikace si ji musí aplikovat sama)
- příklady:
	- SSL - Secure Sockets Layer
	- SDP - Sockets Direct Protocol
	- RPC - Remote PRocedure Call Protocol
	- NetBIOS - Network Basic Input Output System
	
# L6 - Prezentační vrstva
- hlavní úkoly: *konverze přenášených dat do jednotného formátu*
- na různých architekturách různé *intepretace dat* (kódování znaků, čísel)
	- one's complement vs two's complement
	- Little Endian vs Big Endian
- nutnost **jednoté interpretace dat** = úkol Prezentační vrstvy
	- 2 základní možnosti:
		- přímý převod do stylu druhého (každý umí převést do jiného formátu)
		- převod do společného mezitvaru
- využívá se společný mezitvar
	- pro popis přenášených dat se využívá **ANS.1 (Abstrac Syntax Notation versin 1)**
	- aplikace prezentační vrstvě předává **data + jejich popis v jazyce ASN.1**
- další služby: **šifrování a komprese dat**

## Závěr
- v TCP/IP modelu se předpokládá, že úkoly prezentační vrstvy si zajistí aplikace sama
- příklady:
	- AFP, Apple Filling Protocol
	- ASCII, American Standard Code for Information Interchange
	- EBCDIC, Extended Binary Coded Decimal Interchange Code