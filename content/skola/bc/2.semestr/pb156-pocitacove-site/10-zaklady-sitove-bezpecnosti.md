+++
title = "10 - Základy síťové bezpečnosti"
date = 2021-05-06
+++

# Základy síťové bezpečnosti
- **Bezpečnost v počítačových sítích** - bezpečná komunikační síť by měla nabíze následující služby:
- **AAA**:
	- Authentication (***Autentizace*** - nepsát jinak)
	- Authorization (**Autorizace**)
	- Accounting (**Účtování**)
- + **zabezpečená komunikace**:
	- Důvěrnost
	- Integrita
	- Nepopíratelnost
- jiný pohled na bezpečný systém
	- Dostupnost - pohotovost provedení služby
	- Spolehlivost
	- Zabezpečení
	- Důvěrnost
	- Udržovatelnost
	
## Autentizace (Authentication)
- *ověření identity uživatele (původce zprávy)*
	- součástí je představení identity ověřováného subjektu
	- zahrnuje prokázání identity jak vůči koncovému ystému, tak vůči komunikujícímu partnerovi
- základní metody pro zjištění identity:
	- *podle toho, co uživatel zná* - správná dvojice uživatelské jméno a heslo / PIN
	- *podle toho, co uživatel má* - nějaký technický prostředek, který uživatel vlastní (USB dongle, smart card, privátní klíč, apod.)
	- *podle toho, co uživatel je* - uživatel má vlastnosti, které lze prověřit (otisk prstu, snímek oční zornice, apod.)
	- **podle toho, co uživatel umí* - umí správně odpovědět na náhodně vygenerovaný kontrolní dotaz
	
## Autorizace
- *oprávnění použít určitou službu nebo zdroj*
	- následuje po autentizaci
- udělení oprávnění nebo odepření přístupu
	- na základě seznamů pro řízení přístupu - definice pro vykonání určitéh operace ři pro přístup k prostředkům počítače
	
## Accounting (=účtování)
- *sledování využívání síťových služeb uživateli*
- informace mohou být využity pro správu, plánování, skutečné účtování nebo další účely

## Důvěrnost (Confidentiality)
- *ochrana přenášných dat před neautorizovaným odhalením*
- pouze odesílatel a příjemce by měli rozumnět obsahu přenášené zprávy
- zajištěno šifrováním zpráv

## Integrita (Integrity)
- *ochrana přenášených dat před neautorizovanou modifikací*
- zajištění, aby nedošlo ke změně zprávy při cestě od odesílatele k příjemci
- man in the middle attack

## Nepopíratelnost (Non-repudiation)
- *Nepopíratelnost odesílatele* a *Nepopíratelnost doručení* slouží k tomu, aby příjemce (odesílatel) mohl prokázat protistraně odesílání (přijetí) zprávy a tím zabránil pozdějšímu popření této akce protistranou

# Zabezpečná síťová komunikace
- = klasický problém kryptografie

## Kryptografie (Cryptography):
- nauka o metodách utajovaných smyslu zpráv převodem do podoby, která je čitelná jen se speciální znalostí (= *klíčem*)
- základní mechanismy:
	- kryptografie s využitím symetrických klíčů (*symetrická kryptografie*)
	- kryptografie s využitím asymetrických klíčů (*asymetrická kryptografie*)

## Symetrická kryptografie
- k šifrování i dešifrování využit jediný klíč
- výhody:
	- nízká výpočetní náročnost
	- vhodné pro šifrování dlouhých zpráv
- nevýhody:
	- nutnost sdílení tajného klíče
- např. DES, 3DES, IDEA, atp.

## Asymetrická kryptografie
- také *kryptografie veřejným klíčem*
- k šifrování je použit jiný klíč než pro dešifrování
	- oba klíče se dohromady nazývají *pár klíčů* (*keypair*)
	- šifruje se pomocí *veřejného klíče (public key)*, dešifruje pomocí *soukromého klíče (private key)*
		- zpráva zašifrovaná veřejným klíčem lze dešifrovat **pouze** pomocí příslušejícím soukromým klíčem
- výhody:
	- není potřeba nikam posílat šifrovací klíč => snížení rizika jeho vyzrazení / odposlechnutí
	- veřejný klíč možno dát všem
- nevýhody:
	- rychlost => asymetrické šifry jsou vhodné pro krátké zprávy
- např. RSA, DSA, atp.

### Certifikát veřejného klíče
- *Certifikát* - informace, která váže identitu entity (uživatel, server, ...) s jeho veřejným klíčem
- 4 základní informace obsažené v certifikátu:
	- *jméno vlastníka (držitele)*
	- **hodnota veřejného klíče**
	- *doba platnosti veřejného klíče*
	- *podpis vydavatele certifikátu*
- certifikáty vydávají tzv. *Certifikační autority*
	- organizace, kterým se důvěřuje
	- vydané certifikáty mohou být dostupné na veřejném serveru (kdokoliv může o jeho kopii požádat)
	
### Autentizace komunikujících stran
#### Autentizace heslem
- Alice se autentizuje Bobovi zasláním hesla
- heslo je šifrováno sdíleným symetrickým klíčem
- - negarantuje čerstvost hesla
	- heslo mohlo být uloženo a nyní se jedná o pokus o opakovanou autentizace (možný útok)
	
#### Autentizace s využitím náhodných čísel
- Alice si od Boba vyžádá zaslání náhodného čísla (tzv. *keksík*)
- Alice toho náhodné číslo zašifruje symtrickým klíčem
- + řeší problém čerstvosti hesla
- problém, co když máme někoho uprostřed, kdo si zahraje na Boba

#### Vzájemná autentizace s využitím náhodných čísel
- stejné jako předchozí, autentizace je obousměrná
- je zde forma piggybacking

#### Vzájemná autentizace s využitím náhodných čísel - asymetrická kryptografie

### Zajištění důvěrnosti přenosu 
#### šifrování
- Diffe-Hellman algoritmus
	- společně sdílejí znalost dvou prvočísel G a N, která jsou volně šířená
	- princip K = (G^x mod N)^y mod N = (G^y mod N)^x mod N = G^{xy} mod N

#### Digitální podpis
- obrácený mechanismus kryptografie
	- zpráva podepisována (= šifrována) soukromým klíčem odesílatele, ověřována (=dešifrována) veřejným klíčem odesílatele
- 2 základní mechanismy
	- podpis celého dokumentu
	- *podpis otisku dokumentu* (tzv. *message digest, hash*)
		- nejčastěji využívané
		- ze zprávy vypočten *otisk (hash)*, který je podepsán (= šifrován soukromým klíčem odesílatele) a odeslán spolu s původním (**nijak nešifrovaným**) dokumentem
		- řeší problém dlouhých zpráv, pro které jsou asymetrické šifry nevhodné = otisk je vždy *pevné (malé) délky*
		

##### Hashovací funkce
- musí poskytovat dvě základní vlastnosti:
	- *jednosměrnost* - jakmile je z dokumentu vytvořen otisk, **nelze** (žádným způsobem) z otisku získat původní dokument
	- *one-to-one* - je velmi malá pravděpodobnost, že dvě různé zprávy budou mít stejný otisk
- pro jakkoli dlouhý dokument má vždy pevnou délku

# Protokoly pro zajištění zabezpečené komunikace v Internetu
- lze realizovat v:
	- aplikační vrstvě
	- transportní vrstvě
	- síťové vrstvě

## IPSec - Síťová vrstva
- kolekce protokolů pro zajištění bezpečné komunikace na síťové vrstvě
	- protokol *Authentication Head (AH)* - určen pro zajištění autentizace odesílatele a integrity zprávy
	- protokol *Encapsulating Security Payload (ESP)* - určen pro zajištění autentizace odesílatele, integrity zprávy i důvěrnosti přenosu
- operuje ve 2 módech
	- *Transportní mód* - IPSec hlavička je vkládána mezi IP hlavičku a tělo zprávy
	- *Tunelovací mód* - IPSec hlaviča je vkládána před původní IP hlavičku, následně je generována nová Ip hlavička
- výhody: zabezpeční všech datových toků mezi dvěma komunikujícími uzly, není potřeba upravovat aplikace
- nevýhody: žádné automatické prostředky pro správu kryptografických klíčů

## SSL/TLS - transportní vrstva
- Secure Sockets Layer (SSL) / Transport Layer escurity (TLS)
- HTTP -> HTTPS
- FTP -> FTPS
- nevýhody: nutnost úpravy aplikací

## Aplikační vrstva
- zabezpečení komunikace na základě vlastních mechanismů síťových aplikací
- např. **Pretty Good Privacy (PGP)**

# Bezpečná architektura sítě - principy
- Základní principy a protokoly
	- síťové principy a protokoly
	- princip redundance v designu sítě
- Bezpečné a pružné směrování
	- ochrana spojení a cest
	- agregace spojení
	- vícecestné směrování
- Odolné překryvové sítě (RON)
- Zabezpečené DNS