+++
title  = "02 - Fyzická a linková vrstva"
date = 2021-03-11
+++

# Fyzická vrstva
- propojení mezi 2 médii --> není potřeba adresace
- řídí děje v **přenosovém médiu** (je **pasivní**)
- poskytuje služby vrstvě nad ní (datového spoje)
- transformuje bity na signály a naopak
- určuje počet logických kanálů souběžně přenášejících data z různých zdrojů
- **cíl**: přenést bity (obsah rámců) mezi odesílatelem a příjemcem
- vlastnosti (parametry a návaznosti signálů, zapojení konektorů apod.) jsou určeny standardy

**přenosové médium** -- přenáší data mezi uzly

-   **analogové přenosové médium** -- éter
-   **digitální přenosové médium** -- koaxiál, kroucená dvojlinka

## Služby
- **Bit-to-Signal Transformation** -- převod bitů na signál
- **Bit-Rate Control** -- kontrola počtu bitů za sekundu
- **Bit Synchronization** -- synchronizace příjemce a odesílatele
- **Multiplexing** -- rozdělení na logické kanály za účelem vyšší efektivit
- **Circuit Switching** -- přepínání okruhů

## Signály
- data jsou přenášena pomocí (elektromagnetických) signálů
- je nutné provést převod binárních dat na signál

**signál** -- časová funkce reprezentující změny fyzikálních vlastností přenosového média

- **analogový signál** -- spojitý v čase, šíření po vodiči nebo bezdrátově (éter)
- **digitální signál** -- diskrétní (nespojitý) v čase (mění se skokově), šíří se pouze po vodiči 

### Defekty signálů
- **slábnutí** -- ztráta energie, příčina: odpor vodiče
- **zkreslení** -- ztráta tvaru, příčina: různá rychlost šíření signálů na různých frekvencích
- **šum** -- příčina: vliv cizí energie

## Přenos dat

### Analogový přenos binárních dat
- modulace
- amplitudová, frekvenční nebo fázová modulace
- **modem** = MOdulátor + DEModulátor

### Digitální přenos binárních dat
- transformace *kódováním*
- **kódování** -- binární data -> digitální signál
- problém: synchronizace vysílače a přijímače (nerozeznáme počet 1 v dlouhé posloupnosti 111...)

**přímé kódování** -- 0 je 0, 1 je 1, bez možnosti synchronizace

**NRZ kódování** -- dva signály, jeden přenáší data a druhý mění amplitudu, pokud následující bit == 1

**kódování Manchester** -- pokles amplitudy == 0, nárůst amplitudy == 1, nižší efektivní přenosová kapacita, plná samosynchronizovatelnost

**kódování 4B/5B** -- uměle zavedená redundance, umí detekovat chyby; substituce 4-bitových bloků speciálními 5-butovými vzorky

## Přenosová média
- prostředí pro činnost fyzické vrstvy
- **voděná** -- fyzický kanál mezi dvěma zařízeními (kroucená dvojlinka, koaxiál, optické vlákno...)
- **nevoděná** -- nepoužívají fyzický vodič, signály se šíří éterem (rádiové, mikrovlnné či infračervené vysílání...) - ovlivněná prostředím

### Multiplexning
- technika sdílení dostupné přenosové kapacity přenosového média souběžnými komunikacemi
- lze uplatnit především u optických a bezdrátových přenosů
- pro analogové signály:
    - **Frequency-Division Multiplexing (FDM)** -- stanice vysílající v éteru na různých frekvencích
    - **Wave-Division Multiplexing (WDM)** -- FDM pro optická vlákna, různé vlnové délky (každá barva reprezentuje jeden kanál)
- pro digitální signály:
    - **Time-Division Multiplexing (TDM)** -- v jeden okamžik vysílá pouze jeden vysílač, střídají se, nutno dobře synchronizovat

# Linková vrstva (vrstva datového spoje)
- na úrovni *LAN*(Local Area Network)
- přenosové médium sdíleno více stanicemi (nutnost adresace stanic)
- tzv. **node-to-node delivery**
<!-- markdown cheats -->
- přijímá *pakety* do síťové vrstvy a transformuje je do *rámců*
- přenáší rámce mezi komunikujícími uzly, které jsou určeny MAC adresami
- zaručuje spolehlivost přenosu (vynucení opakování chybně přenesené informace)
- brání zahlcení cílového uzlu
- řídí přístup uzlů k sdílenému **přenosovému médiu**
- detekuje a opravuje chyby
    - Error Detection, Automatic Request for Retransmission (ARQ)
    - Forward Error Correction (FEC) -- samoopravné kódy (např. Hammingův kód viz. MB104)
- kolizní doména

## Služby
- **tvorba rámců** (Framing) -- balení paketů ze síťové vrstvy do rámců
- **adresování** (Addressing) -- fyzické/MAC adresy
- **chybové řízení** (Error Control) -- detekce a korekce chyb z fyzické vrstvy
- **řízení toku** (Flow Control) -- zabraňuje zahlcení příjemce (stop-and-wait, sliding-window)
- **řízení přístupu k médiu** (Medium Access Control - MAC) -- eliminace kolizí při násobném vysílání

## Tvorba rámců, adresace
- Ethernetový rámec:
	- preambule (identifikace počátku rámce)
	- cílová adresa
	- zdrojová adresa
	- typ
	- data
	- CRC (Cyclic Redundancy Check) -- cyklický redundantní součet

## Chybové řízení
- chyby (změna hodnot bitů) vznikají na fyzické vrstvě
- na linkové vrstvě probíhá detekce a korekce
- vysílač přidá redundantní kontrolní bity, přijímat podle nich zkontroluje data
- **Error Detection, Automatic Request for Retransmission (ARQ)** -- detekce & opakování přenosu (náročná operace; vodné pro málo chybová zařízení)
- **Forward Error Correction (FEC)** -- detekce & oprava (např. Hammingův kód)

### Kódy pro detekci chyb
- sudá/lichá parita (silnější: dvoudimenzionální parita)
- cyklické kódy s kontrolní redundancí (CRC - *Cyclic Redundancy Check*) -- viz MB104 (n,k)-kódy, spolehlivě opravuje jednoduché chyby

#### CRC
- pro blok k=bitů dat se vygeneruje (n-k)-bitová posloupnost přidávaná ke k-bitům zprávy
- přenášená zpráva (rámec, n-bitů) reprezentuje polynom M(x) stupně (n - 1)
- *klíč* - vhodně zvolený polynom C(x) stupně (n - k)
- přidávaná posloupnost (CRC) - zbytek po dělení M(x)/C(x) (=> n - k bitů)

## Řízení přístupu k médiu (MAC - Medium Access Control)
- funkcionalita odpovědná za koordinaci přístupu více stanic ke sdílenému přenosovému médiu
- cíl: eliminovat kolize
- neřízený, řízený nebo multiplexově-orientovaný přístup

### Protokoly neřízeného přístupu
- **Aloha** -- stanice vysílá kdykoli má připravený rámec, potvrzení o přijetí, při kolizi počká náhodou dobu a pak to zkusí znovu, neefektivní
- **CSMA/CD** -- vysílá jen když je klid, používá se v LAN Ethernetu, nelze použít v nevoděném médiu (CD = Collision Detection)
- **CSMA/CA** -- obcházení kolizí, použitelné v nevoděném médiu (CA = Collision Avoidance)

### Protokoly řízeného přístupu
- stanice smí vysílat, pouze když získá svolení
- **rezervace** - vysílá se v předem domluvených intervalech
- **vyzývání** - centrální stanice vyzývá a vybírá stanici, která bude vysílat
- **předávání příznaku** - předání peška indikující právo k vysílání

### Protokoly multiplexově-orientované přístupu
- v médiu se přenáší víc signálů najednou
- **FDMA** -- frekvence
- **TDMA** -- čas

## L2 sítě
-   lokální počítačové sítě (LANs)

**topologie** -- uspořádání stanic na médiu

**kolizní doména** -- určena stanicemi, které sdílí přenosové médium, při souběžném vysílání nastává kolize

### Sběrnicová topologie (bus)
- relativně snadná na instalaci
- kolizní doména zahrnuje všechny připojené stanice
- protokol CSMA/CD pro řízení přístupu k médiu 
- při výpadku kabelu vypadne celá síť (je náchylná k defektům)

### Kruhová topologie (ring)
- zprávy putují v jednom směru
- kolizní doména zahrnuje všechny připojené stanice
- právo vysílat se určuje metodou peška
- při výpadku kabelu nebo stanice vypadne celá síť (je náchylná k defektům)

### Hvězdicová topologie (star)
- centrální bod (hub, bridge, switch)
- obtížnější na instalaci
- kolizní doména závisí na propojovacím bodu
    - *hub* (L1) -- všechny připojené stanice
    - *bridge, switch* (L2) -- pouze sousedící stanice
- při výpadku kabelu vypadne pouze dané zařízení

### Budování L2 sítí
- **bridge** (můstek) -- transparentní propojení sítí, zabraňuje šíření kolizí, všechen provoz prochází můstkem
	- **switch** -- víceportový můstek
- založena na MAC adresách
	- můstek se učí umístění jednotlivých stanic (Backward Learning Algorithm) -- nutné pro správné přepínání rámců
	- rámce se směřují dle cílové adresy
- vlastnosti:
	- lze vytvořit sítě s cykly --> problém
		- distribuovaný *Spanning Tree Algorithm* pro výpočet kostry
	- nevhodné pro velké sítě
		- přepínací tabulky rostou s počtem stanic - pomalá konvergence
 
#### Distribuovaný Spanning Tree Algorithm
- cíl: zabránit cyklům (nepoužívat některé porty můstků)
- všechny můstky periodicky posílají informaci o <své adrese, adresa kořenového můstku a vzdálenosti od kořene>
- pokud můstek obdrží zprávu od souseda, upraví svoji definici nejlepší cesty (kořen s menší adresou, menší vzdálenost (popř. nižší adresa))
- **výběr kořenového můstku**
    - všechny můstky se prohlásí za kořeny
    - rozešlou informace a vyberou ten s nejnižším ID
- **výběr kořenových portů**
    - každý můstek si vybere svůj nejkratší port ke kořenovému můstku
    - ostaní porty vypne
-   **výběr aktivních/neaktivních portů**
    -   kořenový můstek nastaví všechny své porty jako aktivní
    -   na spojích, kde nejsou kořenové porty, se vyměňují informace
    -   ten s nižším můstkovým ID se vypne

* animace: http://frakira.fi.muni.cz/~jeronimo/vyuka/Cisco-spanning_tree.swf
<!--stackedit_data:
eyJoaXN0b3J5IjpbNDEyMjEyMzldfQ==
-->