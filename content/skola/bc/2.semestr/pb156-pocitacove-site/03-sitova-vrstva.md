+++
title = "03 - Síťová vrstva"
date = 2021-03-18
+++

# Síťová vrstva
* potřebujeme ji k vybudování geograficky rozlehlé sítě
* multicast (propojujeme jeden bod s N body)

## Poskytované služby
* **Propojování fyzických sítí** (Internetworking) -- iluze uniformní WAN
* **Tvorba paketů** (Packetizing) -- přijaté segmenty od transporstní vrstvy transformuje na pakety (IP protokol)
* **Fragmentace paketů** (Fragmenting) -- rozděluje segmenty na pakety (**MTU** (Maximum Transition Unit) - délka závisí na vlastnostech/schopnostech sítě)
* **Adresace** (Addressing) -- poskytuje jednoznačnou identifikaci každého zařízení (IP adresy)
* **Mapování IP adres na/z fyzické adresy** (Address Resolution) -- mapuje síťové adresy na fyzické adresy (ARP, RARP protokoly)
* **Směrování** (Routing) -- hledání nejvhodnější cesty mezi komunikujícími entitami
* **Metoda základního monitoringu stavu sítě** (Control Messaging) -- základní informace o nedoručitelnosti paketů, stavu sítě, uzlů atp. ICMP protokol

## Internetworking (propojování sítí)
* propojování jednotlivých sítí ve větší celek
* tvoří se hierarchie
* důvody: zpřístupnění, zvětšení dosahu služeb, optimalizace fungování sítě, překonání technických překážek

**internet** (zkráceno z internetwork) -- jakékoli propojení dvou a více sítí

**Internet** -- konkrétní síť, celosvětový Internet

**směrovač** (router) -- propojovací zařízení

### Modely zajištění síťových služeb
* **přepínání okruhů** (Circuit Switching) -- přímé spojení mezi odesílatelem a příjemcem, bez paketů, spojovaná služba
* **přepínání paketů** (Packet Switching) -- zasílání paketů (nezávislých datových jednotek), virtuální kanály, spojovaná služba
* **datagramový přístup** -- nespojovaná komunikace

## Adresace
* *jednoznačná identifikace* každého zařízení připojeného k Internetu
* systematické přidělování adres -> snadné směrování
* IPv4 32 bitů -- čtyři bloky (každý s max. hodnotou 255) oddělené dvojtečkou
* IPv6 128 bitů

### IPv4: typy adres
* **Unicast (individuální) adresy** -- jediný příjemce/odesílatel
* **Broadcast adresy** -- více příjemců v rámci LAN, identifikace odesílatele je unicastová
* **Multicast (skupinové) adresy** -- příjemcem je každý, kdo o data projevil zájem, identifikace odesílatele je unicastová

### IPv4: Classful Addressing
* první metoda přidělování adresního prostoru
* 5 tříd adresního prostoru
* **třída A** -- 2^7 sítí, každá má 2^24 uzlů (`0`)
* **třída B** -- 2^14 sítí, každá má 2^16 uzlů (`10`)
* **třída C** -- 2^21 sítí, každá má 2^8 uzlů (`110`)
* **třída D** -- multicastové adresy (`1110`)
* **třída E** -- rezervní prostor (`1111`)

**adresa sítě (NetID)** -- identifikuje síť, využívá se pro směrování

**adresa uzlu/rozhraní (HostID)** -- identifikuje jedinečný uzel v síti NetID

` př. HostID = 147.251.48.1 -> třída B: NetID = 147.251.0.0`

* **problém:** nedostatečné granularita -- plýtvání adresním rozsahem
* **řešení:** více síťových adres menší třídy
* **nový problém:** nárůst směrovacích tabulek (jejich prohledávání má lineární složitost)
* **nové řešení:**
    * **subnetting** -- rozdělení dle organizačních složek v rámci organizace (rozložení mezi jednotlivé fakulty), tříúrovňová hierarchie (adresa sítě, podsítě a uzlu)
    * **supernetting** -- agregace původně samostatných síťových adres v jednu větší, sníží velikosti směrovacích tabulek, protože se bude jednat o souvislý blok adres určité třídy
* identifikace bitů, které označují síť -- princip masky sítě (1 na místech, kde je síťová adresa, 0 relativní adres v rámci sítě, použijeme operaci &&)

### IPv4 Classless Addressing
* zobecnění a rozšíření subnettingu a supernettingu
* variabilní délka bloku obsahujícího adresu sítě
* identifikace sítě: **adresa + maska sítě**
* hierarchické přidělování adres

#### Classless Inter-Domain Routing (CIDR)
* konvence pro použití IP adres, význam masek, subnettingu a supernettingu
* CIDR bloky (namísto tříd A, B, C), jejichž velikost určuje maska
* adresy závislé na poskytovateli (při změně je nutné přečíslovat)
* snížení tempa vyčerpání adres

### IPv4: Network Address Translation (NAT)
* postup pro snížení tempa vyčerpání adres
* skrytí vnitřní sítě za jednu/více externích adres
* způsobuje ochranu vnitřní sítě
* překlad adres při odchodu ze sítě je triviální
* pro překlad adres při příjmu (kterému stoji paket náleží?) slouží překladové tabulky

#### IPv6: adresy
* 128 bitů
* hexadecimální zápis, 8 oddílů (po dvou bytech)
* zkracování zápisu: vynechání počátečních nul, vynechání sekvence nulových bloků (pouze jedné!)
* hierarchie definována `RFC 3587`:
  * n-bitů: globální směrovací systém
  * 64-n bitů: adresa podsítě
  * 64 bitů: adresa rozhraní
* globání směrovací prefix = adresa sítě
* adresa podsítě obvykle 16 bitů = globální prefix 48 bitů
    * prvních 16 bitů = 2001<sub>16</sub>
    * dalších 16 bitů přiděluje regionální registrátor (RIR)
    * dalších 16 bitů přiděluje lokální registrátor (LIR)
* k popisu využíváme CIDR, třídy neexistují
* typy adres (broadcast se nevyužívá):
    * unicast
    * multicast
    * anycast (data se doručí pouze nejbližšímu příjemci!)
* IP datagram
* ARP (Adress Resolution Protocol) -- slouží k mapování dynamických adres v IPv4
* _na zkoušce bývá:_ porovnání IPv4 a IPv6 hlaviček

## Interakce L3 se spojovou vrstvou (L2)
* hop-by-hop mechanismus doručení dat v IP sítích
* předání/doručení na základě fyzických (MAC) adresy
* **příjemce je na stejné LAN jako odesílatel** -- IP datagram obsahuje: IP adresu příjemce, rámec L2 vrstvy a MAC adresu příjemce
* **příjemce je na jiné LAN než odesílatel** -- IP datagram obsahuje: IP adresu příjemce, rámec L2 vrstvy a MAC adresu směrovače; směrovač jej vloží do nového rámce a pošle dál

**statické mapování** -- statická tabulka párů IP a MAC adres

**dynamické mapování** -- ARP (Address Resolution Protocol)

### ARP protokol
* zjištění MAC adresy na základě IP
1. ARP request paketu (IP, MAC odesílatele, IP příjemce) **všem** uzlům v LAN (broadcast)
2. odpoví ten uzel, jehož IP adresa se shoduje v hledanou
3. uzel odpovídá ARP reply paketem (obsahuje jeho MAC adresu)

* dříve se využíval RARP (Reversed Address Resolution Protocol) ke zpětnému překladu MAC adres na IP adresy

## IP protokol
* nejrozšířenější protokol síťové vrstvy
* zajišťuje transport datagramů na místo určení přes mezilehlé směrovače
* *host-to-host* delivery (jednoznačně určeny IP adresou)
* *datagramový přístup*, *nespojovaná* komunikace
* nespolehlivá služba - *best-effort*
* doplňujíc ho podpůrné protokoly (ICMP, ARP, RARP, IGMP), které např. ošetřují nestandardní situace
* Internet Protocol verze 4 (IPv4) -- 1981, RFC 791
* Internet Protocol verze 6 (IPv6) -- 1998, RFC 2460

### IP protokol verze 4
* **version** (VER)
* **header length** (HLEN) - délka hlavičky
* **differentiated services**/**type of services** (DS/TOS) -- třída datagramu v rámci kvality služby (QoS), slouží k rozlišení důležitých datagramů
* **total length** -- v bajtech
* **identification** -- identifikace původního datagramu
* **flag** -- 3 bity: rezerva, do-not-fragment bit (nesmí být fragmentován), more-fragment bit (není posledním fragmentem)
* **offset** -- pozice fragmentu v původním datagramu, 13bitů &rarr; max 8191, jednotka 8 B
* **time to live** (TTL) -- vkládá se zhruba 2× větší číslo, než je vzdálenost mezi uzly, při průchodu se dekrementuje
* **protocol** -- identifikace protokolu vyšší vrstvy, kterým má být datagram doručen (ICMP, IGMP, TCP, UDP, ...) (http://www.iana.org/assignments/protocol-numbers)
* **header checksum** -- kontrolní součet hlavičky (musí se přepočítat po změně TTL)
* **source IP address** -- 32-bitová IPv4 adresa odesílatele
* **destination IP address** -- 32-bitová IPv4 adresa příjemce
* **options** -- volitelná součást určená pro případná rozšíření (pravděpodobně k tomu ani nedojde)
* **data**

**Maximum Transfer Unit (MTU)** -- maximální velikost dat (total size datagramu) které lze přenést využitím L2 protokolem

#### Fragmentace datagramu
* pokud je datagram příliš velký, dojde k **fragmentaci**
* datagram se rozdělí na několik menších datagramů = **fragmenty**
* každému fragmentu vytvoří novou hlavičku
* využívají se pole Identification, Flags a Offset
* fragmentace probíhá na zdrojovém uzlu nebo na směrovačích
* datagram se skládá **pouze** na cílovém uzlu (riziko ztráty fragmentu)

#### ICMP - Internet Control Message Protocol
* RFC 792
* doprovodný protokol k IP protokolu
* poskytuje informace o chybách při přenosu IP datagramů a informace o stavu sítě
* zprávy jsou baleny do IP protokolů, hodnota _Protocol_ nastavena na 1
* zprávy obsahují část paketu, který způsobil chybu nebo se váže na odpověď
* využití:
     * `ping` -- ICMP Echo request/reply
     * `traceroute` -- ICMP Time exceeded
* ochrana proti rekurzivnímu generování -- negeneruje se při:
    * ICMP chybě
    * broadcast nebo multicast zprávě
    * poškození IP hlavičky
    * chybě fragmentu (kromě prvního)
* generování zpráv je často výkonnostně omezeno

#### Příklady zpráv
* chyby
    * destination unreachable
    * time exceeded -- vypršení TTL
* stav sítě/uzlu
    * echo request/reply -- požadavek na odpověď

### IP protokol verze 6
#### Důvody vzniku:
* rychlé vyčerpání IPv4
* slabá podpora přenosů aplikací reálného času
* žádná podpora zabezpečení komunikace na úrovni IP
* žádná podpora autokonfigurace zařízení
* žádná podpora mobility
* ...

#### Vlastnosti
* rozšířený adresový prostor -- 128 bitů
* jednodušší formát hlavičky
* možnosti dalšího rozšíření
* podpora přenosů reálného času -- značkování toků, prioritazace provozu
* podpora zabezpečení přenosu -- autentizace, šifrování a verifikace integrity dat
* podpora mobility -- domácí agenti
* podpora autokonfigurace zařízení -- stavová a bezstavová konfigurace

#### Základní hlavička datagramu
* velikost 40 B
* neobsahuje: kontrolní součet (není), volby a fragmentační informace (v rozšiřující hlavičce)
* **Version** (VER)
* **Priority** (PRI)
* **Flow label**
* **Payload length** -- celková délka bez základní hlavičky
* **Next header** -- hlavička transportního protokolu nebo rozšiřující hlavička
* **Hop limit** -- zhruba odpovídá TTL v IPv4
* **Source/Destination address**

#### Rozšiřující hlavičky
* Hop-By-Hop Options -- volba pro všechny
* Routing -- směrování
* Fragment -- fragmentace
* Encapsulating Security Payload (ESP) -- autentizace datagramu, nebo šifrování obsahu
* Authentication Header (AH) -- autentizace datagramu
* ...

#### Zabezpečení IP datagramů
**IPSec** - implementace zabezpečené komunikace na síťové vrstvě, povinné (doporučené) v IPv6,
- poskytuje možnost **šifrování** a **autentizace** dat (proti záměně datagramu)
    - *AH (Authentication Header)* - untentizace datagramu (ověření pravosti)
    - *ESP (Encapsulating Security Payload)* - autentizace datagramu + možnost šifrování obsahu

**transportní režim ochrany** -- vloží se rozšiřující bezpečností hlavičky

**tunelující režim ochrany** -- datagram se zabalí jako data do nového datagramu s bezpečnostními hlavičkami

**bezpečnostní asociace (Security Association, SA)** -- virtuální spojení dvou počítačů, které zajišťuje zabezpečený přenos dat (jednosměrné), používá AH, nebo ESP, o správu se stará Internet Key Exchange (IKEv2) Protocol (RFC 4306)
