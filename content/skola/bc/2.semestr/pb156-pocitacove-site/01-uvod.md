+++
title = "01 - Úvod"
date = 2021-03-04
+++

# Úvod
**počítačové sítě** = skupina počítačů a zařízení propojená komunikačními kanály, které umožňují vzájemnou komunikaci a sdílení dat mezi uživateli
- základní součástí *přenos dat*
- využití: komunikace, sdílení HW, SW, souborů dat a informací
- základní vlastnosti sítě:
	-  **Delivery** (správný příjemce) - systém musí data doručit správnému příjemci
	-  **Accuracy** (správnost dat) - systém musí data doručit nepoškozená
	-  **Timeliness** (včas) - systém musí doručit data včas

# Základní součásti komunikačního systému
**sender** (odesílatel) -- zařízení zasílající datovou zprávu

**receiver** (příjemce) -- zařízení přijímající datovou zprávu

**message** (zpráva) -- informace, které je mezi odesílatelem a příjemcem vyměňovaná

**transmission medium** (přenosové médium) -- fyzické médium, skrze které je zpráva mezi odesílatelem a příjemcem přenesena (optický kabel, bezdrátové médium, ...)

**protocol** (protokol) -- sada pravidel řídících komunikaci mezi zúčastněnými stranami

## Základní parametry síťových toků
- **bandwidth** (propustnost) -- udává kapacitu přenosového kanálu (bps, kbps, Mbps, Gbps...)
- **packet loss** (ztrátovost paketů) -- průměrný počet ztracených paketů za určité období vyjádření v % vzhledem k celkovému počtu přenesených paketů
-   **delay, latency** (zpoždění přenosu) -- čas mezi odesláním a přijetím zprávy (ms) (nelze sw upravit)
    -   **RTT delay (Round-Trip-Time delay)** -- čas trasy (odesílatel odešle -> příjemce přijme -> příjemce odešle -> odesílatel přijme)
-  **jitter** (rozptyl, kolísání zpoždění) -- variabilita zpoždění přenosu (ms, μs...)

| Ideální síť              | Skutečná síť                             |
|--------------------------|------------------------------------------|
| transparentní (pouze *end-to-end*) | vnitřní struktura ovlivňuje doručení dat |
| neomezená propustnost    | omezená propustnost                      |
| bezztrátová              | ztrátová                                 |
| bez zpoždění a jitteru   | zpoždění a jitter                        |
| zachování pořadí paketů  | možné prohození pořadí paketů            |
| data nejsou poškozena    | data mohou být poškozena                 |

## Požadované vlastnosti
- **efektivita** (maximální využití dostupné přenosové kapacity)
- **spravedlivost** (stejná priorita pro všechny toky)
- **decentralizovaná správa**
- **rychlá konvergence při adaptaci na nový stav** (síť je bezstavová)
- **multiplexing/demultiplexing** (jednou optikou více kanálů; problém s fyzickým propojením)
- **spolehlivost**
- **řízení toku dat** (ochrana proti zahlcení sítě a přijímacího uzlu)

## Základní přístupy
### Spojované sítě (stavové) = přepínání okruhů
- před začátkem komunikace je vytvořeno spojení (**okruh**) a to je udržováno po celou dobu komunikace
- pevný (předvytvořený) okruh × okruh vytvořený na přání
- slečny spojovatelky
- snadná implementace
- špatná škálovatelnost
- např. analogové telefonní sítě

### Nespojované sítě = přepínání paketů
- směrování paketů
- data nejdou po předem připravené cestě, ale jsou rozdělena do menších kousků (**pakety**) a jednotlivé části sítě vybírají, kam paket pošlou dál (snaží se jej přiblížit cíli)
- pakety mohou být v síti směrovány libovolnými/různymi cestami, slučovány či fragmentovány
- na přijímací straně jsou z paketů extrahovány příslušné části dat, které jsou následně znovu složeny do původní podoby
- není potřeba uchovávat stav sítě
- složitější implementace QoS (tzv. best-effort služba)
- např. Internet

## Implementace funkcionality
### End-to-End (E2E)
- není nutná kooperace vnitřních síťových prvků
- vše je v koncových bodech sítě
- zajišťuje věrnost přenesených dat
- větší zpoždění

### Hop-by-Hop (HbH)
- funkcionalita se opakuje na každém uzlu sítě (rozhodování, kam data pošlu)
- prvky uvnitř sítě si pamatují její stav -> limitovaná škálovatelnost
- důležitější je minimalizovat zpoždění -> real-time systémy

# Síťové modely
- dekompozice úkolu (analogie s posíláním dopisu poštou)

## ISO/OSI model
- ISO = název organizace, OSI = název modelu
- 7-vrstvý model vytvořený pro zajištění kompatibility a interoperability komunikačních systémů různých výrobců
- každá vrstva plní určitou funkcionalitu
- každá vrstva komunikuje pouze se svými přímými sousedy
- vrstvy jsou pouze abstrakce funkcionality, implementace je jiná

**Fyzická vrstva** -- přenosový média, signály, bitová reprezentace (bity)

**Vrstva datového spoje** -- MAX a LLC, fyzická adresa (rámce)

**Síťová vrstva** -- logické síťové adresování, směrování (pakety)

**Transportní vrstva** -- proces-proces komunikace, spolehlivost (segmenty -- TCP, datagramy -- UDP)

**Relační vrstva** -- relace, správa relací (data)

**Prezentační vrstva** -- datová reprezentace (data)

**Aplikační vrstva** -- síťové aplikace (data)

![https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.lifewire.com%2Fthmb%2FOY1lnnhR6g5bNqEqwUYyFyvEXRs%3D%2F768x0%2Ffilters%3Ano_upscale()%3Amax_bytes(150000)%3Astrip_icc()%2FOsi-model-jb.svg-57f7b9af3df78c690f6305e8.png&f=1&nofb=1](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.lifewire.com%2Fthmb%2FOY1lnnhR6g5bNqEqwUYyFyvEXRs%3D%2F768x0%2Ffilters%3Ano_upscale()%3Amax_bytes(150000)%3Astrip_icc()%2FOsi-model-jb.svg-57f7b9af3df78c690f6305e8.png&f=1&nofb=1)

### Vrstvy ISO/OSI
#### Aplikační vrstva
- rozhraní mezi uživatelem a počítačovou sítí
- síťové aplikace (data) a síťové programy
#### Prezenční vrstva
- zajišťuje jednotnou reprezentaci dat na obou stranách
#### Relační vrstva
- spravuje ustavení spojení (relace) mezi komunikujícími aplikacemi
#### Transportní vrstva
- zajišťuje identifikace (adresaci) a doručení dat (segmentů, datagramů) mezi dvěma komunikačními procesy
#### Síťová vrstva
- zajišťuje identifikaci (adresaci) a doručení dat (paketů) mezi dvěma komunikujícími uzly
- také nalezení vhodné cesty mezi komunikujícími uzly (**směrování**)
#### Vrstva datového spoje (Spojová vrstva)
- zajišťuje přenos dat (rámců) mezi dvěma komunikujícími uzly propojenými sdíleným přenosovým médiem (včetně řízení přístupu k médiu)
#### Fyzická vrstva
- řídí děje v přenosovém médiu (např. vysílání/přijímání přenášených dat (bitů), kódování dat do signálů, ...)

![Difference Between TCP/IP and OSI Model (with Comparison ...](https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Ftechdifferences.com%2Fwp-content%2Fuploads%2F2016%2F03%2FUntitled-1.jpg&f=1&nofb=1)
# Síťové komunikační modely
- komunikace musí být nějak standardizovaná
- průběh komunikace:
    - výzva ke komunikaci
    - akceptace komunikace
    - akceptování/odmítnutí/timeout

<!-- important -->
**síťový protokol** -- definice formátu a pořadí zpráv vyměňovaných mezi entitami a akce probíhající při odesílání a příjmu, **určuje syntax, sémantiku a časování** (např. _UDP, TCP, IP, IPv6, SSL, TLS, SNMP, HTTP, FTP, SSH, Aloha, CSMA/CD,..._)

### Standardizace
-   stanovení norem
-   cíle: kvalita, bezpečnost, kompatibilita, interoperabilita, portabilita
-   **de facto** -- obecně akceptovaná vhodná technická řešení
-   **de jure** -- standardy nařízené kompetentními organizacemi/autoritou

#### Standadizační instituce
-   ANSI (American National Standards Institute) - byli napřed, stali se celosvětovou normou
-   EIA (Electronics Industry Assiciation) -- normalizace na úrovni fyzické vrstvy
-   IEC (International Electrotechnical Commission) - spolupracují v ISO oblastech, co ISO nepokrývají
-   IEEE (Institute of Electrical and Electrionic Engineers)
-   IETF (Internet Engineering Task Force) -- příprava specifikací pro Internet
    -   **RFC** (Request for Comments) - po té, co je přijato, stane se normou
-   ISO (International Organization for Standardiation)
-   ITU-T (International Telecommunications Union - Telecommunications Standardization Sector)

### Příklady reálných sítí
-   CESNET2
-   GEANT2
-   Internet2/Abilene
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTEzNTMyMDkzMTksLTcxODQ4NTc4Niw0NT
kwMzYzNzUsLTUwODYzMjY3NiwtNDEwMjkyNjI4LDEwOTQxNDg3
MDIsMjA4NjI5NTY0MiwxNzAxNzI1NjU5LDEwNTk0NDU2NjIsOT
Q2OTQ0MDY0LDY3NTY4NTY4OF19
-->