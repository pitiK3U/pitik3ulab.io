+++
title = "06 - Kvalita služby (QoS)"
date = 2021-04-23
+++

# Úvod
- TCP/IP - best-effort service
- některé toky je podřeba *upřednostnit* - nebo jim poskytnout parametry přenosu, které potřebují = **kvalita služby (Quality of Service, QoS)**
    - **spolehlivost** (reliability) - maximální ztrátovost
    - zpoždění (latency, delay)
    - přenosová kapacita (bandwidth)
    - rozptyl zpoždění(jitter)
- důležité pro *real-time* přenosy (videokonference, multimediální přenosy)
    - haptika (operace na dálku)
    - kritická řídící data - obsluha jaderného 
- mechanismy zajištění kvality služby:
    - **plánování** (Scheduling)
    - **formování / omezování toků** (Traffic shaping)
    - **prevence zahlcení** (Congestion avoidance)
- nutno nzajistit na více vrstvách (L2,L3,L4)

## Plánování (Scheduling)
- souvisí s obsluhou vstupních a výstupních front na odesílateli, příjemci a vnitřních uzlích
- ovlivňuje garance zpoždění přenosu

### FIFO (FCFS)
- FIFO = First In First Out
    - FCFS = First Come First Serve
    - kdo dřív přijde, je dřív obsloužen
- nejjednodšší uspořádání
    - pouze jedna fronta
- nevýhody:
    - žádná podpora priority
    - agresivný proudy zvýhodněny (větší pakety mají výhodu)

### Priority queue
- příchozí pakety jsou zařazeny do prioritnách tříd
- každá prioritní třída má svou FIFO frontu
- pakety ve třídě s vyšší prioritou jsou obsluhovány dříve
    - pakety z nižší třídy se neodesílají dokud nejsou odeslány všechny pakte vyšších tříd
- výhody:
    - síťové toky s vyšší prioritou mají garantovánu přednostní obsluhu (nižší zpoždění jejich bsluhou)
- nevýhoda:
    - **vyhladovění** - pokud stále chodí pakety s vyšší prioritou na nižší se nedostane

### Weighted Fair Queueing
- pakety opět přiřazovány do prioritních front
- frontám jsou přiřazeny **váhy**
    - vyšší váha = vyšší priorita
- fronty jsou obsluhovány střídavě dle přiřazené váhy
    - sttřídavě = tzv. **Round Robin** mechanismus
    - čím vyšší váha, tím více paketů je z fronty odebráno (a zpracováno)
- výhoda:
    - řeší problém vyhladovění priotirní fronty
- agresivní toky porušují spravedlivost

## Formováno / omezování toků (Traffic shaping)
- mechanismus pro řízení množství a rychlosti odesílaných paketů
- řízená kritéria:
    - *average rate* - průměrná rychlost odesílaných paketů (dlouhodobé chování)
    - *peak rate* - špičková rychlost odesílaných pakteů (krátkodobé chování)
    - burst size = maximální počet paketů, které lze do sítě poslat najednou
- mechanismy:
    - *Leaky bucket* - vyhlazování přenosu (ovlivňujě average rate)
    - *Token bucket*- umožnění krátkodobých špiček (ovlivňuje peak rate a burst size)

### Leaky bucket
- obdoba děravého kyblík
    - nezávisle na doplňování tekutiky do kbelíku, voda dírkou ale vyté vždy **konstatntní rychlostí**
- využito pro vyhlazování toku
    - nepravidelný tok je průměrován
    - pokud je kyblíček, plný, nově příchozí pakety jsou zahazovány
- tokeny jsou přiřazovány odtékajícím paketům a jsou doplňovány přitékajícími pakety
- je velmi restriktivní - penalizuje nečinné uzly (nedovoluje nashromáždit uzlu tokeny v okamžiku nečinnosti)

### Token Bucket
- umožňuje shromažďování tokenů v okamžiku nečinnosti uzlu
- za každou přenesenou datou buňku je z kyblíčku odebrán token
- velikost kyblíčku ovliňuje velikost krátkodobých špiček

* možná kombinace obou přístupů (umožňuje vstupní špičky spolu s vyhlazováním toku - otázka: V jakém pořadí?)

## Prevence zahlcení (Congestion avoidance)
- standardní fronta:
    - fronta se plní, pakety nejsou zahazovány
    - jakmile je fronta plná, nově příchozí jsou zahozeny
    - nevýhody:
        - nemožnost reakce za blížící se zaplnění
        - může dojít k zahlcení mezi více směrovači
- **Random Eardly Detection (RED)**
    - přesáhne-li zaplnění fronty určitou mez, začne směrovač zahazovat pakety náhodně vybraných toků (odesílatel toku sníží rychlost, viz TCP)
    - pravděpodobnost zahození paketu se zvyšuje se zvyšujícím se zaplněním fronty
    - odstraňuje problém globaální synchronizace
- **Weighted Random Early Detection (WRED)**
    - totéž, co RED, avšak pravděpodobnost zahození paketu závisí také na prioritě paketu

## Kvalita služby v Internetu
### Integrované služby (Integrated services)
- vysílající oznámí, jaké paramerty musí být zachovány
- síť sestaví cestu a ověří parametry (fáze *řízení přístupu* (Admission Control))
- pokud nelze požadavkům vyhovět, spojení je zamítnuto
    - aplikace buď sleví nebo počká
- udělá rezervace na všemi uzly cesty
    - nutnost rezervačního protokolu (RSVP - Resource reSerVation Protocol)
- oznámí, že může vysílající vysílat
- **problém**: zavádíme stav &rarr; snížíme škálovatelnost

### Rozlišované služby (Differentiated services)
- není potřeba oznamovat požadavky  &rarr; žádné rezervovací protokoly
- založeno na značkování paketů (přiřazování paketů do definovaných tříd provozu a jejich prioritní obsluha na vnitřní prvcích sítě)
    - označení pouze na vstupu do sítě
    - značka na umístěna do pole `Type of Service (IPv4)` nebo `Traffic Class (IPv6)`
    - na základě třídy je s paketem na vnitřních prvcích sítě zacházeno
- výhoda:
    - jednoduché
    - není stav &rarr; lépe škáluje
    - žádné úvodní zpoždění přenosu dané nutností úvodní rezervace zdrojů
