+++
title = "11 - Samoorganizujiící se sítě (P2P a ad-hoc sítě)"
date = 2021-05-14
+++

# Překryvové sítě & P2P
- P2P síť je typicky "virtuální" síť utvořená nad existující síťovou infrastrukturou
	- *překryvová síť* eje využita pro indexování a zjišťování sousedů (peerů) -> P2P systém tak je nezávislý na topologii základové (=fyzické) sítě
	- data obvykle přenášena po fyzické síti
- nový peer musí pro připojení k P2P síti získat informaci o nejméně jednom jejím členovi
	- nezbytné síťové informace: IP adresa, port, atd.
	- informace o dalších peerech mohou  být získány od něj

## Členění
- **centralizované** - jeden nebo více centrálních serverů, které poskytují služby
- **decentralizované** - neobsahují žádné centrální servery
	- **struktura**
		- **plochá (flat, single tier)**
		- **hierarchická (multitier)**
	- **topologie překryvné sítě**
		- **nestrukturovaná**
		- **strukturovaná**
		
### Centralizované
- klient-server + decentralizované distribuované systémy
- nevýhody: citlivé na útok, slabá škálovatelnost

### Decentralizované tzv. Pure
- všichni peerové mají stejná práva a povinnosti
- výhody: imunní vůči *single point of failure*

#### Plochá
- rovnoměrně rozložená funkcionalita

#### Hierarchická
- směrovací struktury sestávají z více vrstev
- nový uzel si musí najít místo -> budování je složitější, jak plochá


##### Nestrukturovaná
- každý peer zodpovědný za své data
- udržuje informaci o sousedech
- neexistuje mapování mezi identifikátory objektů a identifikátory peerů
- -> obtížná lokalizace dat
- -> neexistuje garance na kompletnost odpovědi (problém procházení celé sítě)
- -> není garance na dobu potřebnou na odpověď

#### Strukturovaná
- lokalizace dat je pod kontrolou definované strategie (*distribuované hashovací tabulky - DHT*)
- existuje mapování mezi daty a peery, na kterých jsou data uložena
- -> poskytují garanci na dobu nezbytnou pro nalezení odpovědi
- -> vyšší režie (nutno spravovat dodatečné informace)


### Hybridní
- neexistují centrální servery = škálovatelnost
- některé peer uzly jsou prohlášeny za tzv **super peers** nebo **ultrapeers** (slouží ostatním peerům jako servery)
- -> lokalizace dat je kombinace decentralizovaného a centralizovaného přístupu

# Bezdrátové sítě
- bezdrátové sítě -> přístup z mobilních zařízení
- tradičně založeny na tzv. **buněčné infrastruktuře**
	- *buňka* - území pokryto pevným vysílačem, které je napojeno do sítě
	- vyžaduje danou infrastrukturu - problém s živelnými katastrofy
- **Wireless Ad-hoc Networks**
	- síť bez nutnosti infrastruktury
	- pouze s využitím *síťových schopností účastníků*
	- *ad-hoc síť* = síť konstruovaná na požádání "pro specializované účely"
	
## Bezdrátové ad-hoc sítě
- kolekce autonomních uzlů, které spolu komunikují skrze jimi zformovanou bezdrátovou multihop síť, přičemž tato síť je spravována / udržována decentralizovaným způsobem
- každý uzel je *koncový uzel* ale i *síťový směrovač*
- topologie je dynamická
- mnoho komplexních problémů (když né každá vidí na všechny)

### Výhody
- velmi rychlé vybudování
- odolnost
- efektivnější využívání rádiového spektra než u buněčných sítí

### Problémy / výzvy
- neexistence centrální entity organizující participující uzly (musí si organizovat samostatně => *samoorganizace*)
- omezený dosah bezdrátové komunikace - musí být po cestách s více uzly (dynamická identifikace a správa směrovacích cest)
- mobilita uzlů	- mohou se pohybovat
	- -> **Mobilní ad-hoc sítě (Mobile Ad-hoc Networks, MANETs)**
- potřeba řešit otázky
	- *řízení přístupu k médiu*
	- *směrování*

### Mobilní bezdrátové ad-hoc sítě

#### Aplikace
- záchranné operace po přírodních katastrofách
- vojenské operace
- hledání prázdných parkovacích míst ve městech (bez dotazů na centrální server), vyhýbání se dopravním zácpám (= VANET)

#### Dopravní ad-hoc (Vehicular Ad-hoc Networks, VANETs)
- pohybující se auta jako uzly/směrovače mobilí sítě
- lépe uskutečnitelné než MANETy
 
## Bezdrátové senzorové sítě
- interakce s **prostředím** (místo lidmi)
	- síť zasazena do určitého prostředí
	- uzly jsou všechny stejné, vybaveny senzorem a sledují prostředí
	- vzájemně bezdrátově komunikují
- -> **Bezdrátové senzorové síte (Wireless Sensor networks, WSNs)*
- aplikace:
	- záchranné akce
	- monitoring prostředí
	- precizní zemědělství
	- inteligentní budovy, mosty

### Důležitost efektivního používání energie
- uzly často vybaveny baterií 
- -> potřeba co nejdelší časovou dostupnost
	- individuální zařízení
	- síť jako celku
- nutné použití vhodných protokolů
	- *energie per bit* = využívat cesty, které představují nízkou spotřebu
	- brát v úvahu kapacitu baterií
	- Jak vyřešit konflikty mezi rozdílnými optimalizace?
- lze využít formu dobíjení - solární články, energie z prostředí, ...

## MANETs vs P2P
- podobnosti:
	- stejné paradigma
	- samoorganizující se síť
	- dynamická topologie
	- zodpovědnost za směrování dotazů v distribuovaném prostředí
	- neexistence centrální spravující entity
- mnoho rozdílů
- -> MANETy spíše *platforma pro P2P aplikace*
- -> problém s efektivitou -> P2P nad MANETy se musí upravit pro efektivní využiyí