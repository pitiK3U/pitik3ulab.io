+++
title = "05 - Transportní vrstva"
date = 2021-04-15
+++

# Transportní vrstva
* přijímá data odesílající aplikace a transformuje je do _segmentů_
* _segmenty_ předává cílové aplikace
* zajišťuje doručení dat (segmentů) mezi komunikujícími aplikacemi/procesy
    * zajišťuje spolehlivost, poskytuje logický kanál
    * *process-to-process* delivery
* nejnižší vrstva poskytující *end-to-end* služby

## Služby
* **tvorba paketů** (Packetizing) -- aplikace posílá pakety s transportní hlavičkou
* **řízení spojení** (Connection Control) -- spojované a nespojované služby
* **adresace** (Addressing) -- označení aplikací pomocí portů `IPadresa:port`
* **zajištění spolehlivosti přenosu** (Reliability) -- řízení toku (Flow Control), řízení chyb (Error Control)
* **řízení zahlcení sítě** (Congestion Control)
* **zajištění kvality služby** (Quality of Service, QoS)

### Adresace na L4
* číslo portu -- adresa služby, 16bitové číslo (0--65535)
* port jak příjemce, tak odesílatele
* Rozděleny do 3 tříd:

#### well-known porty
* 0--1023
* známé konkrétní služby
* přiděluje organizace IANA

#### registrované porty
* 1023--49151
* volně použitelné, jdou zaregistrovat

#### dynamické porty
* 49151--65535
* většinou zdrojové porty odesílacích aplikací

**multiplexing** -- na straně odesílatele je mnoho aplikací a jeden transportní protokol -- identifikace zdrojovým portem

**demultiplexing** -- na straně příjemce jeden transportní protokol, vybíráme vhodnou aplikaci pro doručení, která je identifikována cílovým portem

### Řízení spojení
* **spojované služby** -- číslované pakety s potvrzením (důležité pro flow control a congestion control)
* **nespojované služby** -- nečíslované pakety bez potvrzení (nezajímá jestli přijdou, komu přijdou, ...)

## UDP protokol (User Datagram Protocol)
* nejjednodušší transportní protokol poskytující **nespojovanou nespolehlivou (=nezajištěnou)** službu
* best-effort služba
* přenos bloku dat
* služby IP vrsty + process-to-process komunikace + jednoduchá kontrola chyb
* zajištění spolehlivosti přenosu je na aplikaci
* jednoduché, minimální režie (nespojované, neuchovává stavové informace na komunikujících stranách, malá hlavička)
* aplikace: **DNS** (stačí jednoduchá komunikace), TFTP (má interní řízení toku a kontrolu chyb), real-time přenosy, multicastové přenosy, aktualizace směrovacích tabulek RIP protokolem,...

### Hlavička paketů
* zdrojový port (source port)
* cílový port (destination port)
* délka UDP paketu (length)
* kontrolní součet (checksum)

## Mechanismy zajištění spolehlivého přenosu
* L2 vrstva kontroluje chyby pouze mezi uzly, nikoli v nich
* mechanismus potvrzování -- pakety jsou sekvenčně číslovány
    * pozitivní potvrzování -- potvrzení přijetí
    * negativní potvrzování -- potvrzení nepřijetí/ztráty
* **ARQ** (Automatic Repeat reQuest) -- znovuposlání dat

### Stop-and-Wait ARQ
* nejjednodušší
* odesílatel čeká na potvrzení, pokud nastane _timeout_, pošle znovu
* pakety číslovány střídavě 0, 1, 0, 1, ...
* pozitivní potvrzování s číslem dalšího očekávaného paketu
* **piggybacking** -- u obousměrného přenosu posíláme data i s ACKem
* nevýhoda: lze posílat jen jeden paket

### Go-Back-N ARQ
* číslujeme sekvencí a nečekáme na potvrzení
* **mechanismus plovoucího okna** (sliding window) -- uchovávání informace o odeslaných/přijatých paketech
* okno odesílatele má velikost max. 2^m - 1 (m je počet bitů pro uchování SEQ; čeká na ACK na poslední prvek okna)
* okno příjemce má velikost 1 bit (očekáváme jeden určitý paket)
* *kumulativní potvrzování* - ACK se sekvenčním číslem následujícího (očekávaného) paketu
* nevýhoda: neefektivní pro vysoce ztrátové linky

### Selective-Repeat  ARQ
* číslujeme sekvencí
* rozšíření okna příjemce -- bufferování out-of-order paketů
* kumulativní pozitivní a negativní potvrzování
* okno odesílatele má velikost max. 2^(m-1) (m je počet bitů pro uchování SEQ) kvůli detekci duplicit

## TCP protokol (Transmission Control Protocol)
* **spojované služby**
* **plně spolehlivý (=spolehlivou)**
* přenos proudu bytů
* handshake
* pouze *point-to-point* spojení (dvoubodové spojení; nepodporuje multicast)
* multiplexing, demultiplexing a detekce chyb stejné jako v UDP
* TCP dostane od aplikace proud bytů, segmentuje je, opatří hlavičkou a předá síťovému protokolu (vytváří iluzi roury pro aplikace)

### Služby
* **přenos proudu bytů**
* **odesílací a přijímací buffery** -- kompenzují rozdílné rychlosti a řídí rok chyb
* **segmentace dat** -- velikost segmentu omezena MSS (Maximum Segment Size), určuje TCP nebo OS, přidání hlavičky, očíslování bytů

### Hlavička segmentů
* **zdrojový port**
* **cílový port**
* **sekvenční číslo**
* **číslo potvrzovaného segmentu** -- číslo dalšího očekávaného bytu (piggybacking)
* **délka hlavičky** (ve 4 bajtových slovech)
* **rezervovaná pole**
* **řídící data** -- 6 bitů identifikující řídící informace (URG, ACK, PSH, RST, SYN, FIN)
* **velikost okna**
* **kontrolní součet**
* **urgentní data** -- zasílání dat mimo pořadí
* **volby**

### Správa spojení
* full-duplexní přenos -- obě strany musí iniciovat spojení (třícestný handshake)
    * A: SYN
    * B: SYN + ACK
    * A: ACK
* ukončení spojení je iniciováno jednou stranou a musí být uzavřeno oběma stranami
    * A: FIN
    * B: FIN
    * B: ACK
    * A: ACK

### Řízení chyb
* segment může být poškozený, ztracený, duplikovaný nebo out-of-order
* kontrolní mechanismy: kontrolní součty, potvrzování přijatých segmentů, timeout (většinou timeout = 2×RTT Round-Trip Time)

### Řízení toku (Flow Control)
* příjemce informuje odesílatele o stavu svého bufferu

### Řízení zahlcení (Congestion Control)
* přizpůsobení rychlosti vysílání kapacitě sítě
* závisí na informacích o síti (L4 žádné info nemá, musí odhadovat)
* zahlcení nastává ve frontách směrovačů/switchů
    * **proaktivní přístup** -- snaha předcházet zahlcení
    * **reaktivní přístup** -- při zahlení snížit rychlost vysílání
* cwnd (okno zahlcení) -- kolik dat lze do sítě zaslat, aniž by došlo k zahlcení
    - fáze **Slow Start** - malá rychlost přenosu dat
    - fáze **Additive Increase** - malý růst rychlosti přenosu, velká rychlost
    - fáze **Multiplicative Decrease** - zahlcení - snížení rychlosti, férovost mezi TCP proudy

### Varianty TCP
 - Slow Start - nelze urychlit

#### TCP tahoe
- po výpakdu se jde od znovu

#### TCP Reno
- po výpadku se vynechává slow-start fáze

#### TCP Vegas
- *proaktivní* varianta TCP - snaží se předcházet výpadu
- monituruje RTT - při zahlcení sítě se RTT zvyšuje
- při zvyšování RTT je cwnd lineárně zmenšováno

### Problém
- Síťové spoje s vysokou kapacitou a vysokou latencí
- Tradiční TCP není připraveno pro takové prostředí
    - vysílací okno 83 333 paketů
    - ztráta jednoho paketu za 1 hodina 36 minut
    - &rarr; abychom přenášeli maximum **nesmí se nic ztrácet** -> nelze splnit
- Jak dosáhnout lepšího využití sítě?
- Jak zajistit rozumnou koexistenci s tradičním TCP?
- Jak zajistit postupné nasazování nového protokolu?

### Vylepšení TCP
#### Vliv RTT
- Problém mezi planteráního internetu
    - vysoké RTT &rarr; tradiční TCP nepoužitelné
    - http://en.wikipedia.org/wiki/Interplanetary_Internet
- Řízení toku
    - explicitní zpětná vazba od příjemce poomcí *rwnd*
    - deterministické
- Řízení zahlcení
    - přibližný odhad pomoc odesílatelem určovaného *cwnd*
- Finální výstupní okna *ownd*
    - `ownd = min(rwnd, cwnd)`
- použitá šířka pásma *bw* je pak
    - `bw = (8 * ownd * MTU) / RTT`

### Víceproudové TCP
- Zlepšuje chování TCP pouze pro statické chyby, né zahlcení
- Při zahlcení půjdou dolů všechny proudy
- dostupné díky snadné implementaci
    - `bbftp`, `GridFTP`, ...
- Nevýhody:
    - komplikovanější než TCP (obvykle více vláken)
    - nastartování je zrychleno nanejvýš lineárně
    - sychronní přetěžování front a vyrovnávacích pamětí směrovačích

### Konzeravativní rozšíření TCP
#### GridDT
- sbírdka ad-hoc modifikací
- jiné korekce sstresh - rychlejší slowstart
- modifikace AIMD řízení
    - additivně přidávám okno - pro úspěšné RTT: `cwnd = cwnd + a`
    - zcástu multikativně

#### Scalable TCP
- řízení zahlcení není již AIMD:
    - pro úspšené RTT: `cwnd = cwnd + 0.01 * cwnd`
    - per ACK: `cwnd = cwnd + 0.01`
    - pro výpadek: `cwnd = 0.875 * cwnd`
        - &rarr; Miltiplicative Increase Multiplicative Decrease (MIMD)
    - pro malé velikosti okna nebo větší množství ztrát v síti se přepíná do AIMD

#### High-speed TCp (HSTCP)
- RFC3649
- řízení zahlcení AIMD/MIMD
- emuluje chování tradičního TCP pro malé velikosti okna a/nebo větší množství ztrát v síti

#### Early Congestoin Notificatoin (ECN)
- Kdyby TCP věděla víc, co se děje se sítí &rarr; bylo by to rychlejší
- Součást Advanced Queue Management (AQM)
- Bit, který nastavují routery pro detekci zahlcení linky/fronty/bufferu
- TCP má na ECN reagovat stejně jako na výpadek
- ECN příznak mubý být odzrcadlen přijímačem

#### E-TCP a FAST
- E-TCP
    - navrhuje ozrcadlit ECN bit je jednou (poprvé)
    - vyžaduje umělé náhodné výpadky, aby nevytlačila ostatní 
    - vyžaduje změnu chování k ECN bitu na přijímačích a konfiguraci na směrovačích
- FAST - Fast AQM Scalable TCP
    - používá end-to-end delay, ECN a ztráty paketů pro detekci / vyhýbání zahlcení

### Přístupy odlišné od TCP
#### tsunami
- záplava daty
- 2 kanálový
- TCP je out-of-band řídící kanál - parametry, ukončení, požadavky na znovu poslání
- UDP pro přenos dat - MIMD, vysoce konfigurovatelný

#### Reliable Blast UDP - RBUDP
- Out-of-band TCP pro řízení, UDP pro přenos
- naráz pošle všechna data
- po skončení dat, kontrola, že jsou všechny
- kdyžtak přeposlání, které chybí

#### Další
- XCP
- SCTP
- DCCP
- STP
- Reliable UDP
