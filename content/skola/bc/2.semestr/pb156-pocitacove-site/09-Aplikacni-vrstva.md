+++
title = "08 - Aplikační vrstva"
date = 2021-04-30
+++

# L7. Aplikační vrstva
- L4 nestačí: z pohledu uživatele potřebujeme síťové aplikace
- poskytuje služby pro uživatele:
	- aplikační programy (aplikace) specifické pro požadovaný účel (elektronická pošta, WWW, DNS, ...)
	- aplikace = hlavní smysl existence počítačových sítí
- zahrnuje **síťové aplikace / programy* a *aplikační protokoly*
	- aplikační protokoly (HTTP, SMTP, atd) jsou **součástí** síťových aplikací
		- nejedná se o aplikace samotné
		- protokoly definují formu komunikace mezi komunikujícími aplikacemi
	- aplikační protokoly definují:
		- typy zpráv, které se aplikace předávají (*request / response*)
		- syntaxi přenášených zpráv
		- sémantika přenášených zpráv (jednotlivých polí)
		- pravidla, kdy a jak se aplikace vysílají
		
## Základní členění aplikací
- Dle *využitého aplikační modelu*:
	- Client-Server model
	- Peer-to-peer model
- Dle *přístupu k informacím*:
	- pull model
	- push model
- Dle *nároků na počítačovou síť*:
	- aplikace s nízkými nároky na přenosovou síť
	- aplikace s vysokými nároky na přenosovou síť
	
### Komunikační modely

#### Client-Server
- komunikace iniciována klientem (klient = aplikace ovládaná uživatelem)
- po ustanovení komunikačního kanálu klient zasílá požadavky na server, ten mu odpovídá (mechanismus *request-response*)
- po ukončení komunikace je komunikační kanál uzavřen
- nevýhoda: centralizace zdrojů
- valná většina aplikací v internetu (WWW,FTp,DNS,SSH,...)

##### Tenký (thin) klient
- klient vykonává minimum apl. logiky (většina na straně server)
- výhody: jednodušší, menší nároky na HW (může tak být levnější)
- nevýhody: menší škálovatelnost (server dělá moc práce), vyšší objemy přenášených dat, existence *Single point of failure* (server)
- příklad: vzdálené terminály

##### Tlustý (fat) klient
- většinu vykonává klient (opak tenkého klienta)
- výhody: menší nároky na server (=> dobrá škálovatelnost), většinou nižší objem přenesených dat, možnost práce *offline*
- nevýhody: komplexní provedení i instalace, značná spotřeba lokálních zrojů (CPU, paměť, disk)
- příklad: Firefox

#### Peer-to-peer
- jednotlivý klienti spolu komunikují přímo (uzly rovnocenné)
- každý uzel poskytuje své zdroje (výpočetní síla, úložná kapacita, atp.) ostatním uzlům
- každý uzel využívá zdrojů poskytovaných ostatními uzly
- decentralizace zdrojů
- např. sdílení souborů (Gnutella, G2, FastTrack), Skype (dříve), VoIP, atp.

### Přístup k informacím

#### Pull model
- přenos dat iniciován klientem (forma request-response)
- např. webové prohlížeče
- vlastnosti:
	- asymetrický datový tok (dotaz - datově úspornější, jak odpověď)
	- rozmanité požadavky na propustnost

#### Push model
- přenos dat iniciován serverm automaticky na základě znalosti uživatelova profilu (požadavků)
- např. streamimng multimedií
- vlastnosti:
	- jednosměrný datový tok
	- definované (a stálé) požadavky na propustnost (a zpoždění, jitter, ...)
	
### Nároky na počítačovou síť - nízké vs. vysoké
- Základní parametry sítě pohledem aplikací:
	- **ztrátovost (loss)** - pravděpodobnost ztráty (poškození) přenášených dat
	- **propustnost (bandwidth)** - objem přenesených dat za časovou jednotku
	- **časová omezení (timing)** - **zpoždění (delay)** (doba nutná pro přenos po síti) a **kolísání zpoždění (jitter)**
	
## Vybrané síťové aplikace
- *Jmenná služba - DNS*
- *World Wide Web - HTTP*
- *Ekektronická pošta - SMTP*
- *Přenos souborů - FTP*
- *Multimediální přenosy - RTP, RTCP*

### Jmenná služba - DNS
- **Domain Name System (DNS)** - služba pro překlad doménových jmen IP adresy a zpět
- V začátku pomocí tzv. **host** souborů
	- soubor s dvojicemi doménové jméno, IP adresa
	- neškálovatelné (nemožnost mít soubory celého internetu, vyhledávání, aktualizace, ...)
	
#### Jmenný prostor
- *Jmenný prostor* - způsob pojmenování předmětných entit
- 2 základní varianty:
	- *plochý jmenný prostor* - jména bez jakékoliv vnitřní prostory (`mujRouterBrno`, hlavní nevýhoda: nemožnost využití ve velkém systému (duplicity))
	- *hierarchický jmenný prostor* - jména s hierarchickou vnitřní strukturou
		- jména sestávají z několika částí, každá s definovaných významem
		- např. `mujRouter.DomavBrne.cz`
		- hlavní výhoda: možnost decentralizace správy (přidělování a kontroly) jmen
		
#### Jmenný prostor Internetu
- *Jmenný prostor internetu* - **doménový jmenný prostor (Domain Name Space)**
	- varianta hierarchického uspořádání
	- struktura invertovaného stromu
	- maximální počet úrovní = 128
	- každému uzlu přidělena tzv. *jmenovka* (label) a *doménové jméno*
		- *label* - řetězec (max. 63 znaků) popisující daný uzel (jmenovka kořenového uzku je prázdný řetězec)
		- *domain name* - sekvence jmenovek (oddělených `.`) od daného uzlu ke kořenovému (plné doménové jméno vždy končí `.`)

#### Domény v internetu
| label | Description |
|-------|-------------|
| com   | Commercial organizations |
| edu   | Educational institutions |
| gov   | Goverment institutions   |
| int   | International organizations |
| mil   | Military group |
| net   | Network support centers |
| org   | Nonprofit organizations |

##### Národní domény
Národní domény (Country Domains) definují uzlu podle jejich příslušnosti ke státu.
Na první úrvni jsou využity dvoupísmenné zkratky státu (cz, sk, ca, us, ...)

## World Wide Web - HTTP
- **HyperText Transfer Protocol (HTTP)** - protokol pro přístup k datům na WWW
	- přenášená data mohou být ve formě textu, audia, videa, ...
	- základní idea: klient vysílá požadavek, WWW server zasílá odpověď
		- komunikace TCP protokolem na portu 80

### URL
- Součástí požadavku je tzv. *Uniform Resource Locator (URL)*
	- standardní mechanismus pro specifikace čehokoliv na Internetu
	- definuje zdroj, který chce klient získat
	- součástí URL je:
		- *method* - metoda (protokol), který má být využit pro pšístup k odkazovanému zdroji
		- *host* - uzel, kde se odkazovaná informace nachází (kde má být vyhledána)
		- *port* - volitená součást, pokud je využit jiný než standardní port
		- *path* - cesta, kde se odkazovaná informace nachází (případně další informace (parametry))
	- `<method>://<host>:<port>/<path>`
	
### Dokumenty
- Základní kategorie WWW dokumentů
	- *statické* - na serveru uložené dokumenty s pevným obsahem (např. HTML dokumenty)
	- *dynamické* - neexistují v předem definovaném formátu; jsou tvořeny webovým serverem dle požadavků klienta (GCI scripty)
	- *aktivní* - serverem poskytnuté programy spouštěné na straně klienta (JAVA aplikace)
	
## Elektronická pošta - SMTP
- **Simple Mail Transfer Protocol (SMTP)** - standardní mechanismus pro posílání el. pošty (electronic mail, email)
- Struktura SMTP emailové zprávy:
	- *obálka (envelope)* - obsahuje adresu odesílatelem, adresu příjemce a další informace
	- *vlastní zpráva (message)* - dělí se na hlavičky a tělo zprávy
		- hlavičky - definují odesílatele, příjemce, předmět zprávy, ...
		- tělo zprávy - vlastní přenášená zpráva
- emailové adresy:
	- skládají se z tzv. *lokální část* a *doménového jména*
		- lokální část definuje jméno souboru, kam je doručována pošta předmětného uživatele (tzv. *mailbox*)
		- doménové jméno dané organizace
	- doručení emailu probíhá na základě emailových adres uvedených v **obálce zprávy**
	- `<local part>@<domain name>`
	- (adresa v hlavičce a adresa v obálce se může lišit)
	
### Doručení elektronických zpráv
1. doručení emailu lokálnímu poštovnímu serveru (mailserveru) (SMTP)
2. předání emilu cílovému poštovnímu serveru (SMTP)
3. předání / čtení emailu cílovým poštovním klientem (POP3 nebo IMAP4)

## Přenos souborů - FTP
- **File Transfer Protocol (FTP)** - standardní mechanismus Internetu určený pro přenos soubory mezi uzly
	- ustavuje **dvě samostatná TCP spojení**
		- řídící zprávy zasílány tzv. *out-of-band*
		1. *řídíci spojení* - (TCP, port 21) otevřeno celou dbu relace
		2. *datové spojení* - (TCp, port 20) otevíráno / zavíráno pro každý přenášený soubor
	- *řídící komunikace* - přenos pořadavků klienta a odpovědí serveru
		- domluva na parametrech spojení
			- typ souboru (textový vs. binární), vnitřní struktura (nepoužívá se), přenosový mód (proudový, blokový komprimovaný)
			- nezbytné pro překonání heterogenity komunikujících stran
	- *datová komunikace*
	
## Multimediální přenosy
- posun k využití sítě pro přesun *dynamických dat* (přenosy audia a videa)
- vyžadují velké objemy přenášených dat
- specifické nároky na přenos (chybovost, latence, jitter)
	- požadavky na přenos zásadně ovlivňují možnosti zpracování

## Aplikace multimediálních přenosů
- Streaming uloženého audia / videa
- *Streaming live audia / videa* (multimediální obsah vzniká živě při streamování)
- *Videokonference, Internetová telefonie*
	- aplikace požadují zcela konkrétní vlastnosti přenosu (např. minimální end-to-end zpoždění)
	- jednoznačný požadavek na interaktivitu
	
### Zpracování zvuku
- *zvuk* - podélné mechanické vlnění v látkovém prostředí (vzduch), které je schopno vyvolat v lidském uchu sluchový vjem
	- akvizice => analogový signál spojitý v čase
- zpracování zvuku:
	- vzorkování a kvantování - převod analogového signálu do digitálního
	- zpracování digitálních dat - použití filtrů (ekvalizace, odstranění šumu / echa, atp)
	- komprese - snížení datového objemu
		- pro audio data není nezbytná (objem audio dat je relativně malý)
		- MP3, OGG, WMA, RA
- zpoždění:
	- přenos přes síťové médium nemá takový vliv
	- ale začátek, konec a odbavování paketů má největší vliv na zpoždění

#### Vzorkování
- *vzorkování* = **odebírání vzorku signálu** v definovaných časových intervalech (*vzorkovací frekvence*)
	- převádí spojitý časový průběh signálu na diskrétní reprezentaci
	
#### Kvantování
- *kvantování* = diskrétní reprezentace **hodnoty intenzity zvuku** v okamžiku odebíraných vzorků
	- rozdělení svislé osy svukové křivky na diskrétní hodnoty
	
### Zpracování obrazu
- *obraz* = elektromagnetické vlnění s velmi úzkou šířkou spektra (viditelné světlo) odrařené od objektů v okolí a dopadající na světlocitlivě buňky sítnice oka
- akvizice video kamerou nebo mobilem
- *framerate* = počet snímků za sekundu pro zachování iluze pohybu (typicky 25 fps, níž je pohyb trhaný)
- většinou se používá komprese
	- ztrátová = nějaký počet se ztratí, nelze se vrátit k původním kopii
	- bezztrátová = lze obnovit původní obraz
	

### Transportní protokoly
- **TCP**
	- zajištění bezchybnosti přenosu - zvýšení (ent-to-end) latence
	- zajištění férovosti nedovoluje dostatečnou šířku pásma na vytížených linkách --> problém pro video data
- **UDP**
	- neověřuje a nezajišťuje bezchybnost přechodu
	- minimalistický, efektivnější, rychlejší
		- nenavyšuje tolik latenci přenosu
	- => vhodný pro přenos multimediálních dat
		- vhodný pro interaktivní přenosy
		- využíván ve většině případů

### Přenosová síť
- Internet poskytuje best-effort službu
- vzhledem k využití UDP protokolu je potřeba se vyrovnat s chybovostí přenosu
	- latence, jitter