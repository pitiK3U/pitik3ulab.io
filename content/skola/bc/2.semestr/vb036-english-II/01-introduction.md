+++
title = "01 - English II"
date = 2021-03-08
+++

# Introduction
## Presentations
- 6-7 minutes
- [MUNI template](https://sablony.muni.cz/fakulty/fakulta-informatiky/ostatni/ppt-prezentace)
- IT-related topic
- Q&A
- Handout + discussion
	- a heading 
	- the goal of the presentation (key fact/message(s) to remember)
	- a bullet-point summary of the presentation
	- important key words to remember
	- and a few discussion-provoking questions.

### Presentation skills
- sign-posting = https://www.bbc.co.uk/worldservice/learningenglish/business/talkingbusiness/unit3presentations/expert.shtml

#### Structure
- Introduction - important 
- Body
- Conclusion - important

- tell them what you are going to tell them
- tell them
- tell them what you told them

##### Introduction
- Attention:
	- Question
	- Provoking topic
- The ABCD model: Attention - benefits - credibility - direction

##### Body
- **Message** - provoking sentence
- support message by repetition

##### Conclusion
- most memorable moment
- summarize
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTcyNDAyNjM4NSwzNzU4OTY1MiwtMTY1Nj
kyMjcyOSw0ODI5MTQwNDIsOTQ3Njg2MzM3LC0xMDEzOTA4MDE1
XX0=
-->