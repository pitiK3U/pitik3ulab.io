+++
title = "02 - Složitost a korektnost rekurzivních algoritmů"
date = 2021-03-09
+++

# Rekurzivní algoritmy
- při tvorbě algoritmu používáme přístup **rozděl a panuj** (divide et impera) --> **rekurze**

## Rozděl a panuj
- **rozděl** (divide) problém na podproblémy, které mají menší velikost než původní problém
- **vyřeš** (conquer) podproblémy stejným postupem (rekurzivně); jestliže velikost podproblémy je malá, použij přímé řešení
- **zkombinuj** (combine) řešení podproblémů a vyřeš původní problém

## Korektnost
- důkaz mat. indukcí vzhledem k velikosti vstupní instance
- důkaz, že rekurzivní volání je aplikováno na podproblémy vstupního problému
- báze rekurze je bází indukce
- induktivní krok: z předpokladu o korektnosti řešení pro podproblémy odvodíme korektnost řešení vstupního problému
- ukážeme, že algoritmus je konvergentní a parciálně korektní

## Složitost
- nechť $T(n)$ je časová složitost rekurzivního algoritmu na vstupu délky $n$
- složitost zapíšeme pomocí **rekurentní rovnice**, které vyjadřuje $T(n)$ pomocí složitosti výpočtů na menších vstupech
- označme $B(n)$ složitost výpočtu pro bázový případ ($n \le c$)
- označme $D(n)$ složitost konstrukce podproblémů (*divide*)
- označme  $C(n)$ složitost řešení podproblémů a nalezení řešení původního problému (*combine*)
- vstup velikosti $n$ rozdělíme na $k$ podproblémů velikosti $n_1,...,n_k$
$$T(n) = \begin{cases}
		B(n)  &  \text{pro } n \le c \\\\
		\sum_{i=1}^k  T(n_i) + D(n) + C(n)  &  \text{jinak} \\\\
	\end{cases}$$

## Explicitní vyjádření

### Metoda rekurzivního stromu
- řešení rovnice "zpětným dosazováním"

# Master theorem / Kuchařková věta
- Nechť $a \ge 1$, $b > 1$ a $c \ge 0$ jsou konstanty a nechť $T(n)$ je definované na nezáporných číslech rekurentní rovnicí
$$T(n) = aT(n/b) + f(n)$$
kde $f(n) \in \Theta(n^c)$. Potom platí
$$T(n) \in \begin{cases}
	\Theta(n^c)  &  \text{když } a < b^c \\\\
	\Theta(n^c\log n)  &  \text{když } a = b^c \\\\
	\Theta(n^{\log_b a})  &  \text{když } a > b^c 
\end{cases}
$$

## Problém maximální podposloupnosti
- rozdělení na 2 častí, zjišťování největší posloupnosti v polovinách
- chybí nám ale ještě podposloupnosti, které přesahují mezi 2 částmi
	- vyřešíme tak, že hledáme největší podposloupnost na levé straně, které končí prostředním prvkem, a přičteme podposloupnost z prvé částí, která začíná prostředním prvkem 
