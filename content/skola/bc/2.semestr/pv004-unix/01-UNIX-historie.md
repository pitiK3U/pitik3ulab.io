+++
title = "01 - UNIX historie"
date = 2021-03-03
+++

# Historie
- vznik 1970
- UNIX  = přístup pomocí terminálu

## Schéma přístupu k UNIXu
- **Terminály** v kancelářích připojené sériovou RS-232 linkou k **serveru** uzavřeném na počítačovém sále. 
	- Nemá procesor, ani paměť; pouze klávesnici a obrazovka (ani myš nemá)
	- rozlišení obrazovky 80 sloupců × 25 sloupců
	- problém adresace kurzoru - nemohli jsme se vracet s kurzorem nazpět
	- ovládání příkazovou řádkou

* Pojem "konzole": Terminál připojený přímo k počítači.

## Pre-UNIX Time
- 1965-69 - Pokus o sw projekt **Multics** (Bell Labs, GE, MIT) (multi uživatelský, multi procesový)

## Historie UNIXu
- Minipočítač - PDP-7, údajně použité pro první unixový kód
	- Vnitřní paměť 4 Kslov (18 bitové slovo, 9KB) až 32Kslov, disk 1 Mslov
- 1970
	- **Ken Thompson** na papíře vytváří nový mechanismus multiuživatelské správy souborů.
	- **Dennis Ritchie** a K.T. jej implementují na PDP-7
	- hra simulace přistání na měsíci --> bez OS se to dělalo obtížně
- 1971 
	- Přesvědčí Bell Labs o vhodnosti zakoupit PDP-11/20 pro vývoj systému na zpracování textů pro patentové oddělení.
	- Vzniká název **UNIX** (údajný autor **Brian Kernighan**)
	- K. Thompson zahajuje implementaci Fortranu pro UNIX; vzniká jazyk **B**
	- D. Ritchie přidává datové typy se strukturami a vzniká jazyk **C**. (Na to dobu vyšší programovací jazyk; ostatní systémy byly v assembleru)
- 1973
	- Většina operačního systému přepsána do jazyka C. 

### Ochranná známka UNIX
- "UNIX is a registered trademark of The Open Group"
- 
<!--stackedit_data:
eyJoaXN0b3J5IjpbODEzNzg2NDMyLDk1NDk0Mzk1Niw5NTgwMz
I1MzYsLTE3NDc5ODIzODgsMzA0NjgyNjc3LC0xMzMwMjM5MDcz
LC04Mjk1NzQ4NDUsLTUyOTYwNTQzMV19
-->