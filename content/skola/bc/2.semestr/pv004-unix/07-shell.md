+++
title = "07 - Shell"
date = 2021-04-15
+++

# Uživatelské rozhraní
Známé:
- Bourne shell (**sh**)
- C-shell (**csh**)
- Korn shell (**ksh**)
- Bourne-again shell (**bash**)

Bourne shell: \
&emsp; Původní Thompson shell `/bin/sh`. V roce 1979 nový Bourne shell se stejným názvem `/bin/sh` (autor Stephen Bourne)

Co je shell? \
&emsp; Interpretačiní programovací jazyk. Čte příkazy z terminálu nebo ze souboru a provádí je.

Obecný formát příkazového řádku: \
&emsp; `arg0 arg1 arg2 ...` \
&emsp; Řetězec `arg0` je vždy jméno příkazu. \
&emsp; Příkazy
 - vnitřní
 - vnější
&emsp; `arg0` se nejprve hledá v seznamu vnitřních příkazů a potom podle seznamu cest proměnné PATH

Ukončovací kód (exit status): \
&emsp; Každý příkaz při svém ukonření vrací numerický stav. \
&emsp; Hodnota `0` znamená OK. \
&emsp; Nenulová hodnota značí chybu. \
&emsp; `echo $?`

Bílá místa == oddělovač argumentů (nezáleží na počtu.)

`;` == oddělovač příkazů.

# Řídíci znaky
- Obrácené lomítko `\` (backslah):
    - Znak následující `\` ztrácí svůj řídící charakter a stává se obyčejným znakem.
    - Výjimka je `\` na konci řádku sděluje, že další řádek je pokračováním tohot.
- Apostrof (single quotes):
    - Uzavření znaků mezi dva apostrofy `'...'`, ztratí všechny řídící charakter.
- Úvozovky (double quotes):
    - Uzavřením znaků do dvojice úvozovek `"..."` ztrácejí znaky svůj řídící význam kromě:
        - `$` (dolar)
        - `\`` (obrácený apostrof)
        -
    
# Přesměrování vstupu a výstuppu
- Shell přesměrování-propojení zajistí před provedním příkazu.
- File descriptor numner (n)
    - `příkaz n> soubor`
    - `příkaz n< soubor`
- Každý proces má nastaveno:
    - `0` - standardní vstup
    - `1` - standardní výstup
    - `2` - standardní chybový výstup
- Implicitní varianty:
    - `1>` je totéž jako `>`
    - `0<` je tottéž jako `<`
- Přesměrovávání vstupu:
    - `příkaz n< soubor` - Soubor nemusí existovat
- Přesměrování výstupu:
    - `příkaz > soubor`
        - v případě, že soubor neexistuje je soubor vytvořen
        - pokud soubor existuje, v okamžiku otevření se zkrátí na velikost 0 a bude se přepisovat
        - Pokud však byl proveden příkaz `set -C` (nebo `export noclobber=`), potom nelze přesměrovat výstup do existujícího souboru. Toto omezení odstraní operátor `>|`. (cesta do pekel, jsme tam kde jsme byly)
    - **Nesmí se použít** `ls > a > b`
        - Soubor `a` zůstane prázdný, soubor `b` se naplní stdout. Problém lze řešit např.: `ls|tee a > b`
- Připojení výstup na konec souboru:
    - `příkaz n>> soubor`
        - Pokud neexistuje, vytvoří se.
- Vstup dokumentu ze stejného zdroje jako příkazy
    - `příkaz n<<ukončení`
        - `ukončení` je symbol konce vstupu (řetězec označující konec vstupu)
        - Řetězec  `n<<ukončení` musí být bez mezer. (zpracuje se jako by byl v `".."`)
```bash
mail novak <<EOF
První řádek dopisu
Druhý řádek dopisu
EOF
```
    - Ve variantě `n<<-ukončení` se vypouštějí všechny znaky TAB na začátku čtených řádků.
```bash
mail novak <<-EOF
        První řádek dopisu
        Druhý řádek dopisu
EOF
```
- Použití souboru spojeného s jiným deskriptorem:
    - `n>&m`
    - `ls neni ji > dirlist 2>&1` - Deskriptor 1 se přiřadí soubor dirlist a deskriptor 2 se přiřadí k 1.
    - není-li `n` ani `m` uvedeno, potom `>&` slouží jako operátor přesměrování std out a stderr do souboru
    - `ls neni je >& vse`
- Přesměrování stdout na chybový:
    `echo Chyba >&2`

# Spojování příkazů do kolon
`příkaz1 | příkaz2 [ | příkaz 3 ...]`
- Operátor `|` propojucí stdout příkazu1 se stdin příkazu2 **rourou**
- Příkazy takto propojené nazýváme **kolonou**
- Ukončovacím kódem kolony je ukončovací kód posledního příkazu v koloně.
- Zápisem `! příkaz1 | příkaz2 [ | příkaz 3 ...]` se ukončovací kód kolony neguje
    - Negace ukončovacího kód znamená:
    - 0 se mění na 1
    - nenulová hodnota se mění na 0

# Job control 
## Spustění příkazu na pozadí
- `prikaz1 & [prikaz2 & ...]`
- Shell spustí příkaz asychronně, tj. nečeká na dokončení procesu.
- Stand. vstup takto spuštěného procesu je napojen na `/dev/null`.
- Na takto spuštěný proces(y) se nevztahuje znak **intr**.

## Job control
- Jak dostat úlohu pod správu job control?
1. Spustíme-li úlohu na pozadá (`&`), vypíše se identifikace:
```
ping aisa > /dev/null &
[1] 1234
```
- Číslo úlohy je 1 a má top-level process 1234

2. Běží-li úloha normálně spustěná na popředí a chceme s ní nějak naložit, siskneme snak `susp`. Shell pošle procesu signál `STOP` a vypíše se:
```
ping aisa > /dev/null
^Z
[2]+ Stopped    pring aisa > /dev/null
```

## Příkazy
- `jobs` - vypíše seznam řízených úloh
```
[1]- Running    ping aisa > /dev/null &
[2]+ Stopped    ping aisa > /dev/null
```
    - `+` znamená current job, `-` znamená previous job
    - Na úlohu se zle odkazovat několika způsoby
        - `%+` nebo `%%`

- `fg [job]`
    - Úloha se spustí na popředí.
- `bg [job]`
    - Úloha se spustí na pozadí.

# Seznamy
- posloupnost příkazů, odděleno buď novým řádkem, středníkem `;` nebo ampersandem `&`
- Shell provádí příkazy v jakém jsou zapsány.
- Pokud zápis končí ampersandem (`&`), ihned se zahájí provádění následujícího.
- Návratový kód seznamu je návratový kód posledního prováděného procesu
- `&&` - provede se další příkaz **pouze** pokud návratová hodnota předchozího procesu byla 0
- `||` - provede se další příkaz **pouze** pokud návratová hodnota předchozího procesu byla různá od nuly

# Substituce příkazů
- ``` `příkaz` ```
- `$(příkaz)` - (je preferovaný způsob, více vnořování)
- Operátor obrácený apostrof.
- Uvnitř znaků pro substituci příkazů se vykoný daný příkaz a následně je výstup toho příkazu předán na `stdin` příkazu nadním.
- Znaky nového řádku se nahradí mezerou.

# Proměnné
- Deklarace a přiřazení hodnoty proměnné: `jméno=[hodnota]`
- Hodnotou je řetězec (i prázdný) podléhající všem dosud popsaným expanzím (nepodlého `*`, viz dále)
- použití proměnné:
    - `$jméno`
    - `${jméno}`
- výpis nastavených proměnných `set`
- Zrušení proměnných: `unset proměnná ...`
- Proměnné se explicitně nedědí:
    - `export proměnná ...` - umožňuje proměnnou exportovat do potomků
- Načítání obsahu proměnných ze stdin:
    - `read proměnná ...` - Příkaz čte ze  std in slova a aplikuje expanzi

## Proměnné používané shellem
- `HOME` - Domovský adresář pro příkaz "cd bez parametru" apod.
- `PWD` - Běžný (pracovní) adresář; `pwd` (`echo $PWD`)
- `OLDPWD` - Předchozí adresář; `cd -`
- `MAIL` - Jméno souboru s poštovní schránkou uživatele.
- `PATH` - Seznam adresářů prohledávaných při spuštění souboru.
- `MANPATH` - Senzam adresářů prohledávaných příkazem `man`.
- `PS1` - Řetězec primárního promptu
- `PS2` - Řetězec sekundárního promptu (implicitně `>`), tj. prompt pokračování řádků.
- `IFS` - Internal Field Separator - oddělovače polí v příkazu. Proměnná normálně obsahuje: mezeru, tabulátor, nový řádek.

# Závorkování
- `(seznam)`
    - Posloupnost příkazů `seznam` se provede ve vnořeném shellu.
    - Tzn. že proměnné nastavené uvnitř závorek se po opuštění pravé závorky ztrácejí.
    - Návratový kód je návratový kód seznamu.
    - `()` - jsou znaky s řídícím významem
- `{ seznam;}`
    - Posloupnost příkazů `seznam` se provede v aktuálním shellu.
    - `{}` - nejsou znaky s řídícím významem

# Složené příkazy
## For
```bash
for jméno [ in slovo; ] do seznam; done
```
- Příkaz expanduje slovo a postupně vytvářené položky přiřazuje proměnné `jméno` a provádí posloupnost příkazů `seznam`.
- Není-li `in slovo;` zadáno, potom se `seznam` provede pro každý poziční parametr.

## Case
```bash
case slovo in
vzorek [| vzorek ...]) seznam;;
...
esac
```

- Příkaz expanduje slovo a hledá je mezi zadanými vzorky s použítím expanzních znaků pro jména souborů. Jakmile se shoduje, provede se seznam příkazů. Po nalezen první shody se dále už nehledá.

## If
```bash
if seznam; then seznam; [elif seznam; then seznam; ] ...
[else seznam;] fi
```
- Provede se `if seznam;`. Pokud jeho návratový kód je nulový (OK), provede se `then seznam;`. Jinak se provede `elif seznam;` (...) nebo `else seznam;`.
- Návratový kód je kód posledního provedeného procesu nebo `0`, pokud se neprovedl žádný příkaz.

## While
```bash
while seznam; do seznam; done
until seznam; do seznam; done
```
- Seznam `do seznam;` se provádí tak dlouho, dokud je návratový kód
	- `while seznam;` nulový
	- `until seznam;` nenulový
```bash
break [n]
```
- Ukončí n-tou úroveň cyklu `for`, `while`, `until`.
```bash
continue [n]
```
- Zahájí další iteraci cyklu.
```bash
true
```
- Návratový kód vždy `0`.
```bash
false
```
- Návratový kód vždy `1`.

## Příkaz test
```bash
test výraz
[ výraz ]
```
- Příkaz vyhodnotí výraz a nastaví návratový kód `0 (true)` nebo `1 (false)`.

### Testování typu souboru
- `-b soubor` - `soubor` existuje a je blokovým speciálním souborem..
- `-c soubor`
- `-f soubor` - soubor existuje a je normálním souborem.

### Testování přístupových práv
- `-x soubor` - `soubor` existuje a je spustitelný

### Testování charakteristik souborů
- `-e soubor` - `soubor` existuje.
- `-s soubor` - `soubor` existuje a má větší velikost než nula.
- `soubor1 -nt soubor2` - `soubor1` je novější (podle času poslední modifikace) než `soubor2`

### Testování řetězců
- `-z řetězec` - Délka řetězce je nulová
- `-n řetězec` - Délka řetězce je nenulová.
- `řetězec1 = řetězec2` - Řetězce jsou shodné.
- `řetězec1 != řetězec2` - Řetězce nejsou shodné.

### Numerické testy
- Na místě numerického argumentu se smí vyskytovat číslo (může být i záporné).
```bash
[ číslo1 -eq číslo2 ] # číslo1 = číslo2
[ číslo1 -ne číslo2 ] # číslo1 ≠ číslo2
[ číslo1 -lt číslo2 ] # číslo1 < číslo2
[ číslo1 -le číslo2 ] # číslo1 ≤ číslo2
[ číslo1 -gt číslo2 ] # číslo1 > číslo2
[ číslo1 -ge číslo2 ] # číslo1 ≥ číslo2
```

### Logické výrazy
- `! výraz` - `výraz` je nepravdivý.
- `výraz1 -a výraz2` - `výraz1` a `výraz2` jsou pravdivé.
- `výraz1 -o výraz2` - `výraz1` nebo `výraz2` je pravdivý.
- Priority můžeme upravovat i závorkami `();` nesmíme však zapomenout závorky odstínit od shellovských expanzí např. `\( \)`

# Shell skript - scénář
- Soubor s příkazy pro shell.
- Soubor `skript` lze číst:
	- Přístupové právo `r`.
	- Spuštění ve vnořeném shellu: `bash skript` (Soubor `skript` hledá v běžném adresáři a `$PATH`)
	- Spuštění v běžném shellu: `. skript` (Soubor `skript` se hledá v `$PATH` a v běžném adresáři.)
- Soubor `skript` lze číst a provést:
	- Přístupová práva `rx`.
	- Spuštění ve vnořeném shellu: `skript`. (Soubor `skript` se hledá v `$PATH`)
	- Shell skript se provede vždy tím programem (shellem), ze kterého byl spuštěn.
	
## Shebang
- Program, kterým se shell skript má provést, lze specifikovat na prvním řádku od prvního sloupce:
```bash
#!/bin/sh
```
- Soubor se shell skriptem **musí** být vždy čitelný (`r`).
- Shell skript **nelze** SUID, SGID.
- Komentáře lze psát za znak `#` (znak je buď na začátku řádku, nebo jej bezprostředně předchází bílé místo).

## Poziční parametry
- Poziční parametr je pojmenován číslem větším než 0.
```bash
$1 $2 $3 ... $9
${1} ${2} ... ${9} ${10} ${11} ...
```
- Shell poziční parametry implicitně nastavuje na hodnoty argumentů uvedených na spouštěcím řádku.
- Explicitně se poziční parametry nastavují příkazem set následovně:
```bash
set -- a b c
echo $1 $2 $3
```
- `shift [n]` - Příkaz "posune" obsahy pozičních parametrů o n nebo jeden doleva. Počet pozičních parametrů `$#` se sníží o jeden.

## Zvláštní parametry
- `$*` - Expanduje se do pozičních parametrů od prvního do posledního zadaného. Je-li parametr uveden uvnitř dvojice úvozovek, expanduje se do jednoho slova tak, že se parametry oddělí prvním znakem uvedeným v `IFS` (nebo mezerou, není-li `IFS`).
- `$#` - Expanduje se do počtu pozičních parametrů.
- `$?` - Expanduje se do návratového kódu posledního dokončeného procesu.
- `$-` - Expanduje se do řetězce znaků zadaných jako volby.
- `$$` - Expanduje se do čísla procesu shellu, který provádí expanzi (typický příklad na vytvoření pomocného souboru jedinečného jména).
- `$!` - Expanduje se do čísla nejposlednějšího procesu spuštěného na pozadí.
- `$0` - Expanduje se do uživatelem zadaného jména skriptu.

## Expanze proměnných
- `${proměnná}` - nejjednodušší tvar
- `${proměnná:-slovo}`
	- Použij implicitní hodnotu.
	- Pokud je `proměnná` nedefinovaná nebo prázdná, použije se expadnované slovo.
- `${proměnná:=slovo}`
	- Přiřaď implicitní hodnotu.
	- Pokud je je `proměnná` nadefinována nebo prázdná, přiřadí se a použije se expandované slovo.
- `${proměnná:?slovo}`
	- Oznam chybu.
	- Pokud je proměnná nedefinovaná nebo prázdná, předá se na stderr chybový výstup hlášení `slovo`.
- `${proměnná:+slovo}`
	- Použij jinou hodnotu.
	- Pokud je proměnná nadefinována nebo prázdná, předá se prázdná hodnota. Je-li neprázdná, použije se `slovo`.
	- Vypustí-li se dvojtečka, testuje se pouze nedefinovatelnost.
- `${#proměnná}` - Délka řetězce proměnné ve znacích.

### Řetězce
- `${proměnná%suffix}` - nejkratší
- `${proměnná%%suffix}` - nejdelší
- `${proměnná#prefix}` - 

### Vlnková expanze
- `~novak` - Domovský adresář uživatele novak (např. `/home/novak`)
- `~` - Domovský adresář běžného uživatele (podle `$HOME`)
- `~/.profile` - Vždy se vykoná po spuštění
- `~+` - Běžný adresář (podle `$PWD`)
- `~-` - Předchozí běžný adresář (podle `$OLDPWD`)

### Hvězdičková expanze
- `*` - Vyhovuje libovolnému počtu znaků
	- Neexpanduje se na soubory začínající `.` - nutno zadat explicitně `.*`
- `?` - Vyhovuje jednomu libovolnému znaku.
- `[]` - Definuje třídu vyhovujících znaků; obsah hranatých závorek popisuje právě jeden znak
	- `[ACd-i]*` - vyhovuje jménům začínajícím znakem A,C,d,e,...,i.
	- `[!0-9]*` - Vyhovuje jménům, které nezačínají číslicí.

## Funkce v shellu
- definice: `jmeno() { seznam;}`
- volání: `jmeno [argumenty]`

### Proměnné globální a lokální:
- **Globální**: nastavovány klasickým způsobem
- **Lokální**: `local promenna[=...]`
- Poziční parametry se nastaví tak, jak byly zadány, při spuštění funkce (kromě `$0`).
- příkaz `return [návratový kód]`
	- interní příkaz shellu
	- Ukončí právě prováděnou funkci.
- Funkce se provádějí v běžném shellu.
- Mohou být rekurzivní.
- Funkce se nedědí.

## Aritmetické expanze
- `$((výraz))`
- alternativní způsob: `$[výraz]`
- pravidla
	1. Výraz se interpretuje tak, jako by byl uzavřen do uvazovek (uvozovky uvnitř výrazu nemají řídící význam).
	2. Ve výrazu se expandují parametry (`$..`) a substituce příkazů `\`...\``
	3. příkaz se vyčíslí
- Pouze v aritmetice v long integer
- Můžeme použít `()` pro změnění priority
- Konstanty
	- `0n` - osmičková soustava
	- `0xn` - šestnáctková soustava
	- `báze#n` - soustava, báze je 2 až 36
	
## Nalezení a provedení příkazu
1. shellovské funkce
2. interní příkazy
3. normální programy

## Volba `--`
- Po `--` nebudou argumenty brány jako volba