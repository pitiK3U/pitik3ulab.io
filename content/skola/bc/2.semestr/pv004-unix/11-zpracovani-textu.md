+++
title = "11 - Zpracování textu"
date = 2021-05-13
+++

# Textové editory
- první editor `ed`
- `vi` = `ed` + adresace kurzoru 

## Neinteraktivní editor sed

# Rozšířené regulární výrazy (egrep, awk)
- `+` - alespoň jeden výskyt
- `?` - žádný nebo jeden výskyt