+++
title = "02 - Pravidla vývoje UNIXu"
date = 2021-03-11
+++

# Pravidla vývoje UNIXu
- Psát programy, které budou dělat právě jednu věc, a tu budou dělat dobře.
- Psát programy tak, aby mohly navzájem spolupracovat.
- Psát programy tak, aby povely přijímaly hromadně ze vstupu v textové podobě.
- Psát programy tak, aby výstupy produkovaly v textové podobě a mohly být použity jako vstupy do programů dalších.

# Příčiny popularity UNIXu
- Systém je napsán programovacím jazykem vyšší úrovně.
- Jsou dostupné zdrojové texty systému.
- Má jednoduché a zdokumentované uživatelské rozhraní.
- Nabízí prostředky na budování komplexních programů z jednodušších.
- Poskytuje jednoduché konzistentní rozhraní periferních zařízení.

# Vlastnosti operačního systému UNIX
- multiprogramový
- multiuživatelský
- s terminálovým přístupem

Operační systém musí uživateli u terminálu vytvořit pocit, že počítač schovaný za terminlem je "pouze" jeho.
- ochrana uživatelského prostředí
- ochrana souborů
- ochrana procesů
- ochrana systému proti uživatelům

(Démon = proces je spuštěn, spící; čeká na příkaz)
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTExMjg1NzI1OTQsLTEwMzIxNTE2NTIsLT
k1ODYyMDkyXX0=
-->