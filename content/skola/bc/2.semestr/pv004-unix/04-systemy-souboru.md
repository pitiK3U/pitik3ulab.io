+++
title = "04 - Systémy souborů"
date = 2021-03-25
+++

# Systémy souborů
- Souborový systém (file system) je zúůsob organizace adresářů a souborů "na disku".
- Kořenový či základní systém souborů, **UFS** - Unix File System: např *ext4* pro Linux
- Ostatní:
    - Linux: XFS, JFS, btrfs, ext2, ext3
    - macOS: APFS, HFS+
    - Windows: FAT, NTFS, exFAT
    - síťové: NFS, SMB
    - média: ISO 9660 a UDF pro CD, DVD, Blu-ray
- Nezbytné systém souborů **odpojit** (umount/unmount) před vyjmutím média

## Základní unixový systém souborů
- **Zaváděcí blok** (Bootblock, sw nezbytné pro zavedení OS)
- **Superblok** (údaje o systému souborů)
- **Seznam i-uzlů** (i-node/inode list, jeden i-uzel obsahuje informace o jednom souboru)
- **Seznam datových bloků** (zřetězené bloky obsahuují data souboru)

## Jméno souboru a adresáře
- Jméno souboru smí být dlouhé nejvýše **255 znaků**. Na tuto délku je zkracováno bez varování
- Může obsahovat libovolné znaky. Komplikace způsobují řídící znaky shellu. Výjimka: v kořenovém adresáři nesmí být soubor jména "/".
- Rozlišují se malá a velká písmena.
- Zvláštní význam mají jména souborů začínající znakem `.` (tečka), např. `.profile`. Při expanzi nahrazovacího znaku `*` se taková jména nepoužijí. `ls` vs `ls -a`

### Adresáře
- Oddělovačem jmen adresářů je znak `"/"` (lomítko)
- **Kořenový adresář** (root) `"/"`.
- **Běžný** (pracovní) **adresář**.
- Rozlišujeme cestu **absolutní** `"/.../.../..."` a **relativní** `".../.../..."`
- `cd -` - vrátí do posledního adresáře
- Každý adresář obsahuje položky `.` (tento adresář) a `..` (nadřazený adresář)

## Reprezentace souboru na disku
- Obsah souboru:
    - Z pohledu UNIXu je soubor posloupností bajtů. Systém obsah neinterpretuje
- Informace o souborech jsou soustředěny do jednoho místa na paměťovém médiu - do **seznamu i-uzlů** (i-node)
- Jeden soubor (nebo adresář) je popsán právě jedním i-uzlem
<!-- markdown cheats -->
- i-uzly neobsahují jména souboru
- `ls -il` vypíše obsah adresáře vč. i-uzlu
- i-uzel číslo **2** ukazuje vždy na kořenový adresář
- i-uzel neobsahuje jméno &rarr; více jmen může ukazovat na stejný i-uzel = **tvrdý odkaz**
- Prázdná položka má číslo i-uzlu nulové

## Odkaz
- soubor ukazuje na stejní inode
- `ln <soubor> <odkaz>`
- adresář ukazuje na sebe - `.` a na adresář nad sebou `..`
- počet odkazů pomocí `ls -i`

### Symbolické okdazy
- odkazy na adresáře
- okdazy mimo systém souborů
- `ln -s exesting new`
- kompletně nová položka
- v datech je cesta k danému objektu

# Formát i-uzlu
- "i" jako index
- k dispozici 2x:
    - i-uzel na disku
    - i-uzel v paměti

## na disku
- volný × obsazený
- ID vlastníka souboru
- ID skupiny vlastníků
- typ souboru (obyčejný, adresář, znakový nebo blokový sepciální soubor, FIFO)
- přístupová práva
- datum a čas poslední modifikace obsahu souboru, modifikace i-uzlu, přístup k souboru
- počet odkazů na soubor
- seznam diskových adres uložení
- velikost souboru v bajtech

## paměťová kopie obsahuje navíc
- identifikace uzamknutí (pro výlučný přístup)
    - výlučný - může mít pouze jeden proces, pokud bude proces zapisovat
    - sdílené - pokud bude proces pouze číst, může uzamknout více procesů
- ...
- počer odvolávek (napr. kolik instacní souboru je otevřených)
- soubor je místem připojení dalšího systému souborů

# Superblok
- velikost systému souborů
- počet volných bloků
- seznam volných bloků
- index následujícícho volného bloku v seznamu
- velikost seznamu i-uzlů
- počet volných i-ulzů
- seznam volných i-uzlů
- index následujícícho volného i-uzlu
- pole zámků pro seznam volných bloků a i-uzlů
- příznak modfikace superbloku

# Ext2, ext3 a ext4 pro linux
- jeden blok 4KiB
- 12x přímý odkaz na blok dat souboru
- 1x nepřímý jednoduchý - 1024 přímých odkazů
- 1x nepřímý dvoucestý - 1024^2
- 1x nepřímý trojcestný - 1024^3
- celkem max 4TB, avšak na linuxu max 2 TB
- max svazek 16 TB

## Ext4
- oběma směry kompatibilní s ext2 a ext3
- **extent** - rozsah bloku o velikosti až 128MiB
- max velikost souboru: 16 TB
- max velikost svazku: 1 EB (1024^6)

# Speciální soubory
- Rozlišujeme
    - Znakový speciální soubor
    - Blokový speciální soubor
- Součástí jádra jsou "ovladače zařízení"
- Uživatelské rozhraní vůči zařízením se realizuje prostředníctvím systému do souborů
- **Speciální soubor** je odkaz na i-uzel, který ukazuje na ovladač
- příkaz pro vytvoření pseciálního souboru: `mkdnod jméno typ hlavní_číslo vedlejší_číslo`
    - `typ` - je `b` = blokové zařízení nebo `c` = znakové zařízení
    - `hlavní_číslo` - je číslo udávající typ zařízení - ukazuje do tabulky zařízení
    - `vedlejší_číslo` - je číslo jednotky
- `/dev/rst12` - magnetická páska (před zápisem souboru se vždy přvine na začátek) 
- `/dev/nrst12` - magnetická páska bez převinutí

## FIFO - pojmenovaná roura
- `mknod jméno p`
- p (pipe) = roura
- Pro přenos dat mezi procesy
- Při zápisu do roury se data ukládají na disk téměř stejně jako při zápisu do souboru
- i-uzel obsahuje ukazatel pro čtení a ukazatel pro zápis
- Ukazatele nelze měnit jinak než čtetním/zápisem . důsledně FIFO

# Typický adresářový strom
- `/unix`, `/bsd`, `/boot` - Jádro OS
- `/bin/ls`, `cp`, `sh` - Základní systémové programy a příkazy
- `/dev` - adresář speciálních souborů
- `/etc` - adresář většinou konfiguračních souborů systému
- `/lib` - adresář knihoven
- `/mnt` - pomocný adresář pro připojování dočasných souborových systémů
- `/tmp` - veřejný adresář pro pomocné a dočasné soubory
- `/home` - Adresář s domovskými adresáři uživatelů
- `/usr/bin`, `etc`, `lib`, `tmp` - adresáře se soubory, které typicky z kapacitních důvodů nejsou v kořenovám adresáři.
- `/usr/include` - `.h` soubory pro překladač jazyka C
- `/usr/man` - Manuálové stránky
- `/usr/local/bin`, `man`, `etc`, `lib`, `...` - programy lokálně instalované
- `/usr/bin` - Systémové programy určené zpravidla superuživateli
- `/usr/X11`, `openwin` - Okénkový systém
- `/var` - Adresář pracovních / administrativních / ... souborů systému
- `/var/mail` - Poštovní schránky uživatelů
- `/var/spool` - dočasné soubory systémových operací
- `/var/adm` - Záznamy o činnosti systému a uživatelů
- `/var/tmp`

# Přístupová práva
V i-uzlu pro vyhodnocení přístupových práv jsou:
- **vlastník souboru** - Číselné UID toho uživatele, který soubor vytvořil nebo kterému jej superuživatel věnoval.
- **skupina vlastníka** - Číslené GID skupiny, do které byl uživatel v okamžiku vytváření souboru přihlášen nebo na kterou bylo GID změněno.
- **přístupová práva** - Přístupová práva jsou v objektu uložena ve 12 bitech.

Ve výpisu `ls -l` \
`-rw-r--r-- 1 piti piti  6555 Apr  5 15:09 assignment.adoc` \
**vlastník** (označuje se jako `u` jako `'user'`) - Vlastníkem je ten, jehož UID j zapsáno v i-uzlu. (první trojice zleva) Smí práva měnit. \
**skupina** (označuje se `g` jako `'group'`) \
**ostatní** (označuje se `o` jako `'others'`) - Touto kategorií určujeme, co s tímto objektem mohou provádět ostatní. \
Přístupová práva se vyhodnucují v pořadí `ugo` - ale platí pouze první trojice, do které patříme. \


Pro **soubor** se operace defunují následovně: \
`r` - Soubor je povoleno číst. \
`w` - Do souboru je povoleno zapisovat. \
`x` - Soubor je povoleno spustit 

Pro **adresář** mají operace tyto významy: \
`r` - Adresář je povoleno vypsat; nikoli však zpřístupnit soubory v něm odkazované. \
`w` - Do adresáře je povoleno zapisovat; tj. lze výtvářet a rušit soubory (smazání pevného odkazu). \
`x` - Do adresáře je možné vstoupit; tj. adresář může být argumentem příkazu `cd` a lze zpřístupnit i-uzly souborů, na které se adresář odkazuje.

Problém u adresářu typu `/tmp` \
`drwxrwxrwt 18 root root 4096 Apr  8 07:21 /tmp/` \
Pro tyto adresáře se zavádí tzv. **sticky bit** - `t` \
Soubor v takto označeném adresáři nepovoluji se smazat nikomu jinému než vlastníkovi souboru nebo vlastníkovi adresáře. \
`t` - je pod ním malé `x` \
`T` - není pod ním malé `x`

**Soubor běží pod UID toho, kdo jej spustil.** \
Chci, aby soubor běžel pod UID vlastník souboru bez ohledu na to, kdo jej spustil: **Set User ID (SUID)** bit `rwsr-xr-x` \
Totéž pro skupinu: použiji **Set Group ID (SGID)** bit `rwxr-sr-x` (U adresáře znamená, že nový soubor bude mít implicitně skupiny daného adresáře)

**Přístupová práva jsou uložena ve 12 bitech**:
| bity | popis |
|------|-------|
| 4000 | SUID  |
| 2000 | SGID  |
| 1000 | sticky bit |
| 0400 | `r` pro vlastníka |
| 0200 | `w` pro vlastníka |
| 0100 | `x` pro vlastníka |
| 0070 | `rwx` pro skupinu vlastníků |
| 0007 | `rwx` pro ostatní |

Všechny kombinace jsou možné, ale né však významné.

## Příkaz `umask`
- Zadávání implicitních přístupových práv pro vytváření souborů a adresářů.
- Interní příkaz shellu.
- `umask nnn`
- Zadává se zpravidla třemi somičkovými číslicemi (nejvýše třemi).
- Maska se zadává inverzně. (co se nemá nastavovat)
```
umask 022
touch soubor (rw-r--r--)
mkdir adresar (drwxr-xr-x)
```

## Příkaz `chmod`
- `chmod mode soubor ...` - Nastavení smí vlastník souboru a superuživatel.
- mode asbolutně - 1-4 osmičkovými číslicemi
- nebo gramatikou

## Příkazy přístupových práv
- `chown vlastník soubor` - změna vlastníka souboru nebo adresáře (pouze superuživatel)
- `newgrp skupina` - implicitní změna skupiny, význam pouze při vytváření nových objektů
- `chgrp skupina soubor` - změna skupiny u souboru nebo adresáře
- Rozšířené nastavení práv: ACL - Access Control Lists
    - `-rw-rw-r--+` (označeno pluskem na konci přístupových práv)
    - `getfacl` a `setfacl`
