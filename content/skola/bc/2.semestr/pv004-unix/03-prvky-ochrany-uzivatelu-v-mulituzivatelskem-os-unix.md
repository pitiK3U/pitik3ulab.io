+++
title = "03 - Prvky ochrany uživatelů v multiuživatelském OS UNIX"
date = 2021-03-18
+++

# Prvky ochrany uživatelů
- Účet uživatele:
    - přihlášení
    - práce v sezení = **relace**
    - odhlášení (rozpad spojení == odhlášení)
- Účet obsahuje: `/etc/passwd`
    - **Uživatelské jméno**: login name, 3-32 znaků, začíná písmenem, jedinečné v systému, záleží na velikosti písmen
    - **Heslo**: password
        - není uloženo v otevřené podobně
        - je uložen v podobě **hash** - kontrolní součet (nejznámnější md5)
        - kontroluje se hash zadaného heslo s hashem hesla v systému
        - hash hesla je uložen v `/etc/shadow`
            - `jmeno:$<znaceni algoritmu>$<salt>$<hash>$`
        - k hashování se používá "sůl" (**salt**) - string vygenerovaný pro uživatele, přidá se k heslu a až to se zahashuje
    - **Uživatelské číslo**: user indentification = **UID** (**root** má UID = 0)
    - **Primární skupina** - groud GID (skupina aktivní o přihlášení)
        - skupiny jsou popsané v `/etc/group`
    - Doplňující identifikace uživatele - jméno, příjmení
    - Domácí adresář
    - Shell (interpret příkazů) - `/bin/sh`, `/bin/bash`, `bin/zsh`
    - Další informace - čas posledního přihlášení, doba platnosti hesla, ...
- Práva pro sřstup k souborům a adresářům:
    - určují se zvlášť pro
        - majitele souboru (u - user)
        - skupinu uživatelů (g - group)
        - ostatní, svět (o - other)
    - zvlášť pro
        - čtení/zápis/provedení (soubor) - read/write/execute
        - výpis/modifikace/vstup (adresář)
- Speciální uživatel root (superuživatel, UID=0)
    - Jediný uživatel v systému, kterému se přístupová práva nekontrolují.

## Přihlášení
- login: xnovak; nemusí být znám typ terminálu - proto nemusí fungovat znaky BACKSPACE apod.
- password: <moje heslo>; neopisuje se, libovolné znaky včetně mezer a CTRL

## Po přihlášení
- nastavení shellu, domovského adresáře
- prompt závisí na proměnné `PS1`
- ukončení: `exit` nebo `^D`

# Zadávání příkazů
- case sensitive
- příkazy se interpretují po stisku Enter
- příkazy lze zadávat "do zásoby" (`sleep 10; echo ahoj; echo hi`)
- některé shelly si pamatují historii příkazů s možností editace a opětovného použítí
- dokud řádek není ukončen stiskem Enter, lze
    - smazat poslední znak stiskem znaku `erase`
    - zrušit celý rádek stiskem znaku `kill`
- Popis vlastností terminálů: `/usr/share/terminfo/*`
- `stty -a` - Set TeleType
    - `intr` - Násilné ukončení procesu (`^C`)
    - `eof` - Ukončení vstupu (`^D` nebo `EOF`)
    - `stop` - Pozastavení výpisu (`^S` nebo `X-off`)
    - `start` - pokračování ve výpisu (`^Q` nebo `X-on`)
    - `susp` - Pozastavení procesu (`^Z`) - převední pod job control; nebezpečné zapomínat procesy

# Datum a čas v UNIXu
- zobrazení příkaz `date`
- počítá se v sekundách
- "epocha", tj. čas 0 nastal 1. 1. 1970
- uloženo na 32 bitech se **znaménkem**
- záporná hodnota před 1. 1. 1970
- **konec 32bitového světa: 03:14:08 UTC on 19 January 2038 !!**
