+++
title = "01 - Komplexní čísla"
date = 2021-03-01
+++

Příklady z analytické geometrie:
- Určete vzdálenost a odchylku dvou přímek v $\R^3$.
- V $\R^3$ určete objem čtyřstěnu zadaného vrcholy.
- Rozhodněte, zda se dva čtyřstěny v $\R^3$ protínají.
- Dejte předpis pro zobrazení z $\R^3$ do sebe, které je symetrií podle dané přímky (otočení kolem dané přímky o úhel $\alpha$).

* [slides (příklady)](https://is.muni.cz/auth/el/fi/jaro2021/MB151/um/slajdy_-_jaro_2021/lin-mod-jaro-21-uvod-C-1.pdf?predmet=1323783)

# Čísla
- $\mathbb{N} = \lbrace 0,1,2,3,...\rbrace$, operace: $+$,$\cdot$
- $\mathbb{Z} = \lbrace ..., -2, -1,0,1,2, ... \rbrace$, operace: $+$,$\cdot$, $-$
- $\mathbb{Q} = \lbrace \frac{p}{q}, p \in \mathbb{Z}, q \in \mathbb{N} - \lbrace 0 \rbrace \rbrace$, operace: operace: $+$,$\cdot$, $-$, $\div$ (částečně)
- $\mathbb{R}$ - reálná čísla (obsah kruhu, uhlopříčka čtverce), ,\,operace" navíc: odmocňování kladných
- $\mathbb{C}$ - ,\,operace" navíc: odmocňování

# Komplexní čísla
- Rovnice $x^2 + 1 = 0$ nemá reálné řešení, hodilo by se nám aby nějaké "imaginární" řešení existovalo, označíme jej $i$.
- Přidejme toto $i$ (které má vlastnost $i^2 = -1$) k reálným číslům.
- $a + bi$ pro $a,b \in \mathbb{R}$
- $(a + bi) + (c + di) = (a + c) + (b + d)i$
- $(a + bi) \cdot (c + di) = ac + adi + bi \cdot c + bi \cdot di = (ac - bd) + (ad + bc)i$ 
* $\mathbb{C} = \lbrace a + ib, a,b \in \mathbb{R} \rbrace$
* Absolutní hodnota (vzdálenost od počátku): $|z|^2 = z\bar{z} = (a + ib)(a - ib)$
* Goniometrický tvar: $z = |z|(\cos \phi + i \sin \phi)$
* Moivreova věta: $z^n = |z|^n(\cos(n\phi) + i \sin(n\phi))$.
* Komplexní čísla jsou "dokonalá" (algebraicky uzavřená):
	* **Libovolný nekonstantní polynom s komplexními koeficienty má komplexní kořen.**
	* Příklad: $(x^4 + x^3 + x^2 + x + 1)(x - 1) = x^5 - 1$, a proto $x^4 + x^3 + x^2 + x + 1 = 0$ nemá reálné řešení.
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE4Nzk2Nzk4NDgsLTEzMDI3MDU1NzYsND
EyMzkwMjQ4LDE3NDQwNjM0NTAsMTUwNDY2ODM0MiwtMTYzNzM0
NTE0MV19
-->