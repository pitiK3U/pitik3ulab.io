+++
title = "03 - Kernel"
date = 2021-03-19
+++

# Privilegovaný mód

## Módy CPU
- **privilegovaný** (supervisor) a **uživatelský** mód
- platí pro všechny **general-purpose** procesory (ale nemusí platit pro mikro-kontrolery)
- X86 poskytuje 4 různé pirvilegované úrovně
    - většina systémů využívá pouze **ring 0** a **ring 3**
    - Xen paravirtualizace používá ring 1 pro guest kernel

## Privilegovaný mód
- mnoho operací je **omezeno** na **uživatelský** mód (takto jsou spuštěny **uživatelské programy** a většina OS)
- SW běžící v privilegovaném módu může dělat "cokoliv"
    - hlavně může programovat **MMU** (memory management unit)
    - mód ve kterém běží kernel

## Memory management unit
- je to subsystém procesoru
- stará se o **adress translation**
    - uživatelský SW používá **virtual addresses**
    - MMU je překládá na **physical addresses**
- Mapování je spravováno OS kernelem

## Paging
- fyzická paměť je rozddělena na **frames** (rámce)
- virtuální paměŤ je rozdělan na **pages** (stránky)
- rámce a stránky mají stejnou velikost (nejčastěji 4 KiB)
- rámce jsou místa, stránky jsou obsah
- **page tables** mapují mezi stránky a rámce

## Swapping pages
- RAM bývala nedostatečný zdroj
- stránkování povolije OS **přesouvat stránky** pryč z RAM
    - stránka může být zapsána na disk
    - rámec může být použit pro jinou stránku
- není tak důležité pro moderní HW
- užitečné pro **memory mapping files**

## Processes
- procesy jsou primárně definovány jejich **address space** (validní vrtuální adresa)
- je to implementováno pomocí MMU
- **context switch** - při změně procesu se načte jiný page table
- **page table** - definují, co může proces vidět

## Memory maps
- rozdílný pohled na stejný problém
- OS **mapuje** fyzickou paměť do procesů
- **shared memory** - vícero procesů může mít namapovánou stejnou oblast RAM
- často část RAM je mapována jen **jednomu procesu**

## Page Tables
- MMU je programováno pomocí **translation tables**
    - tyto tabulky jsou uloženy v RAM
    - normálně se jim říká **page tables**
- o ně se stará kernel
- kernel může MMU požádat o vyměnení page table (takto jsou procesy od sebe izolovány)

## Kernel protection
- kernelová paměť je často mapována do **všech procesů**
    - tímto se často **zvyšuje výkon** na mnoho CPUs (než přišel **meltdown**)
- kernelové stránky mají nastavený speciální 'supervisor' flag
    - kód, který se vykonává v uživatelském mód, **se jich nesmí dotknout**
    - jinak, user code could **tamper** with kernel memory

# Booting
## Starting the OS
- po zapnutí je systém v **default state**
    - hlavně protože **RAM je volatile**
- celá **platforma** musí být **inizializována**
    - první je **CPU**, pak **console** HW (klávesnice, myš, ...) a ostatní zařízení

## Boot process
- proces začání inicializací vestavěného HW
- poté HW nastartuje **firmware**
    - na 16 a 32 bitových systémech to býval BIOS
    - aktuálně byl obměněn EFI na **amd64** platformách
- poté firmware načte **bootloader**
- bootloader načte **kernel**
- kernel nainicializuje **device drivers** (olvadače) a **root filesystem**
- následně předá kontrolu **`init`** procesu
- poté převezme kontrolu **user space**

## User mode Initialisation
- `init` mounts the remaining file systems
- `init` proces nastartuje **systémové služby** v uživatelském módu
- poté nastartuje **aplikační služby**
- a nakonec `login` proces

## After Log-In
- `login` proces inicializuje **user session**
- načte **dekstop** moduly a **aplikační software**
- načte uřivatele do (textového nebo grafického) **shellu**
- počítač je připraven k použití

## CPU init
- závislá na **achitecture** a **plarform**
- x86 CPU strtuje v **16-bitovém** režimu
- na starých systémech, BIOS & bootloader v tomto režimu zůstávají
- kernel změní na **protected mode** během bootování

## Bootloader
- historicky omezen na desítky **kilobajtů** kódu
- hledá kernel **na disku** (může nabídnout výběr rozdílných kernelů)
    - **omezené** chápání **souborových systémů** (aby bootloader kernel našel)
- **načte obraz kernelu** do **RAM**
- předá kontrolu kernelu

## Modern booting on x86
- bootloader dneska běží v **protected mode** (nebo i v long mode na 64-bit CPU)
- firmware chápe `FAT` filesystem
    - umí **načíst soubor** do paměti
    - velmi **zjednodušuje** boot proces

## Booting ARM
- na ARM deskách **není unifikovaný firmware** rozhraní
- U-boot je nejblíže, jak to jde k unifikaci
- bootloader potřebuje **low-level** znalost HW
- toto způsobuje psaní bootloaderů pro ARM velmi **zdlouhavé**
- aktuální U-boot může použít **EFI protokol** od PCs

# Kernel architecture
## Architecture types
- **monolithic** kernels (Linux, *BSD)
- microkernels (Mach, L4, QNX, NT, ...)
- **hybrid** kernels (macOS)
- type 1 **hypervisors (Xen)
- exokernels, rump kernels

## Microkernel
- stará se o **memory protection**
- (HW) interrupts
- task / process **scheduling**
- **message passing**

* vše ostatní je **separované**

## Monolithic kernels
- vše, co dělá microkernel
- + device drivers
- file systems, volume management
- a network stack
- data encryption, ...

## Microkernel Redux
- potřebujeme o dost víc, než microkernel poskytuje
- v "pravém" microkrenel OS je mnoho modulů
- každý **device driver** běží v **samostatném procesu**
- stejné platí pro **file systems** a networking
- tyto moduly / procesy se nazývají **servers**

## Hybrid kernels
- založen na microkernel a "vykuchaném" monolithic kernel
- monolithic kernel je velký server
    - stará se o to, o ce se microkernel nestará
    - jednodušší na implementaci než pravý microkernel OS
    - výkon někde uprostřed

## Micro vs Mono
- microkernels jsou více **robustní**
- monolithic kernels jsou **efektivnější** (less context switching)
- jednoduchost implementace je diskutabilní (v krátkodobém ohledu mono vyhrává)
- hybrid kernels jsou **kompromis**

## Exokernels
- menší než mircokernel
- o dost **méně abstrakcí**
    - aplikace dostanou pouze **block storage**
    - networking je omezeno
- existují pouze **reserch systems**

## Type 1 Hypervisors
- také známo jako **bare metal** nebo **native** hypervisors
- jsou dost podobné microkernel (nebo exokernel) OS
- "aplikace" pro hypervisor jsou **operační systémy**
    - hypervisor can use **coarser abstractions** než OS
    - celý storage device místo filesystemu

## Unikernels
- kernel pro běh **jedné aplikace**
    - nemá velký výnam na reálném HW
    - ale může být užitečný na **hypervisor**
- uskupuje aplikace jako **virtuální mašiny**
    - bez přetížení general-purpose OS

## Exo vs Uni
- exokernel běží **více aplikací**
    - obsahuje izolaci založenou na procesech
    - ale **abstrakce** jsou velmi **bare-bones**
- unikernel běží pouze **jednu aplikaci**
    - poskytuje víceméně **standardní služby** (standardní hierarchický file system)
    - socket-based network stack / API

# [System calls](../02-system-libraries-and-apis/#system-calls)
- předá vykonávání **kernel routine**
- přednání **argumentů** do kernelu
- získání **navrácené hodnoty** od kernelu
- vše musí být provedeno bezpečně

## Trapping into the kernel
- velmi málo možností
- detaily jsou závislé na **specifické architektuře**
- obecně kernel nastaví a fixní **entry address**
    - instrukce změní CPU do privilegovaného režimu
    - **a zároveň** skočí na danou adresu

### Trap example: `X86`
- `int` instrukce na těchto CPU
- tato instrukce je **softwarový interrupt**
    - interrupt je normálně hardware záležitost
    - interrupt **handlers** běží v **privilegovaném režimu**
- je to synchronní způsob
- handler je nastaven v `IDT` (interrupt descriptor table)

### Software interrupts
- jsou dostupné na velkém množství CPU
- obecně **nejsou velmi efektivní** pro systémové volání
- další úroveň indirection
    - adresa handler je získana z paměti
    - **hodně stavu CPU** musí být uloženo

#### SW interrupts on PCs
- jsou používány i v **reálném režimu**
    - starý 16-bitový režim 80x86 CPUs
    - BIOS (firmware) přes `int 0x10` & `0x13`
    - MS-DOS API přes `int 0x21`
- na starších CPU v 32-bitovém **protected mode**
    - Windows NT používá `int 0x2e`
    - Linux používá `int 0x80`

### Trap example: `amd64`/`x86_64`
- instrukce **`sysenter`** a **`syscall`** (a jejich **`sysexit`**/**`sysret`**)
- vstupní bod je uložen do **machine state register**
- je pouze **jeden vstupní bod** (oproti sw přerušením)
- o dost **rychlejší** než přerušení

### Which system call?
- často je **mnoho** system callu (300 na Linuxu, 400 na 32-bit Windows NT)
- ale málo přerušení a jedna `sysenter` adresa

#### [Reminder: System call numbers](../02-system-libraries-and-apis/#system-calls)
- každý sys call má **číslo**
- na POSIX systémech dostupné jako `SYS_write`
- pro univerzálnní `int syscall( int sys, ... )`
- toto číslo je dáno CPU registru

#### System call sequence
- prvně `libc` připraví **argumenty** syscallu
- do správného registru nasaví **číslo**
- přepne CPU do privilegovaného mód
- předá kontrolu **syscall handler**

#### System call handler
- prvně vezme **číslo** syscallu a rozhodne se co dál
- můžeme si představit jaky velký `switch`

#### System call arguments
- každý syscall má jiné **argumenty**
- přední do kenrelu je **CPU-dependent**
- na 32-bit x86, většina je předána **v paměti**
- na `amd64` Linuxu jsou všechny argumenty v **registrech** (6 registrů na argumenty)

# Kernel services
## What does a kernel do?
- správa **paměti** a procesů
- task (thread) **scheduling**
- device drivers (SSDa, GPUs, USB, bluetooth, HID, audio, ...)

## Additional services
- inter-process **communication**
- timers and time keeping
- process tracing, profiling
- security, sandboxing
- cryptography

## Kernel services
- normálně nás nezajímá, **který server**, co poskytuje
    - každý system je rozdílný
    - pro služby bereme **monolithic** view
- služby jsou používány skrze **system libraries**
    - abtrahují mnoho detailů (jestli služba používá **syscall** nebo **IPC call**)

## User-space drivers in monolithic systems
- né **všechny** ovladače jsou součástí kernelu
- case in point: **printer** driver
- taky některé **USB zařízení**
- část GPU/grafického stack
    - memory and output management in kernel
    - most of OpenGl in **user space**
