+++
title = "07 - Device drivers"
date = 2021-04-16
+++

# Drivers, IO and interrupts
## Input and Output
- převážně přemýšlíme v IO
- periferie produkují a konzumují **data**
- **input** - čtení dat vyprodukovaných zařízením
- **output** - posílání dat do zařízení
- **protocol** - validní sekvence IO událostí

## What is driver?
- **softwaer** který se s daným **zařízením** baví
- často velmi psecifiký / **unportable**
    - úzce spjatý s daným **zařízením**
    - ale také k **OS**
- často součástí **jádra**

### Kernel-mode drivers
- jsou často součástí jádra
- běží s plnými **kernel privileges**
    - taky **unrestricted** hw přístup
- zádné nebo **minimální** context switching **overhead** (rychlé ale nebezpečné)

## Microkernel
- ovladače jsou **mimo** microkernel
- ale ovladač stále potřebuje **hw přístup**
    - toto může být speciální **paměťová oblast** (memory region)
    - může potřebovat **reagovat na přerušení**
- všechno může být uskutečněno **nepřímo** (indirectly)
    - ale to může být velmi **drahé**

## User-mode drivers
- mnoho ovladačů může kompletně běžet v **user space**
- toto zlepšuje **rebustness** a **bezpečnost**
    - chyby v ovladačích nemohou **celý systém** shodit
    - a nemohou copmromise **bezpečnost** systému
- pravděpodobně za **cenu výkonu**

### Drivers in processes
- user-mode ovladače typicky běží v jejich **procesech**
- toto znamená **context switches**
    - pokáždé když si zařízení řekne o pozornost (interrupt)
    - pokaždé když **jiný proces** chce zařízení použít
- ovladač potřebuje **system calls** aby mohl se zařízením komunikovat
    - toto zapříčiňuje více overhead

## In-process drivers
- co kdyby (velké část) ovladače mohla být **knihovna**
- nejlepší z obou světů
    - **žádný** context switch **overhead** pro požadavky
    - chyby a bezpečnost zůstanou **isolována**
- čato použio pro GPU-accelerated 3D graphics

## Port-mapped IO
- staré CPU měli velmi limitovaný **adress space**
    - 16-bitové adresy znamená 64KB paměti
- periferie mají **odlišný** adress space
- **speciální instrukce** pro přírup k daným adresám
    - `in` a `out` na `x86` procesorech
## Memory-mapped IO
- zařízení **sdílí** adress space s pamětí
- **velmi časté** v aktulních systémem
- IO používá stejné instrukce jako přístup do paměti
    - `load` a `store` na RISC, `mov` na `x86`
- umožňuje **selective** user-level access (pomocí MMU)

## Programmed IO
- vstup a výstup je **řízen CPU**
- CPU musí **čekat** než je zařízení připraveno
- normálně bězí v **bus speed**
- PIO by komunikovat s **bufferem** zařízení 

## Interrupt-driver IO
- periferie jsou **velmi** pomalé než CPU
    - **polling** zařízení je drahé
- periferie mohou **signalizovat** dostupnost dat
    - a také **readiness** k přijetí více dat
- toto **uvolní CPU** aby mohlo dělat jinou práce v mezičase

## Interrupt handlers
- také známo jako **první úroveň** interrupt handler
- musí běžet v **privilegovaném** režimu
    - jsou z definice součástí **jádra**
- low-level interrupt handler musí skončit **rychle**
    - it will mask its own interrupt to avoid **re-entering**
    - a **naplánuje** nějaké dlouho běžící práce na později (SLIH)

## Secend-level handler
- dělá nějaké drahé **interrupt-related** processing
- může být vykonáno **vláknem jádra**
    - ale také uživatelským ovladačem
- často není časově ritické (proti first-level )
    - může použít standardní **uzamykací** mechanismus

## Direct memory access
- povoluje zařízení číst / psát přímo do **paměti**
- toto je **veliký** zlepšení oproti **programmed IO**
- **přerušení** singalují **plný** / **prázdný** buffer
- zařízení může číst a psát do jakékoliv **fyzické** paměti
    - otevírá problémy s **bezpečností** / spolehlivostí

## IO-MMU
- jako MMU, ale pro DMA transfers
- povuluje OS **limitovat** přístupy zařízení do paměti
- velmi užitečné při **virtualizaci**
- pouze nedávno si našlo cestu do **consumer** computers

# System and expansion busses
## history: ISA (Industry Standard Architecture)
- 16-bitový systém **expansion** sběrnice na IBM PC / AT
- **programmed** IO a **přerušení** (ale žádné DMA)
- fixní počet hw-configured **interrupt lines**
    - likewise fpr I/O port ranges
    - HW nastavení můsí být **typedback** pro SW
- paralelní přenos data a adres

## MCA, EISA
- MCA: Micro Channel Architecture
    - **propriertary** k IBM, patent-encumbered
    - 32-bitová, **sw-driven** nastavení zařízení
    - drahá a nakonec **selhalo** v marketingu
- EISA: Enhanced ISA
    - 32-bitová rozšíření ISA

## VESA Local Bus
- memory mapped IO & **DMA** na jinak ISA sstémech
- **spjato** k 80468 lince Intel CPUs (a AMD klonů)
- primárně pro **grafické karty** ale taky použito pro pevné disky
- rychle vypadl z ppouží po příchodu PCI

## PCI: Peripheral Component Interconnect
- 32-bitový následník ISA
    - 33 MHz (vůči 8 MHz pro ISA)
    - pozdější verze 66MHz, PCI-X 133MHz
    - přidána podpora pro **bus-mastering** a DMA
- stále **sdílený** paralení sběrnice
    - všechna zařízení sdílí stejné dráty

### Bus mastering
- normálně je CPU bus **master**
    - znamená, že rozhoduje kdo bude mluvit a začíná komunikaci
- je možné mít vícero msters
    - musí souhlasit na conflict resolution protocol
- často používáno pro přístup paměti

## DMA (Direct Memory Access)
- nejčastější forma bus masteringu
- CPU říká zařízení co a kam zapsat
- zařízení pošle data přímo do RAM
    - CPU může pracovat na ostaních věcech mezitím
    - ukonční je signalizováno pomocí přerušení

## Plug and play
- ISa systémy pro IRQ konfigurace bylo **messy**
- MCA vytvořilo sw-konfigurační zařízení
- PCI později zlepšilo MCA s "Plug and Play"
    - každé PCI zařízení má ID, které je **řekne** systému
    - umožňuje **enumeraci** a automatickou **konfiguraci**

### PCI IDs and drivers
- PCI povoluje enumeraci zařízení
- **identifikátory** zařízení budou párována k **olvadačům** zařízení
- toto umožňuje OS načíst a nakonfigurovat jejich ovladače
    - nebo odkonce sáthnout / naistalovat od dodavatele

## AGP: Accelerated Graphics Port
- PCI se stalo moc **pomalé** pro GPUs
    - AGP je založeno na PCI a jen **zlepšuje výkon**
    - enumeratce a konfigurace zlstává stejné
- přidává dedukovaný **point-to-point** připojení
- víceropřenosu ořes clock (až 8, pro 2 GB/s)

## PCI Express
- aktuální vysoko-rychlostní pefirefní sběrnice pro PC
- staví / **rozšiřuje** PCI
- point-to-point, **sériová** data interconnect
- velice zlepšený **throuput** (up to ~30GB/s)

## USB: Universal Serial Bus
- primárně pro **externí** periferie
    - klávsnice, myš, tiskárny, ...
    - náhrada **legacy ports**
- pozdější revize povolují **hish-speed** přenosy
    - použitelné pro sorage devices, kamery, ...
- enumerace zařízení, schopnost **negotiation**

### USB Classes
- a set of **vendor-neutral** protocols
- HID = human-interface device
- mass storage = disk-like devices
- audio equipment
- printing

### Other USB uses
- ethernet adapters
- usb-serial adaptéry
- wifi adapters (dongles)
    - není zde universální protocol
    - akaždé USB WiFi adaptér potřebuje speciální driver
- bluetooth

## ARM Busses
- ARM je typicky použity v System-on-a-Chip design
- toto jsou **proprieraty** sběrnice připojené k periferiím
- je zde méně potřeby enumerace
    - celý systém je spojen v jeden chip
- periferie mohou být **pre-configured**

## USB and PCIe on ARM
- neither ISB nor PCIe jsou exkluzivní k PC platformě
- většina ARM SoC's podporují USB zařízení
    - pro pomelé a středně rychlé off-SoC zařízení
    - např. použito pro **ethernet** na RPi 1
- některé ARm SoC podporuí PCI Express
    - toto povuluje **vysokorychlostní** off-SoC periferie

## PCMCIA & PC Card
- PC = Personal Computer
- MC = Memory card
- IA = International Association
- **hotplug**-capable notebook **expanzivní** sběrnice
- použito pro paměťové karty, network adapters, modems
- přichází s vlastními drivery

## ExpressCard
- **expansion card** standard jako PCMCIA / PC card
- založeno na PCIe a USB
    - může převážně **znovu použít** ovladače k těmto standardům
- už není velmi používáno
    - poslední update 2009
    - společnost se rozpadla

## miniPCIe, mSATA, M.2
- jsou to **fyzické rozhraní**, né speciílní sběrnice
- poskytují nějaký mix PCie, SATA a USB
    - také další protokoly jako IC, SMBus, ...
- hlavně používáno v SSD a wireless
    - také GPS, NFC, bluetooth

# Grahics and GPUs
## Graphics cards
- ze začátku pouze zařízení k **ovládání diplejů**
- čte pixely z **paměti** a poskytuje **displej** signal
    - DAC with a clocl
    - paměť může být součástí graf. kart
- evolved **acceleration** capabilities

## Graphics accelerator
- povoluje běžné **operace** zhotověny v **hw**
- jako kreslený linek nebo vyplněných **polygonů**
- pixely jsou počítány přímo ve video RAM
- toto může **šetří** spoustu CPU času

## 3D Graphics
- rendering 3D scén je **početně náročené**
- CPU-based, **sw-only** renderování je možné
    - texture-less v brzkých leteckých simulátorech
    - bitmap textury od 95/96
- CAD worstatins měli 3D accelerátory (OpenGl 92)

## GPU (Graphical Processing Unit)
- s názvem přisla nVidia na konci 90. let
- původní důvod - postavení **hw renderu**
    - založeno na polygonal meshes and Z buffering
- postupně více **flexibilní** a **programovatelné**
- on-board RAM, vysokorychlostní připojení k systémové RAM

## GPU drivers
- rozděleno na několik komponentů
- grafický výstup / frame buffer přístup
- **memory management** je často v kernelu
- geometry, texutres, ... jsou předpřipraveny **v procesu**
- front end API: OpenGl, Direct3D, Vulkan, ...

## Shaders
- aktuální GPU jsou **počítací** zařízení
- GPU má vlastní machine code pro **shaders**
- GPU ovladač obsahuje **shader compiler**
    - buď přímo z vysoko úrovňové jazyky (HLSL = High-level shader language)
    - nebo začínáme s intermediate code (SPIR = Standard protable intermediate representation)

## Mode setting
- pracuje s nastavení **obrazovky** a **rozlišení**
- i podpora pro např. **více diplejů**
- často podporuje primitivní (SW-only) **framebuffer**
- often in-kernel, s minimální user-level podporou

## Graphics servers
- vícero programu nemůže všechny používát grafické karty
    - graf. hw musí být **sdílený**
    - jedna možnost je **grafický server**
- poskytuje IPC-based **drawing** a/nebo **windowing** API
- vykonává **painting** místo aplikací

## Compositors
- přímá cesta ke sdílení graf. karty
- každá aplikace dostane **vlastní buffer** to nakreslit
- kreslení je čsto uskutečněno pomocí (context switched) GPU
- individuátlní buffery jsou **composed** onto screen
    - composition is also hw-accelerated

## GP-GPU
- general-purpose GPu (CUDA, OpenCl, ...)
- používáno pro **výpočet** místo jen grafiky
- v podstatě vrázení vektorových procesorů
-blízké k CPUs ale není součístí normální OS scheduling

# Persistent sorage
## Drivers
- Rozděleno do adaptéru, sběrnice a ovaldače zařízení
- často je jeden ovladač k zařízení
    - alespon pro pevné disky a CD-ROM
- **enumerace** a **konfigurace** sběrnice
- adresování dat a **přenos dat**

## IDE / ATA
- Integrated Drive Electronics
    - disk controller je stává součástí disku
    - standardizování jako ATA-1 (AT Attachemnt ...)
- založeno na ISA sběrnici, ale s kabely
- pozděi přizpůsobeno pro non-disk použítí pomocí ATAPI

## ATA Enumeration
- každé ATA **rozhraní** může připojit jen 2 ovladače
    - ovladače jsou HW-konfigurovány jako master / slave
    - toto zapříčiňuje enumeraci velmi lehkou
- vícero ATA rozhraní bylo standardizováno
- není potřeba specifické HDD ovladače

## PIO vs DMA
- originálně IDE mohlo použít pouze **programmed** IO
- toto je stalo velkým **bottleneck**
- později revize obsahovali **DMA** módy
    - až do 160 MB/s s nejvyšším DMA módem

## SATA
- **sériový**, point-to-point náhrada za ATA
- hw-level incompatible k (paralelní) ATA
    - ale SATA zdědila **command set** od ATA
    - legacy mody používají PATA ovladače k SATA diskům
- how-swapable - změna disků v **běžícím systému**

### AHCI (Advanced Host Controller INterface)
- **vendor-neutral** rozhraní k SATA kontrolerům
    - v teorii je jen jediní AHCI ovaldač potřeba
- alternatva k legacy modu
- NCQ = Natice Command Queueing
    - umožňuje změnění pořadí požadavků
    - další vrstva IO scheduling

## ATA and SATA drivers
- host controller je převéžně vendor-neutral
- **ovladač sběrnice** exposes ATA command set
    - podporuje **command queueing**
- ovladače zařízení používají ovladač sběrnice ke komunikaci se zařízeními
- částečně znovu použité v SCSI ovladačích pro ATPI ...

## SCSi (Small Compter System Interface)
- originálně s minipočítači v 80. letech
- více komplikované a **schopné** jak ATA
    - ATAPI v podstatě uzavřelo SCSI přes ATA
- **enumerace** zaízení, včetně **agregace**
    - např. celé encluoures s mnoho drives
- také podpora CD-ROM, tapes, scanners

### SCSI drivers
- rozděleno do: host bus adapter (HBA) driver
- generic SCSI bus a command component
- a per-device nebo per-class ovladače
    - optické disky, tapes, CD/DVD-ROM
    - standardní disky a SSD

## iSCSI
- v podstatě SCSI přes TCP/IP
- úplně **sw-based**
- povoluje standardních cpočítačům sloužit jako **block storage**
- bere výhodu rchlého levného ethernetuu
- znovu používá mnoho **SCSI driver stack**

## NVMe: Non-Volatilve Memory Express
- docela jednoduchý protokol pro PCIe-připojení storage
- optimalizované pro SSD zařízení
    - velmi velké a větší **command queues** než AHCI
    - lepší / rycleší interrupt handling
- stresses **concurrency** v kernel block layer

## USB mass storage
- třída USB zarízení (jeden ovladač pro celou třídu)
- typicky USB **flash drives**, ale také externí disky
- USB 2 není vhodné pro vysokorychlostní uložistě
    - USB 3 introduces UAS = UASB-Attached SCSI

## Tape drives
- povoluje pouze **sekvenční** přístup
- potřebuje podporu pro media **ejection**, **rewinding**
- může být připojeno přes SCSI, SATA, USB
- části ovladače budou **bus-neutral**
- převážně pro **zálohu** dat, velikost 6-15TB

## Optical drives
- převážně používány jako **read-only** distribuční médium
- laser-facilitated čtení z rotačních disků
- zase můžeme být připojeno pomocí SCSI, SATA nebo USB
- používáno pro **audio plaback** &rarr; velmi pomalý seek

### Optical disk writes (burners)
- chová es více jako **tískarna** pro optické **disky**
- ovladače jsou často v **user space**
- připojeny jedním standardních **disk busses**
- **speciální programy** potřebují výpálit disky
    - alternativa: packet-writing driver

# Networking and Wireless
## Networking
- síť umožňuje **více počítačům** se vyměňovat **data**
    - mohou to být soubory, streamy nebo zprávy
- síťě jsou buď **kabelové** (wired) nebo **bezdrátové**
- zatím pouze **nejnižší vrsty**
- NIC = Network Interface Card

## Ethernet
- specifikuje **fyzické** médium a zároveň protokol
- **on-wire** format and **collision** resolution
- v moderních systémech nejčastěji **point-to-point** spojení
    - používání aktivního **packet switcing** zeřízení
- přenáší data v **rámcích** (frames - low-level packets)

## Addressing
- na této úrovni je pouze **lokální** adresace (nejvíce jediný LAN segment)
- používá vestavěné MAC adresy - unikátní adresa každé karty
    - MAC = Media Access Control
- adresace náleží **interfaces**, né pc

## Transmit queue
- **pakety** jsou sbírány z **paměti**
- OS **připravuje** pakety do transmit **queue**
- zařízení je přijímá **asynchronně**
- podobné jak fungují příkazy blokovým 

## Receive queue
- data jsou také **zařazena do fronty** v opačném směru
- NIC kopíruje pakety do **receive queue**
- toto vyvolá **interrupt**, který OS oznámí nové itemy
    - NIC může zpracovat vícero paket za interrupt
- jestli není fronta uvolněna rychle -->  **packet loss**

## Multi-queue adapter
- rychlé adaptéry mohou **saturate** CPU
    - např. 10GbE karty nebo více portová GbE
- tyto NICs ,ůže spravovat **více** RX a TX queues
    - každá fronta má vlastní interrupt
    - rozdílné fronty &rarr; pravděpodobně jiný **CPU core**

## Checksum and TCP offloading
- složitější adaptéry mohou **offload** některé vlastnosti
    - např. výpočet **checksums** důležitým paketům
- ale také TCP-related features
- potřebuje, jak podoporuju **driveru**, tak **RCP/IP** stack

## WiFi
- **wireless** network interface - "bezdrátový ethernet"
- **sdílené** médium - elektromagnetické vlny ve vzduchu
- (skoro) povinná **encryption**
    - jinak by byl velmi jednoduchý **eavesdrop** nebo aktivní **útok**
- velmi **komplexní** protocol (relativně vůči hw standardům)
    - assisted by **firmware** běžící na adaptéru
