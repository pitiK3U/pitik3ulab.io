+++
title = "05 - Processes, thread & scheduling"
date = 2021-04-02
+++

# Processes and virtual memory
## Prehistory: batch systems
- první pořítače běželi pouze s **jedním programem**
- programy byly rozplánovány **dopředu** (**ahead of time**)
- používání děrovaných štítků, počítače zabírali celé místonsti

## History: time sharing
- v 'mini' počítačích mohl programy **interaktivně**
- teletype **terminály**, obrazovky, klávesnice
- **více uživatelů** zároveň
- proto i **více programů** zároveň spustěných

## Processes: early view
- proces je **vykonávaný** program
- může být **vícero procesů**
- procesům patří mnohé **zdroje**
- každý proces patří dnaému uživateli

### Process resources
- **paměť** (address space)
- **processor** time
- otevřené soubory (file descriptors), také pracující adresář a síťové připojení

### Process memory segments
- program **test** - obsahuje instrukce
- data - statická a dynamická **data** s oddělenou read-only sekcí
- **stack** memory - vykonávaný zásobník - return adresy, automatické proměnné

### Process memory
- každý proces má vlastní **address space**
- procesy jsou navzájem **izolované**
- požaduje aby CPU mělo MMU
- implementováno pomocí **paging** (page tables)

### Process switching
- změna procesů znamená změna **page tables**
- fyzické adrase se **nemění**, 
- ale **mapping** virtuálních adres se mění
- velká část fyzické paměti **není mapována**
    - může být nealokované (nepoužité)
    - nebo patří **jiným procesům**

### Paging and TLB
- překlad adres je **pomalý**
- nedávno použité stránky jsou uloženy v TLB
    - zkratka pro *Translation Look-aside Buffer*
    - velmi rychlá **hw** cache
- TLB musí být **flushed** na process switch
    - toto je poměrně **drahé** (microseconds)

## Threads
- moderní jednotka CPU scheduling
- jednotlivé vlákna běží sekvenčně
- jeden proces může mít **více vláken**, taková vlákna sdílí jeden address space

### What is a thread?
- vlákna je **sekvence** instrukcí - instrukce závisí na výsledku předchozích instrukcí
- různá vlákna spouští různé instrukce
    - as opposed to SIMD or many-core units (GPUs)
- každé vlákno má vlastní **zásobník**

## Processor time sharing
- čas CPU je rozdelěn do **time shares**
- time shares (slices) jsou jako paměťové **rámce** (memory frames)
- process **computation** je jako paměťové **stránky** (memory pages)
- procesy jsou alokované do **time shares**

## Multiple CPUs
- vykonání vláken je **sekvenční**
- jeden CPU = jedna **instrukční sekvence** za čas
- fyzický limit na rychlost CPU &rarr; **more cores**
- více CPU jader = větší throughput (propustnost)

## Modern view of process
- v moderním pohledu jsou procesy **address space**
- vlánka jsou správná **scheduling abstraction**
<!-- markdown cheats -->
- **process**je jednotka **správy paměti** (**memory management**)
- **vlákno** je jednotka **výpočtu** (**computation**)
- starý pohled: jeden proces = jedno vlákno

## Memory segment redux
- jeden (sdílený) **text** segment
- sdílený read-write **data** segment
- read-only **data** segment
- jeden **zásobník** pro **každé vlákno**

## Fork
- Jak se tvoří **nové procesy**?
- pomocí `fork`-ování existujících procesů
- fork vytvoří **identickou kopii** existujícího procesu
- vykonávání pokračuje v obou procesech
    - každý dostane rozdílnou **return** hodnotu

## Lazy fork
- stránkování může `fork` udělat velmi efektivním
- začneme zkopírováním **page table**
- ze začátku jsou všechny stránky označené jako **read-only**
- procesy začínají pameť **sdílet**

### Faults
- sdílená paměť se stane **copy on write**
- **fault** když nějaký proces se pokusí zapsat (pameť byla označena read-only)
- OS ověří, jestli paměť **má být** zapisovatelná, jestli ano, vytvoří **kopii** a povolí zápis

## Init
- na UNIXu, `fork` je jediný způsob, jak vytvořit proces, ale `fork` rozdělí existující proces do 2
- **první proces** je speciální, je přímo vyvolán kernelem při bootu

## Process indentifier
- procesům jsou přiřazeny **číselné identifikátory** - známe jako PID (Process ID)
- tyto čísla jsou používány v **process management**
- použití calls jako `kill` nebo `setpriority`

## Process vs executable
- proces je **dynamická** entita
- spustitelný soubor je **statický** soubor
- executable obsahuje začínající **memory image**
    - tota nsataví memory layout
    - a content **text** a **data** segmentů

## Exec
- na UNIXu procesu jsou tvořeny pomocí `fork`
- Jak se tedy **spouští programy**?
- `exec`: načte novou **executable** do procesu
    - toto kompletně **přepíše** paměť procesu
    - execution začíná z **entry point**
- spouštení programů: `fork` + `exec`

# Thread Scheduling
## What is a scheduler?
- scheduler má dvě přidělené práce
    - **plánování** kdy které vlánko má běžet
    - **switch** vláken a procesů
- normálně část **kernelu** (i u micro-kernelů OS)

## Switching threads
- vlákna **stejného procesu** sdílí address space
    - **částečný** context switch je potřeba
    - jen **stav registru** musí být ulože na uchován
- no TLB flushing - zmenšení **overhead**

## Fixed vs dynamic schedule
- **fixní** schedule = všechny procesy jsou známé **napřed** (in advance)
    - užitečné pouze ve speciálních / embedded systémech
    - může **conserve resources**
    - plánoavání není součástí OS
- většina systémů používá **dynamic scheduling**, co běží dál je **rozdhodnuto periodicky**

## Preemptive scheduling
- úkoly (vlákna) běží jako když jsou vlastněny CPU
- OS jim násilně **CPU odebere**
    - toto se nazávý **preemption**
- pro: šatný progam **nemůže zablokovat** systém
- tak nějak **méně efektivní** než cooperative

## Cooperative scheduling
- vlákna **cooperate** ke sdílení CPU
- každé vlánka musí explicitně **yield** CPU (vzdát se CPU dobrovolně)
- toto může být **velmi efektivní**když nadesignováno dobře
- ale **špatný program** může **zablokovat systém**

## Scheduling in practice
- cooperative na Windows 3.X pro všechno
- **cooperative** pro **vlákna** na klasickém macOS
    - ale **preemptive** pro **procesy**
- **preemptive** na snad každém moderním OS (včetně real-time i embedded systémech)

## Waiting and yielding
- vlákna často musí **čekat** na zdroje nebo **události** (mohou také používat sw timers)
- čekájící vlákno by **nemělo konzumovat** CPU time
- taková vlánko se CPU vzdá (**yield** the CPU)
- a je zapsáno na list a později **probuzeno** kernelem

## Run queues
- **runnable** (non-waiting) vlákno jsou řazeny do **fronty**
- mohou být **prioritní**, round-robin nebo jiné typy front
- scheduler **vybere** vlákno z run queue
- **preepmted** threads are put back

## Priorities
- jaký **díl** CPU by mělo vlákno dostat?
- **priority** jsou statické i dynamické
- **dynamické** priority jsou přizpůsobeny za běhu vláken (toto dělá systém/scheduler)
- **statická** priorita je přidána **uživatelem**

## Fairness
- **equal** (nebo priority-based) díl per **thread**
- Co když má proces **více vláken**?
- Co když má uživatel **více procesů**?
- Co když jedna skupina uživatelů má **více aktivních uživatelů**?

## Fair share scheduling
- můžeme použít **více-úrovňové** scheduling scheme
- CPU je rozděleno podobně přes **uživatelské skuupiny**
- poté přes **uživatele**
- následně přes **procesy**
- a nakonec přes **vlákna**

## Scheduling strategies
- first in, first served (batch systems)
- earliest **deadline** first (realtime)
- round robin
- **fixed priority** preepmtive
- **fair share** scheduling (multi-user)

## Interactivity
- **throughput** vs **latency**
- **latency** je více důležité pro **interaktivní** práci (například teledon, desktop nebo také web servery)
- **throughput** je více důležité pro **batch** systémy
    - render farms, compute grids, simulation, ...

## Reducing latency
- **kratší** time slices
- větší odhodlanost task switching (více **preemption**)
- **dynamické** priority
- prioritní zvýšení pro procesy **v popředí**

## Maximasing throuput
- **delší** time slices
- **reduce context switches** na minimum
- cooperative multitasking

## Multi-core schedulers
- tradičně jedno CPU, více vláken
- dneska: mnoho vláken, mnoho CPUs (jader)
- více komplikované **algoritmy**
- více komplikované a **concurrent-safe** data structures

## Scheduling and caches
- vlákna se mohou **přemisťovať** (move) mezi CPU **jádry**
    - důležité když jsou různá **jádra idle** a runnable thread **čeká na CPU**
- ale to něco **stojí**
    - vlákna/data procesů jsou hodně **cachována**
    - caches typicky **nejsou sdílené** všemi jádry

## Cora affinity
- moderní schedulers se snaží **vyhnout přesouváním vláken**
- vlákna jsou **affinity** to a core
- extrémní případ je **pinning**
    - toto zabraňuje vlánka být **migrated**
- prakticky toto **zlepšuje throughput**, přestože nomal core **utilisation** může být nižší

## Numa systémy
- **non-uniform memory** architecture
    - jiná paměť je přiřazená jiném CPUs
    - každá SMP block je v NUMA se nazývá **node**
- **migrating** procesu do **jiného node** je drahé
    - thread vs node ping-pong může zabít výkon
    - vlákna **jednoho procesu** by měli žít na jednom node

# Interrupt and Clocks
## Interrupt
- způsob jak si hw **žádá o poroznost**
- CPU **mechanismus** jak ?
- částečný (CPU state only) **context switch**
- změna na **privilegovaný** (kernel) CPU mód

## Hardware interrupts
- **asynchronní**, narozdíl od sw interruptům
- spuštěň přes **signál na sběrnici** do CPU (**bus signal**)
- `IRQ` = interrupt request (jiný název pro hw int)
- `PIC` = programovatelný **interrupt controler**

## Interrupt constrollers
- `PIC`: **jednoduchý obvod**
    - typicky 8 vstupních linek
    - periferní připojení k PIC pomocí drátů
    - PIC přináší prioritizované singály do CPU
- `APIC`: advanced programmable int controller
    - rozděleno do sdílených **IO APIC** a per-core **local APIC**
    - typicky 24 vstupních **IRQ linek**
- OpenPic, MPIC: podobný APIC, používán Freescale

## Timekeeping
- `PIT`: **programmable interval timer
    - crystalový oscilátor + dělitel
    - **IRQ linka** do CPU
- local APIX timer: vestavěný, **per-core clock**
- `HPET`: high-precision event timer
- `RTC`: real-time clock

## Timer interrupt
- generován PIT nebo lokálním APIC
- OS může **nastavit frekvenci**
- hw int se uskuteční každý **tick**
- toto vytváří možnosti pro bookkeeping a **preemptive scheduling**

## Timer interrupt and scheduling
- měření jak dlouho to aktuálnímu vláknu trvalo
- jestli došel ze svých slice, **preepmt it**
    - **vyber** nové **vlákno** na vykonávání
    - uskutečni context switch
- kontrole ja provedena každým tickem
    - **rescheduling** je méně časté

## Timer interrupt frequency
- typicky je 100 Hz
- to znamená, že 10ms je **scheduling tick** (quantum) - nejmenší time slice
- 1kHz je také možný
    - ale škodí throughput, ale **lepšují latency**

## Tickless kernels
- timer interrupt **wakes up** the CPU
- toto může být **neefektivní** jestli je systém **idle**
- alternativa: použití **us-off** timers
    - umožňuje CPU **spát delší dobou**
    - toto **zlepšuje pwer efficiency** při slabém využití

### Tickless scheduling
- **quantum length** se stane část plánování
- jádro je udle, probudí se při dalším **sw timer** - synchronizace sw časovačů
- ostatní int jsou **deliverd as normal**
    - síť nebo atkivita disku
    - klávesnice, myš, ..

## Other interrupts
- seriový port - **data jsou dostupná** na portu
- **síťový** hw - data jsou připravené v paket frontě
- klávesnice, myš
- USB zařízení obecně

## Interrupt routing
- né všechna CPU jádra vidí všechny int
- APIC může dostat jak bude IRQs doručeno - OS může **přesměrovat** do CPU jader
- multi-core ystems: IRQ **load balancing**
    - užitečné pro **rozložení** IRQ zátěže
    - speciálně užitečné pro **vysoko rychlostní sítě**