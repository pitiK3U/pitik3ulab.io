+++
title = "01 - Anatomie Operačních systémů"
date = 2021-03-05
+++

# Operační systém
- **Software**, který ovládá hardware
- usnadňuje psaní software
- **abstrakce** nad "železem"
- catch-all phrase for low-level software
- hranice nejsou přesně dané

## Co OS není
- firmware: velmi nízkoúrovňoví software
	- **hardware-specific**
	- often executes on auxiliary processors
- aplikační software
	- běží na OS

## Co OS dělá
- **iteraguje** s uživatelem
- **správa** hardware
- **správa jiného software**
- **organizuje** a spravuje **data**
- **služby** pro jiné programy
- vynucuje **bezpečnost**

# Komponenty
## Složení
- the kernel (jádro)
- system libraries
- system daemons / services
- user interface
- system utilities

**Každý OS** má tyto komponenty.

## The Kernel (jádro)
- **nejnižší úroveň** operačního systému
- pracuje v **privilegovaném módu**
- spravuje ostatní software (včetně OS komponent)
- vynucuje **izolaci a bezpečnost**
- nabízí programům **nízkoúrovňové služby**

## System libraries
- vrstva nad kernelem
- nabízí **vysoko úrovňové** služby
	- používá kernelové služby na pozadí
	- **jednodušší k použití** než kernel interface
- například **libc**

## System Daemons
- programy běžící na **pozadí** (**in the background**)
- buď nabízí **přimo služby**
- nebo vykonává **údržbu** (**maintance**)
- periodic **tasks**
- nebo vykonává úkoly **požádané kernelem**

## User interface
- **interakce** mezi uživatelem a počítačem
- hlavní **shel** je typicky součástí OS
	- command line on UNIX or DOS
	- grafická rozhraní na desktopech a windows
	- také tlačítka na mikrovlnce
- **building blocks** pro UI aplikace
	- tlačítka, záložky, text rendering, OpenGL, ...
	- poskytováno sytémovými knihovnami nebo/a daemony

## System utilities
- malé programy potřebné pro OS-related tasks
- konfigurace systému
- údržba souborového systému, daemon management, ...
	- programy jako `ls`/`dir`, `newfs`, `fdisk`
- programy jako file managers

## Optional components
- bundled **application** software (comes out of the box; webový prohlížeč, přehrávač medií, ...)
- (3rd-party) **software management**
- a **programming** environment
	- a C compiler & linker
	- C header files &c.
- source code

# Interfaces
## Programming Interface
- kernel nabízí **system calls**
	- **ABI**: Application Binary Interface
	- defined in terms of **machine instructions**
- systémové knihovny nabízí API
	- Application Programming Interface
	- **vysoko úrovňové** interfaces
	- typicky definoavé pomocí **C funkcí**
	- system calls jsou dostupné i jako API

## Message passing
- ne vždy jsou APIs jako C funkce
- message-passing interfaces jsou
	- založeno na **inter-process communication**
	- možná komunikace i přes **počítačovou síť**
- API poskytováné **systémovými daemony** může být i "wrapped" C API
- `syslogd`

## Portability
- některé úkoly OS požadují **HW cooperation**
	- **virtual memory** and CPU setup
	- platform-specific **device drivers**
- but many do not
	- **scheduling** algorithms
	- memory **allocation**
	- all sorts of management
- porting: změna programu, aby běžel v **novém prostředí**
	- u OS je to typicky nový hardware

## Hardware Platform
- CPU **instruction set** (ISA)
- buses, IO controlles
	- PCI, USB, Ethernet
- **firmware**, power management

## Platform & Architecture portability
- OS podporuje mnoho **platform**
	- Android na různých ARM SoC's
- často také rozdílné **CPU ISAs**
	- dlouhodobá tradice v UNIX-like systémech
	- NetBSD runs 15 různých ISA
- special-purpose systemy jsou méně portable

## Code Re-Use
- it makes a lot of sense to re-use code
- většina OS kódu je **HW independent**
 - dříve byly OS psané v machine language, porting == rewriting again

## Application Portability
- pro aplikace je důležitější OS než HW
	- apps jsou psané v high-level jazycích a používají systémové knihovny
- k portování aplikací většinou stačí program recompile
- některé aplikace mohou fungovat stejně na různých OS (POSIX family)
- některé aplikace mohou běžet na Windows, macOS, UNIX, ...
	- Java, Qt (C++)
	- webové aplikace
- mnoho systémů používá stejné služby
	- rozdíl je v interfaces, které často high-level knihovny abstrahují 

## Abstraction
- **instruction set** sabstract over CPU details
- **compilers** abstract over **instruction sets**
- **operating systems** abstract over **hardware**
- portable runtimes abstract over operating systems
- applications sit on top of the abstractions

### Abstraction costs
- more complexity
- less efficiency
- leaky abstractions

### Abstraction benefits
- easier to write and port software
- fewer constraints on HW evolution

### Abstraction trade-offs
- powerful hardware allows more abstraction
- embedded or real-time systems not so much
	- the OS is smaller & less portable
	- same for applications
	- more efficient use of resources

# Classification
## General-purpose OS
- vhodné pro většinu situací
- **flexibilní**, ale **komplexní** a rozsáhlé
- běží, jak na **serveru** tak na **clientu**
- zmenšené verze běží na **smartphonech**
- podporuje velkou řadu hardware
- Př.: MS Windows, macOs, iOS, Android, Linux, ...

## Special-purpose OS
- **embedded** systémy
	- limited budget
	- **malé**, pomalé, power-constrained
	- těžko až skoro nemožné aktualizovat
- **real-time** systémy
	- musí **reagovat** na real-world události
	- často **safety-critical**
	- roboti, autonomní auta, ...

## Size and Complexity
- OS jsou typicky velké a komplexní
- často mají více jak **100K** řádků
- často až **10+ milionu**
- many thousand man-years of work
- special-purpose systémy jsou mnohonásobně menší

## Kernel revisited
- chybu (bugs) v kernelu jsou velmi špatné
	- zapříčiňují systém crashes, ztrátu dat
	- **critical** security problems
- bigger kernels means more bugs
- third-party drivers inside the kernel?

### Monolithic Kernels
- lot of code in the kernel
- less abstraction, less isolation
- **faster** and more efficient

### Microkernels
- move as much as possible out of kernel
- more abstration, more **isolation**
- slower and less efficient

####
- real-time & embedded systems often use microkernels
- isolation is good for reliability
- efficiency also depends on the **workload**
	- throughput vs latency
- real-time does not necessarily mean fast
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTcyMTQwNjY1MiwtMjA5NzM1NTkxMCwyMD
AyNTA5NjcsNDA5MzY5NTY5LC0xNjc4MDk5MTYxLDEyMDkxNDY3
MTYsLTE5OTM2Nzc0OTAsLTE3OTg2OTk3NzBdfQ==
-->