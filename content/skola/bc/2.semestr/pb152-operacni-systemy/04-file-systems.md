+++
title = "04 - File systems"
date = 2021-03-26
+++

# Filesystem basics
- (převážně) **hierarchická** sbírka **souborů** a **adresářů**
- přístupné uživatelům
- normálně **persistent** (zachovává se přes restart)

## Soubor
- sekvence **bajtů**
- a nějaká **metedata** (vlastník, group, timestamp)
- OS **nezajímá** obsah
    - text, obrázky, videa, zdrojový kód jsou všechny stejné
    - jen spustitelné soubory jsou jiné

## Adresář
- struktura - ke **jménům** přiřazuje i-uzly
- podobné kontejnerům
    - semanticky nejsou hodnoty homogenní
    - syntakticky jsou pouze **i-nodes**
- jeden adresář = jedna komponenta cesty
    - `/usr/local/bin`

## i-node
- **anonymní**, file-like object
- může být:
    - **regulární soubor**
    - adresář
    - speciální soubor
    - symlink

## Files are anonymous
- Takto to funguje v UNIXu (né všechny filesystémy takto fungují)
- jsou zde výhody i nevýhody - otevřené soubory mohou být **unlinked**
- názvy jsou přiřazeny pomocí **directory entries**

## Byte sequence
- znaky přicházející z **klávesnice**
- bajty uložené na magnetické **pásce**
- audio data z **mikrofonu**
- data přicházející z **TCP connection**

### Writing byte sequences
- posílání dat do **tiskárny**
- přehrávání **audia**
- psaní texto do **terminálu** (emulátoru)
- posílání dat přes **TCP stream**

## Speciální soubory
- mnoho věcí je chová podobně jako **soubory**
- využití této vlastnosti a spojení se soubory
- **API** stejná pro speciální a obyčejné soubory 
- ale **různá** implmentace

## File system types
- fat16, fat32, vfat, exfat (DOS, flash media)
- ISO 9660 (CD-ROMs)
- UDF (DVD-ROM)
- NTFS (Windows NT)
- HFS+ (macOS)
- ext2, ext3, ext4 (Linux)
- ufs, ffs (BSD)

## Multi-user systems
- vlastnictví souborů
- přístupová práva souborů
- disk quotas

### Ownership & permissions
- **discretionary** model - vlastník rozhoduje o tom, co se se soubory bude dít
- vlastnistcví může být předáno
- vlastník rozhoduje o přístupových právech (kdo může číst, zapisovat, spouštět)

### Disk quotas
- disky jsou velké, ale né **nekonečné**
- naplnění souborového systému zapříčiňují různé problémy:
    - denial of service
    - programy mohou failnout nebo porušit data
- **quotas** jsou omezení místa pro uživatele

# The block layer
## Disk-like devices
- diskové uložiště poskytují **block-level** access
- čtení a zapisování dat v 512-bajtových prvcích (na moderních zařízení 4K)
- a big numbered **array of blocks**

### Aside: Disk adressing Schemes
- CHS: Cylinder, Head, Sector
    - **strukturovaná** adresace je použita ve velmi starých discích
    - informace ohledně relative seek times
    - zbytečné pro cylindry proměnné délky
    - 10:4:6 CHS = 1024 cylindrů, 16 hlaviček, 63 sektorů
- LBA: Logical Block Adressing
    - **lineární**, nestrukturovaná adresace
    - začalo jako 22 bit, později 28, ... až aktuálně 48 bitová

## Block-level access
- moderní disky se chovají jako **lineární adresní prostor**
- **nejmenší jednotka** zapsání/čtení je jeden sektor (block)
- mnoho sektoru bůže být zapsáno "naráz":
    - **sekvenční** přístup je rychlejší než náhodný
    - maximální **throughput** vs **IOPS**

### Aside: Access Times
- blokové zařízení jsou **pomalé** (v porovnání s RAM)
    - RAM je **pomalá** (v porovnání k CPU)
- nemůžeme brát disky jako RAM
    - latency: HDD 3-12 ms, SSD 0.1 ms, RAM 70ns

## Block access cache
- **caching** se používá na schování latency (stejné u CPU a RAM)
- soubory, které byly nedávno použity, jsou uloženy v RAM
    - existuje mnoho **cache management** policies 
- celé implementováno v OS
    - mnoho zařízení implementují vlastní caching
    - ale množství rychlé paměti je velice limitováno

## Write buffers
- **zápis** ekvivaletní blokové cache
- data jsou uchovávana v RAMce než budou uloženy na disk
- musí se synchronizovat s caching (ostatní uživatelé mohou číst ten soubor)

## I/O scheduler (elevator)
- uživatelé požadují čtení a zápisy
- pořadí přístupů je velmi důležité na mechanickém disku (na SSD né tolik)
    - sekveční čtení je **velmi preferováno**
- požadavky jsou ve **frontě**
    - ale **nejsou** ve **FIFO** pořadí 

## RAID
- pevné disky jsou **unreliable** (zálohy pomáhají, ale trvají velmi **dlouho obnovit**)
- RAID = Redundant Array of Inexpensive Disks
    - **živě** replikovaná stejná data přes více zařízení
    - různé konfigurace
- systém **zůstavá online** přes selhání disků

### RAID performance
- RAID ovlivňuje výkon blokové vrstvy
- často **zlepšuje čtecí** throughput
    - data jsou kombinací z více kanálů
- zapisovací výkon je **různý**
    - může potřebovat velké množství výpočtů
    - potřeba zapsat **víc dat** pro **redundancy**

## Block-level encryption
- **symetrická** a zachovává délku
- encryption key is derived from **passphrase**
- taky známé jako "full disk encryption"
- zapříčiňuje malou **výkonnostní cenu**
- velmi důležité pro **bezpečnost** / privacy

## Storing data in blocks
- rozdělování dat do fixně velikých chunks je nepřirozené
- neexistuje systém na přistupová práva jednotlivých bloků
    - to je rozdílné od **virtual (paged) memory**
    - bylo by to velmi nepřijemné pro uživatele
- procesy nejsou stálé, ale **block storage** je

## Filesystem as resource sharing
- normálně je jenom 1 nebo pár disků na počítač
- mnoho programů chce ukládat **stálá** (persistent) data
- souborový systém **alokuje místo** pro data
    - který blok patří kterému souboru
- různé programy mohou zapisovat do různých souborů (není risk zkusit **použít stejný blok**)

## Filesystem as abstraction
- umožňuje data **organizovat** do souborů
- umožňuje uživateli **spravovat** a měnit data
- soubory mají arbitraty & **dynamic size**
    - bloky jsou **transparentně** alokovány a recyklovány
- **struckturovaná** data místo flat block array

# Virtual filesystem switch
## Virtual file system layer
- mnoho různých souborových systémů
- OS se chce ke **všem chovat stejně**
- VFS poskytuje interní **in-kernel API**
- **System calls** souborových systému jsou hooked up to VFS

### VFS in OOP terms
- VFS poskytuje abtraktní třídu `filesystem`
- každý filesystem implementuje derivát `filesystem`
    - např. `class iso9660 : public filesystem`
- každý souborový systém dostance instanci
    - `/home`, `/usr`, `/mnt/usbflash`
    - kernel používá abstraktní interface ke komunikaci

### The `filesystem` class
```c
struct handle { /* ... */ };
struct filesystem
{
    virtual int open( const char *path ) = 0;
    virtual int read( handle file, ... ) = 0;
    /* ... */
}
```

## Filesystem-specific operations
- `open`: **najdi** soubor pro přístup
- `read`,`write`
- `seek`: posuň štecí/zapisovací pointer
- `sunc`: flush data na disk
- `mmap`: memory-mapped IO
- `select`: IO readiness notfication

## Standard IO
- **normální** cesta k používání souborů
- otevření souboru
    - oprace ke čtení a zapsání **bajtů**
- data jsou **buffered** in user space
    - a **kopírované** z/do kernelu
- není to velmi efektivní

## Memory-mapped IO
- používá **virtuální paměť**
- chovej se k souboru **jako by to byl** swap space
- soubor je **namapován** do paměti procesu
    - **page faults** indikuje, že data musí být znovu přečteny
    - **dirty** pages zapříčinují zápisy
- dostupné jako `mmap` sys call

## Sync-ing data
- disk je velmi **pomalý**
- čekat na každý zápis k dosažeí disku je inefficient
- když data jsou uložena v RAM, co se stane při **výpadku paměti**
    - `sync` operace zajišťuje, že data dosáhli na disk
    - často použivané v implementací databází

## Filesystem-agnostic operations
- načítání programů (handling **executables**)
- `fcntl` handling
- speciální soubory
- správa **file descriptors** 
- file **lock**

### Executables
- **memory mapped** (like `mmap`)
- může být načteno líně (**lazily**)
- spustielné soubory musí být **immutable while running**
- ale stále mohou bát unlinked z adresáře

### File locking
- více progarmů zároveň píšících do stejného souboru je špatné
    - operace budou v **náhodném** pořadí
    - výsledný soubor bude nepořádek
- file locks řeší tento problém
    - APIs: `fcntl` vs `flock`
    - rozdílné na networked filesystems

### The `fcntl` syscall
- normálně operace související s **file descriptors**
    - **synchronní** vs **asynchronní** přístup
    - blockng vs non-blocking
    - close on exec
- jeden z mnoho **locking** APIs

### Special files
- zařízení, pipes, sockets, ...
- jen **metadata** pro speciální soubory jsou na disku
    - tota obsahuje **přístupová práva** a vlastnictví
    - typ a **vlastnosti** speciálních souborů
- jsou pouze rozdílné druhy **i-node**
- `open`, `read`,`write`, ... obchází filesystem

## Mount points
- je zde pouze **jeden stromový adresář**
- ale je zde **vícero disků** a souborových systémů
- souborové systémy mohou být **spojeny** do adresářů
- **root** jednoho se stane **subadresářem** jiného

# The UNIX filesystem
## Superblock
- obsahuje **toplevel** informace o souborovém systému
- lokace i-node tabulek
- lokace i-nodes a **volného místa** bitmap
- **block size**, velikost souborového systému

## I-Nodes
- i-node je **anonymní soubor** (nebo adresář nebo speciální soubor)
- jsou **očíslované**
- **adresáře** spojují jména k i-nodes

### I-Node allocation
- často **fixní počet** i-uzlů
- i-uzly jsou buď **použity nebo volné**
- volné i-uzly jsou uloženy v **bitmap**
- alternativy: B-stromy

### I-node content
- přesný obsah záleží na typu i-uzlu
- běžné soubory i-uzlů obsahují list **datových bloků** (přímých i nepřímých)
- symolické **odkazy** obsahuje cílovou **cestu**
- speciální soubory **popisují** které **zařízení** reprezentují

### Attaching data to I-Nodes
- několik **přímých** blok adres v i-uzlu (1é refs, 4K bloky, max. 40 kilobajtů)
- **nepřímé** data bloky
    - blok plný adres jiných bloků
    - jeden nepřímý blok je přibližně 2MiB data
- **extents**: a contiguous range of blocks

## Fragmentation
- **vnitřní** - ne všechny bloky jsou plně použity
    - soubory jsou různé délky, bloky jsou fixní
    - 4100 batjů souboru potřebue 2 bloky 4KiB
    - toto zapříčiňuje **waste** of disk **space**
- **externí** - volý prostor je non-contiguous
    - stane se když více souborů se zvětší zároveň
    - toto zapříčiňuje **fragmentaci nových souborů**

### External fragmentation problems
- **výkon**: nemožnost použít rychlé sekvenčního IO
    - programy často ččtou souboru sekvenčně
    - fragmentace &rarr; random IO on the device
- velikost metadat: nemožnost použití dlouhých extents

## Directories
- používá **data bloky** (jako obyčejné souboru)
- bloky obsahují **jméno &rarr; i-uzel mapy**
- často se používá **hashování** nebo **stromy**
- formát je **filesystem-specific**

### File name lookup
- často potřebujeme najít soubor vzhledem k cestě
- každý komponent znamená hledání v adresáři
- adresáře mohou mít velmi hodně souborů

### Old-style directories
- neseřazené sekvenční listy položek
- nové položky vloženy nakonec
- unlinking vytváří díry
- hledání je neefektivní

### Hash-based directories
- potřebuje pouze jeden blok čtení v **průměru**
- často nejefektivnější způsob
- **extendible** hashing
    - adresáře mohou růst v průběhu
    - gradually allocates more blocks

### Tree-Based directories
- samo-balancující vyhledávací stromy
- optimalizované pro block-level access
- **B trees**, B+ trees, B* trees
- logaritmický počet čtení (nejhorší případ)

## Hard links
- **vícero jsem** může odkazovat na **stejný i-uzel**
    - jména jsou dány **adresářovým položkám**
    - takováto více jmenné soubory jsou **hard links**
    - je zakázano hard-linkování adresářů - tvorba cyklů ve stromu
- nemohou být na různých zařzení (i-uzly jsou unikátní jen na daných zařízeních)

## Soft link (symlinks)
- existují kvůli limitaci na jedno zařízení
- povoleno symlink na adresáře (může způsobit **loops**)
- soft link i-uzel obsahuje **cestu** (může se změnit, když se změní cesta)
- **dangling** link: ukazuje na neexistující cestu

## Free space
- podobný problém k i-node alokaci
- cíl: **rychlé** určení datových bloků k použití
    - zanechání dat jednoho souboru **blízko sobě**
    - **minimalizace** externí **fragmetace**
- často bitmapy nebo B-stromy

## File system consistency
- co se stane **při výpadku proudu**?
- data buffered in RAM jsou **ztraceny**
- IO scheduler může **re-order** zápisy disků
- souborový systém může být **poškozen**

## Journalling
- také známo jako **intent log**
- zapisuje, co se mělo stát **synchronně**
- fixnutí metadat na základě journalu
- má **vykonostní penalty** na run-time
    - **reduces downtime** kvůli rychlejším consistency checks
    - mohou **zabránit ztrátu dat**

# Advanced features
## What else can filesystems do?
- transparentní souborová komprese
- souborová enkrypce 
- bloková de-duplikace
- snapshoty
- checksums
- redundant storage

## File compression
- používá jeden ze standardních compresních algoritmů
    - musí být **general-pupose** (tk. **ne** JPEG)
    - a také **lossless**
    - např. `LZ77`,`LZW`,Huffman Coding, ...
- poměrně **náročné na implementaci**
    - délka souboru se může nečekaně měnit
    - efektivní **random access** do souboru

## File encryption
- používá **symetrické** šifrování pro jednotlivé soubory
    - musí být **transparentní** pro vyšší vrstvy (aplikace) 
    - symetrické krypto zachovává délku
    - **zašifrované adresáře**, ingeritance, ...
- nové ?
    - **správa klíčů** a hesel

## Block de-duplication
- občas stejné **datové bloky** se objevují **mnohokrát**
    - např. obrazy virtuálních mašin
    - také **kontejnery** a tak ...
- některé souborové systémy
    - vnitřně ukazují mnoho souborů na **stejný blok**
    - **copy on write** pro zachování iluze samostatných souborů

## Snapshots
- je convinient to be able to **copy** enire filesystems
    - ale je to i **drahé**
    - snapshoty nabízí **efektivní** řešení
- snapshot je **zmražený obraz** souborového systému
    - levné, protože sdílí úložistě
    - jednodušší než de-duplikace
    - znovu implementováno jako **copy-on-write**

## Checksums
- HW je **unreliable**
    - jednotlivé bajty nebo sektory mohou být **poškozeny**
    - může nastat bez hw povšimnutí
- **checksums** mohou být ukládány zároveň s metaday
    - a také i **obsahem souboru**
    - zachovává integrity of filesystem
- ale: **není** kryptograficky bezpečné

## Redundant storage
- jako filesystem-level RAID
- data a metada bloků jsou **replikované**
    - může být mezi více lokálních bloků zařízení
    - ale taký přes a **cluster** / více počítačů
- drasctically improves **fault tolerance**
