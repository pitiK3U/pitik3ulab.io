+++
title = "00 - Questions"
date = 2021-03-04
+++

#### 01
1. What are the roles of an operating system?
2. What are the basic components of an OS?
3. What is an operating system kernel?
4. What are API and ABI?

#### 02
5. What is a shared (dynamic) library?
6. What does a linker do?
7. What is a symbol in an object file?
8. What is a file descriptor?

#### 03
9. What CPU modes are there and how are they used?
10. What is the memory management unit?
11. What is a microkernel?
12. What is a system call?

#### 04
13. What is a block device?
14. What is an IO scheduler?
15. What does memory-mapped IO mean?
16. What is an i-node?

#### 05
17. What is a thread and a process?
18. What is a (thread,process) scheduler?
19. What do `fork` and `exec` do?
20. What is an interrupt?

#### 06
21. What is a mutex?
22. What is a deadlock?
23. What are the conditions for a deadlock to form?
24. What is a race condition?

#### 07
25. What is memory-mapped IO and DMA?
26. What is system bus?
27. What is a graphic accelerator?
28. What is a NIC receive queue?

#### 08
29. What is ARP (Address Resolution Protocol)?
30. What is IP (Internet Protocol)?
31. What is TCP (Transmission Control Protocol)?
32. What is DNS (Domain Name Service)?

#### 09
33. What is a shell?
34. What does variable substitution mean?
35. What is an enviroment variable?
36. What belongs into the GUI stack?

#### 10
37. What is a user?
38. What is the principle of least privelege?
39. What is an access control object?
40. What is a sandbox?

#### 11
41. What is a hypervisor?
42. What is paravirtualisation?
43. How are VMs suspended and migrated?
44. What is a container?