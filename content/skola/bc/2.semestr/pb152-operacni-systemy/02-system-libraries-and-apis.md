+++
title = "02 - System Libraries and APIs"
date = 2021-03-12
+++

# Systémové knihovny a APIs
## Programovací rozhraní
- kernelové **system call** rozhraní
- **systémové knihovny** / APIs
- inter-process protocols
- command-line utilities (scripting)

## Programovací jazyk C
- "nejnižší programovací jazyk" (až na assembly)
- může být bráno jako **portable assembly**
- C funkce a datové struktury se volají velmi jednoduše

### The language of operating systems
- velké množství (většina) kernelů jsou v **C**
- i systémové knihovny často v C
- někdy je skoro celý OS v C
- OS, které nejsou v C, poskytují **C APIs**

## Systémové knihovny
- převážně **C funkce** a **datové typy**
- rozhraní jsou definované v **header files**
- definice jsou v **libraries** (knihovnách)
	- *statické* knihovny (archives): **`libc.a`**
	- *shared* (dynamic) libraries: **`libc.so`**
	- ve Windows: **`msvcrt.lib`** a **`msvcrt.dll`**
- a mnohé další knihovny

### Declaration: **what** but not how
```c
int sum( int a, int b );
```

### Definition: **how** is the operation done?
```c
int sum( int a, int b )
{
	return a + b;
}
```

### Library Files
- `/usr/lib` on most UNIX 
	- mohou zde být i **aplikační knihovny**
	- také `/usr/local/lib` pro uživatelské/aplikační knihovny
- na Windows `C:\Windows\System32`
	- uživatelské knihovny jsou **bundled** s programy

### Statické knihovny
- uchované v `libfile.a` nebo `file.lib`
- potřebné pouze při a pro **kompilaci** (linking) programu
- kód je **zkopírován** do executable
- výsledná executable je taky nazývaná **static**
	- je jednodušší pro práci s OS
	- but also more wasteful

### Shared (dynamic) libraries
- potřebné k **běhu** programu
- linking je až při spuštění **execution time**
- less code duplication
- mohou být **aktualizované** samostatně
	- dependency problem (DLL hell)

### Header files
- na UNIXu: **`/usr/include`**
- obsahuje **prototypes** C funkcí
- a definice C datových struktur
- potřebné ke **kompilaci** C a C++ programů

### The POSIX C library
- **`libc`** - the C runtime library
- obsahuje ISO C funkce
	- `printf`, `fopen`, `fread`
- a také POSIX funkce
	- `open`, `read`, `gethostbyname`, ...
	- C wrappers for system calls

### [System calls](../03-kernel/#system-calls)
#### Numbers
- systémové volání jsou prováděny na strojové úrovni (**machine level**)
- které systémové volání bude použito určuje dané **číslo** systémového volání
	- př. `SYS_write` je 4 na OpenBSD
	- tyto čísla jsou definované v **`sys/syscall.h`**
	- čísla se liší na každém OS

#### `syscall` funkce
- existuje c funkce **`syscall`**
	- prototype: **`int syscall( int number, ... )`
	- implementuje **low-level** syscall sequence
- bere **syscall number** a syscall parametry
	- něco jako `printf` - první parametr, rozhoduje, jaké budou další parametry

#### Wrappers
- přímé používaní `syscall()` je nepříjemné
- `libc` má funkci pro každý system call
	- `SYS_write` --> `int write( int, char *, size_t )`
	- `SYS_open` --> `int open( char *, int )`
	- ...
- mohou používat vnitřně `syscall()`

#### Portability
- knihovny nabízí **abstraction layer** nad vnitřním fungováním OS
- umožňují **application portability**
	- společně se standardizovaným filesystem locations
	- a uživatelskými nástroji
- vysoko-úrovňové programovací jazyky závisí na systémových knihovnách

## NeXTSTEP and Objective-C
- NeXT OS je postavěný na  **Objective-C**
- systémové knihovny mají ObjC APIs
- v API ohledu, ObjC je velmi **rozdílné od C**
	- velmi rozdílné od C++
	- tradiční **OOP** vlastnosti (jako Smalltalk)
- toto bylo částečně použito do **macOS**
	- Objective C evolved into Swift

## Systemové knihovny: UNIX
- matematická knihovna: **`libm`** (funkce `sin`, `exp`)
- knihovna na práci v vlákny: **`libpthread`**
- práce s terminálem: **`libcurses`**
- kryptografie: **`libcrypto`** (OpenSSL)
- C++ standardní knihovna: `libstdc++` nebo `libc++`

## Systémové knihovny: Windows
- `msvcrt.dll` - ISO C funkce
- `kernel32.dll` - základní OS API
- `gdi32.dll` - rozhraní pro grafické zařízení
- `user32.dll` - standardní GUI 

## Documentation
- manual pages on UNIX
	- sekce 2: systémové volání
	- sekce 3: knihovní funkce
- MSDN pro windows

# Compiler & Linker
## C compiler
- mnoho POSIX systémy mají C compiler defaultně
- kompilátor bere **zdrojový kód** C souboru jako vstup
- a produkuje **object file** jako výstup
	- binární kód se strojovým kódem
	- nemůže být přímo vykonán

## Object file
- obsahuje native **machine** (executable) code
- včetně statických dat (string literals)
- pravděpodobně rozdělen na **sekce**
	- `.text`, `.rodata`, `.data`, ...
- a metadata (list **symbolů** (název funkcí) a jejich adresy)

### Formáty
- **`a.out`** - první UNIX object file formát
- COFF - Common Object File Format
	- přidává podporu sekcí oproti `a.out`
- PE - Portable Executable (MS **Windows**)
- Mach-O - Mach Microkernel Executable (**macOS**)
- **ELF** - Executable and Linkable Format (all modern UNIX)

## Archives
- statické knihovny v UNIXu se nazývají **archives** (`.a` přípona)
- jsou jako zip soubory plné **object files**
- plus tabulka symbolů (název funkcí)

## Linker
- (Linked editor)
- object files are **incomplete**
- mohou odkazovat na **symboly**, které nedefinují (jiné object files nebo knihovny)
- **linker** kombinuje více object files do sebe
	- vytváří **single executable**
	- nebo shared library

## Symbols vs Addresses
- používáme symbolické **názvy** na volání funkcí, ale instrukce `call` potřebuje **adresu**
- executable bude v budoucnu v paměti
- data i instrukce potřebují dostat nějakou **adresu**
- linker tyto adresy **přiřazuje**

## Resolving symbols
- linker zpracovává jeden object file po sobě
- udržuje si **symbol table**
	- mapuje symboly (názvy) na adresy
	- automaticky se aktualizuje, čím víc object files zpracovává
- relokace jsou typicky vykonané najednou až na konci 
- **resolving symbols** = hledání jejich adres

## Executable
- zhotověný **image** programu, který má být spustěn
- často ve stejném formátu jako **object files**
- ale hotový s resolved symbols
	- ale mnoho může používat **shared libraries**, v tomto případě jsou **některé symboly** unresolved

## Shared libraries
- každá sdílená knihovna stačí být v paměti pouze jednou
- sdílené knihovny používají **symbolic names** (jako v object files)
- v OS je "mini linker", který tyto jména překládá
	- známý jako **runtime** linker
	- překlad adres = hledání adres
- sdílené knihovny muhou použít jiné sdílené knihovny
	- mohou vytvořit **DAG**(Directed Acyclic Graph)

## Addresses revisited
- spuštění programu, načte program **do paměti**
- části programu odkazují na jiné části programu
	- znamená to, že musí vědět, **kde** budou načteny - o to se stará **linker**
- sdílené knihovny používají **position-independent code**
	- funguje bez ohledu na to, na jakou bázovou adresu je program načtený

## Compiler, Linker, ...
- C kompilátor je často nazývá **`cc`**
- linker jako **`ld`**
- the archive (static library) manager je **`ar`**
- **runtime linker** je **`ld.so`**

# File-Based APIs
## Everything is a File
- část UNIXového **design philosophy**
- **directories** (adresáře) jsou soubory
- **devices** (zařízení) jsou sobory
- **pipes** (roury) jsou soubory
- síťová připojení jsou (skoro) soubory
<!-- markdown cheats -->
- **re-use** the comprehensive **file system API**
- re-use existing file-base command-line tools
- chybu jsou špatné --> **jednoduchost** je dobrá

## Co je to Filesystem
- množina **souborů a adresářů**
- často jsou on a single block device (ale mohou být i vrituální)
- adresáře a soubory tvoří **strom** (tree)
	- directories are internal nodes
	- files are leaf nodes

## File paths
- souborové systémy používají **cesty** (**paths**) k ukazování na soubory
- je to řetězec s `/` jako delimiter adresáře (na Windows `\`)
- začínájící `/` značí **filesystem root** (`/usr/include`)

## The file hierarchy

## The role of files and filesystems
- **velmi** důležitá role v **Plan9**
- hlavní ve většině UNIX systems
	- cf. Linux pseudo-filesystems
	- `/proc` nabízí informace o všech procesech
	- `/sys` informace ohledně kernelu a zařízení
- **menší** role ve Windows
- velmi **nízká** role v Androidu

## The filesystem API
- **otevření** souboru (`open()` sys call)
- `read()` a `write()` dat
- po dokončení `close()`
- `rename()` a `unlink()` souborů
- `mkdir()`

## File descriptors
- kernel si udržuje tabulku otevřených souborů
- **file descriptor** je index do této tabulky
- pomocí file descriptors děláme vše
- non-UNIX systémy mají podobný koncept
	- ve Windows se naývají **handles**

## Regular files
- obsahují **sequential data** (bytes)
- mohou mít vnitřní strukturu, o kterou ale se OS nestará
- k souborům jsou připojeny **metada**
	- poslední modifikace
	- kdo může a nemůže
- `read()` a `write()`

## Directories
- **list** souborů a jiných adresářů
- mohou být otevřeny jako soubory
	- ale `read()` a `write()` není povoleno
	- `open()`, `create()`, `mkdir()`, `opendir()`, `readdir()`

## Mounts
- UNIX joins all file systems into a single hiearchy
- the root of one filesystem becomes a directory in another is called **mount point**
- Windows používá **drive letter** místo toho (`C:`,`D:`, ...)

## Pipes
- pipes are simple communication device
- progam může `write()` data do roury
- jiný může `read()` ty data
- každý konec pipe má vlastní **file descriptor**
- a pipe can live in the filesystem (named pipe)

## Devices
- **block** and **character** devices are (special) files
- block devices are accesed one **block at a time**
	- a typical block would be a **disk**
	- includes USB mass storage, flash storage, etc
	- you can create a **file system** on block device
- **character** devices are more like normal files
	- terminals, tapes, serial ports, audio devices

## Sockets
- the socket API comes from early BSD Unix
- socket represents a (possible) **network connection**
- sockets are more complicated than normal files
	- establishing connections is hard
	- messages get lost much more often than file data
- you get a **file decriptor** for an open socket
- you can `read()` and `write()` to sockets

### Socket Types
- sockets can be **internet** or **unix domain**
	- internet sockets connect to other computers
	- Unix sokcet live in the filesystem
- sockets can be **stream** or **datagram**
	- stream socket are like files
	- you can write a continuous **stream** of data
	- datagram sockets can send individual **messages**
<!--stackedit_data:
eyJoaXN0b3J5IjpbODQ1NTE5ODMyLC00ODQ1MTQ4MTIsLTE5Nz
MwMzQzNTMsNjAxODMzMzczLC0xMDcxNjI4MDAxLDE2NjMyNDY1
MjAsMjM5NzQzMjc5LC0xOTc3MDI1NjkwLC0xODY3MzY2NzQwLD
YyMDY2Mjk3OCwtNTM5NDE1ODM0LC0zOTI0NDM3NDAsLTkxMjc1
MTM3OSwtMTg4ODMzNjM0M119
-->
