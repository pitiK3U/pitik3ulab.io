+++
title = "11 - Virtualisation & containers"
date = 2021-05-14
+++

# Hypervisors
## What is a hypervisor
- také známé jako Virtual machine monitor
- povoluje spuštění **vícero OS**
- jako kernel, který běží kernely
- zlepšuje **hardware utilizaci**

## Motivation
- OS-level sdílení je tricky
	- **izolace uživatele** je často **nedostatečná**
	- pouze `root` může instalovat software
- hypervisor/OS rozhraní je **jednoduché** (s porovnáním OS-aplikační rozhraní)

## Virtualisation in general
- mnoho zdrojů je "virtuálních"
	- fyzická **paměť** od MMU
	- **periferie** od OS
- dělá **resource management** jednodušší
- povoluje **izolaci** komponent

## Hypervisor types
- typ 1: bare metal (samostatný, microkernel-like)
- typ 2: hosted
	- běží na normálním OS
	- často potřebuje **kernel support**

### Type 1 - Bare metal
- IBM z/VM
- Xen
- Microsoft Hyper-V
- VMWare ESX

### Type 2 - Hosted
- VMWare (Workstation, Player)
- Oracle VirtualBox
- Linux KVM
- FreeBSD bhyve
- OpenBSD vmm

## Desktop virtualisation
- `x86` hw lacks **virtual supervisor mode**
- **sw-only** řešení možné od pozdních 90. let

## Paravirtualizace
- VMI v 2005 od VMWare
- alternativně Xen v 2006
- závisí na **modifikace guest OS**
- skoro native speed bez hw podpory

## The virtual `x86` revolution
- 2005: virtualizační rozšíření na `x86`
- 2008: MMU virtualizace
- **nemodifikovaný** guest až skoro native speed
- většina **sw-only** se staly **obsolete**

## Paravirtual devices
- speciální **ovladače** pro **virtuální zařízení** (network, rng, block storage)
- **rychlejší** a **jednodušší** než emulace (orthogonal k CPU/MMU virtualizaci)

## Virtual computers
- také jako Virtual Machines
- vše v pc je virtuální
	- buď pomocí hw (VT-x, EPT)
	- nebo sw (QEMU,`virtio`,...)
- mnohem **jednodušší na správu** než hw

## Essential resources
- CPU a RAM
- persistentní (block) storage
- síťové připojení
- console device

### CPU sharing
- stejné principy jako normální **procesy**
- v hypervisoru je **scheduler** (jednodušší, s jinými tradeoffs)
- privilegované instrukce jsou trapped

### RAM sharing
- velmi podobné ke standardnímu **stránkování**
- sw (shadow paging)
- nebo hw (second-level translation)
- fixní množství RAm pro každou VM

#### Shadow page tables
- **guest** system **nemůže** zpřístupnit MMU
- nastavení **shadow table**, neviditelné to the guest
- guest page tables jsou synchronizovány s sPT pomocí VMM
- gPT být read-only aby zapříčinila traps

#### Second-level translation
- hw-asistované MMU virtualizace
- přidá guest-physical to host-physical layer
- velmi **zjednodušuje** VMM
- také velmi **rychlejší** než shadow page tables

### Network sharing
- normálně paravirtualizovaný NIC
	- přenáší **rámce** mezi guest and host
	- často připojená k **SW bridge** in the host
	- alternativy: routing, NAT
- jedna fyzická NIC je používána všemi

### Virtual block devices
- často také paravirtualised 
- často backed normálními **soubory** (možná ve speciálním formátu)
	- např. **copy-on-write**
- ale může být reálné **block device**


## Special resources
- hlavně užitečné v **desktop systémech**
- GPU / grafický hw
- audio equipment
- printers, scanners, ...

## PCI passthrough
- proti-virtualizační technologie
- založeno na IO-MMU (VT-d, AMD-Vi)
- **virtuální** OS se může dotýkat **reálného** hw (pouze jeden OS zároveň)

## GPUs and virtualisation
- může být **přiřazeno** (pomocí VT-d) k **jednomu OS**
- nebo **time-shared** používáním native ovladačů (GVT-g)
- paravirtualizované
- sdíleno by other means (X11, SPICE, RDP)

## Peripherals
- užitečné přes **passthrough**
- nebo **standarním sdílením**

### Peripheral passthrough
- **virtuální** PCI, USB nebo SATA bus
- **přeposílání** skutečnému zařízení

## Suspend & Resume
- VM může být jednoduše **zastaveno**
- RAM zastavené VM může být **zkopírován**
	- např. **soubor** v host filesystem
	- s **registry** a další state
- a později také **načten** a **resumed**

## Migration basics
- uložený stav může bát **posláno přes síť**
- a resumed na **jiném hostu** (do té doby, dokud je virtuální prostředí stejné)
- je to známo jako **paused** migrace

## Live migration
- používá **asynchronní** memory snapshot
- host copíruje stránky a nastaví je read-only
- snapshot je poslán až je to zkonstruováno
- změněné stránky jsou poslány na konci

### Live migration handoff
- VM je pak pausnutá
- registry a posledních pár stránek je posláno
- VM je **resumed** na vzdáleném konci
- často v řádu **několika milisekund**

## Memory Ballooning
- jak **dealokovat** fyzickou paměť?
	- vrácení do hypervisoru
- často chtěné ve virtualizaci
- potřebuje speciální host/guest rozhraní

# Containers
## What are containers?
- OS-level virtualizace	
	- např. virtualizovaný **network stack**
	- nebo omezený **filesystem** access
- **není** to kompletní virtulní počítač
- turbocharged processes

## Why containers
- VM trvá nabootovat
- každý VM potřebuje vlastní **kernel**
	- toto se sečte při více VM
- jednodušší ke efektivnímu **sdílení paměti**
- jednodušší k oříznutí OS image

## Kernel sharing
- vícero containers sdílí **jeden kernel**
- ale né user tables, process tables, ...
- kernel musí explicitně povolovat
- další úroveň **isolace** (proces, uživatel, container)

## Boot time
- lehká VM trvá vteřinu/dvě
- container může trvat pod 50ms
- ale SM mohou být suspended a resumed
- ale většina VM zabírá mnoho místa

## `chroot`
- hlavní část všech container systems
- není velmi sofistikované nebo bezpečné
- ale povoluje OS images pod jedním kernelem
- všechno ostatní je sdíleno

## `chroot`-based 'Containers'
- process tables, síť, atd. je sdíleno
- super uživatel je také sdílený
- containers mají **jejich vlastní view** of the file system
	- i s **system libraries** a **utilities**

## BSD Jails
- evoluce `chroot` container
- přidává separaci **uživatele** a **process table**
- a virtualizovaný network stack (každý jail má vlastní IP adresu)
- `root` v jailu má omezený power

## Linux VServer
- jako BSD jails ale na Linuxu (FreeBSD jails 2000, VServer 2001)
- není součástní hlavního kernelu
- jailed `root` uživatel je částečně isolovaný

## Namespaces
- **viditelnost** compartments v Linux kernelu
- virtualizace běžných OS zdrojů
	- the filesystem hierarchy (včetně mountů)
	- process tables
	- networking (IP address)

## `cgroups`
- kontroluje **alokaci hw zdrojů** v linuxu
- CPU skupina je férová scheduling jednotka
- memory group nastavuje omezení na použití paměti
- převážně orthogonal to namespaces

## LXC
- mainline Linux way to do containers
- založeno na namespaces a `cgroups`
- poměrně nový (2008)
- feature set podobný k Vserver, OpenVZ, ...

## User-Mode linux
- na půlcesty mezi VM a containerem
- an early fully paravirtualised system
- Linux kernel běží jako proces na jiném Linuxu
- od verze Linux 2.6 v 2003

## DragonFlyBSD Virtual Kernels
- velmi podobný k User-mode Linux
- od r 2007
- používá `libc` oproti UML
- paravirtuální ethernet, storage a console

## User mode kernels
- jednodušší k bezpečnému nasazení
	- použitím existujících zabezpečení
	- pro host, převážně běžný proces
- kernel musí být ported (analogové k nové hw platformě)

## Migration
- není široce podporováno, oproti hypervisors
- stav procesu je mnohem těžší serializaci (file descriptors, network, ...)
- tradeoff s rychlým vypnutím/zapnutím

# Management
## Disk images
- disk image je "ztělesnění" VM
- virtualní OS musí být naistalováno
- image může být pouhý soubor
- nebo dedikované zařízení na host

## Snapshots
- vytváření kopie image = snapshot
- může být uskutečněno výkonně: copy on write
- alternativa k OS instalaci
	- vytvoření kopie k **čerstvě instalovaném** obrazu
	- a spuští aktualizace po klonování obrazu
	
## Duplication
-