+++
title = "08 - Network stack"
date = 2021-04-23
+++

# Networking intro
## Host and domain names
- **hostname** = lidsky čitelný název počítače
- **hierarchický** systém, little endian: `www.fi.muni.cz`
- FQDN = fully-qualified domain name
- **lokální suffix** může být vynechán (`ping aisa`)

## Network Addresses
- adresa = **machine**-friendly a číselné
- IPv4 adresy: 4 oktet (bajty): `192.168.1.1`
- IPv6 adresy: 16 oktet
- Ethernet (MAC): 6 oktet, `c8:5b:76:bd:6e:0b`

## Network types
- LAN = Local Area Network
    - Ethernet: **wired**, až do 10Gb/s
    - WiFi (802.11): **wireless**, až do 1Gb/s
- WAN = Wide Area Network (the Internet)
    - PSTN, xDSL, PPPoE
    - GSm, 2G(GPRS, EDGE), 3G (UMTS), 4G (LTE)
    - také LAN technologie - Ethernet, WiFi

## Networking layers
2. Link (Ethernet, WiFi)
3. Internet / Network (IP)
4. Transport (TCP, UDP, ...)
7. Application (HTTP, SMTP, ...)

## Networking and operating systems
- **network stack** je standaardní část OS
- velké část tohoto stacku žije v **kernelu**
    - ale to pouze v **monolithic kernels**
    - microkernely používají **user-space** networking
- další část je v systémových **knihovnách** a **utilities**

### Kernel-side networking
- **ovladače** zařízení pro síťový hardware
- **protokoly** síťové a transportní vrsty
- **routing** a packet filtering (firewalls)
- networking-related **sytem calls** (sockets)
- network **file systems** (SMB, NFS)

### System libraries
- **sockets** a jejich APIs
- host **name resolution** (překlad adres, DNS klient) 
- **šifrování** a **autentifickace** dat (SSL, TLS)
- práce a validace s **certifikáty**

### System utilities & services
- **nastavení** sítě (`ifconfig` `dhclient`, `dhcpd`)
- routování (`route`,`bgpd`)
- **diagnostika** problému se sítí (`ping`, `traceroute`)
- packet loggin and inspection (`tcpdump`)
- other netowrk services (`ntpd`,`sshd`,`inted`)

### Networking aspects
- formát paketů
    - Co jsou **jednotky komunikace**
- adresování
    - jak se odesílater a příjemce **jmenují**
- doručení paket
    - jak je zpráva **doručena**

## Protocol nesting
- protokoly běží **na sobě**
- proto se tomu říká **network stack**
- vyšší úrovně používají nižší úrovně
    - HTTP používá abstrakci poskytovanou TCP
    - TCP používá abstrakci poskytnutou IP

## Packet nesting
- **pakety** vyšší úrovně jsou pouze **data** z pohledu nižší úrovně
- Ethernetový **rámec** může přenášet **IP paket** v sobě
- **IP paket** může přenášet **TCP paket**
- **TCP paket** může přenášet **HTTP požadavek**

## Stacked delivery
- doručovaní je (v abstrakci) **point-to-point**
    - routování je **schováno** of vyšších vrstev
    - vyšší vrstva požádá **doručení** na danou **adresu**
- protokoly nižší vrstvy jsou obecně **packet-oriented**
    - packet size mismatch může znamenat **fragmentaci**
- paket může projít přes **různé** nízko úrovňové **domény**

## Layers vs addressing
- není tak přímé jako packet nesting
    - 
- **speciální protokoly** existují pro překlad adres
    - DNS pro hostname vs IP adres mapování
    - ARP pro IP vs MAC adres mapování

## ARP (Address Resolution Protocol)
- najde danou MAC k IP adrese
- potřebné k **doručování paket**
    - IP používá **linkovou vrstvu** k doručování paket
    - linková vrstva musí dostat **MAC adresu**
- OS si staví **mapu** z IP &rarr; MAC **překladů**

## Ethernet
- komunikační protokol na **linkové vrstvě**
- převážně implementován **hw**
- OS používá dobře definované rozhraní
    - přijímaní a posílání paketů
    - používání MAC adres

## Packet switching
- **sdílená média** jsou neefektivní kvůli **kolizím**
-ethernet je typicky **packet switch**
    - **switch** je obecně **hw zařízení** (ale také v sw pro virtualizaci)
    - fyzické připojení formují **star topology**

## Bridging
- mosty operují na druhé vrstvě
- most je dvouportové zařízení
    - každý port je připojen od **jiné LAN**
    - most spojuje LANs **přeposíláním** rámcům
- můe být jak v hw tak v sw
    - `brctl` na Linuxu, `ifconfig` na OpenBSD

## Tunneling
- tunely jsou **virutální zařízení na druhé nebo třetí vrstvě**
- **zapouzřují** (encapsulates) trafiku pomocí protokolů vyšších vrstev
- tunelování může být implementováno pomocí **Virtual Private Networks**
    - **sw bridge** může operovat přes UDP tunel
    - tunely jsou **šifrované**

## PPP (Point-to-Point Protocol)
- protocol **linkové vrstvy** pro **2-node networks**
- dostupné přes mnoho **fyzických spojení**
    - telefoní dráty, mobilní sítě, DSL, Ethernet
    - často používaná k připojení konců k ISP
- podporováno většinou OS
    - rozděleno mezi **jádrem** a **system utilities**

## Wireless
- WiFi je převážně (pomalý, unreliable) ethernet
- nutnost **šifrování** protože kdokoliv může poslouchat
- také **autentifikace** k zabránění **rogue connections**
    - PSK (pre-shared key), EAP / 802.11x
- šifrování potřebuje **key management**

# The TCP/IP stack
## IP (Internet Protocol)
- používá 4 bajty (v4) nebo 16bajtů (v6) adresacave
    - rozděleno na **síť** a **host** části
- je to protokol založený na paketech
- **best-effort** protokol
    - pokety se mohou ztratit, změnit pořadí nebo poškodit

## IP networks
- IP síťě odpovídají LANs
    - hosts jsou na **stejné sítě** located pomocí ARP
    - **vzdálené** sítě jsou zpřistupňěny pomocí **routers**
- **netmask** rozděluje adresu na síť/host části
- Ip typicky běží na Ethernetu nebo PPP

## Routing
- routery **přesposílajjí** pakety **mezi sítěmi**
- něco jako **mosty** ale v **třetí vrstvě**
- routery se mohou chovat jako normální **LAN endpoints**
    - ale mohou reprezentovat celé remote IP sítě
    - nebo také celý Internet

## ICMP: Internet Control Message Protocol
- **control** messages (packets)
    - destinace host/síť jsou nedostupné (unreachable)
    - TTL (time to live) exceeded
    - potřeba frogmentace
- **diagnostic** packets, např. `ping` příkaz
    - `echo request` a `echo reply`
    - kombinuje s TTl pro `traceroute`

## Services a TCP/UDP Port numbers
- sítě jsou převážně používány k **poskytování služeb**
    - každý pc může hostovat více služeb
- různé **služby** mohou běžet na různých **portech**
- port je 16-bitové číslo a některé mají daná jména
    - port 25 je SMTP, port 80 je HTTP, ...

## TCP: Transmission Control Protocol
- **stream**-oriented protokol nad IP
- funguje jako **pipe** (transfers byte sequence)
    - musí respektovat **pořadí doručení**
    - a také **re-transmit** ztracených paket
- musí ústálit (establish) **spojení**

### TCP connections
- koncové body musí prvně ustálit **spojení**
- každé spojení slouží jako vlastní **data stream**
- spojení je **oboustranné**
- TCP používá 3-cestý handshake: SYN, SYN / ACK, ACK

### Sequence numbers
- TCP pakety udržují **sekvenční čísla**
- tyto čísla slouží ke **znovu poskládání** streamu
    - IP pakety mohou být doručeny **out-of-order**
- také slouží k **acknowledge reception**
    - a také ke správě re-transmission

### Packet loss and re-transmission
- pakety mohou být **ztraceny** z mnoha důvodů
    - **spojení spadne** na dlouho dobu
    - **buffer overruns** on routing equipment
- TCP pošle **acknowledgements** k přijetým paketům
    - ACKs používají **sekveční čísla** k identifikaci paketů

## UDP: User (Unreliable) Datagram Protocol
- TCP je s non-trivial **overhead**
    - a tyto garantace **nejsou vždy potřeba**
- UDP je mnohom **jednodušší** protokol
    - velmi tenká vrstva okolo IP
    - s velmi **minimal overhead** nad IP

## Firewall
- **název** pochází ze stavění budov (ohně vzdorná bariéra mezi částmi stavění)
- idea je **odděli sítě** od sebe
    - útoky z outside jsou mnohem těžší
    - **limiting damage** in case of compromise

### Packet filtering
- filtrování paketů je implementace **firewallu**
- může být uskutečněna na **routeru** nebo **endpoint**
- **dedikované** routery + paketové filtry jsou **více bezpečné**
    - **jeden** takový **firewall** chrání **celou síť**
    - menší pravděpodobnost pro misconfiguration

### Packet filter operation
- filtry paketů opraují nad množinou **pravidel**
    - pravidla jsou obecně **operator**-provided
- každý příchozí paket je **klasifikován** podle pravidel
- a poté **dispatched** accordingly
    - may be **forwarded**, dropped, **rejected** or edited

#### Packet filter examples
- filtrování paketů je součástí **kernelu**
- the rule parser is a sstem utility
    - načte pravidla z **konfiguračního souboru**
    - a nastaví kernel-side filter
- je více **implementací**
    - `iptables`,`nftables` na Linuxu
    - `pf` v OpenBSD, `ipfw` v FreeBSD

## Name resolution
- uživatelé si nechtějí pamatovat **číselné adresy**
- používají se místo toho host **names**
- může být uloženo v souboru, např. `/etc/hosts`
    - není velice praktické pro 3 a více počítačů

## DNS: Domain Name system
- hierarchický **protokol** pro name resulution
    - běží na TCP nebo UDP
- doménový **jména jsou rozdělena** do částí pomocí teček
    - každá doména ví, koho se zeptat na další bit
    - databáze jsem je efektivně **distribuovaná**

### DNS recursion
- např. `www.fi.muni.cz.` 
- překlad začíná z prava na **root servers**
    - root servers refer us to the `cz.` servers
    - the `cz.` servers refer us to `muni.cz`
    - finally `muni.cz.` tells us about `fi.muni.cz`
`$ dig www.fi.muni.cz. A +trace`

### DNS record types
- `A` je pro (IP) adresu
- `AAAA` je pro IPv6 adresu
- `CNAME` je pro alias
- `MX` je pro mailový server
- a mnohé další

# Using networks
## Sockets reminder
- **socket API** je z brzkého BSD Unixu
- socket reprezentuje (možné) **síťové spojení**
- pro otevření socket dostaneme **file descriptor**
- můžeme `read()` a `write()` do soketů
    - ale také `sendmsg()` a `recvmsg()`
    - a `sendto()` a `recvfrom()`

## Socket types
- sokety mohouo být **internet** nebo **unix domain**
    - internetové sokety pracují přes síť
- **stream** sockets jsou jako soubory
    - můžeme zapisovat continuous **stream** dat
    - často implementováno pomocí TCP
- **datagram** sockety posílají individuální **zprávy**
    - často umplementováno pomocí UDP

### Creating sockets
- socket je vytvořen pomocí funkce `socket()`
- může být změněn v **server** pomocí `listen()`
    - jednotlivé **spojení**(connections) jsou establish pomocí `accept()`
- nebo v **klienta** pomocí `connect()`

## Resolver API
- `libc` obsahuje **resolver**
    - dostupná jako `gethostbyname` (a `getaddrinfo`)
    - také `gethostbyaddr` for **reverse lookups**
- může se dívat na mnoho různých míst
    - většina systémů podporuje alespoň `/etc/hosts`
    - a DNS-based lookups

## Network services
- servery **poslouchají** na socket na příchozí zprávy
    - klient atkivně ustanoví **spojení** k serveru
- síť jednoduše **přenáší data** mezi nimi
- interpretace dat je problém **sedmé vrstvy**
    - mohou to být **příkazy**, soubory, ...

### Examples
- (secure) remote shell - `sshd`
- the internet **email suite**
    - MTA = Mail Transfer Agent, speaks SMTP
    - SMTP = Simple Mail-transfer Protocol
- the **world wide web**
    - web servers provide content (files)
    - clients and servers speak HTTP and HTTPS

## Client software
- `ssh` příkaz používá SSH protocol
- **webový prohlížeč** je klient pro www
    - prohlížeče jsou komplexní **aplikační** programy
    - některé větší jak OS
- **emailový klient** je také známý jako MUA (Mail User Agent)

# Network File Systems
## Why network filesystems?
- kopirování souborů tam a zpět je nepraktické
    - a také **error-prone** (jaká je poslední verze?)
- co ohledně ukládání dat v **centrální lokaci**
- a **sdílení** toho se včemi pc přes LAN

## NAS (Network-attached storage)
- (malý) **počítač** dedikovaný k **ukládání souborů**
- normálně běží ořezanou verzi OS
    - často založenou na Linuxu nebo FreeBSD
- poskytuje **souborový přístup** k síti
- někdy další **aplikační služby**
    - např. photo management, media streaming, ...

## NFS (Network File System)
- tradiční UNIX **sítový souborový systém**
- hooked hluboku do kernelu
    - předpokládá obecně reliable network (LAN)
- souborové systémy jsou **exportovány** pro použití přes NFS
- klientská strana **mounts** NFS-exported volume

### NFS History
- začalo v **Sun Microsystems** v 80. ltetech
- v2 implementováno v System V, DOS, ...
- v3 se objevilo v 95 aje **stále v použítí**
- v 4 přišlo v 2000, zlepšuje **bezpečnost**

## VFS Reminder
- **implementační mechanismus** pro vícero FS typů
- objektově orientovaný způsob
    - `open`; **look up** the file for access
    - `read`,`write`
    - `rename`: přejmenuj soubor nebo adresář

## RPC (Remote Procedure CALL)
- jakýkoliv **protokol** pro **volání funkcí** na **remoce hosts**
    - ONC-RPC = Open Network Computing RPC
    - NFS je založeno na ONC-RPC (také známo jako Sun RPC)
- NFS v podstatě běží VFS operace s použitím RPC
    - **jednoduché k implementaci** na UNIX-like systémech

## Port mapper
- ONC-RPC je spuštěno přes TCP nebo UDP
    - ale to je více **dynamické** wrt dostupných služeb
- TCP/UDP **čísla portů** jsou přiřazeny **na požádání**
- `portmap` **přeloží** RPC služby na čísla portů
    - port mapper poslouchá na portu `111`

## The NFS Daemon
- také jako `nfsd`
- poskytuje NFS přístup k **lokálnímu souborovému systému**
- může běžet jako systémová služba
- nebo může být součástí kernelu (toto je typický z důvodů **výkonu**)

## SMB (Server Message Block)
- **síťový souborový systém** od Microsoftu
- dostupné ve Windows od verze 3.1 (1992)
    - orignálně běželo na NetBIOS
    - pozdější verze používali TCP/IP
- SMB1 nabral hodně na **složitosti**

## SMB 2.0
- **jednodušší** než SMB1 kvůli **méně zpětných vazeb**
- lepší **výkon** a **bezpečnost**
- podpora **symbolic links**
- dostupné od roku Windows Vista (2006)
