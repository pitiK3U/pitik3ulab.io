+++
title = "10 - Access Control"
date = 2021-05-07
+++

# Multi-user systems
## Users
- originálně proxy pro **lidi**
- aktuálně více **obecná abstrakce**
- uživatel je jednotka **vlastnictví**
- mnoho **práv (permissions)** jsou user-centered

## Computer Sharing
- počítač je (často drahý) **zdroj**
- problém je efektivita používání
	- jeden uživatel vzácně používá plně pc
- sdílení dat tvoří access control nutností

## Ownership
- různé **objekty** v OS může být **vlastněno**
	- hlavně **souboru** a **procesy**
- vlastník je typicky ten, kdo objekt **vytvořil**
	- ale vlastnictví můž být **předáno**
	- často jsou omezené

## Process ownership
- každý **proces** vlastní nějaký uživatel
- proces pracuje **na základě** uživatele
	- proces má stejné práva jako vlastník
	- toto zároveň proces **omezuje** a **posiluje**
- procesy jsou **aktivní** účastníci systému

## File ownership
- každý **soubor** patří nějakému uživateli
- toto dává **práva uživateli** (nebo jejich procesům)
	- soubor mohou **číst** nebo **zapisovat**
	- mohou **změnit práva** nebo vlastniství
- soubory jsou **pasivní účastníci**

## Access control models
- **vlastníci** si určují kdo může přistoupit k jejich objektům
	- je to známo jako **discretionary** access control
- ve vysoce bezpečných prostředích toto není povoleno
	- známo jako **mandaroty** access control
	- centrální autorita nastavuje policy
	
## (Virtual) system users
- uživatelé jsou užitečná **abstrakce** vlastnění
- mnoho systémových služeb má své vlastní *fake* uživatele
- toto jim povoluje **vlastnit soubory** a **procesy**
- a také **omezuje** jejich **přístup** ke zbytku OS

## Principle os least privilege
- entity by měli mít **minimum** privilegií, které potřebují
	- aplikuje se na **sw** komponenty
	- ale také na **lidské** uživatele
- **omezí** rozsah **chyb**
	- ale také cílených útoků
	
## Privilege separation
- různé části systému potřebují různé privilegie
- least privilege si vyžaduje **rozdělení** systému
	- komponenty jsou **izolovány** od sebe
	- jsou jim dány jen práva, které potřebují
- komponenty **komunikují** jen použitím velmi jednoduchého IPC

## Process separation
- vzpomeňme se, že každý proces běží ve svém **address space**
	- **shared memory** musí být explicitně požádaná
- každý **uživatel** má pohled na **filesystem**
	- defaultně je mnoho věcí ve filesystmu sdíleno
	- especially **namespace** (hierarchie adresáře)

## Access control policy
- jsou zde 3 části informací
	- **subjekt** (aktivní účastník - uživatel)
	- the **action / verb** (co se má stát)
	- **objekt** (pasivní účastník - soubor nebo jiný zdroj)
- je zde mnoho způsobů, jak **zakódovat** tyto informace

## Access rights subjects
- v typických OS jsou to **uživatelé**
	- jsou možné sub-user jednoty (programy)
	- **role** a **skupiny** také mohou být subjekty
- subjekt musí být **pojmenován** (jména, identifikátory)
	- jednoduché na jednom systému, **složité** přes **síť**
	
## Acess rights actions (verbs)
- možné 'verbs' (akce) záleží na typu **objektu**
- typickým objektem je **soubor**
	- soubory můžeme **číst**, **psát**, **vykonat**
	- **adresáře** mohou být **prohledávány** nebo **listed** nebo **změněny**
- síťové připojení může být začato, ...

## Access rights objects
- cokoliv, co může být **manipulovatelné programy**
	- ale né všechno je subjekt k access control
- mohou to být **soubory**, **adresáře**, **sokety**, sdílená **paměť**, ...
- **jméno** objektu závisí na jejich typu
	- cesty souborů, i-node čísla, IP adresy, ..
	
## Subjects in POSIX
- dva typy subjektů: **uživatelé** a **skupiny**
- každý *uživatel** může patřit **do více skupin**
- uživatelé se dělí na **normální** uživatele a **root** (**superuživatel**)

## User and group identifiers
- uživatelé a skupiny jsou reprezentovány jako **čísla**
	- toto zlepšuje **efficiency** mnoho operací
	- čísla jsou `uid` a `gid`
- tyto čísla jsou validní jen na **jednom počítači**

## User management
- systém potřebuje **databízi uživatelů**
- v síti **identifikace** uživatelů musí být **sdílená** 
- může být tak jednoduché jako **textový soubor**:
	- `/etc/passwd` a `/etc/group` na UNIX systémech
- nebo tak komplexní jako distribuované databáze

## Changing identities
- každý **proces** patří danému **uživateli**
- vlastnictví je **zděděno** přes `fork()`
- procesy **superuživatelů** mohou použít `setuid()`
- `exec()` může občas změnit vlastníka procesu

## Login
- proces superuživatele spravuje **user logins**
- uživatel napíše jejich jméno a **heslo**
	- `login` program **autentizuje** uživatele
	- poté zavolá `seuid()` a změní vlastníka procesu
	- a použije `exec()` k nastartování shellu uživatele

## User authentication
- uživatel se potřebuje **autentizovat**
- **hesla** jsou často používaný způsob
	- **sytém** potřebuje rozpoznat správné heslo
	- uživatelé by měli být schopni si heslo změnit
- **biometrické** způsoby metody jsou také docela populární

## Remote login
- autentizace přes **síť** je více komplikovanější
- **hesla** jsou nejjednodušší, ale né úplně jednoduché
	- k přenosu hesla je potřeba **šifrování**
	- také s **autentizací počítače**
- **2-factor** autentizace je populární zlepšení

## Computer authentication
- Jak zajistit, že posíláme heslo **správnému partnerovi**?
	- útočník může **impersonate** náš remote computer
- často pomocí **asymetrické kryptografie**
	- privátní klíč může být použit k **podepsání** zprávy
	- server podepíše challenge k ověření **identity**
	
## 2-factor authentication
- 2 různé způsoby autentizace
	- težší na porušení **obou** zároveň
- několik faktorů na výběr:
	- něco, co uživatel **zná** (heslo)
	- neco, co uživatel **má** (klíče, tokeny)
	- co ten uživatel **je** (biometric)

## Enforcement: Hardware
- všechny **enforcement** začínají s hardwarem
	- CPU poskytuje **privilegovaný režim** pro kernel
	- DMA memory a IO instrukce jsou **protected**
- MMU umožňuje kernelu **izolovat procesy**
	- brání se svou integrity
	
## Enforcement: kernel
- kernel používá **hardware prostředky** k implementaci bezpečnosti
	- stojí mezi **zdroji** a **procesy**
	- přístup je prostředkován mezi **syscalls**
- **file systems** jsou součástí jádra
- **abstrakce uživatelů a skupin** jsou součástí kernelu

## Enforcement: system calls
- jádro je chová jako **arbitrator**
- proces je omezen na svůj **adress space**
- procesy používají syscalls k přístupu ke zdrojům
	- jádro se může rozhodnout, co zpřístupnit
	- založeno na **access control model** a **policy**
	
## Enforecemnt: service APIs
- userland procesy mohou enforce access control (často systemové služby, které poskytují IPC API)
- např. přes `getpeereid()` syscall
	- řekne volajícímu, **který uživatel** je **připojen** k socketu
	- user-level access control závisí na **kernel** facilities
	
# File systems
## File access rights
- **file systems** jsou součástí studie v access control
- všechny moderní souborové systémy uchovávají **přístupová práva**
	- jediné **výjimka** je FAT (USB)
- různé systémy mají jinou reprezentaci

## Reprezentace
- souborové systémy jsou často **object-centric**
	- práva jsou připojena k jednotlivým objektům
	- jednoduše odpovídá na "Kdo může k souboru přistoupit?"
- je zda **fixní** množina **sloves**
	- mohou být rozdílné pro **soubory** a **adresáře**
	- different **systems** povolují **jiné** slovesa
	
## The UNIX model
- každá soubor a adresář má jediného **vlastníka**
- plus jedinou vlastnící **skupinu**
	- není limitován, že vlastník musí být součástí skupiny
- **vlastnictvi** a **práva** jsou připojena k **i-nodes**

## Access vs ownership
- POSIX spojuje **ownership** a **scess rights**
- pouze 3 subjekty
	- vlastník
	- vlastnící skupina
	- ostatní

## Access verbs in POSIC file systems
- čtení: **čtení** souboru, **list** adresáře
- zápis: **zápis** souboru, **link / unlink** i-nodes k adresáři
- vykonání: `exec` programu, vstup do adresáře
- vykonání jako vlastník: `setuid` / `setgid`

## Permission bits
- základní UNIX **práva** mohou být encoded v **9 bitech**
- 3 bity pro 3 subjekty
	- prvně vlastník, pak skupina, nakonec ostatní
	- zapsáno jako `rwxr-x---` nebo `0750`
- plus dvě čísla pro identifikaci vlastníka / skupiny

## Changing file ownership
- vlastník a root mohou měnit vlastníky souboru
- `chown` a `chrp` systémové utility
- nebo pomocí C API
	- `chown()`, `fchown()`, `fchownat()`. `lchown()`

## Changing file permissions
- zase vlastník a superuživatel mohou měnit
- `chmod` je user space utilita
	- buď numerický argument: `chmod 644 file.txt``
	- nebo symbolický: `chmod +x script.sh`
- a jejich související syscalls

## `setuid` and `setgid`
- **speciální práva** na **spustitelných** souborech
- povolují `exec` změnit vlastníka procesu
- často pro povolování extra privilegií
	- např. `mound` běží jako superuživatel
	
## Sticky directories
- vytváření a mazání souboru je právo **adresáře**
	- toto je problematické pro **sdílené adresáře**
	- halvně v `/tmp` adresáře
- ve **sticky** adresáři jsou jiná pravidla
	- nové soubory jsou vytvářeny stejně
	- ale jen **vlastník** může **unlink** daný soubor z adresáře
	
## Access control lists
- ACL je list ACE's (access control **elements**)
	- každý ACE je pár subjekt + sloveso
	- může to být jméno normálního uživatele
- ACL je připojeno k objektu (soubor, adresář)
- více flexibilní než tradiční UNIX systémy

## ACLs a POSIX
- část POSIX.1e (security extensions)
- většina POSIX systémů implementují ACLs
	- toto **nenahrazuje** UNIX permission bits
	- místo toho jsou interpretovány jako čás ACL
- podpora **filesystemu** není univerzální (ale je velmi rozšířená)

## Device files
- UNIX reprezentuje **zařízení** jako **speciální i-uzly**
	- kvůli tomuto jsou subjekty k normálnímu access control
- dané zařízení je popsané v **i-uzlu**
	- jen **superuživatel** může vytvořit uzly zařízení
	- ale uživatelé mohou získat přístup k jakémukoliv zařízení
	
## Sockets and pipes
- **pojmenované** sockety a pipy jsou pouze **i-uzly**
	- také sbujekt ke standardním souborovým právům
- užitečné hlavně u **socketů**
	- služby nastavují **pojmenované sockety** v souborovém systému
	- **práva souboru** rozhodují, kdo může se službou komunikovat
	
## Special attributes
- značky, které povolují **speciální omezení** použití souboru
	- např. **immutable** files (nemohouo být nikým změněny)
	- **append-only** soubory (pro logovací soubory)
	- komprese, copy-on-write 
- **non-standard** (Linux `chattr`, BSD `chflags`)

## Network file system
- NFS 3.0 jednoduše přenáší číselné `uid` a `gid`
	- čísla musí být **synchronizované**
	- může být uskutečněno **centrální databází**
- NFS 4.0 používá **per-user** autentizaci
	- uživatel se autentizuje přímo serveru
	- filsystem `uid` a `gid` hodnoty jsou mapovány

## File system quotas
- **storage space** je limitováno, **sdíleno** uživateli
	- soubory zabírají místo
	- vlastnictví souborů je **liability**
- **quotas** nastavují **limit** místo pro použití uživateli
	- včerpání quot může zapříčinit **denial of access**

## Removable media
- access conrol na filesystemu úrovni nemá smysl
	- oastní pc se mohou rozhodnout **ignorovat** práva
	- **uživatelné jména** nebo id by tak č itak neměli smysl
- způsob 1: **šifrování** (pro zamezení čtení)
- způsob 2: **hw**-level controls
	- často read-only vs read-write na celé zařízení
	
## The `chroot` system call
- každý proces v UNIXu má svj vlastní **root directory**
	- pro většinu je totožný se **systémovým rootem**
- root adresář může být změněn pomocí `chroot()`
- může být užitečné k **limitaci** filesystem access
	- napč. **privilege separation** scenarios
	
## Uses of `chroot`
- `chroot` sám o sobě **není** zabezpečovací mechanismus
	- proces superuživatele může jednoduše **vrátit ven**
	- ale není to jednoduchý pro proces **normálního uživatele**
- také užitečné pro **disagnostiku**
- ale také lightweight alternativa k **virtualizaci**

# Sub-user granularity
## Users are not enough
- ne vždy jsou uživatelé správná abstrakce
	- **vytváření uživatelů** je poměrně **drahé**
	- jen superuživatel může vytvářet nové uživatele
- můžeme chtít brát **programy jako subekty**
	- nebo raději kombinaci uživatel + program
	
## Naming programs
- uživatelé mohou mí jména, co ohledně programů?
- způsob 1: kryptografické **signatures**
	- **přenositelné** přes počítače, ale **komplexní**
	- **identifikace** založená na **programu samotném**
- způsob 2: i-uzel **spustitelného souboru**
	- jednoduchý, lokální, identifikace založená na **lokaci**
	
## Program as a subject
- program: pasivní (soubor) vs aktivní (procesy)
	- pouze **proces** může být subjekt
	- ale **identita** programu je propojená se souborem
- práva **procesu** závisí na jeho **programu**
	- `exec()` změní privilegia

## Mandatory access control
- přístupová práva spracuje **central authority**
- často spojeno s **security labels**
	- klasifikuje **subjekty** (uživatelé, procesy)
	- a také **objekty** (soubory, sockety, programy)
- určuje kdo, co může
- vlastník **nemůže** změnit práva objektů

## Capabilities
- ne všechny akce (slovesa) potřebují brát objekty
- např. vypnutí počítače
- mounting file systems
- poslouchání na portech < 1024

## Dismantling the `root` user
- tradiční `root` uživatel je **všemocný**
	- porušuje least privilage pravidlo
- mnoho speciálních vlastností `root`a jsou capabilities
	- `root` se pak stane uživatel se všemi capabilities

## Security and execution
- pokud nevíme, co spouštíme, nemůžeme se bavit o bezpečnosti
- bezpečnost řeší co **je možné spustit**
- **arbitary code execution** jsou nejhorší exploits
	- umožňuje **neautorizované** spuštění kódu
	- stejný efekt jako **impersonating** user
	- skoro stejně špatné jako ukradené credentials

## Untrusted input
- program často procesují **data z různých zdrojů**
	- image viewers, audio & video přehrávače
- bugs v programech mohou být **exploited**
	- program může být zneužit k **executing data**

## Process as a subject
- některé privelegia mohou být vázány k danému **procesu**
	- tyto jsou aplikovány pouze po **dobu života** procesu
	- častěji **omezení** než privilegia
	- **privilae dropping**
- restrikce jsou **zděděny** přes `fork()`

## Sanboxing
- snaží se **omezit poškození** způsobené code execution **expliots**
- program **zahodí** všechny privilegia jaké může
	- stane se předtím, než se zpracová jakýkoliv **vstup**
	- útočník je uzavřen s **omezenými privilegiemi**
	- často může zabránit úspěšný útok

## Untrusted code
- tradičně chceme spouštět pouze **trusted** code
	- často založeno na **reputaci** a dalších **externích** faktorech
	- does not **scale** na mnoho vendors
- je časté spouštět **untrusted** code - může to být ok s dostatečným **sandboxing**

## API-level access control
- **user-level resources**
- enforcement např pomocí **virtual machine**
	- nené aplikované ke spuštění **native code**

## Android/iOS permissions
- aplikace z obchodu jsou jako **semi-trusted**
- často **single-user** zařízení
- práva jsou spojeny s **aplikacemi** místo uživatelů