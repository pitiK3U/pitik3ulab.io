+++
title = "09 - Shells & User Interface"
date = 2021-04-30
+++

# Command interpreters
## Shell
- **Programovací jazyk** založený na interakci s OS
- zakládní **control flow**
- **netypovaný**, proměnné jsou většinou textové
- problematické error handling

### Interactive shells
- skoro každý shell má **interaktivní mód**
- uživatel zadává vstup v podobě příkazů na klávesnici
- po potvrzení je příkaz **ihned vykonán**
- toto je základ **command-line interfaces**

### Shell scripts
- **shell script** je (spustitelný) soubor
- v nejjednodušší formě je to **sekvence příkazů**
	- každý na vlastním řádku
	- spouštění skriptu je stejné jako psaní příkazů
- ale může použít **structured programming** constructs

### Shell upsides
- velmi jednoduché k psaní jednoduchých skriptů
- první volba k automatizaci
- často užitečné pro ušetření opakovaného psaní
- určitě **není** vhodné pro velké programy

### Bourne shell
- specifický jazyk v shell family
- první shell s konzistentní programovací podporou (od r. 1976)
- stále velmi používaný
	- nejznámější implementace je `bash`
	- `/bin/sh` je spravován POSIXem

### C Shell
- také známý jako `csh` (1978)
- syntax více jako C než `sh` (ale né jako C)
- zlepšilo interaktivní mód (nad `sh` z 76)
- také stále používaný dnes (`tcsh`)

### Korn shell
- také jako `ksh` z 1983
- něco mezi `sh` a `csh`
- základy POSIX.2 požadavků
- existuje mnoho implementací

## Příkazy
- často název spustitelného souboru
	- také control flow nebo vestavěné příkazy
- spustitelný soubor je vyhledán v file systemu
- shell udělá `fork + exec`
	- pro každý příkaz je nový proces
	- vytváření procesu je poměrně drahé

### Built-in commands
- `cd` změna pracujícího adresáře
- `export` pro nastavení prostředí
- `echo` vypsání zprávy
- `exec` nahraď shellový proces (žádný `fork`)

## Proměnné
- jména proměnné jsou tvořené z písmen a číslic
- **používání** proměnných je označeno `$`
- nastavování proměnných **nepoužívá** `$`
- všechny proměnné jsou globální (kromě subshellů)

```bash
VARIABLE="some text"
echo $VARIABLE
```

### Variable substitution
- **proměnné** jsou nahrazeny jako **text**
- `$foo` je jednoduše změněno s obsahem `foo`
- **aritmetické operace** nejsou moc dobře podporovány ve většině shellů nebo expression syntax (relační operace)
	- `z=$(($x + $y))` pro přidávání v `bash`

### Command substitution
- v podstatě jako proměnná **substituce**
- zapsáno jako `command` nebo `$(command)`
	- prvně se **spustí** daný příkaz
	- a zachytí stdout
	- poté nahradí `$(command)` s výstupem

### Quoting
- whitespace v shellu je **oddělovač argumentů**
- více slovné argumenty musí být **quoted**
- quotes jsou buď `"x"` nebo `'x'` (dvojité podporují var **substitution**)

#### Quoting and substitution
- whitespace z variable substitution musí být **quoted**
	- `foo="hello world"`
	- `ls $foo` je rozdílné od `ls "$foo"`
- špatné quoting je velmi častým zdrojem **bugů**
- názvy souborů s mezerami ?

### Speciální proměnné
- `$?` je výsledek posledního příkazu
- `$$` je PID aktuálního shellu
- `$1` až `$9` jsou pozční argumenty
	- `$#` je počet parametrů
- `$0` název shellu (`argv[0]`)

## Prostředí
- **jako** proměnné shellu, ale né stejné
- prostředí je předáno **všem** vykonávaných **programům**
	- potomek nemůže měnit prostředí rodiče
- proměnné jsou přesunuty do prostředí pomocí `export`
- proměnné prostředí fungují často jako **nastavení**

### Important environment variables
- `$PATH` říká systému, kde programy hledat
- `$HOME` domovský adresář aktuálního uživatele
- `$EDITOR` a `$VISUAL` který editor použít
- `$EMAIL` je emailová adresa aktuální adresáře
- `$PWD` je aktuální pracovní adresář

## Globbing
- patterns for quickly **listing** vícero **souborů**
- např `ls *.c` zobrazí všechny soubory končící s `.c`
- `*` značí jakýkoliv počet znaků
- `?` značí jeden znak
- funguje na **cesty** `ls src/*/*.c`

## Conditionals
- povoluje **podmíněné spuštění** příkazů

## `test`

## Loops

## Case analysis

## Command chaining

## Pipes

## Functions
- můžeme definovat **funkce** v shellu
- často light-weight **alternativa skriptů**
	- není potřeba `export` proměnných
	- ale nemůže být invoked non-shell programy
- funkce může být také **set** proměnných

# The command line

## Interactive shell
- shell zobrazí **prompt** a čeká
- uživatel **napíše příkaz** a zmáčkne enter
- příkaz je **vykonán** ihned
- **výstup** je vypsán na **terminál**

## Command completion
- většina shellů používá tak k **auto-completion**
- interaktivní historie: šipka nahoru - předchozí příkaz (také hledání v historii)

## Prompt
- řetězec, který se vypíše, **očekává se příkaz**
- ovlivňěn `PS1` proměnnou prostředí
- často `username` a `hostname`, ale také `pracovní adresář`, ...

## Job control
- pouze jeden program může běžet v **popředí** (terminálu)
- běžící program může být **uspán** (suspended - C-z)
- a **resumed** v pozadí (background - `bg`) nebo popředí (`fg`)
- použití `&` spustí program v pozadí: `./spambot &`

## Terminal
- může **psát text** a číst z **klávesnice**
- normálně je vše vypsáno na poslední řádek
- text může obsahovat **escape** (control) sequence
	- pro vypisování barevného textu nebo mazání obrazovky
	- nebo také vypís na **specifické souřadnice**
	
## Full-screen terminal apps
- aplikace mohou použít **celou obrazovku** terminálu
- knihovna abstrahuje bokem tyto low-level **control sequences**
	- `ncurces` jako `new curses`
	- různé terminály používají různé control sequences
- existují speciální znaky k vykreslení **frames** a **separators**

## UNIX Text Editors
- `sed` - stream editor, ne-interaktivní
- `ed` - line oriented, interactive
- `vi` - visual, screen oriented
- `ex` - line-oriented mode of `vi`

## TUI: Text User Interface
- program kreslí **2D rozhraní** na terminálu
- často jsou pohodlné k použití
- často jednodušší k **naprogramování** než GUIs
- velmi nízké požadavky bandwidth pro **remote use**

# Graphical interfaces
## Windowing system
- každá aplikace běží ve svém **vlastním okně** (nebo vícero oken)
- **více aplikací** může být na obrazovce
- okna mohou být posunuty, zvětšeny, ...
	- zprostředkováno rámy okolo oken
	- obecně známý jako **window management**

### Windows-less system
- nejpopulárnější na **menších obrazovkách**
- aplikace jsou přes celou obrazovku
- **task switching** pomocí dedikované obrazovky

## A GUI stack
- graphics card **driver**, mode setting
- **drawing** / paiting (často hw-akcelerováno)
- multiplexing (např. používání oken)
- **widgets**: tlačítka, labels, listy, ...
- **layout**: co jde kam na obrazovce

### Well-known GUI stacks
- Windows
- macOS, iOS
- X11
- Wayland
- Android

### Portability
- GUI "nástroje" tvoří **portability** jednoduchou
	- Qt, GTk, Swing, HTML5+CSS, ...
	- mnoho jich běží na **všech hlavních platformách**
- **code** portability není jediný problém
	- GUIs jdou s **look and feel** guidelines
	- přenositelný mohou **selhat k přizpůsobení**
	
### Text rendering
- překvapivě **komplexní** úkol
- oproti terminálům, GUIs používají proměnné pitch fonts
	- problémy jako **kerning**
	- těžko odhadnutelné **pixelová šířka** čáry
- špatná interakce s **tisknutím** (cf. WYSIWIG)

#### Bitmap fonts
- znaky jsou reprezentovány jako **pole pixelů** (často černobílé)
- tradičně pixely nakresleny **rukou** (velmi časově náročné - mnoho písmen, variant, ...)
- výsledek je **ostrý** ale **jagged** (not smooth)

#### Outline fonts
- Type1, TrueType - založeno na **splines**
- mohou být **naškálovány** do obyčejných pixelových velikostí
- stejný font může být použit na **obrazovku** a pro **tisk**
- rasterizace je často dělána v **software**

#### Hinting, Antialiasing
- obrazovky jsou zařízení s **nízkým rozlišením**
	- typický HD displeje mají DPI okolo 100
	- laserové tiskárny mají DPI 300 a více
- **hinting**: deformuje outlines aby lépe seděli do pixelové mřížky
- **anti-aliasing**: smooth outlines using grayscale

### X11 (X Window System)
- tradiční UNIX windowing system
- mé C API (xlib)
- vestavěná **network-transparency** (založená na socketech)
- core protocol version 11 from 1987

#### X11 Architecture
- X **server** poskytuje grafiku a vstup
- X **client** je aplikace, která X používá
- **windows manager** je speciální klient
- **compositor** je další speciální klient

#### Remote displays
- **aplikace** běžící na počítači A
- displej **není** konzole A
	- může být dedikovaný **grafický terminál**
	- může být jiný **počítač** na LAN
	- nebo přes internet

##### Remote display protocols
- jeden přístup je **pushing pixels**
	- VNC (Virtual Network Computing)
- X11 používá vlastní **vykreslovací** protokol
- jiné používají **high-level** abstrakce
	- NeWS (založeno na PostScriptu)
	- HTML5 + JavaScript

##### VNC (Virtual Network Computing)
- posílá **kompresované pixelové data** přes kabel
	- 
	- může posílat **incremental update**
- a **vstupní události** v druhém směru
- není podpora pro **periferie** nebo synchronizace souboru

##### RDP (Remote Desktop Protocol)
- Více sofistikované než VNC (ale proprietary)
- může poslat **kreslící příkazy** přes kabel
	- jako X11, ale používá DirectX na kreslení
	- také podporuje **OpenGL**
- podpora pro audio, remote USB, ...

##### SPICE
- Simple Protocol for Independent Computing Env.
- otevřený protocol mezi VNC a RDP
- může posílat OpenGL (ale jen přes **lokální socket**)
- obousměrné **audio**. USB, **clipboard** integrace
- stále převážně založeno na **pushing pixels** (compressed)

##### Remote Desktop Security
- uživatel musí bý **autentizován** přes síť
	- hesla jsou jednoduché, biometrické data méně
- datový stream by měl být **šifrován**
	- není součástí X11 ani NeWS protcolu
	- nebo ani defaultně pro HTTP