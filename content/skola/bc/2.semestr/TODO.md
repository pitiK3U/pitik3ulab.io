+++
draft = true
+++

# Testy
## MB151
- P1 (látka týdnů 1-5): 
	- [x] Út 6. 4. 2021 18:00 Doma - přes IS
- P2 (látka týdnů 6-10):
	- [ ] [11.5.] 18:00 Doma - přes IS
- Řádný termín: první polovina června
- Opravné termíny: konec června, začátek července.

## PB152
- Vnitra - 8 otázek, 15 min, max 1 chyba (cvičný test 1 týden napřed)
	- [x] [9.4.] 16:00 - 16:30
	- [ ] [7.5.] 16:00 - 16:30
	- [ ] [4.6.] 16:00 - 16:30

# Úkoly
## IB002
- [ ] [Čt] - Odpovědník - od 17:55 do 18:05
- [ ] [St 24.3.] Domácí úkol
- [x] [**čtvrtek 22. dubna 20:00 - 22:00**] - Speciální domácí úkol za 3 body

## MB151
- [x] [Čt 25.3.2021 23:59] Odpovědník - Vektory, ...
- [ ] [Po 31. 5. 2021 23:59] Varianta C - Komplexní čísla

## PB071
- [x] [do Pá 19. 3. 2021 24:00] - Odpovědník - Týden 03
- [x] [bonus 18.3.2021 24:00; final 21.3.2021] - Domácí úkol - [base58](https://www.fi.muni.cz/pb071/hw/homework-01/)
	- [ ] oprava
- [ ] [bonus 1.4.2021 24:00; final 4.4.2021] - Domácí úkol - [sudoku](https://www.fi.muni.cz/pb071/hw/homework-02/)
	- [ ] vypracovaní
	- [ ] nanecisto (10x 5x 5x) 0x 0x 0x
	- [ ] naostro (3x) 0x 
	- [ ] oprava

## PB152cv
- [x] 01
- [x] 02
- [x] 03
- [ ] 04
	- [ ] odevzdáno
	- [ ] opravit

## PV004 UNIX
- [x] [do Čt 8. 4. 2021 14:00] Milník 1 - Úvod

## VB036 English II
- [ ] ROPOTs
- [x] [5.4.] Presentation
- [ ] Presentation style review

