+++
title = "02 - Standardní numerace, problém zastavení, věta o numeraci"
date = 2022-09-22
+++

# Numerace množiny
- Slide 2 - Každému prvku množiny M přiřadíme číslo
    - Příklady:
        - $v(0) = a$, $v(1) = b$, $v(2) = c$, $v(n) = \bot, n > 2$
- Slide 4
    - Příklady $v(n) = 2n$
    - `v'(n) = { n pokud n je sudé, nedef jinak`
    - `v(n) = { n/2 pokud n je sudé, - (n+1)/2 jinak`
- Slide 7
    - `code(P_empty) = ` $ 23^{2^1} \times 29^{7^2 \times 11^1} \times 31^{13^1 \times 17^2 \times 19^1} = 23^2 \times 29^{539} \times 31^{71383} $

## Standardní numerace programů

## Počet indexů funkce
-

## Věta o numeraci
- $\phi$ - univerzální funkce
- "trochu totéž jako interpretery - dáme funkci a argumenty a interpret se chová jako univerzální funkci"

### Párující funkce
- **kódování více proměnných do jedné proměnné**
- kódujeme po diagonále
- prvky v diagonále předtím = $1 + 2 + 3 + ... + (i + j) = \frac{(i + j) \times (i + j + 1) }{2}$
- plus prvky v dané diagonále předtím $i$
- celkem $\frac{(i + j) \times (i + j + 1) }{2} + i$

#### Standardní kódovací funkce
- $\pi_{k1}(\tau_k(i_1, ..., i_k)) = i_1 = \pi_1^{k-1}(i)$
- $\pi_{kl}(\tau_k(i_1, ..., i_k)) = i_l = \pi_2(\pi_1^{k-l}(i)) \text{, pro } l > 1$