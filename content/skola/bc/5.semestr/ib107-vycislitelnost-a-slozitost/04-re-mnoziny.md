+++
title = "04 - r.e. množiny a jejich standardní numerace"
date = 2022-10-12
+++

# Programy jako generátory
```
begin
    n := 1;
    while true do begin
                    n := 2 * n;
                    output(n);
                  end
end
```

- $A = \emptyset$
```
begin
end
```

- `StepCounter` = simulace paralelního počítání sekvenčně - technika paralelního zpracování
- str 5.
```
begin
    n := 0
    while true do begin
                output(f(n));
                n := n + 1
    end
end
```
- str. 12
    - $x \in K \iff \varphi_x(x) \neq \bot \iff \exists y$ takové, že $P_x$ na $x$ zastaví během y kroků

# Alternativní charakterizace r.e. množin
- ve skriptech: efektivní charakterizace r.e. množin
- str 25.
    - Nechť $g = \varphi_e$
```
begin
    x := pi1(x1);
    y := pi2(x2);
    if SC (e,x,y) = 1 then x1 := x
                      else x1 := a_0
end
```