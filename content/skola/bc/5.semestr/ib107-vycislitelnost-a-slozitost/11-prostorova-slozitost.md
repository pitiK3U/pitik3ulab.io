+++
title = "11 - Prostorová složitost"
date = 2022-11-30
+++

# 
## determi..
- slide 14
- $SPACE(f(n)) \subseteq \bigcup_{k \in \mathbb{N}} TIME(k^{f(n)}) = TIME(2^{\mathcal{O}(f(n))})$
- $NSPACE(f(n)) \subseteq \bigcup_{k \in \mathbb{N}} NTIME(k^{f(n)})$