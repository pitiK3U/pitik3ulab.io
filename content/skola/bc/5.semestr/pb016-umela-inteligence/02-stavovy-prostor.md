+++
title = "02 - Prohledávání stavového prostoru"
date = 2022-09-19
+++

# Stavový prostor
- konfigurace všech možností řešení
- **cílová podmínka** = měla by být jednoduchá, volá se po každé změně
- **prohledávací strategie** = nejvíce mění čas řešení (jak vybíráme další stavy)

## Prohledávací strategie
- úplnost = najde všechna řešení
- optimálnost = první najité řešení je nejblíže kořeni
- časová složitost = kolik kroků je potřeba
- prostorová složitost = kolik uzlů stromu je potřeba

### Neinformované prohledávání 