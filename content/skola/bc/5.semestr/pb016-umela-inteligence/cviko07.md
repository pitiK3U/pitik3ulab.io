+++
draft = true
+++

# Příklad 1
a) $\forall x \exists y . u(x) \implies n(y) \land z(x, y)$, z(x,y) student x zná studenta y, u(x) student x je usilovný, n(x) student x je nadaný

b) $\forall x \forall y . (x + y = y + x)$

c) $\forall x \exist y . ( 4 \vert x \implies ( 4 \vert ( x + 4 )) )$

# Příklad 2
a) $x$ 2x volný, $y$ 2x vázaný

b) $2$, $x$, $y \cdot 2$, $y$, $sin( x + y )$, $x + y$, $1$

c) $\implies$, $\lor$

d) $\cdot$ binární, $2$ nulární, $\sin()$ unární, $+$ binární, $1$ nulární

e) $\vert$ binární, $>$ binární

# Příklad 3
a) Ne, formule.

b) Ne, predikát je formule.

c) Ano.

d) Ano.

e) Ne, nerespektují aritu.

f) Ano.

# Příklad 4
a) Ano.

b) ~~Ano.~~ Ne.

c) Ne.

d) Ne.

e) Ne, nerespektuje aritu.

f) Ano.

# Příklad 5
a) $\neg \exist x((P(x)\land Q(x)) \lor R(x))$ = $\forall x((\neg P(x) \lor \neg Q(x)) \land \neg R(x))$

b) $\exist x (P(x) \land \exist y \neg Q(y))$

c) $\exist x (\neg P(x) \land \forall y \neg Q(y))$

d) $\exist x (P(x) \land \neg Q(x)) \lor \forall x (\neg R(x) \lor \neg S(x))$

# Příklad 6
a) Ano.

b) ??

c) Ano.

d) Ne.

# Příklad 7
a) $D_I = \mathbb{R}$, $I(P) = \emptyset$

b) $D_I = \mathbb{R}$, $I(P) = $

# Příklad 8
a) $\alpha(x) \equiv (x + x = x)$

b) $\beta(x,y) \equiv \exist z . ( K(z) \land x + z = y)$

c) $\gamma(x, y) \equiv (x + y = 0)$

d) $\delta(x) \equiv \exist y .(K(y) \land x < y)$

# Příklad 9
a) Ano.

b) Ano.

c) Ne.

d) Ne.

e) NE