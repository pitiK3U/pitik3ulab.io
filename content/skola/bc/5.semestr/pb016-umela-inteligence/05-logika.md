+++
title = "05 - Logika, Výroková logika"
date = 2022-10-10
+++

# Klasifikace logik
- predikátová
    - 1\. řádu = kvantifikátory proměnných
    - 2\. řádu = kvantifikátory nejen proměnných

## Výroková logika syntax
- `NAND`, `NOR` - lze vyjádřit negaci (!p = NAND(p,p)), konjunkci a disjunkci = úplný systém
- unární verum = všechny výsledky jsou nějaké
    - unární verum ?? = vždy pravda
    - unární verum falsum = vždy nepravda
    - unární verum projekce: pravda = pravda, nepravda = nepravda

p | g | b.f.[^bf] | NOR | &not;(g &rArr; p) | &not;p | &not;(p &rArr; g) | &not;g | XOR | NAND | p &and; g
--|---|---|---|---|---|---|---|--|--|--|
0 | 0 | 0 | 1 | 0 | 1 | 0 | 1 | 0 | 1 | 0 |
0 | 1 | 0 | 0 | 1 | 1 | 0 | 0 | 1 | 1 | 0 |
1 | 0 | 0 | 0 | 0 | 0 | 1 | 1 | 1 | 1 | 0 |
1 | 1 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 1 |

[^bf]: binární falsum

## Normální formy
- úKNF (úplná konjunktivní norm. forma): pro všechny 0 napíšeme formule, pomocí disjunkce &or;, aby vraceli nulu, pak vše formule spojíme pomocí konjunkce &and;
- úDNF: naopak