+++

date = 2020-10-19
+++

## Příklad 1

a) $\neg m \implies \neg f \lor \neg e$
- $m$ = motor běží
- $f$ = motor je funkční
   - $e$ = elektřina jde

b) $(a \land n) \implies (a \lor \neg n)$
- $a$ uchazeč mluví anglicky
- $n$ uchazeč mluví německy

Ano, b je tautologie.


## Příklad 2

| p | q | r | | p &and; &not; r | r &and; &not; q | (p &and; &not; r) &rArr; (r &and; &not; q) |
|---|---|---|-|---|---|---|
| 0 | 0 | 0 | | 0 | 0 | 1 |
| 0 | 0 | 1 | | 0 | 1 | 1 |
| 0 | 1 | 0 | | 0 | 0 | 1 |
| 0 | 1 | 1 | | 0 | 0 | 1 |
| 1 | 0 | 0 | | 1 | 0 | 0 |
| 1 | 0 | 1 | | 0 | 1 | 1 |
| 1 | 1 | 0 | | 1 | 0 | 0 |
| 1 | 1 | 1 | | 0 | 0 | 1 |


a) Ne.

b) Ano.

c) Ne.

d) Ne.

e) Ne. viz. a)


p | q | r | (r implies p) ∨ ¬(q ∧ r) |
|--|--|--|--|
| T | T | T | T |
| T | T | F | T |
| T | F | T | T |
| T | F | F | T |
| F | T | T | F |
| F | T | F | T |
| F | F | T | T |
| F | F | F | T |

a) Ne.

b) Ano.

c) Ano.

d) Ne.

e) 1., 2.

f) Není

## Příklad 4.
| p | q | r | |(p &and; &not; r) &rArr; (r &and; &not; q) | (r implies p) ∨ ¬(q ∧ r) |
|---|---|---|-|---|---|
| 0 | 0 | 0 | | 1 | 1 |
| 0 | 0 | 1 | | 1 | 1 |
| 0 | 1 | 0 | | 1 | 1 |
| 0 | 1 | 1 | | 1 | 0 |
| 1 | 0 | 0 | | 0 | 1 |
| 1 | 0 | 1 | | 1 | 1 |
| 1 | 1 | 0 | | 0 | 1 |
| 1 | 1 | 1 | | 1 | 1 |

- $\varphi$
    - ÚDNF: $(\neg p \land \neg q \land \neg r) \lor (\neg p \land \neg q \land r) \lor (\neg p \land q \land \neg r) \lor (\neg p \land q \land r) \lor (p \land \neg q \land r) \lor (p \land q \land r)$
    - ÚKNF: $(\neg p \lor p \lor q ) \land (\neg p \lor \neg q \lor q)$
- $\psi$
    - ÚDNF: $(\neg p \land \neg q \land \neg r) \lor (\neg p \land \neg q \land r) \lor (\neg p \land q \land \neg r) \lor (p \land \neg q \land \neg r) \lor (p \land \neg q \land r) \lor (p \land q \land \neg r) \lor (p \land q \land r)$
    - ÚKNF: $(p \lor \neg q  \lor \neg r)$

## Příklad 5
a) $(p \land \neg q) \lor r$

b) $(p \land \neg q) \lor (\neg p \land q) \lor r \lor s$

## Příklad 6
a)

b) $((\neg p \lor q) \land (\neg p \lor r)) \lor ((p \land \neg q) \land (p \land \neg r))$

## Příklad 7
a) Nelze.

b) $r = q = \neg p = 1$

## Příklad 8
a) Ano.

b) Ne.