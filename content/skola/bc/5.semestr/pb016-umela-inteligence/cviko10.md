+++
draft = true
+++

# Příklad 1
a) ×, #, ␣: konstanty pro značky „křížek“, „kolečko“, „prázdné políčko“
b) MarkOf(p): funkce, reprezentuje značku hráče p
c) TurnAt(s): funkce, reprezentuje hráče, který je na tahu v situaci s
d) MarkAt(q, s): funkce, reprezentuje značku na poli q v situaci s

# Příklad 2
a) $\forall p . Player(p) \Leftrightarrow p = p_\times \lor p = p_\bigcirc$

b) $Opponent(p_\times) = p_\bigcirc$

c) $MarkOf(p_\bigcirc) = \bigcirc$

d) $WinningPosition(q_{11}, q_{21}, q_{31}), ... $
$WinningPosition(q_{11}, q_{12}, q_{13}), ...$

# Příklad 3
a) Ne. zvířecí.

b) Ano.

c) 4

d) hnědá

# Příklad 4
1. Buckbeak
    - instance: hipogryf
    - nohy: 2
    - část: křídla
    - podtřída: napůl kůň
    - barva: hnědá

2. Firenze
    - zaměstnání: učitel
    - instance: kentaur
    - inteligence: lidská
    - podtřída: napůl kůň
    - nohy: 4

3. napůl lev
    - podtřída: tvor
    - inteligence: zvíření

# Příklad 5
- napůl kůň
    - podtřída: tvor
    - *nohy: 4
    - *barva: hnědá
- Firenze
    - *zaměstnání: učitel
    - instance: kentaur
    - inteligence: lidská
    
# Příklad 6
a) Ano. protože sum pro kazdy prvek z omega musi byt 1

b) Necht B = Omega - A, pak P(B) je [0,1], a P(A u B) = 1 = P(A) + P(B)

c) Omega = {a,b,c}, A = {a, b}, B = {b, c}, pak P({a,b,c}) = 1, ale P(A) + P(B) = 2/3 + 2/3 = 4/3, zbytek důkazu je ponechán na čtenáře na procvičení :)

# Příklad 7