+++
title = "04 - Hry a základní herní strategie"
date = 2022-10-09
+++

# Hry
- **hra v UI**
    - *Zero-sum* buď výsledek opačný nebo remíza
- Adversarial search
    - sdílené prohledávání, vždy prohledává opačná strana

# Ohodnocovací funkce
- **Důležité** je **správné nastavení váh** jednotlivých rysů