+++
draft = true
+++


# Příklad 1
a) intenzio

b) extenzi

c) intenzi

d) extenzi

e) extenzi

f) intenzi

g) intezni

h) extenzi

# Příklad 2
a) 8

b) 4

# Příklad 3
a) $\diamondsuit prší \lor \diamondsuit svítí$

b) $\diamondsuit (prší \land svítí)$

c) $duha \implies \square(prší \land svítí)$

# Příklad 4
a) ne

b)


# Příklad 7
a) Ano. 1/2

b) Ano. 1

c) Ano. 1/2

d) Ne. 0

# Příklad 8
a) 0.6

b) 1.0

c) 0.5

d) 0.7

# Příklad 9
- p = 0, r = 0