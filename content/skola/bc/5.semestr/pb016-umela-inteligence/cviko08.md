+++
draft = true
+++

## Příklad 4
a)
$\exist z (\forall y (\exist x P(x,y) \implies Q(y,z)) \land \exist y(\forall x R(x,y) \lor Q(z,y)))$
$\exist z \forall y \forall x \exist i \forall j ( ( \neg P(x,y) \lor Q(y,z)) \land ( R(j,i) \lor Q(z,i)))$

b) $\exist z \forall y \forall x \exist i \forall j ( ( \neg P(x,y) \lor Q(y,z)) \land ( R(j,i) \lor Q(z,i)))$
$\forall z \forall y \forall x \forall i \forall j ( ( \neg P( x,y) \lor Q(y,f(z))) \land ( R(j,h(i) ) \lor Q(f(z), h(i) )))$

## Příklad  5
a) $x/g(a), v/g(a); y/b, w/b; z/c, u/c$

b) $y/a, x/g(b), w/f(b); v/b$

c) $x/g(d); z/f(g(d),g(c)); y/d, w/g(c), u/g(d)$

## Příklad 6
a) Předpoklady:
- $\exist x D(x)$
- $\forall x (D(x) \implies (S(x) \lor L(x)))$
- $\forall x ((D(x) \land H(X)) \implies \neg S(x))$
- Závěr: $\forall x ((D(x) \land H(x)) \implies L(x))$
- Negace závěru: $\exist x(D(x) \land H(x) \land \neg L(x))$

c)
- $D(a)$
- $(\neg D(x) \lor S(x) \lor L(x))$
- $((D(x) \land H(X)) \implies \neg S(x))$
- $\{D(a)\}$
- $\forall x (\neg D(x) \lor \neg H(x) \lor L(x))$