+++
title = "04"
date = 2022-10-18
+++

# Tipy extraktů
## Standardizované
- marker = nějaká látka, které je tam dostatečně mnoho a lze jednoduše zjistit její množství, má vztah k účinnosti extraktu

## Kvantifikované
- marker = nemusí mít vztah k účinku

## Ostatní
- dány postupem, nejsou definované markery a nemusí ani být známy

# Příklady

## Propolis
- včely napatlávají tím úl zevnitř, impregnují
- balzamující směs na mumie
- pro zlepšení imunity, antivirový pro ústní použití, antibakteriální

## Česnek setý (Allii sativi bulbus)
- inhibice agregace krevní destiček (srážení krvinek)
- aliin - hypofilní silice - těkavá
- mnoho netěkavých látek
- vitamínferon = zdroje vitamínu, ale nejsou dostatečně významné = museli bychom sníst neskutečně velké množství aby měli význam 

## Konopí
- extrémně potentní = látková továrna

## Ženšen
- adaptogen = posílení vitality, zvýšení výkonu, proti efektu "stárnutí"
- **steaming** = aplikace horké páry 120 °C po dobu 3 hodin

## Lékořice
- na vykašlávání, antibakteriálně, zvyšuje kortikoidy 