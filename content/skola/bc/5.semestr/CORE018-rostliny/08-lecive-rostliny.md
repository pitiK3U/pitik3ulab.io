+++
title = "08 - Léčivé rostliny a koření"
date = 2022-12-06
+++

- Lidstvo přišlo, že když určitým způsobem použijí určitou rostlinu mohu ji zakonzervovat na delší dobu a jejich účinek může být prospěšný. 

# Koření

## Gastroinestinální trakt
- Digestiva (eupeptia) - sekrecí žaludku, slinivku
- Stomachika, amara - stimulace žaludku - zvyšují chuť k jídlu
- Antacida, Antiulceróza - stresové, "pálí nás žáha" 
- Cholagoga - modulují sekreci žluči - trávení tučných jídel, (sideeffect - odvádíme toxické věci pro lidi, pomáhá játrům)
<!-- markdown cheats -->
- Karminativa - zabraňují plynatosti
- Spasmolytika - rozpouští křeče hladkých svalů
<!-- markdown cheats -->
- Emetika - problém se zvracením, toxická látka dojde ke kontaktu s vrškem těla - může být problém; spíš žaludeční výplach
- Antiemetika - pro nás lepší, snížení zvracení

### Léčiva a dutiny ústní a hltanu
- Pokud má působit v ústní dutině, musí tam nějakou dobu být - ústní vody, puntkury
- Antiseptika
    - heřmánek
    - šalvěj
    - rebarborový kořen
- Adstringencia - afty a jiná ústní poranění
    - v sliznici interagují s různými proteiny
    - paralelně působí antisepticky
    - kořen mochny
    - ratanový kořen
    - duběnky
    - kúra dubu?
    -
- protektiva (chránění - slizy - brání dalšímu dráždění sliznice, zakryje)
    - Islandský lišejník (obsahuje ještě lišejníkový kyseliny - antibakteriální)
- baktericidní látky
    - propolis - včely viz kap. 1, !!alergenní materiál!!
    - Lysozym - na některé proteiny výrazné antibakteriální a antiflogistický účinek

### Léčiva ovlivňující trávící činnost
#### Digestiva
- Dyspepsie - příznak při poruchách trávení
    - druhotná - může být vyvolána změnou kolonií v trávicím traktu
    - funkční - chyba v životosprávě (tučná jídla), psychosociální vlivy (stres, obavy, úzkosti, napětí)
- Léčiva:
    - doplňování chybějících enzymů - substituční terapie
    - stomachika - kořeniny - zvýšení vylučování žaludečních šťáv

#### pankracea
- proteiny, škroby, tuky

### Pepsín
- lze nahradit papájou
- ze šťávy lze získat protein - **papain**
- funkční do 60-70C -> masoprůmysl (změkčení masa) a prací prášek

### Kapsaicin
- pálivé papriky
- Scoville hot unit - jednotka pálivosti, ředění - kolikrát musíme naředit, aby nepálila
- dihydrokapsaicin - postupné pálení a postupné přestání
- zvyšuje sekreci žaludku
- dostatečným množstvím a aplikací např. náplasti znehodnotí receptor 
- Dobrá tolerance
    - extrémní pálení, dráždění (s použitím pepřáku jsou zaznamenány úmrtí šokem) 
    - lipofilní - pozor na šáhnutí do oka, **nejde umýt**!! - umýt saponátem nebo nejlépe lihem

### Piperis nigri fructus

### Puškvorec
- aromatické rostlina, výrazná vůně a chuť
- dostala se k nám od Asijských nájezdníků
- množí se vegetativně - snadno pěstovat
- často do nějakých likérů

### Muškátové semeno
- třeme oříšky o sebe a prášek napadá do jídla
- silice je kořenina a taky psychoaktivní složka (Myristicin, )
- dráždění v malé pánvi - abortivum,
- v některých dávkách může vyvolávat křeče
- opatrně!

### Zázvor
- Zingiberen a zingiberoi - voní jako zázvor
- gingeroi, shogaoi - chutná jako zázvor
- různé žvýkačky, lízátka proti zvracení

### Kardamom
- Indie, Jižní Amerika
- kardomomová káva
- jedno z nejdražších koření

### Skořice
- skořicová kůra
- 2 skupinové látky
- eugenol a **skoř. aldehyd** - vůně
- farmacie: **eugenol** a skoř. aldehyd

### Hřebíček

### Šafrán
- Francie, Írán
- drahé koření
- používají se pouze tyčinky

## Amara - hořčiny
- látky které jsou pouze hořké - **používá se pouze pro hořkou chuť**
- stimuluje chuť k jídlu
- žaludeční likér
- života budič
- **chmel** - používají se pouze samičí rostliny
    - lupulin - jakoby hašiš, ale z chmelu
    - silice vůně
    - pryskyřice - chuť

### Lupuli flos a Lupulinum
- sedativum - pouze 1 pivo, jinak alkoholické účinky, případně čajovina
- výroba piva

### Kondurango
- steroidy - rekonvalescence