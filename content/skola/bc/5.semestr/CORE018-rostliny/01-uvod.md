+++
title = "01 - Historie, rostlinné metabolity"
date = 2022-09-13
+++

# 
- **Farmakognozie** (řec. „pharmakon“ = léčivo, „gnosis“ = poznání) 
- **Fytoterapie** (z řeckých slov fyton, tj. rostlina a therapeuein, tj. pečovat, léčit) = **bylinkářství**
- **první izolovaný alkaloid 1806 - morfin** z opia
- mák => opium, 95% na heroin
- droga = zpracovaný materiál (např. sušení) z rostliny, živočicha nebo mikroorganismu
- zkreslování rostlin = sucho, uv záření

# Tkáňové kultury
- Shikonin = barvivo
- Ubichinon = Q10
- Taxol = proti rakoviny vaječníku = cytostatikum = brání buněčnému dělení = nízká koncentrace, špatně se získává, (z kůry tisu)
- Berberin = diabetes, tuková degenerace jater

# Toxicita
- nedozrálé malé tvrdé dýně
- toxicita cuket - přestárlé nebo napadené houbami = hepatotoxické a glykemie
- antidot:
    - proti otrávení hned velmi silný černý čaj