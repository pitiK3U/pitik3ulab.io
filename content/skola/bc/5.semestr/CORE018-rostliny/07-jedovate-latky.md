+++
title = "07 - Jedovaté látky a rostliny"
date = 2022-11-22
+++

# Jedovaté rostliny

## Dafnetoxin, mezerein
- Lýkovec jedovatý
    - jedovaté plody i listy - odporná, hořká chuť
- Vyhledat lékařskou pomoc!
- Nesnažit se vyvolat zvracení - hrozí vstřebání více jedovatých látek

## Taxol
- Tis
    - Pacifický: roste na Západě USA, jed z dřeva, asi 20-30 kg neboli několik tisíc stromů na použitelné množství
    - Evropský: látka přítomná v jehličí, není potřeba kácet stromy
- **mitotický jed**
    - využívá dělení buněk - rychlé množící buňky - spermie, krevní buňky a tenké tlusté střevo

## Taxin
- taky v Tisu, větší množství než Taxol
    - jediná dužina míšku tisu není toxická, vše ostatní ano i semeno je potřeba vyplivnout
    - dužina není nechutná, semínka hořká

## Toxické triterpeny
- Kukurbitaciny A-V, Q1
- plody
    - cukety - pouze za podmínek špatného skladování, napadení houbou/plísní, staré cukety - veliké, dvoubarevné, není pěkný vnitřek - **zlikvidovat**, neodkrajovat!
    - citrusy
    - okrasný dýně
    - pouštní meloun
- Toxicita
    - Neurotoxicita 

## Toxické antrachinonové glykosidy
- průjmy
- Intoxikace
    - Obvykle předávkování léčiva - laxativa
- chronická intoxikace
    - laxative abuse syndrom
        - změna barvy moči/
- Akutní otravy
    - Ztráta vody a elektrolytů
        - hypokalémie - problém pro starší lidi, ztráta draslíku (banány)

### Hypericismus
- Třesalka
- reaguje se slunečním zářením -> nesmí na sluníčko

## Chinolizidinové alkaloidy
- podobné látky: vlčí bob 

### Cytisin, N-methylcytisin
- zlatý déšť, janovce
- substituce nikotinu, ale není nikotin, podobné otravy
    - v balení na odvykání cigaret je dostatečné množství na intoxikaci
- otravy
    - náhodné
    - sebevražedné pokusy

### Koniin
- Bolehlav
- značně bolestivá a krutá smrt, paralýza a bolest svalstva
- extra jedovatá záležitost

## Pyrrolizidinové alkaloidy
- heliotropium - otočník
- kostival - jedovatý, nepoužívá se perorálně
- u rostlin v kořenech, rozpustné ve vodě
- hepatotoxicita
    - buňky se zvětšují
    - jaterní buňky se vymění za tukovou tkáň
    - zničí játra
    - látky po zničení jater pokračují do těla -> smrt, jde to do plic/srdce
    - **podběl** - né déle než 1 týden, obsahuje tyto látky, nebezpečné pro tehotně ženy

## Kolchicin
- Záměna ocúnu a medvědího česneku
- rostou na stejné lokalitě
- při odvětení ocúnu vypadá podobně jako mědvedí česnek
- používá se na léčení Dny (potřeba vyloučit káva, čaj, maso, zelenina, ovoce)
- mitotický jed
- toxicita
    - vypadávání vlasů

## Brucin, strychnin
- malé dávky strychninu se používalo na zvýšení pozornosti
- intoxikace tetanu je podobná křečím
    - přecitlivělý na zvuky, světlo
- používal se na krysy, nepoužívá se, pochopili to - mají ochutnávače

## Diterpenové alkaloidy
- **Akonitin**
    - látka vzhledem k struktuře labilní
    - velká toxicita, ale rychle se rozpadá = špatně zjistitelné
    - "dračí zuby" - tvrdá látka

## $\alpha$-Solanin
- brambory - květy a nadzemní plody, nadě

## Toxické aminokyseliny
- aminokyseliny pro nás důležité pro proteiny
- přímé působení
    - přes trávící trakt
    - problém v těhotenství
        - může působit abortivně
- nepřímé
    - metaoxidace mastných kyselin
    - zabraňuje energetické využívání tuků -> **smrtící** -> organismus plně naběhne na plné zpracování sacharidů = glukózy, nemáme ji moc (v krvi, v játrech, svaly a kůže) -> rychlé vyčerpání  -> hypoglykémie
    - Strumigeny = hormony štítné žlázy, reakce zvýšením svých buněk, zhutňuje se, ale nedaří se, tyroxinu je málo
        - rosliny brazikace = v křenu, hořčicí = štiplavé látky
    - Metabolismus selenu = rostliny obsahují selen obsahující mastné kyseliny, selen, -> likviduje donory SH skupin, potřeba pro různé detoxikační účely 
- různé fazolovité rostliny
    - hrachor
- Proteinové AMK
    - glutamát = reakce, poškozují se membrány, až destrukce nervových buněk
    - syndrom čínských jídelen
    - pro neurotoxický syndrom je potřeba hodně velké množství glutamátu (asparátu) 

## Hypoglycin B a A
- musí být dostatečně zralá, jinak jedovaté
- hluboká hypoglykémie (zvlášť u dětí)

# Toxické proteiny
## Lektiny (fytohemaglutininy)
- Jedny z nejvíce toxických rostlin!
- Váží se na cukerné buňky a vysráží je ven
- Inhibice proteinové syntézy eukaryot! 
- Některé lektiny
    - zabíjení rakovinných buněk - jmelí
- Výskyt v rostlinách
    - Semena Fabaceae (tepelnou úpravou se látky rozpadají)

### Abrin
- tvrdé fazole - nejde jíst
- používá se na náramky - vrtání, prášek vstřebají
- Extrémně jedovatá rostlina!
- látka se dostane dovnitř buňky a zevnitř ji zabije
- pro myš smrtelná dávka 25 ug, pro člověka v přepočtu asi 100 mg

### Ricin D
- z třídy Euphorbiaceae
- ricínový olej - laxativum
- orální intoxikace - ricinismus - 1mg smrtící dávka pro člověka
- značně toxická látka!
- často součástí bojových toxinů

## Kyanogenní glykosidy
- uvolnění kyanovodíku - relativně toxická látka
- meruňky, broskve a mandle - (především semínka)

### Maniok
- pěstuje se kvůli hlíze -> mouka -> pečivo
- v rozvojových zemí 3. nejpoužívanější rostlina?
- toxická při špatném zpracování

# Organismy
## Sinice
- vývojově staré
- vodní květ = tvorba pěny na jaře
- modrozelená barva, ale i červené (red-tide = krvavý příliv)

## Řasy
- potřeba monitorovat (i sinice), kvůli množství

## Neurotixické a paralytické jedy
- spíše teplejší oblasti
- **Shellfish poisons** = chytají se na měkkýše
- při přemnožení se dostanou na měkkýše a při sběru jsou posbíráni
- značná intoxikace - může být problém pro turisty, látka je sezónní

## Tetrodoxin TTX
- 30 mg na kilo - velmi toxická látka! = působí během pár minut
- tetraodon pufferfish - neprodukuje ryba, ale nějaké bakterie

## Domoová kyselina
- Amnestic shellfish poisoning
- někdy krátkodobě podobné Alzheimerově nemoci
- ztráta krátkodobé paměti dlouhodobě

## Antracis
- mohou být otráveni bubenící - při hraní na bubny se šíří jednotlivé buňky
