+++
title = "02 - "
date = 2022-10-08
+++

# Renesance
- W. Withering
    - nové: zavedení náprstníku do léčení, formulováno s popisem, dávkováním, ...
- **Carl Linné**
    - botanická a zoologická systematika - **dvojslovné názvy** - používají se dodnes
- William Piso
    - kořen *Ipeca*
        - liána, obsahuje alkaloidy, vyvolávají zvracení
        - antidoty při požití jedu
- B. Orfila
    - "TRAITE DE POISONS"
- asd
    - emetin při zkoumání Ipeca
- Claude Bernard
    - zkoumal šípový jedy
- P. Touery
    - strychninem
        - inhibitor inhibiční aminokyseliny
        - všechny vnější podměty reaguje organismus křečemi (na silný zvuk, hmatový podměty, světlo/tma)
        - zábrany: absorbenty při perorálním podání
- 1860 - 1870 - vynález injekční stříkačky - spojení s kokainem a morfín
- R. Kobert
    - námel: paličkovce mechová
    - houba produkuje hodně alkaloidů
    - nejznámější: LSD
    - zkoumání:
        - zábrana po porodního krvácení
        - stahování dělohy
- bazické látky: hlavní látka dusík
- Geiger a Hesse - **atropin**

## Jedovaté a psychoaktivní rostliny
- artemisinin - izolace pro léčbu malárie, jako ostatní dojde k resistenci

## Biogenese přírodních látek
- založené na fotosyntéze = fixace oxidu uhličitého
- -> jednoduché cukry
- primární metabolity = jsou skoro pro všechny živočichy stejné
    - prvky energetického metabolismu
    - "jednoduché stavební prvky"
- sekundární metabolity
    - specializované látky, které fungují v sekundárních metabolismů
    - energeticky náročný

### Proč to dělají
- sekundární metabolity jsou náročné na vytvoření
- potřeba aminokyselin, které jsou důležité, protože obsahují dusík, kterého je málo
- proč "vaří" místo toho aby narostly biomasu
- nevíme

- oleandr = vysoce toxický

## Kategorie podle vzniku
5. Isoprenoidy = vůně

