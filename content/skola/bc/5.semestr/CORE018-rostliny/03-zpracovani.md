+++
title = "03 - Zpracování léčivých rostlin, jejich částí a drog"
date = 2022-10-11
+++

# Zpracování
## Obsahové látky
- hlavní = mají účinek, efekt
- vedlejší = modulují vstřebávání, jak rychle se z organismu dostanou látky
- balastní látky = látky, co nepotřebujeme, např celulóza

## Zpracování
- Vodné extrakty - instantní produkt = káva

## Typy extraktů
- macerace = položení látky do (studené) vody, trhá dlouho a často neefektivní
- tekuté a husté = přímé použití
- suché = lépe se skladují -> další výroba
- Důležitá znalost **droga - extrakt**
- sonifikace = ultrazvuk = rozvibrování částic

## Extrakce
- solid liquid extraction
- **Fickův difuzní zákon**
    - $\frac{\Delta n}{\Delta t} = - (\frac{DA}{h}) * (c_0 - c)$
    - $A$ - styčný povrch - když povrch upravím, tak difuzi zrychlím
        - nějaké menší částice = není homogenní = není tak predikované
        - menší částice usedají a vznikne jedna větší = chování jako velké částice
    - $h$ - difuzní vrstva = za běžných podmínek není tak ovlivnitelná
    - $(c_0 - c)$ = koncentrační spád
    - zrychlení 
        - můžeme míchat
    - daný extrakt slejeme a extrahujeme znovu (chceme maximálně využít čaj = uděláme jeden čaj, vyndáme sáček a zalijeme teplou vodou znovu, ...)

### Zvýšení účinnosti extrakce
- matrix efekt = soubor všech bariér, které brání extrahovat danou látku
    - volba rozpouštědla = důležitá
    - rozdrcení = narušení matrix efektu

#### Perkolace
- relativně účinný = ze shora neustále přitéká čerstvé rozpouštědlo

#### Soxhlet
- nevýhody
    - musím zahřát - zahřeje i dané látky

#### Destilace s vodní parou
- extrahuje těkavé látky
- pára a odpařená silice
- hydrolát = růžová voda (pro alkohol = aromatický líh)
    - na opláchnutí rukou před jídlem ve východních zemí
    - např. při destilaci pomerančových květů