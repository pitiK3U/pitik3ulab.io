+++
title = "06 - Omamné a psychotropní přírodní látky"
date = 2022-11-01
+++

## Thujon
- toxické efekty - centrální nervová soustava
    - halucinace, delirium, poškození ledvin, jater - kumulativní - poškození se prohlubuje s požíváním 
- Pelyněk - absint
    - v prvním kroku je barva bílá
    - v posledním kroku zelená barva
    - musí obsahovat < 35 mg thujonu na litr!
- šalvěj = antibakteriální, pro zlepšení trávení
- vnitřní použití diskutované kvůli thujonu = neměla by se používat vnitřně, spíš udělat odvar

## Konopí
- obsah THC
- účinky:
    - psychika - efekt na cns
    - plíce - podobně jako u kouření tabáku
        - rakovina plic
    - plodnost (ovlivnění spermií), vliv na plod (pomalejší rozvoj dítěte)
    -

## Salvinorin A
- z jižní části Sev. Ameriky
- špatně se pěstuje, dobré je řízkování
- kytka je v našich podmínkách nelegální
- nesnáší se s ostatními drogami

## Nikotin
- velmi návykové = psychický i fyzický návyk - spojeno se sociální stránkou
- Velmi toxický
- Časté otravy
- Čaj magorák - ve vězení, luhování tabáku
- Perorální podání 40-60 mg smrtelná do 10 minut (přibližně, záleží na zvyku)
- Kouření: bolest hlavy, bledost, studený pot, třes rukou, závratě, nauzea a zvracení

## Arekolin
- Areková palma + arekové semena
- Jihovýchodní Asie
- Vyšší dávky ovlivňují i nikotinové receptory
- 300 milionů uživatelů
- Problém alkalizace - v dutině ústní velmi dráždí -> rakovina dutiny ústní

## Kokain
- rostlina cocca
- výrobci: Kolumbie, Peru
- používání listů koky je už od indiánů
- zlehčovalo drsné podmínky - stimulace CNS
- kokain - doporučován jako náhrada za morfín
- omamná psychotropická látka
- v coca-cole do roku 1904
- mechanismus účinku:
    - Nepřímé sympatomimetikum: zablokuje se systém- nesmí se blokovat moc dlouho
    - Blokuje iontové kanály neuronů (porucha šíření vzruchu)
    - adrenergní stimulace = adrenalin
- Periferní účinky
    - nejčastější komplikace - smrt na zástavu srdce
- Centrální stimulace
    - Euforie (hypereuforie - myslí si, že všechno zvládnou, "superhrdinové")
    - paranoidní psychóza -> vyřazení ze společnosti
- Způsob užití
    - Chlorid - sůl
        - šňupání - není propojení, umírají buňky, rozbití chrupavky
    - Báze
        - Kouření (crack - dělá to zvuk)
        - popáleniny
        - špatně tvarovaní nehty - vypadají jako ptačí zobáky

### Tropanové alkaloidy
- podobné
- Rulík - bobule, zvonkové květy, často v pohádkách při použití k otrávení - nejsou odporné!! nepolykat!!
- Durman
- intoxikace
    - zčervenání tváří, suché sliznice - není jim rozumět
    - parasympatolytika
        - velké zornička, špatné vidění na blízko, může trvat dlouho
    - při velké dávce - respirační selhání - smrt

## Morfin, kodein, heroin
- z máku
- latexové kanálky
- opiody - syntetické opiáty, které reagují se stejnými sensory
- endorfiny - "hormon štěstí"
- ovlivnění respirace - útlum dechového centra velmi závislá na způsobu aplikace - při dostání až do mozku dojde až k usmrcení - řešení - rychlá aplikace opiátových antagonistů

## Kratom
- rostlina mitragynin
- reaguje podobně jako opiody z opiátovými sensory
- návykový - rychle konzumuje větší množství
- hepatotoxický
    - poškozuje játra
- problém složení - nekontroluje se a je z pochybných zemí
    - těžké kovy
- problém s řízením!, není regulováno státním orgánem

## Tryptaminy
### Bufotenin

### $\Beta$-karbolinové indolové alkaloidy

### Psilocyn, psilocybin
- fosforovaný
- houby psylocybe - lysohlávky
- počáteční příznaky:
    - neklidní, zívání, křeče končetin, nausea, zvracení
    - podobné nastupující migréně
- psychické příznaky
    - změny nálad
    - když se člověk cítí dobře, tak funguje jako lupa, zvětší efekt
    - když je člověk v nepohodě, stále jak lupa, efekt zintenzivní, odstrašující efekt
    - poruchy vnímání času, směru i vzdálenosti, falešné představy
- akutní toxicita
    - poměrně nízká (smrtelná dávka psilocybinu pro člověka je asi 17 gramů)
    - riziko neuvážených ukvapených rozhodnutí - vnímáme jinak čas a prostor - přejede nás auto, skok ze stromu
    - latentní psychické choroby (např. schizofrenie)

## Ergoliny
- Ergin
    - odumírání končetin, hlavně v dolní části končetin

## Ibogain, tabernathin
- stromy/keře, ze kterých se používají kořeny (někdy dřevo)
- používali se na přípravu nápojů s rituálním spojením (přechod chlapce na muže)

## Mezembrenon, mezembrin, mezembrinol
- rostliny rodu Sceletium - Kosmatec
- z jižní Afriky
- u nás pod názvem: channa
- kokainový účinnek
- zahání hlad a žízeň, stimuluje náladu

## Aminy
### Efedrin
- chvojník
- Sympatomimetická aktivita
- používán na nosní sliznici

### Khataminy
- Habešský čaj
- rostlina se žvýká
- lipofilní látky
- Florida - dobré podmínky pro růst, potřeba čerstvé na použití - není tolik rozšířené
- stále špatně prozkoumané látky