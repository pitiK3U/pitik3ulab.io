+++
title = "Přehled"
date = 2022-09-12
+++

# PB016 - Úvod do umělé inteligence
- [https://nlp.fi.muni.cz/uui/](https://nlp.fi.muni.cz/uui/)

## Hodnocení předmětu:
- **dotazníky na cvičení** (11 x 2  = max 22 bodů)
    - nutná podmínka k závěrečné zkoušce **>= 8 bodů**
- **průběžná písemka** (20 bodů)
    - v týdnu po 7. přednášce, jediný termín
- **závěrečná písemka** (max 58 bodů)
    - 2 řádné a 1 opravný
- hodnocení - max 100 bodů
    - A > 91 bodů
    - **E > 63 bodů**

## Cvičení
- povinná, max 3 absence
- nemoc - odpovědník???????

# IB107 - Vyčíslitelnost a složitost
- Slajdy nejsou primární vzdělávací zdroj!!
- K učení na zkoušku **primární zdroj jsou skripta.**

## Domácí úkoly
- Během semestru zadáme **tři domácí úkoly** (přibližně na konci první, druhé a třetí čtvrtiny semestru).
- Z každého úkolu bude možné získat až 6 bodů.
- Úkoly nebudou povinné, ale k úspěšnému absolvování bude třeba získat z úkolů alespoň 9 bodů.
- Body získané z úkolů nad tuto hranici slouží jako tzv. měkké body a mohou vylepšit známku v případě úspěšného absolvování předmětu. 

## Hodnocení
- Písemka bude na 100 bodů.
- Kdo z písemky získá **méně než 50 bodů**, dostane hodnocení *F*.
- ?Písemka s materiály a netriviální příklady

## Zkouška
- na přemýšlení -> možnost přinést vlastní materiály, tištěné i psané, 
- dotaz na uzávěrovou vlastnost
- úkoly podobné na styl dú
- 100 minut
- papíry navíc donést vlastní

# PV197 GPU Programming

## Hodnocení
- practical project: 50 + 20 points
    - working code: 25 points
    - efficient code: 25 points (there will be threshold for performance)
    - 20 points compared to other class mates
    - there will be template for perf measurement, **we only need to focus on accelerated code**
    - ~ 100 loc
- Exam: 50 points
    - oral vs written
    - probably written + short oral
- `E >= 66`

## Project
- Difference of galaxies
- Euclidean distances between stars
- ! floating point underflow ! (`tmp` in example)
- compute sqrt + 1/n and 1/(n-1) on CPU
- Deadline: 
    - **parallel CUDA implementaion**: Oct 26th
    - **optimal solution**: November 23rd (75 bil pairs/s on GTX 1070 using 50 000 stars)
        - half perf = half points
    - most efficient implementation: Dec 5th
    - missed deadline **-2** points for each day of delay
    - (hint cache share memory into registers, not only global to shared)


# CORE018 - Rostliny ve zdraví a nemoci
- Konec: jednoduchý dotazník
    - 20 otázek online, vybírání z možností
- Pro úspěšné ukončení stačí přednášky

# VB003 - Ekonomický styl myšlení
- **2 průběžné písemky (7. a 12. týden)** - nutno aspoň 60% odů v součtu (15 otázek po 2 bodech)
- Opravný termín: 14. týden, cca polovina zkouškového
- případně jeden opravný termín ke konci zkouškového
- Pokud chybí 1-2 body, konzultační opravy 

## Vnitro
- a, b, c, d, e
- max 2 body za otázky
- 15 otázek
- za všechny dobré = 2 body
- za víc dobrých jak špatných = 1 bod
- naopak = 0 bodů