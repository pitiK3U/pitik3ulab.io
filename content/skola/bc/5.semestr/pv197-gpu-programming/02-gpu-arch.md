+++
title = "02 - GPU architecture and programming model"
date = 2022-09-22
+++

# Parallelism

## SIMT
- 8 cores finishes 4 instruction before new instruction if fetched -> `8.4 = 32` (warp size)

### Reconvergence
- can create deadlock (unless Volta / Ampere)
```c
if (...)
    1.
else
    2.
// reconvergence point
```
- Slide 11
    - reconvergence point after `while` loop causes deadlock - waits for all threads to reach after `while`, not incrementing `s`

## Thread properties
- GPU threads are hardware scheduled

# Memory hierarchy
- important = what is happening in a warp at the moment