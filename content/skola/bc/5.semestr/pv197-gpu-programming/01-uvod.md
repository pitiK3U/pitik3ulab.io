+++
title = "01 - Introduction"
date = 2022-09-15
+++

# GPU Architecture
## Good
- 2 first warps would do branch `a`
- the third one would do branch `b`
```c
if (ID < 64)
    a
else
    b
```

## Bad
- **only one instruction can run on a warp (32 of threads), 
  meaning branch `a` would run only on 1 thread wasting 31 threads 
  branch `b` would be run on 31 threads and wasting 1 thread**
```c
if (ID == 0)
    a
else
    b
```

## Worst
- worst case, wasting half of threads in multiple warps!!
```c
if (ID%2==0)
    a
else
    b
```
