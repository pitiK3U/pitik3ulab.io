+++
title = "03 - GPU hardware performance"
date = 2022-09-29
+++

# Synchronization

## Shuffle functions
- can be used to bypass shared memory

## synch of mem ops
```c
GA[i] = ...;
__threadfence();
G = 10;
```
- `__threadfence();` = To ensure `GA` array is written before `G` to memory

# Memory access optimization

## Caching
- overfetching `G[id * 1000]` = cache fetches for example 256B for each element (4B) instead of the gpus lower access 32B

## Partition camping

## Transfer between host and GPU memory
- need to minimize counts of transfers