+++
title = "05 - Code optimizations"
date = 2022-10-13
+++

# Profiling
- Compile code with `-lineinfo`
- ssh on airacuda with `-Y`