+++
title = "01 - Úvod"
date = 2022-09-12
+++

- **peníze** - chápe se trochu jinak než obecně

# Ekonomie
- **společenská věda** = sledujeme společnost - ekonomické subjekty
- používá přírodní vědy, modely
- **ekonomika** = hospodářství (to co sledujeme x ekonomie)

## Definice
- je o **volbě** (jsme neustále nuceni volit)
- **vzácné zdroje** = je jich omezené množství
- **užitečné komodity** = někdo je chce
- **rozdělování komodit** = zajímá nás proč (které skupiny) a kdo má kolik komodit oproti jiným

## Proudy
- **liberální**
- **intervencionismus**

## Pojmy

### Potřeba
- je **pocit nedostatku**
- vždy **subjektivní**, **různě naléhavá** (za víc naléhavou jsme schopni dát více statků), **proměnlivá**
- uspokojení potřeb pomocí jiných statků
- **ekonomické potřeby**

### Statek
- to, co **slouží k uspokojení potřeb** (patří zde i služby)
- ekonomický statek je **výsledek výroby**
- v užším smyslu rozlišujeme statky a služby

### Zdroje
- to, co **slouží k výrobě statků**
- rozdělení
    - půda a práce (= **cílevědomá lidská činnost, musí být odměna**)
    - zdroje jsou výsledkem hosp. činnosti => výroby (**kapitál = kapitálové statky**)
- **ZDROJE JSOU OMEZENÉ**

#### Rozdělení
- Půda (A) - pozemky, přírodní bohatství
- Práce (L) - **cílevědomá lidská činnost, musí být odměna**
- Kapitál (K) - kapitálové statky, druhotný výrobní faktor (prvotních nebo i jiných)

### Práce
- neoddělitelná od člověka
- Schopnost ji konat - pracovní síla. 
- různě složitá (jednoduchá x složitá; nekvalifikovaná x vysoce kvalifikovaná)

#### Intenzita vs produktivita
- Intenzita práce = množství práce
- Produktivita práce je účinností vynaložené práce.
- Produktivita je přímo závislá na vybavení, se kterým se pracuje, organizace výroby

#### Dělba práce
- specializace na pracovní činnosti
- při výrobě jsme velmi závislý na ostatních (ostatní výrobci)
- oddělené člověka výrobce a spotřebitele

### Kapitál
- druhotný výrobní faktor
- kapitálové statky - statky vstupující do výrobní spotřeby

## Hranice výrobních možností
- HVM: zobrazuje maximální dostupné kombinace statků, které lze v ekonomice vyrobit při dané technologii a množství a kvalitě VF

- snížení hranice 
    - vyčerpání zdrojů
    - zničení zdrojů = válka, přírodní pohromy

- peníze se rozvíjí, až se rozvíjí směna
- přírodní směna = drahé náklady transakce (sehnat kupce, co chce obchodovat se statky co mám)