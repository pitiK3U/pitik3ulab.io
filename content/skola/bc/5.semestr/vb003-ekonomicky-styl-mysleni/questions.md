+++
draft = true
+++

# 2. kapitola
1. Co je hospodářský mechanismus?
    - **co, jak a pro koho vyrábět?**
2. Vysvětlete, jaké funkce plní hospodářský mechanismus
    - zabezpečuje usměrnění zdrojů vzhledem k potřebám.
    - Nutí výrobce optimálně používat VF
3. Co je tržní mechanismus?
    - proces vzájemného ovlivňování tvorby nabídky, tvorby poptávky a tvorby ceny.
4. Vysvětlete, proč je liberalizace cen předpokladem fungování tržního mechanismu.
    - Aby se odraz změny nabídky/poptávky projevil ve změně ceny.
5. Vysvětlete na konkrétním příkladu, jakým způsobem zajistí tržní mechanismus změnu alokace zdrojů v případě změny potřeb spotřebitelů.
6. Vysvětlete, co vyjadřuje rovnovážná cena na trhu.
    - Jak poptávající, tak nabízející jsou spokojeni s cenou a množstvím daného statku.
7. Pokud se všechno nabízené množství prodá, je na trhu rovnováha?
    - Ne. Může být na trhu daného množství nedostatek. Trh má tedy větší poptávku než nabídku a není v rovnováze.

# 3. kapitola
1. Co je mezní užitek?
    - Je to jaký užitek/potěšení z uspokojení potřeby jedné jednotky daného statku člověku přináší. 
2. Zapište, co vyjadřuje zákon klesajícího mezního užitku, a vysvětlete.
    - Že první jednotka daného statku přinese největší užitek / uspokojení a s dalšími spotřebovanými jednotkami bude tento užitek klesat.
3. Zapište podmínku vyjadřující maximalizaci užitku ze spotřeby statků A a B.
    - $\frac{MU_{An}}{P_A} = \frac{MU_{Bm}{P_B}}$
4. Jaký je vztah mezi poptávaným množstvím a poptávkou?
    - Poptávka závisí na poptávaném množství a ceně.
5. Znázorněte, graficky důchodový efekt u tržní poptávky a pod graf napište čím je vyvolán.
    - Může být vyvolán změnou ceny daného statku, případně změnou ceny substitutu nebo změna ceny komplementárního statku.
6. Znázorněte graficky substituční efekt u tržní poptávky a pod graf napište
čím je vyvolán.
7. Proč je cenová elasticita důležitou vlastností poptávky z pohledu firmy?
    - určuje cenovou náchylnost daného statku na různé změny.

# 4. kapitola
1. Vysvětlete, proč se s růstem objemu produkce přibližuje hodnota průměrných nákladů hodnotě průměrných variabilních nákladů.
    - Protože AC = AVC + AFC, AVC = VC / Q a AFC = FC / Q. FC se neměnní, ale Q a AVC roste, protože pro větší vyrobení množství je potřeba větší VC. AFC se tedy při růstu Q snižuje, a proto má větší podíl na AC AVC, které s růstem produkce roste.
2. Jak se chovají fixní a variabilní náklady, pokud dochází k poklesu produkce?
    - Fixní náklady FC zůstávají stejné, VC variabilní náklady klesají.
3. Jak velké budou celkové náklady, pokud firma v krátkém období přestane vyrábět?
    - TC = FC, protože VC = 0
4. Jaký je vztah mezi zákonem klesajících výnosů a náklady?
    - Mezní náklady jsou ovlivněné ZKV, protože další vyrobená jednotka, přidá menší příjem, než předešlá
5. Vysvětlete, co platí pro mezní náklady, je-li mezní produkt rostoucí.
    - Mezní náklady tím pádem musí být klesající, neboť další jednotka výroby přináší větší přírůstek produkce
6. Musí existovat rozsah výroby, při kterém je firma zisková? Odpovězte slovně i graficky.