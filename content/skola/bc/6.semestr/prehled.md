+++
title = "Přehled"
date = 2022-09-12
+++

# IB031 - Strojové učení
## Podmínky pro absolvování předmětu
- aktivní účast na cvičeních
- vypracování projektu a získání alespoň 15 bodů za projekt
- získání alespoň 15 bodů ze závěrečné zkoušky (netýká se ukončení formou zápočtu)

## Hodnocení předmětu
- semestrální zkouška – až 25 bodů
- projekt na cvičeních – až 30 bodů
- závěrečná zkouška – až 45 bodů
- nadměrná absence na cvičeních nebo získání méně než 15 bodů z projektu: X
- známkování: <50 F, <60 E, <70 D, <80 C, <90 B, >= 90 A
- zápočet: alespoň 40 bodů

# PB138 - Modern markup languages
- Bonusové body za hezké řešení, i vizuálně
- 33 hard points from iterations
- 42 hard points for your team project
- 25 hard points for exams
- 5.2 soft points for completing the mandatory seminar ROPOTs

## Hodnocení
- A	100-94
- B	93-88
- C	87-82
- D	81-76
- E	75-70
- F	0-69

## Exam
- Online English IS ROPOT with mandatory zoom / google meet
- 30 minutes
- 25 questions single or multiple choice 
- Each question is worth 1 point.
- **exact-fit** = only if we select exactly the correct answers
- no penalty for an incorrect answer.

# IB016 - Seminář z funkcionálního programování
## Domácí úkoly
- **lze odevzdat pouze jednou!**
- za každou úlohu jsou max 2 body
- z toho **do konce 7. týdne semestru** (9. 4.) alespoň 2 body
- získat **do konce semestru** (19. 5.) alespoň 5 bodů
- možnost konzultace úkolů před odevzdáním na semináři

# VB000Eng - Introduction to Academic Writing
a. you attend **all** classes (7 total) and send me **two** excerpts of your writing (each 3-5 pages of "**normostrana**", most likely from Methods section; Introductions and Conclusions will **not** be accepted!)
- deadlines for those finishing this semester and for those not finishing this semester

b. you submit all homework for all classes in the corresponding submission vault (**unless otherwise specified, the deadline is the midnight before next class for all assignments**) - **Monday midnight**

c. you submit answers to all exercises from **three** additional practice exercises files by the given deadline (see interactive syllabus; some weeks may NOT have any practice exercises). - **April 6, midnight**