+++
title = "08 - Virtualizace"
date = 2022-04-13
+++

# Virtualizace

## Emulace
- Interpretace intrukcí virtuálním procesorem

## Nativní virtualizace
- Musí podporovat HW
  - dvojté stránkování
- Vnořená virtualizace
  - Spouštět vm ve vm, už né 3. vrstva (s akcelerací)

## Paravirtualizace
-

- Otázka: **Který typ virtualizace je nejrychlejší?**
  - Paravirtualizace = komunikace 2 procesů

# Linux

## Virtio
- zákl. komunikační způsob - kruhové buffery
  - nepotřebují synchronizaci - přidávat může pouze guest, odebírat pouze hypervisor

## QEMU
  - emulátor (nejen procesor ale i celé pc)

## KVM
- Kernel virtual machine - linux
- virtualizace CPU

## XEN
- Dom0 - Linux pro správu hw - speciální guest
- DomU - virtuální stroje

## Firecracker
- od Amazonu, snaha dělat, co nejtenší virtualizační vrstvu
- rychlé spouštění - vs containery, které mají horší bezpečnost

## Virtualizace disků
- formáty:
  - RAW - pouze soubor na host pc, nemás strukturu
  - QCOW2
    - qemu copy on write,
    - má strukturu,
    - umožňuje snímky,
    - thin provisioning - přiřazujeme prostor, ale alokuje se až při použití

### Migrace VM
- v případě přístupu k diskům na obou strojích
- z původního qemu posílám virtuální stránky snímku na druhý stroj
- až odposílám velkou většinu stránek, na poslední stránku zastavím vm a zapnu na druhém stroji

## Virtualizace sítě
- TUN/TAP device (linux) - virtuální síťová karta

## Libvirt
- skládá se z daemona - `libvirtd`

# Privátní cloud
- hybridní cloud
  - on premise - lokální počítače
  - plus nějaký limit s veřejným cloudem

# Kontejnery
- autoři aplikací = stálá verze vs admin = aktualizace knihovny = aktualizace bezpočnostních chyb
- "izolovaný proces"

## Kontejnery s OS
- bootujeme celý user-land
- `systemd-nspawn`
- systemd unit `machine` - při startu stroje nastartuje kontejnery

## Kontejnery s aplikacemi
- proces 1 = samotná aplikace
