+++
title = "01 - Administrace systému"
date = 2022-02-15
+++

# Instalace systému
- kořenový svazek na ramdisku - slouží k instalaci
- otázka "Co vlastně `mkswap(8)` na disk zapisuje?":
  - množina bloků (stránky)
  - nazačátku signatura swapovací
    - nové **uuid** při vytvoření swapovací oblasti, označuje se pomocí uuid a né disku
  - zapisuje bitmapu vadných úseků
- brání se interaktivity (rpm) vs zeptá se uživatele (dpkg)
- po restartu se bootuje z naistalovaného disku

# Rotační magnetické disky
- disk = kruhové stopy,
- rozdělení na výseče = sektor
- latence:
  - změna pozice hlavy = na jinou stopu
- **Zónový zápis**
  - vnější stopy jsou rychlejší = mají více magnetických zrn
  -> začátek disku je rychlejší - sektor 0 je vnější (dvd a cd to mají naopak sektor 0 uprostřed)
- Mechanická živostnost
 - klepnutí = poničení ložisek, naklonění ntb, ...

# Solid-state disky
- vlastnosti, co na úrovni UNIXu není vidět: **atomické operace**
  (celý původní sektor nebo celý nový)
- při přepsání: smazání
- erase-block: např 64kB
- částečný přepis: nahraje blok do paměti, vymaže blok, změní v paměti, zapíše
- řadič + firmware: šetří zápisy na disk
  - moderní přemapovávájí si tabulku -> jedou sekvečně pořád dokola
- potřebujeme speciální fs? Ano (něktéré jsou svým chování lepší pro ssd)
- trim - oznámí se disku, které soubory nejsou potřeba, normálně by musel vše zastavit - linux: fstrim

# Další typy disků
- NVMe disk (eXecute In Place - nemusíme nahrávat do paměti)
- Non-volatile RAM
- SMR disk
  - šindelový zápis, rozdělené po 10 stopách (mezi nimi mezery),
  - hlavičky nakloněné - první část ostrý signál, ale širší rušivý signál, přepisuje okolní zóny
  - když se probudí a zapisuje 10/20 zón vedle sebe -> není schopen nic jiného dělat např. 1 min

# Diskové oblasti

# Správa logických svazků
- PE maps one-to-one to LE

# Device mapper (linux)
- obecný nástroj, přemapování blokových zařízení
- `dm-verity` = obrana broti nabootování systému přes usb a zápis bloků na disk

# RAID
- Redundant array of independant disks
- ochrana proti výpadu ramdisku

## RAID-0
- **není redundatní**
- na HDD jsme schopni amortizovat latenci rotace a posunu hlavičky

## RAID-1 Mirror
- otázka: "Jaká je náročnost zápisu na RAID-1?"
  - skoro stejná
  - data jdou po sběrnici N-krát, ale operační systém s tím nemá žádnou větší starost

## RAID-5
- N+1 disků má stejnou kapacitu jako N disků
- poslední disk paritní (V RAID-4 vždy poslední disk vs v RAID-5 vždy 1 jiný)
- čtení
  - u 16 disků, zápis 1 sektoru -> načtení 14 disků, 1 zápis + parita
  - 2 čtení a 2 zápisy - čtení parity, jakoby odečíst nový zápis, a přepsat
- otázka "Jaký má vliv valikost bloku (stripu) RAID-5 na jeho výkonnost?"
  - malý stripe = musím šahat na víc disků
  - velký stripe = lepší paralelní využití
  - při velkých zápisových operací mohu vypočítat paritu rovnou

## RAID-6
- dva paritní bloky
- otázka:

## RAID-10
- zároveň prokládáme a zároveň kopírujeme
- otázka:
  - Poslední lichý blok se nevyužije

### RAID-10 near/far
- každý sektor má kopii v rychlejší sekci a pomalejší sekci

- hot spare disk = při výpadku se bere jako náhradní a začne se používat

## SW RAID nebo HW RAID
- RAID write hole - atomický zápis jednoho sektoru, není garantovaný zápis + parita atomicky
  - diskový řadič s vlastní cache
  - linux: `write-intent-bitmap` - drží které zápisy má zkontrolovat
  - Jestli existuje na **RAID-1, RAID-5, RAID-6**?
    - **Určitě vadí nad RAID-5** - viz vysvětlení
    - RAID-1 (RAID-10)
      - při spadnutí máme pouze poškozená data, která zapisujeme
      - pokud máme journal fs / transakční databázi, máme změny zaznamenané -> nevadí konzistenci dat
        - buď načteme (ne)zapsané
- **Cache, scheduling** u řadiče (nevýhoda: určuje si, kdy bude, co zapisovat)
- Disaster recovery
  - v linuxu: můžeme přeskládat k jinému pc a rozpozná
  - v hw: prorietárně zaznačeno, co kam patří - nejde rozpoznat, za 2 roky nejde řadič koupit
- **Doporučení: v linuxu použijte SW RAID (md)**
