+++
title = "06 - Zařízení v UNIXu"
date = 2022-03-29
+++

# Zařízení v UNIXu
- hlavní čísla - rozdílné namespace pro blokové a znakové zařízení
- důležité pouze hlavní a vedlejší číslo - zobrazení v adresáři je nepodstatné
- další zařízení - např síŤová karta

## Problémy
- historicky a prakticky je vedlejší číslo víc využité
- kdysi obsahoval všechny možné blokové a znakové zařízení
- problém s pojmenováním = nemělo by být zadrátované v kernelu - administrátorsky řešeno

## SysFS
- umožňuje dynamicky alokovat hlavní čísla

## Hotplug
- vznik zařízení za běhu
- netlink socket - páteřní router bude přes kernel posílat notifikace při změně tabulky
- Coldplug - část informací od začátku

## udev
- vytváří zařízení pro uživatele v `/dev`
- změna práv nepřežije do dalšího restartu -> řešení: konfigurace udev
- `udevadm info -q path -n /dev/sda`

## D-bus
- sběrnice, daemon - komunikace mezi jednotlivými vrstvami

# X window systém
- nově nahrazován Waylandem
- `x server` - tlustší driver grafické karty
- *Spojení* - proudové spojení mezi klientem a serverem
- `XKB` - dělá z klávesnice obecný stavový automat - povoluje více nastavení klávesnice
  - originální měla pouze modifikátory ctrl, alt, shift
- `Shape` - nepravoúhlá okna - např `Xeyes`
- `Randr` - resize and rotate
- `Xvideo` - graf. karta převadí barvy do rgb

## X server
- dříve jednoúčelový počítač na zobrazování
- X server = Display
- lze aby jedna aplikace byla spuštěna na obrazovkách více uživatelům
- `xdpyinfo` - x display info

## Okna
- X server si nemusí pamatovat obsah okna, při zakrytí a odkrytí se může zeptat klienta

## Kurzor
- průhledné pixely a neprůhledné pixely
- černé a bíle kursory
- aktivní místo klikání

## Fonty
- X logical fonr description
- starší - uložené na x serveru
- nové - na klientech

## Obrázky
- `.xpm` - pixmap, je céčkový soubor, lze includnout

## Události
- Server posílá klientovi

## Window manager

## Programování
- Toolkity - umožňují používat widgets

# Subsystémy
## Syslog
- `openlog` - je potřeba otevřít před chroot
- `SOCK_DGRAM` - datagramový socket (UDP)
- facility - které čsát systému zprávu vygenerovala
- možné logovat i po síti na logovací server)
- moderní alternativity - `rsyslog`, `rsyslog-ng` - zprávy s podpisy

## Příklad
- `*.info` - aspoň info
- `*.=info` - právě info
- výstup `*` - poslat všem uživatelům na obrazovku
- `-/var/log/debug` - vypne se po každé správě volání `fsync`
- `|/usr/bin/log-piper` - roura
- `@logost.domena.cz` - logování po síti

## Journal
- systemd logovací systém
- neumožňuje posílat po síti -> musí vedle běžet rsyslog
- *indexované* ukládání zpráv (do binárního souboru; program `journalctl`)
- rozšířená *metadata* - struktura klíč = hodnota
- *důvěryhodné atributy* = začínají `_` a tyto adtributy od uživatele jsou nahrazeny journalem

## Trasovací nástroje
- `strace -f` - sleduje i potommky procesu, na každý řádek vypisuje pid

## Sondy

# BPF (Berkeley Packet Filter)
- Malý prográmek pro testování, které packety nás zajímají - původní pro FreeBSD
- možno pro použití přímo v kernelu bez kompilace

# Linux perf
- využívá trace points jako bpf a hw časové počítadla na získávání informaci o daném programu
- počet instrukcí za cyklus, využitý čas, větvení a branch predictor 
