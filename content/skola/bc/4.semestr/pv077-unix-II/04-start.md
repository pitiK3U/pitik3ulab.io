+++
title = "04 - start systému"
date = 2022-03-15
+++

# Start systému - `init`
- první proces, co běží v userspace a je načtený normálně z disku
- lze přes konzoli kernelu zadat proces jako proces číslo 1
- linux zkusí spustit `/bin/sh`, pokud `init` neprojde

## BSD init:
  - `/etc/gettytab` - terminály pro lokální přístup
  - ``
  - V případě, že rodič skončí dřív než jeho potomek, zařadí potomek k init procesu, který čeká explicitně čeká na skončení potomka


## SMF service management facility
- jako první se snažil řešit problém o spustění subsystému, tak init nevěděl, jak subsystém dopadl
- v konfigurace xml!

## OpenRC
- gentoo linux

## System V init
- init ví, ve které úrovně systému se nachází a do které chce přejít
- `0 - halt` - pokus o korektním zastavení počítače
- `1 - single` - aspoň přihlášení na superuživatele
- umožňuje běžet jako proces 1 nebo jako normální proces (`telinit(8)`)
- `sS` - nemá teď smysl, protože při tomto stavu je vypnuté síťové připojení pouze sériová linka

### Startovací skripty
- co na jakém runlevel má běžet, jak se to má špouštět a jak se to má ukončovat
- každý subsystém má vlastní skript, zároveň ukončovací skript podle 1. argumentu (start/stop)
- symvolické linky:
  - `S` - Start
  - `K` - Kill
  - číslice = priorita/pořadí
- všechny init skripty jsou podobné

#### Fedora:
- `chkconfig` - nastavení sym linků do startovacích skriptů

### `/etc/inittab`
- 1. pole identifikátor (někdy pouze 2 znaky)
- 2. je runlevel
- 3. způsoby spuštění
  - `once` pouze se spustí, `wait` jako `once` ale čeká se na něj
  - pokud
- 4. příkaz + argumenty
- otázka:
  - rozpozná část konfigurece, co odpovídá ještě minulé verzi (načtené verzi)

## Systemd
- Start podle potřeby - na daný port přijde zpráva -> zapne danou službu
  - maximální možná paralelizace - při špuštění daná služby se šáhne na danou jinou službu
- Problém: detekce, jestli pomotek vytváří jiné procesy
  - Solaris to vyřešil pomocí contract fs
  - control groups (taky kdysi fs) - použití pro daní výpočtu nějaké procesory, popř přesně nastavit dané numa uzly
- Problém: když se něco pokazí, není přesně vidět co
- konfigurace: daný subsystém je unit

### unit
- `After` - co chceme spustit před danou unit
- `Wants` - musí doběhnout před spuštěním
- `[Service]` - jak danou service spouštět
- `KillMode=process` - ukončuje pouze hlavní proces (nechceme odříznout už navázaná spojení)

## další vlastnosti
- oddělení namespaces
  - jednotlivé procesy mohou vidět pouze části fs, nebo jako read-only
- `systemd-tmpfiles(8)` - deklarativně předpřipravení struktury pro tmp adresáře
- `systemd-oomd(8)` - neřešit out of memory problémy v kernelu, ale v daemonu
  - mimo GNOME DE jsou všechny uživatelské sessions jako jedna control group - při problému se zabije, celé sezení
  - doporučení na desktopu vypnout
- `systemd-logind(8)`
  - může přidělovat i prostředky
