+++
title = "05 - Svazky"
date = 2022-03-22
+++

# Svazky
- `/etc/fstab`
  - předposlední sloupec - v jakém pořadí se má pouštět `fsck`
  - poslední sloupec - v jakém pořadí / vlně se má pouštět při zálohování daného svazku (`dump`)
  - systemd má uuid vydefinované vlastní pro ?
  - manuální - například foťák, parametry v fstab, potom stačí pouze `mount fotak`
- `/etc/mtab` - není vhodný způsob pro dynamické vznikání a zanikání zařízení
- `mount`
  - `-r` read-only
  - `-t xfs` - typ svazku
  - `-o uid=11500,gid=500` - parametry
  - `-o remount,rw` - přemontovat svazek jako read-write
  - `--move /odkud /kam` - přemountuje namountovaný svazek někam jinam, může být i použiváný
  - `--bind` - tváří se jako hard link na adresář, neměnní `..` parent directory (je na ten adresář, kam jsme to nabindovali)
- `umount`
  - nezle odmountovat pokud je použiván nějakým procesem

# Odkládací prostor - swap
- vzdálený odkládací prostor
  - problém out-of-memory deadlock - při poslání stránky, musíme vytvořit frames - další paměť, čekáme na odpoveď - další potřeba paměti

# Automounter
- mapa všech `automount` pointů a co dělat když se k nim přistoupí

# Uživatelé a skupiny

## GCOS
- komentář, plné jméno uživatele
- GCOS - General Electric Comprehensive Operating System

## Šifrování hesel
- pro každého uživatele jiná sůl které se dává do hashovací funkce a ta je na začátku v otevřené podobě
- nové verze:
  - `$číslo_algoritmu$sůl$zašifrované_heslo` - znak `$` je kvůli zpětné kompatibilitě
- `mkpasswd(1)`
- `crypt(3)`
- nezáleží na rychlosti - útočníkovi by při použítí hrubé síly trvalo dlouho zjistit heslo

## Ukládání hesel
- `/etc/passwd` - už se nepoužívá
- `/etc/shadow`
- americké armádní bezpečností standardy - nemohou být recyklována hesla - nefunguje s `shadow` a `passwd`
- `BSD` - problém s prohledávání tabulky uživatelů - trvá dlouho -> binární soubor s berkleydb
  - `/etc/master.passwd` -> z tohoto souboru se vygenerují ostatní soubory

## Modifikace tabulky uživatelů
- `vipw(8)` - program na editaci, respektuje `$EDITOR`
- `/etc/skel` - template home adresáře pro nově vytvořené uživatele
- otázka "K čemu je dobré mít i skupinu pro každého uživatele":
  - `umask`, například, když chceme aby uživatelé co pracují na společném projektu
  - problémy s nastavováním přístupových práv pro projekt

## Soubor `/etc/shells`
- list použitelných shellů
- změna `chsh(1)`
- `/sbin/no-login` - pro pseudouživatele, při přihlašování by měl být problém

## Name service switch
- obsahuje pluginy pro rozšířené hledání systémových tabulek


### Knihovní funkce
- `getpwnam(3)`, `getpwuid(3)`
- **funkce nejsou reentrantní**!
- zneplatnění dat dalším zavoláním dané funkce!
- vracení všech uživatelů po jednom
  - `getpwnent(3)`
- příkazová řádka: `getent(1)` - `getent passwd user`

## Pluggable Authentication Module
- rozšíření systémových záznamů
- rozšířený open source system, nyní pod Red Hatem
- aplikace musí přilinkovat knihovnu `libpam` a dělají, co jim řekne knihovna
- aplikace museli být velmi přepsány (heslo vs pam)
- dneska `/usr/lib64/pam.conf`
- později -> má smysl, aby každá konfigurace měla vlastní soubor jako samostatný balíček
- -> `/etc/pam.d/`

### Fáze
- první sloupec v konfiguračním souboru
- sekce `password` - heslo bylo vygenerováno a uživatel si musí změnit heslo -> je ve všech konfigurací!
- sekce `session` - mění session uživatele - měnit limity, připojení síťového adresáře, ...
- *řídící hodnoty* - druhý sloupec v konfiguraci - co se má stát, když zavolaný modul něco vrátí
  - requisit - skončí dřív než administrátor zadá heslo přes nebezpečné spojení
  - `pam_nologin` - selže přihlašování normálního uživatele existuje-li soubor `/etc/nologin` - např pro maintaince
  - `use_first_pass` - pro použití hesla pro jiný modul
  - `auth required pam_deny.so` - aby při zkoušení ostatních autentizací mohlo být `sufficient` a připadě neprojití bylo zamítnuto
  - `pam_console.so` - dává větší práva, když sedíme u systémové konzole


# Soubor wtmp
- Když soubor neexistuje, nový soubor **nevytváří**!

# Terminálové procesy
- `getty` - při skončení se znovu spustí na terminálové lince
- `login` - program `getty` zavolá `exec` na `login` 
