+++
title = "09 - Síťě TCP/IP"
date = 2022-04-19
+++

# Úvod
- přepojování paketů - stream je rozkouzkován na jednotlivé pakety a ty jsou seskládány až na koncovém příjemci

- otázka:
  - `172.16.0.0/12` -> `172.31.255.255`

- Otázka: Jak se pozná první fragment packetu?
  - Nastavený flag na `More fragments` a offset `= 0`

- otázka: V jakém pořadí posílat fragmenty?
  - Možná je výhodné posílat poslední fragment -> má počet fragmentů, umožňí příjemci připravit místo pro fragmenty

- otázka:
  - Mohou být fragmenty dále fragmentovány?
    - fragment offset + more fragments
    - stejná hlavička, pokračuje se v fragment offset

# ARP
- **Proxy ARPw**
  - Daný pc v síti má vzdáleně připojený počítač k sobě, který se chová jako lokální/je vidět na lokální síti
  - vzdáleně připojený pc musí být nakonfigurován v ARP tabulce lokálního pc

# IPv6
- **autokonfigurace** - pomocí něčeho, co má ntb dané, např. L2 adresa
- Způsoby přenosu
  - multicast - přijde pouze danému pc
  - **anycast** - přenos pro jeden z daných adres
- otázka: Jaká nejkratší IPv6 adresa lze zapsat na co nejméńě znaků?
  - adresa se samými nulami

## Formát packetů
- **Identifikace toku** - poznačení, které packety patří k sobě

## Interpretacee prefixu adresy
- Úkol:

# Specifické vlastnosti
- Spolupráce s linkovou vrstvou:
  - ušetření komunikace / práce na úrovni kernelu

# L4
- Otázka: K čemu indentifikace?
  - při více pingů ze stejného pc

- Otázka: Kdy přesně se posílá **host unreachable**?
  - Router posílá packet a nezjišťuje, jestli dorazí
  - Poslední node nedostane MAC adresu na ARP request

- Úkol: Jak pozná router, že má poslat ICMP redirect?
  - Když by měl poslat packet zpátky do stejné interface, ze které přišel

- Time limit exceeded - traceroute

# TCP
- Volitelné
  - MSS - maximum segmetn size = maximálné velikost tcp rámce
  - SACK - selective ACK, může být optimalizace, nemusí se dělat restransmit všech paketů od ztraceného

# Bufferbloat
- objevila se v posledních 5 let
- nejvýkonější router je ten, co má nejvíce paketů
- zjistilo se, že mnoho bufferů nepomáhá řízení sítě - tcp je řízeno zahazováním packetů, né odezvou
- užitečná fronta = skoro furt prázdná
- škodlivá fronta = skoro furt plná, paket se dostane na konec fronty, dlouho čeká => zvyšuje latenci
- algoritmus - `fq_codel` - sleduje jak je fronta dlouhá a podle toho zahazuje i pakety z fronty

# Protocol Ossification
- QUIC: všechno nad UDP zašifrujeme aby middleboxy neměnili/neničili pakety

# Sockety
- Úkol:
  - socket pair je obousměrný
  - pipe vždy je jeden konec zapisovací a druhý je čtecí

## `AF_UNIX`
- jak umístit socket do filesystému

## `AF_INET6`
- IPv6 scope je přes jaké rozhraní chci na danou lokální adresu komunikovat
- v cli tools píšeme za adresy za `%` např`%wlan0`

##
- úkol:
  - zjistit, že stdin je socket - `fstat`
