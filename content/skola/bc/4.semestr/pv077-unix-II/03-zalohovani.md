+++
title = "03 - zálohábání"
date = 2022-03-01
+++

# Zálohování

## Problémy
- pásky = elektromagnetické pole
- optické = problém se světlem

## Víceúrovňové zálohování
- Čas vytvoření zálohy musí být na **zálohovacím médiu**

## Formát záloh
- `dump(8)`
  - rozumý rozšířeným informacím FS, čte sekvečně podle i-uzlů -> rychlejší
- `zfs/btrfs send/receive`
  - brts umí i inkrementální
- nutná potřeba s aplikací - může zapisovat do více souborů - při zaláhování může vzniknou nekonzistetní zálohu
- možno udělat snímek a zálohovat snímek, poté snímek smazat
- XFS `freeze` - dojede otevřené transakce a přestane zapisovat -> snapshot nad korektně odpojeném FS

## Zálohování na disk
- `cp -l` kopie pouze adresářů, soubory jako hardlinky
- bacula open source zálohy pro větší počet zařízení

# Rozložení adresářů v systému
- Linux - filesystem hierachy standard (FHS) - dokument se snahou o standardizaci

## Kořenový svazek
- pokud samostatný, tak je malý a není sdílen mezi více stroji
- `MAKEDEV(8)` - při smazání `/dev/` souboru je potřeba maj. a min. číslo
- `/mnt` - dočasné připojení svazku, System V
- `/media` - pro automaticky připojované svazky
- `/opt` (optional) - přidané větší softwarové balíky. Obvykle samostatný svazek.
- `/root` - domovský adresář superuživatele
- `/tmp` - obvekle samostatný svazek

## Adresář `/usr`
- `share` - sdílitelná data nezávislá na architektuře (man pages, dokumentace, terminfo)
- `libexec` - programy, které se nepouští z příkazové řádky
- `local` - adresář pro lokálně instalovaný software
- `sbin` - síťový daemoni, ...
- `src` - zdrojové texty od systémových komponent

## Adresář `/var`
- Data která se mohou měnit (tiskové fronty, mailboxy, různé cache)
- `run` - soubory k běžícím programům, po skončení nemají význam
- `tmp` - přežijí po restartu, obrovské soubory
- otázky:
  `/var/run`

## Adresář `/lost+found`
- jen některé fs
- v kořeni každého svazku
- při nalezení i-uzlů, které jsou označené za používané, ale nejsou nikde nalinkované

## Aktuální vývoj
- `usrmove` - přesun read-mostly dat z `/` do `/usr`
- otázka:
  - co nejvíc zmenšit `/`, co největší read-only svazek (`/usr`)
