+++
title = "07 - Subsystémy"
date = 2022-04-12
+++

# Cron
- dávkové úlohy
  - toto někdy spustit, ale jedno kdy
- cron tabulka - soubor se zapsanými pravidelnými úlohami
- `minuty hodiny měsíce dny den_v_týdnu cesta`
- `*/5` - každých 5 min.
- systémová cron tabulka ještě obsahuje uživatele pod kterým se to má spouštět - mezi časem a cestou

## Anacron
- K čemu je omezení pomocí `START_HOURS_RANGE`?
  - Kdy se to má spouštět
  - 22 - aby se nezměnil v průběhu půlnoci
  - 3 - vyhnutí problému se změnou času

## Jednorázové úlohy
- snaží se systemd požrat - `systemd.timer`
  - když vypří `unit.timer` spustí se soubor `unit.service`
  - oproti timeru umí relativní čas (crontabulka umí pouze absolutní) - např. spustit 15 min po dokončení programu
  - jestli timer má začít počítat při ukončení, selhání, ...

# Recyklace logovacích souborů
- Jaký problém by způsobilo uvedení cesty ve tvaru `/var/log/*`?
  - Soubory by exponenciálně narůstaly - `/var/log/msg` -> `/var/log/msg.1` -> `/var/log/msg.1.1` -> ...

# Promazávání dočasných souborů
- ***Pozor na symbolické linky!***
- `systemd tmfiles.d` - alternativa k `tmpwatch`

# Tiskárny
- UNIX umožňuje na vstupu i výstupy spooling fronty zapnutí/vypnutí

## BSD
- implicitní tiskárna se jmenuje `lp`
- tiskový protokol - `lpr`

## Systém V
- `lp` - tisk
- `cancel` - zrušení
- `lpshut` - vypnutí

## CUPS - Common UNIX Printing System
- Původně od Apple, HTTP server na portu 631
- Rozšířitelný protokol, pro možnost použítí moderních tískáren
- printer property description - popis tiskárny
- sám objevitelný na sítí - zero conf protokol

# Diskové kvóty
- Musí se pro každý svazek uvádět zvlášť.
- Měkká kvóta - dlouhodobě by uživatel neměl překročit.
- Tvrdá kvóta - nelze překročit.
- Při překročení měkké kvóty se pustí časovač, pokud velikost nespadne pod měkký limit než vyprší časovač, tak se tvrdá kvóta nastaví na měkkou.
