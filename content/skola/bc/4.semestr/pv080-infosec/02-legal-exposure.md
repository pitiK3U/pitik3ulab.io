+++
title = "Personal data protection, legal and ethical aspects"
date = 2022-02-22
+++

# Personal data protection

## Privacy (personality) protection
- Reactive protection
- Covered by private law
- Civil law

## Personal data protection
- Active protection
- Covered by public law
- Administrative law

- Privacy-by-design, Privacy-by-default

## Personal data
- If there is possibility that the data can be/could be in future
  personal data we must treat the data as personal

# Privacy techniques and selected issues