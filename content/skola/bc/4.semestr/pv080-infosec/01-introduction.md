+++
title = "01 - Introduction"
date = 2022-02-14
+++

# Privacy
- **Information privacy** - control of information (personal data and sensitive information)
- **Private information**
- **Personal data** - information relating to *identified* or *identifiable* person
  - de-identification of data = removal of identifier of data,
    multiple de-identified data could lead to identification

## Information privacy
1. **Anonymity** = use of system without revealing identity of the user (not protecting the computer system)
2. **Pseudonymity** = similar to anonymity, but user is accountable for their use (identity can be found)
3. **Unlinkability** = multiple uses without others being able to link these together
4. **Unobservability** = use service without other (third parties) being able to observe the use

## Common criteria
- **TOE** = Target of Evaluation - entire system
- **TSF** = TOE Security Functions - HW, SW, FW used by TOE
- **TSC** = TSF Scope of Control = interactions under TOE sec. policy

## Mix communication system
- always from attacker perspective

- IOI = item of interest


# Seminar
- Privacy policy generator [https://app.privacypolicies.com/](https://app.privacypolicies.com/)
- Terms of Service didn't read [https://tosdr.org/](https://tosdr.org/)
- PrivacySpy [https://privacyspy.org/](https://privacyspy.org/)
- Polisis [https://pribot.org/polisis](https://pribot.org/polisis)
