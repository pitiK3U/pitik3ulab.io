+++
title = "03 - Cryptographic building block"
date = 2022-02-28
+++

# Crypto in general
- MAC = message authentication code
- PT = plaintext
- CT = ciphered text

## Vernam cipher
- Fully secure only if the key is as long as the input (5TB data needs 5TB key)

## Randomness
- hard to guess + unique

### True randomness (non-deterministic)
- physical process (thermal noise, ...) - aperiodic, slow

### Pseudorandom (deterministic)
- sw function - periodic and fast
- initialized by seed - fully determines all random data

### Recommended (CSPRNG - cryptographically secure PRNG)

## Symmetric cryptography
- Every output bit should depend on every input bit in complex way

## Breaking DES
- EFF - Electronic Foundation

## ECB & CBC
- ECB = electronic code block
- CBC = cipher block chaining
    - blocks with the same data wont result in same encrypted data
- CRT = counter mode
    - encrypted counter

