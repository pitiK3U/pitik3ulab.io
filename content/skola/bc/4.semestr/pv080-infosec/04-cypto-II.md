+++
title = "04 - Cryptographic building block II"
date = 2022-03-07
+++

# Public key operations
- private key is used for signing = the transferred content is unchanged
- public key encryption = the content is encrypted
- creating signature is slow - can be sped up by using hash function
    - instead of signing the whole message only the hash of the message is used for signing

# Message authentication code (MAC)
- integrity and authentication
- hard to bruteforce correct tag

## HMAC
- RFC2014
- `HMAC_k = Hash[(K+ XOR opad) ++ Hash[(K+ XOR ipad) ++ M]]`
- overhead only 3 hashes

- use different keys for encryption and authentication! 
    - Law get request decryption of message that was sent to us