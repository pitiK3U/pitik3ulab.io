+++
title = "01 - Úvod do sémantiky c++, třídy, řetězce apod."
date = 2022-02-15
+++

# C++
- objekty mají **hodnotovou sémantiku**
- RAII (deterministická správa zdrojů)
- C++17 umí typ vectoru odvodit od prvků `std::vector v = {1, 2, 3};`
- **přiřazení je kopie** i u objektů
- `std::cin` vstup je ukončen bílým znakem
- **nikdy** nepsát `using std::neco;` do globaálního scope hlavičkového souboru
- `inline` říká, že linkování 2 souborů se stejným názvem není problém pro linker
- `this` je odkaz na sebe (není to reference, přibylo dříve)
- `const` za hlavičkou metody => nemění daný objekt : `int age() const;`
- v konstruktoru `:` značí **inicializační sekci**
  - neděje se v pořadí za `:`, ale v pořadí, jak jsou deklarované v třídě po sobě
- preferujeme inicializační sekci před přiřazením uvnitř těla konstruktoru = více efektivní
  - při přiřazení se inicializuje na nulovou hodnotovou a pak se přiřadí! ->
    může být špatné při `const` atributech

## Různé způsoby inicializace
- `std::string empty();` - není to deklarace proměnné, ale deklarace funkce
