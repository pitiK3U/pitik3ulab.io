+++
title = "05 - Vstup a výstup a přetěžování"
date = 2022-03-15
+++

# Standardní vstup / výstup
- `cerr` - není bufferovaný, před zapsáním vyprázdní `cout.flush()`
- `clog` - není svázaný s `cout` a není bufferovaný
- `std::boolapha` - pro výpis true/false

# Manipulátory
- https://en.cppreference.com/w/cpp/io/manip

# Přetěžování operatorům
- dobře použité přetěžované operátory mají lepší čitelnost
