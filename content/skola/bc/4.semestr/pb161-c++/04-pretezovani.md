+++
title = "04 - Přetěžování, objekty a jména, přátelství"
date = 2022-03-08
+++

#
- `static_cast` může být **potenciálně nebezpečný**

# Uživatelsky definované typy
- konstruktor s jedním parametrem se použije jako implicitní konverze

# Argument-dependent lookup
- idiom pro swap
```cpp
using std::swap;
swap(a, b);
```

# Klíčové slovo `friend`
- třída umožňuje sahat venkonvím funkcím, ..., na privátní atributy 
