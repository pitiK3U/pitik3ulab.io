+++
title = "03 - Kontejnery, iteráry, algoritmy"
date = 2022-03-01
+++

# Kontejnery
- datové struktury, které drží více položek

## Dvojice a ntice
- rozbalování ntic (C++17) **strucuted bindings**
```cpp
auto [x, y] = point; // copies
auto& [b, i, s] = answer; // reference
```
- rozbalení do existujících proměnných `std::tie`
- zjištění automaticky dedukovaného typu -> zalování neexistujicí funkce nám kompilátor vyhodí
  chybu s daným typem

## Sekvenční Kontejnery
- varianty s `emplace` berou parametry pro konstruktor a vytváří objekt na místě -> mohou být efektivnější

# Iterátory
- `const ...::iterator` nedává smysl -> vztahuje se k iterátoru, né k vnitřní hodnotě
- při změně objektu může dojít k invalidaci jak iterátorů, tak referencí

### Řetězce
- jakákoli změna řetězce kromě přímé změny 1 znaku, zneplatňuje vše
- `std::string_view` předávat skoro vždy kopií

## `std::map`
- operátor `[]` při neexistenci klíče se automatiky vytvoří ! může být nechtěné `if (map[123] == 1)`

# Algorithms
## erase-remove idiom
