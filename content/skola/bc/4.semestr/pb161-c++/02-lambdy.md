+++
title = "02 - Hodnotová sémantika, reference, lambdy"
date = 2022-02-22
+++

# Nedefinované chování
- detekce: valgrind nebo ubsan: `-fsanitize=undefined`
- Program s nedefinovaným chováním je **vždy nekorektní**

# Hodnotová sémantika, kopísování a přesun (move)

## Implicitní generované speciální metody

### Bezparametriký konstruktoru

### Kopírovací konstruktoru
  - zkopíruje hodnoty všech atributů
    - volá jejich kopírovací konstruktory

### Kopírovací operátor přiřazení

### Přesouvací konstruktor
### Přesouvací operátor přiřazení

### Desktruktor

## Speciální metody vektoru

## Vynechání kopií (copy elision)

## `std::move`
- použtít radši `std::move` než `const &` u drahých objektů

## Konst vs nekost reference
- `l-value`
  - ,,co může stát na levé straně přiřazení"
  - má jméno / adresu

# Automatická dedukce typu
- `auto` znamená vždy **hodnotu**, nikdy referenci

# Lambda
- 
