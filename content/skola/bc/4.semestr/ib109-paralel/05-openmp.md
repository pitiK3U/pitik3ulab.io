+++
title = "05 - OpenMP"
date = 2022-03-30
+++

# OpenMP
- `private(tid)` - variable `tid` is thread-local
- `reduce ` - redukce
  - operace, kdy z více paralelních proměnných chci udělat jednu

## Direktiva `for`
- `static`
  - nevýhody
    - není dobré v případě, že vlákna mají rozdílnou zátěž

- `dynamic`
  - když je poslední vlákno pomalé, řeká se na dokončení posledního vlákna

## Sekce
- různá vlákna vykonávají různý kód

## Bariéra
- počkají na místě komentáře

## Poznámky
- optimalizátor může poznámky vyoptimalizovat např if

## Single
- kritická sekce

# oneTBB (Intel's Thread Building Blocks)

# C++11
- sdílené futures - místo kam budu zapisovat do budoucna

- TSO = total store order
- PSO = partial store order
- sourceforge.org parallel for
