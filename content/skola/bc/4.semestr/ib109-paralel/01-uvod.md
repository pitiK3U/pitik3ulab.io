+++
title = "01 - Úvod"
date = 2022-02-13
+++

# Cache
- **výprask** = algoritmus přistupuje do paměti nečekaně a nelze cachovat přístupy do paměti = cache miss?

# Paralelismus z pohledu OS
- **multitasking** - z pohledu z venku se zdá, že běží současně, ale on se jen tak rychle střídají

# Distribuovaný systém
- **emergentní jev** = z venku pozorovatelné chování, které není v programu zakódované
