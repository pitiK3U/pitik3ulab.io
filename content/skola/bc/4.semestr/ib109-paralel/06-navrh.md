+++
title = "07 - Návrh a implementace paralelních systémů"
date = 2022-04-06
+++

# Dekompozice a úlohy
- matice 1000x1000 nemá smysl paralelizovat

#
- **contention** - vlákna dělají nezávislé práce, ale čtou stejnou paměť -> mohou se serializovat

# Techniky dekompozice

## Rekurzivní
- Restruktoralizovat úlohu = např. hledání minima v poli - iterativně = for cyklus nám nepomůže pro rozděl a panuj

## Průzkumová
- rozdíl oproti sekvenční ve zkoušce

# Charakteristika interakcí
- Jak náročné je přenést úlohu do jiného vlákna

# Mapování
- Pokud by data byla periodická - např mocniny 2 je vhodné toto rozdělení rozbít např modulo 31 místo 32

# Afinitní plánování
- zamezení některých úloh vykonávání na daných jádrech

# Dilema procesu dekompozice-Mapování
- krásná paralelní dekompozice na libovolném počtu vláken nemusí být vhodná pro malý počet vláken

# Redukce ceny komunikace
- boj s latencí = *bufferování*
