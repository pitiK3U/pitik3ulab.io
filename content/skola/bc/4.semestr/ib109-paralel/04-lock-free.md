+++
title = "04 - Implementace Lock-Free datových struktur"
date = 2022-03-23
+++

# Motivace
- zamykání je drahé => nový přístup bez zamykání

# Lock-Free
- ať seběhnou vlákna jakkoli, vždy alespoň jedno úspěšně skončí

# Wait-Free
- neexistuje souběh vláken, tak aby nějaké vlákno někonečně čekalo / bylo zablokováno

# Historie
- CAS (compare and swap) je jediná vhodná instrukce pro lock-free programování

# CAS (compare-and-swap)
- `bool CAS(T* addr, T exp, T val)`
- operace je provedena atomicky a asynchronně

# WRRM mapa (write rarely read many)
- komentář `// DONT delete` - čtenáři mají stále starou kopii

# Hazardní ukazatele
