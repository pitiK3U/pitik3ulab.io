+++
title = "07 - Programování aplikací pro prostředí s distribuovanou pamětí"
date = 2022-04-20
+++

# Struktura předávání zpráv
- Každý stroj má jednoznačnou identifikaci a ví o ní

# `Send`
- když bude blokující:
  - může čekat dlouho - čekat, než cíl obdrží správu
- asynchronní:

- blokující nebafrovaná komunikace
  - chceme, když posíláme velkou zprávu např. 1GB soubor
  - `send` čeká na druhou stranu než bude `receive`

# Neblokující
- funkce začínají `I` např. `MPI_Isend`

# Kolektivní operace
- ***Všechny procesy musí zavolat odpovídající funkci.*** např `broadcast` zavolá i vysílající i přijímací proces
- broadcast - volající má `src` sebe, ostatní daný src
- Všechny typy mají vektoré verianty, mohou posílat zprávy jiné délky, `Gartherv`, připide pole `displs`
