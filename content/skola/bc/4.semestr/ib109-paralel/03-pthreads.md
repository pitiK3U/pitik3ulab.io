+++
title = "03 - Programování v prostředí se sdílenou pamětí"
date = 2022-03-09
+++

# Race condition
- nedokonalost paralelního programování
- nedeterministické chování
- ikrementace o 1
  - pokud chceme součet všech vláken = race
  - pokud chceme aby tam nebyla nula = není race

## -
- kompilátor nemůže vždy doplňovat bariéry, protože záleží o co se programátor snaží
- -> musí si hlídat programátor sám

# Uváznutí - deadlock
- horší je **částečný deadlock**, špatně se detekuje

# Thread-safe a re-entrantní
- při použítí jakýchkoli zámku, program není re-entrantní

# Přístup k sdíleným proměnným
- **serializace** = mezi načtením, změnou a použitím proměnné je nepřerušitelné místo
- **kritická sekce** = nepřerušitelná sekce
  - musí být odolná vůči plánování

# Jednoduché řešení zámku
- pouze v přídadě, že máme HW podporu atomických operací!

# PThread
- `pthread_attr_` - atribut / nastavení vlastností vlákna
- `phtread_create` - vlákno se může spustit dřív, než se vrátí volání této funkce (může tedy i skončit)
  - data pro vlákno musí připraveny před zavoláním funkce
- tip:
  - před výpis z vlákna vypsání id, potom grep id a zjištění všech výpisů daného vlákna

## Podmínkové proměnné
- `pthread_cond_signal` - probudí pouze jedno nějaké řekající vlákno - nemohu si rozhodtnou které
- `pthread_cond_broadcast` - vzbudí se všechny vlákna, ale musí běžet po sobě

## Thread specific data
- pro každé vlákno drží jednu proměnnou

## Read-Write lock
- write lock má přednost před read lock 
