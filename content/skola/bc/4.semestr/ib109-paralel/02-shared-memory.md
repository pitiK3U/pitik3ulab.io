+++
title = "02 - Programování v prostředí se sdílenou pamětí"
date = 2022-03-02
+++

# SMT - Simultánní multithreading
- 2 kopie registrů v jednom jádře
- při přístupu do paměti se rychle vymění vlákno a použije se kopie registrů, než se dostaví informace z paměti
- sdílení cache vlákny! -> může být problém, když si navzájem invalidují cache
- z pohledu OS (Unix / Linux) je to jako 2 jádra! -> problematické: jedna běží jinak než druhá

# Procesy a vlákna
- IPC - porušuje izolaci procesů -> je mimo proces
  - jednak explicitně vytvořit, tak **explicitně zabít** -> je mimo proces, neskončí se skončením procesu
- při vytváření vláken může dojít k vyčerpání místa na zásobníku -> každá vlákno jiný zásobník

# Efektivní užití cache
- při čtení z paměti, cache načte informace z okolí čtené informace
- problém při změně hodnoty - čas zápisu do paměti + cache ostatních vláken/procesorů
- hlavní paměť je rozřezaná na cache line (např. po 64 bajtů)

## Zásady
- prostorová blízkost
- časová blízkost
- zarovnaná alokace paměti

## False sharing
- při načtení celé cache line a zapisování na různá místa do cache line ve více cachí -> invalidace cachí
- zamezení = mezi prvky pole se nacpe místo, které dá další prvky do jiné cache line = není invalidace cache

## Nestále proměnné
- proměnné se může vy-optimalizovat z programu pryč (v daném vlákně nebyla použita)
- řešení: `volatile` - nestálá proměnná, musí se šahat do paměti, nelze být
- pořadí zápisu:
  - intel a amd je zachováno
  - arm nemusí být zachováno
  - řešení: paměťová bariéra
  - na x86 prefix `lock` lokální zamčení pro daný zápis
