+++
title = "08 - analytický model"
date = 2022-04-07
+++

# Analytický model paralelních algoritmů
- manycore vs multicore (desktop vs g-gpu)

# **Ahmdahlův zákon**
- bude na zkoušce

# **Cenově optimální řešení**
- jsou vhodné spíše pro problémy, které jsou nekonečně "nafukovatelné" - např. herní engine, co vykresluje okolo postavy 40 yardů, nepotřebuje optimální cenové řešení

# **Škálovatelnost paralelních programů**
-

# (Izoefektivita) - konstantní efektivita
- efektivitu, jsem schopen zachovat, pokud ten overhead / sekvenční čas je konstantní
-> overhead je roste pomaleji, než sekvenční čas
