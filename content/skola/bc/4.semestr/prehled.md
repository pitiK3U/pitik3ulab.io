+++
title = "Přehled"
date = 2022-02-13
+++

# IB005 - Formální jazyky a automaty
- V předmětu nebude žádná vnitrosemestrální písemka, pouze závěrečná zkoušková písemka a domácí úkoly. Během semestru bude každý týden (až na ten první) zadán domácí úkol. Úkol se bude obvykle skládat ze dvou příkladů a bude na něj možné získat zpravidla 1 bod. Za domácí úkoly bude možné během semestru získat celkem 13 až 14 bodů. Úkoly jsou rozděleny do 3 bloků vždy po 4 úkolech: 1. – 4. úkol, 5. – 8. úkol, 9. – 12. úkol. Nemusíte odevzdat každý domácí úkol, ale k závěrečné zkoušce mohou přijít pouze studenti, kteří z domácích úkolů získají alespoň **7 bodu a zároveň získají z každého ze tří bloků alespoň 2 body**. Body získané z úkolů nad hranici 7 bodů slouží jako tzv. měkké body a mohou vylepšit známku v případě úspěšného absolvování předmětu.

## Zkouška
- Zkoušková písemka bude na 100 bodů a její přesná forma bude vzhledem k okolnostem určena později. Kdo z písemky získá méně než 50 bodů, dostane hodnocení F. Kdo z písemky získá alespoň 50 bodů, tomu se přičtou měkké body z domácích úkolů a známka se určí dle následující tabulky:

# IB109 - Návrh a implementace paralelních systémů
- 10 týdnů přednášek
- poslední 2 týdny miniprojekty - nebudou kontrolované

## Zkouška
- závěrečná písemka
- "všechny mé hloupé vtípky, které řeknu mohou být na zkoušce"
- nerovnoměrné rozdělení bodů za příklady

### Hodnocení
- A >=90%
- B >=82%
- C >=75%
- D >=67%
- E >=60%


# MB153 - Statistika I
- 35 bodů z odpovědníků
- 6 odpovědníků, **nutných 5** vyplnit

## Zkouška
- Na 50 bodů
- 5 otázek na (každá max 10 bodů), 2 otázky teorie (sada v isu v osnnově), 3 příklady
- 100 minut

### Hondocení
- A ... 77 - 85 bodů
- B ... 68 - 76 bodů
- C ... 59 - 67 bodů
- D ... 50 - 58 bodů
- E ... 41 - 49 bodů
- F ...  0 - 40 bodů

# PB161 - Programování v jazyce C++
- semester maximum 72 points
- need **40 points** to pass the semester
- minimum **4 tasks**

## Cvičení
- příravné příklady (typ `p`) - vyřešit 3 do soboty před cvičením
- další (typ `r`) - body za aktivitu
- diskuze řešení

## Exam
- minimum **8 points** from 18 points
- 31.5. 12:30 - 17:00

# PB077 - UNIX -- programování a správa systému II
- průběžné odpovědníky
  - 4 otázky, +2 a -1, alespoň 4 body na splnění
  - 12 dní na splnění
  - **nejvýše tři nesplněné**
  - omluvenka na 7 dní
- na konci nové slajdy!

## Zkouška
- výběr právě jedné správné možnosti


# PV080 - Information security and cryptography
- short 5 min test before seminar, 9% of final mark (9 points)
- 2 worst results doesn't count

## Seminar
- MANDATORY, max 2 absenses

## Assigments
- **5-6 homework, 31% of final mark** (31 points)
- atleast 10 to finish
- roughly every two weeks
- individual
- mark within 10 days

## Exam
- final exam for 60 points

### Marking
- A >=90%
- B >=80%
- C >=70%
- D >=60%
- E >=50%
- F <50%


# VB006 - Panorama fyziky II

# p950 - Tělesná výchova - jóga
