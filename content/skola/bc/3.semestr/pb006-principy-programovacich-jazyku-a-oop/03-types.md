+++
title = "03 - Datové typy"
date = 2021-10-05
+++

# Datové typy
- kompabilita typů = `3 + 2.5`

## Decimální typy
- BCD = binary coded decimal
- přesná reprezentace čísel

## Python
- `Ellipsis` = Unit v Haskellu

# Typová kontrola
- **silně** neměla by se stát operace která není definovaná
- **slabě** = to co není silně typované

## Rovnost
- jmenná ekvivalence = stejné jméno == stejné typy
- strukturální ekvivalence = stejné typy, pokud mají identické struktury

# Pole
- column major Fortran
    - důvod: v některých matematických výpočtech se více hodí chodit po sloupcích

# Seznamy
- homogenní = prvky stejného typu
- heterogenní = prvky různého typu

## Lisp
- `cons` = construct
- hlavička seznamu `car` = Contents of Adress (part of a) Register
- tělo/další prvek `cdr` = Contents of Decrement (part of a) Register

# Typové odvozování (type inference)
- **bude na zkoušce**
- typový konstruktor = vezme typ a vrátí jiný typ

## Unifikační algoritmus
- *Robinson*

# Ukazatele a odkazy
- Java má jak odkazy, tak ukazatele; všechny odkazy jsou ukazatele
