+++
title = "05 - Podprogramy"
date = 2021-10-28
+++

# Podprogramy (subroutines)

## Parametry
### Formální parametry (parametry)
- parametry v hlavičce `int foo(int a, int b)`

### Aktuální parametry (argumenty)
- parametry se kterými funkci voláme `foo(5,1)`

# Předávání parametrů

## Předávání sdílením (pass by sharing)

# Přetížené a generické podprogramy
- koerce
  - které verze se stejným počtem parametrů se má zavolat

## Polymorfismus

### Parametrický
- `sort :: [a] -> [a]`

#### Generics
- explicitní: (c++) typ napíšu
- implicitní: haskell, nemusíme psát typy

# Koprogramy
- pseudoconcurrency: spolupracují spolu, ale v každém okamžiku může běžet pouze jeden
