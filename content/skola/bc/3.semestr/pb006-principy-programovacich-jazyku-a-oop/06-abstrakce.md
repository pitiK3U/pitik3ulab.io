+++
title = "06 - Abstraktní datové typy (ADT)"
date = 2021-11-02
+++

# Abstrakce
## Procesová
- abstrakce programu

## Datová

## Rozdělení funkcí nad ADT
- **creators**
- **producers**
- **observers**
- **mutators**
- *mutable ADT*: creators, observers and mutators
- *immutable ADT*: creators, observers and producers

## Skrývání informace (information hiding)
- skrýt vnitřní implementaci
- nemělo se stát, aby uživatel mohl přímo zasahovat do mého typu -> aby uživatel "nemohl dělat hokej"

## Zapouzdření (encapsulation)
- Spojení dat a operací nad daným typem pod jendím jménem.

# Implementace ADT

## ADT v Pythonu
- information hiding
  - vše veřejné
  - "private" funkce začínající `__` třída `Foo` s metodou `__bar` bude `_Foo__bar` == **mangling**
    - `<objekt>._<název třídy>__<název člena třídy>`

## ADT v C#

### Properties
- něco mezi public a gettter/setters
- `public string name { get; private set } = "";`
