+++
title = "07 - OOP"
date = 2021-11-09
+++

# OOP
- komunikace mezi objekty pomocí rozhraní (**message passing**)

## Dědičnost (inheritance)
- dědění z více směrů = **dopadá špatně**

### Liskov Substitution principle (LSP)
- Objekt typu `T` může být, **bez negativních důsledků**, kdekoliv nahrazen objektem typu `S`, kde `S` je podtřítou `T`

### Konstruktory
- **konstruktory se volají v pořadí od rodičů k dětem**

### Destruktor
- **Destruktor jako poslední operaci volá destruktor svého rodiče**


## Dynamická (pozdní) vazba/polymorfis (dynamic (late) binding)
- v C# se volá funkce typu (1. varianta)

### Časná (statická) vazba
- **Použije se deklarovaný typ proměnné.**
- děje se za **překladu**

### Pozdní (dynamická) vazba
- **Použije se skutečný typ objektu.**
- děje se za **běhu**
- **překrývání metod (method overriding)**

#### Tabulka virtuálních metod (virtual method table)
- objekty stejné třídy odkazují na stejné vtable

# Abstraktní třídy


# Vícenásobná dědičnost (multiple inheritance)
-

## The diamond problem

# Rozhraní (interfaces)

# Kořenová hierarchie
- Třída, ze které dědí všechny třídy = je v kořeni celé hierarchie

## Podtřídy objektu
- `GetHashCode()` = když nevíme jak, tak `^` (xor) všech položek

# Boxing
- Proces konverze nějakého hodnotového typu na **object**.
- Při **odpojení** prvek se osamostatní sám na haldu. (není svázaný na původní hodnotu)
- `ArrayList` (lze představit jako `List<object>`), `Stack` - může vkládat pouze objekty, abych mohli použít int, musíme použít boxing

# Mixins
- využití pro velké programy
- přimixuje si k věcem děděné z rodiče implementace z jiné třídy
