+++
title = "04 - Tok řízení"
date = 2021-10-19
+++

# Výrazy (expressions)

## Pořadí vyhodnocení operátorů
- priorita a asociativita == "přidávání závorek"

### Priorita (precedence)
- `2 * 5 + 3` == `(2 * 5) + 3`

### Asociativita
- `8/2/2` - priorita neříká, pořadí u stejné priority
  - `4/2=2`
  - `8/1=8`

### Prioritní tabulka
- **Na zkoušce**

```
int a = 10, b = 3, c = 2, d = 4, result

a + a * - b / c % d + c * d
a + a * (- b) / c % d + c * d
a + (((a * (- b)) / c) % d) + c * d
((a + (((a * (- b)) / c) % d)) + (c * d))
              -3
         -30
                    -15
  10                     -3       8
              7                   8        = 15
```

```
++x >= y * 2 || y % 2 && z++ % 2
```
- Bacha na side effects  (`z++`)

### Zkrácené (líné) vyhodnocení (lazy evaluation)
- `true || ...`
- `false && ...`

## Přetěžování operátorů (operator overloading)
-

## Přiřazovací příkaz (assignment)
- dvojté volání `index(i)` může vrátit jiný výsledek

## Unární přiřazovací příkazy
- `++a` vrací `a+1`
- `a--` vrací `a`
```c
A[++i] = b; // A[i+1] = b; i += 1;
```

```c
*(t = p, p += 1, t) = *(t = q, q += 1, t); /* same as *p++ = *q++; */
```

# Vedlejší efekt (side effects)
- referenčně průhledné (referential transparent) == zavolání funkce se stejným argumentem je stejný výsledek

# Iterátory
