+++
title = "08 - Výjimky"
date = 2021-11-30
+++

# Výjimky
- při vzniku výjimky se vytvoří **objekt** typu daného výjimkou
- speciální mód řízení = obsluha výjimky (výjímečný stav)
- `try` / `catch` / `finally`
  - `catch(Exception e)` - Pokud nastatne výjímka typu `Exception`
    - (`catch` == `catch(Exception e)`)
    - Může být více bloků, berou se postupně od vrchu
  - `finally` - spouští ve vždy po skončení bloku `try nebo catch` (i v případě vyskočení z funkce v try např. `return`)
    - provede se i pokud daná výjimka nemá `catch` blok
- **perf**:
  - potenciálně velmi drahá operace
  - na perf je lepší explicitní error handling, ale otravnější

## Propagace výjimek
- nezachycená výjimka **bloku** se propaguje do syntakticky nařazeného bloku
- nezachycená výjimka **metody** se propaguje do volající metody
- nezachycená výjimka v **`Main`** způsobí ukončení programu

## Řetězení výjimek
```csharp
catch (Exception e) {
  throw new Exception("new Exception", e); // Bundles the old exception with new one
}
```
