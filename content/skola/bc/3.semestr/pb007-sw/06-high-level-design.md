+++
title = "06 - High-level design"
date = 2021-10-26
+++

# Software reliability and availability
- Reliability =
- Availability =

## Run-time fault detection tactics
- At later stages it gets increasingly difficult to find and quickly fix bugs.
- Bugs must be eliminated before user finds it.

## Performance tactics
- Vertical scaling - tuning within one server - i.e. adding more RAM
- Horizontal scaling - adding more servers

## Open/closed principle
- Opened for extension, closed for modification.

