+++
title = "10 - Evolution"
date = 2021-11-30
+++

# Evolution processes

## Technical debt
- how expensive is it to change feature
- cleaner implementation => cheaper change

## Software reengineering
- make system easier to maintain
- restructure => easier to pay the debt

# Software maintenance
