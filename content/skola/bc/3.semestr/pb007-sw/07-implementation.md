+++
title = "07 - Low-level design and implementation"
date = 2021-11-02
+++

# Design patterns
- abstract solution to specific problem

## Creational
- create / manage creations

## Structural

## Behavioral

## Observer pattern
- subject has list of observers
- notifies observers on change
- observer changes dependant on the change (instead of periodically asking whether the subject changed)

## Facade pattern
- If you have subsystem it makes sense to have only one entrance to the subsystem
- "gate keeper"

## Adapter

# SOLID
- Single responsibility
- Open/closed
- Liskov substitution
- Interface segregation
- Dependency inversion

## Single responsibility principle
- each class should have single responsibility

## Open/closed principle
- **Opened for extension**, but **closed for modification.**
- Use **inheritance** and **interface**

## Liskov substitution principle
- **Objects of type T may be replaced with objects of type S**


## Dependency inversion principle
- create interface between dependency relationship

# Sequence diagrams
- **we care a lot about those in exam**