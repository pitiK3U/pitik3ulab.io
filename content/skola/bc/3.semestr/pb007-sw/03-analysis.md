+++
title = "03 - Analysis and Design"
date = 2021-10-05
+++

# 5. Design models
- Static
    - Packaging
- Dynamic
    - Messaging

# UML for OO methods

## Interaction
-

## Behavioral
- How does object changes through time?
- Lifecycle, behavior happening

# Analysis classes
## Rule of thumb
- *omnipotent* - classes managing everybody
- *functoids* - EnrollStudent

#
- underline = object diagram
- no underline = class diagram

# Abstract operations & classes
- italics == abstract

# OOP
- **low coupling**
    - = málo šipek
    - hodně tříd závisí na jedné třídě, lehko se rozbije hodně kusu kódu
- **high cohesion**
    - = třídy jsou hodně spolu spjaté


# DDD - Domain driven design
- (https://martinfowler.com)[https://www.martinfowler.com]
- AnemicDomainModel (Spatny, je lepsi je DDD)
- CQRS5
