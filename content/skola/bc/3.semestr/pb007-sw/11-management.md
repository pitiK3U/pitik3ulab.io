+++
title = "11 - Software Development Management"
date = 2021-12-07
+++

# Project management

# Project planning

## Gantt chart
- visualization of task's progress
- critical path = path from start to end that task depends on previous one
- allocate one team to the whole critical path to avoid team waiting on another team
- student symptom:
    - the see they can have 1 week buffer, so they start late

## Staff allocation chart

## Team size
- bigger team results in slower communication
- Two pizza rule (optimal team = 4-6 members)

## COCOMO
- Cost Construction Model

## Gallup
- learn top 5 talents

## Optimal team size
- team size of 2
    - pair programming assignment
- team size of 20
    - manager + help desk team

## IT positions
- [www.itpozice.czechitas.cz](www.itpozice.czechitas.cz)