+++
title = "05 - I/O operace"
date = 2021-11-08
+++

# I/O operace
- soubor = proud bajtů

## Otevření souboru
- absolutní cesta začíná `/`
- relativní nezačíná `/`
- `O_SYNC` = nesyslí si data do writeback cache

## `close`
- nevynucuje vylití write-back cache

## Pozice v souboru
- soubor má ukazovátko na aktuální pozici
- na některé soubory nelze volat `lseek` - např. roura

## `O_APPEND`
- v první variantě může dojít k uspání mezi `lseek` a `write`
- atomická operace = nelze přerušit

- při zapisování více procesy, si procesy musí synchronizovat

## Tabulka otevřených souborů
- `struct file` - vytvoření při otevření souboru, nezávisle alokované objekty
- ukazují na i-uzel
- => proces může mít otevřený stejné soubor víckrát pod jinými `struct file` (ale ukazují na stejný i-uzel)
- když je soubor otevřený a je smazán => stále zůstane přístupný programu i po smazání
- V linuxu: cache adresářových položek (`dentry`) - aby nemusel stále procházet časté cesty, např. `/usr/bin/shell`
- pro nový fd se používá první volná položka v tabulce
- úkol:
  - první varianta má sdílený offset, druhá ne
