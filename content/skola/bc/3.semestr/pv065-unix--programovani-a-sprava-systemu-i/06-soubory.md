+++
title = "06 - Práce se soubory"
date = 2021-11-09
+++

# i-uzel
- čtení informací pomocí `stat`
- měli 13 pointerů
  - 10 přímých odkazů
  - 11. datový blok ukazoval na blok nepřímých odkazů - další pointery na datové bloky (`blok size / pointer size`)
  - 12.
  - 13.
- malá režie pro malý soubor, unosná režie pro velký soubor
- => přístup na začátek je rychlý
- přímý přistup ke kterémukoli místu souboru
- díry v souborech: velký soubor, vetší než disk, díry mezi daty
  - když čteme z díry, čteme nulové bajty
- `ls -ls` první číslo, kolik bloků zabírá

## Maximální teoretická velikost souboru

## Extent-based souborové systémy
- (start, length)
- můžeme popsat libovolný dlouhý souvislý blok
- xfs, btrfs

## Stat
- hlavní číslo = číslo drivu v kernelu
- vedlejší = které z možných spravovaných zařízení to je
- sticky bit = adresář s právy pro všechny, nemůžeme mazat cizí soubory
  - dříve se soubor přilepil na swapovací prostor

# Ověření přístupových práv

## `access(2)`
- Ověřuje proti **reálnému** UID/GID místo **efektivnímu**
- úkol:

# Nově vytvářené soubory
- skupina
  - v SVR4 podle GID adresáře když má adresář nastavený `set-gid` tento bit se dědí rekurzivně

## `umask(2)`
-

# Změna přístupových práv
- úkol:
  - `chmod 04754 test`
- úkol:
  -
- úkol
  - If the size changed, then the st_ctime and st_mtime fields (respectively, time of last status change and time of last modification;)

## `truncate(2)`
- změnit soubor na danou délku
- další služba jádra, co změní velikost souboru
  - write
  - open při `O_TRUNCATE`

## `link(2)`
- pouze v rámci filesystému - musí být jednoznačné čísla i-uzlů

## `unlink(2)`
- potřeba právo zápisu do adresáře, né do toho souboru

## Přejmenování souboru
- úkol:

## Časy souboru
- úkol:

# Symbolický link
- obsah souboru je cesta k jinému souboru - kernel nás přesměruje
- absolutní = vyhodnocuje se k root adresáři procesu, který se na něj dívá
- úkol: LINUX
```bash
2883594 -rw-r--r-- 1 piti piti 0 Nov 19 04:56 ježek
2883604 lrwxrwxrwx 2 piti piti 6 Nov 19 04:56 ptakopysk -> ježek
2883604 lrwxrwxrwx 2 piti piti 6 Nov 19 04:56 tučňák -> ježek
```

# Adresáře
- délka jména: dnes až 252 bajtů + 4 bajty i-uzlu
- v cestě se `//` a více zrdcne na `/` (dvě lomítka na začátku mohou být speciální)
- lomítko na konci (`soubor/`) vynutí cestu vyhodnotit jako adresář

## `getcwd(3)`
- vrací pointer do bufferu, nelze počítat, že bude ukazovat na začátek bufferu
- pwd - shell builtin - nezaznamená změnu adresáře
- `/bin/pwd` - projde cestu, zaznamená změnu

## Kořenový adresář procesu
- každý proces může mít jiný kořenový adresář
- úkol:
  - superuživatel (potřeba změnit uživatele i skupinu)
  - pracovní adresář nad root adresářem

# Atomické operace na disku
- zaručení persistence dat (normálně není zaručeno, kdy se zapíše na disk - potřeba např v databázích)

## Synchronizace disku
- snaha nezapisovat data z writeback cache = mohou přijít aktualizace
- nečekat na zaplnění writeback cache
- snaha nezapisovat plnou rychlostí = aby byla možnost číst z disku

### `sync(2)`
- nečeká na ukončení = nevíme, jestli operace doběhla

## Synchronizace deskriptoru
- data která čekají na zápis z toho file deskriptoru zapiš na disk
- oboje skončí až se zápis dokončí
- `fdatasync(2)` = nezapisuje metadata (čas modifikace, ...)
- `fsync(2)` = zapisuje vše (pomalý = musí zapsat do journalu, musí se commitnout i předešlé změny)


## ACL
- uspořádaný list = závisí na pořadí
- vybere se první shoda
- `acl(5)`, `getfacl(1)`, `setfacl(1)`
- extended attributes
  - 1. nový blok
  - 2. možnost větší i-node v fs (místo nutných 64bajtů mámě 128 bajtů)
