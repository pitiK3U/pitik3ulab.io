+++
title = "03 - Jádro systému"
date = 2021-09-27
+++

# Start systému - firmware

## Primární zavaděč
- v jednom sektoru, pevná délka

## EFI - extended firmware interface
- novější jak standardní PC BIOS
- skoro vlastní OS
- adresace disku,
- buď EFI spustíme druhý zavaděč nebo k linuxu připlácneme EFI věci a spustíme linux

# Start jádra: parametry jádra
- Kam se mají zprávy vypisovat: VGA, sériová linka
- Kořenový disk:
- `bootparam(7)`

## Průběh inicializace jádra
- výpisy na konzoli - dvoufázová:
    1. firmware
    2. ovladač uvnitř kernelu
-

## Inicializace zařízení
- **bloková** = mají bloky, sektory, ... (má cenu optimalizovat v jakém pořadí operace provádíme, aby hlavičky moc nemuseli skákat)
- **znaková** = terminály,
- ukázalo se, že se hodí i jiné zařízení - např. síťová karta = reakce na data
    - většina není zpřístupněná k uživateli
- Obsluha zařízení = virtuální tabulka funkcí
    - Privátní data = pro kernel neprůhledné struktura, pointer na data

## Iniciální ramdisk
- malý file system
- obsahuje malé programy pro inicializaci systému - např. zašifrovaný disk

# Procesy

## Procesy uvnitř jádra
- služby jádra = kontext stejný jako u daného procesu, jen se zvýší práva procesu

# Virtuální paměť
- stránková tabulka
    - = překladová tabulka
    - překlad rozsahů virtuálních adres na rozsahy fyzických adres
    - často více-úrovňové
- stránky mohou být ještě nenačteny v paměti

## TLB - Translation Look-aside Buffer
- "cache" procesoru pro stránky
- Sw plněný TLB
    - např riscv, sparc-ix
    - procesor zavolá výjimku při nenalezení stránky v TLB
    - obsluha musí běžet s fyzickými adresy, aby nedošlo z zacyklení
- přepínáme-li v jádru kontext, musíme vylít TLB
- Lazy TLB switch - mapování jádra je ve všech kontextech stejná = nemusíme vylívat TLB

## Virtuální paměť uvnitř jádra
- při přepnutí do jádra, dochází ke změně ukazatele na stack
- omezení = jak velký máme zásobník

## Paměť z hlediska hardwaru
- není možný u ovladače přerušení
    - běží pod kontextem procesu
    - proces může být odswapován

## Přístup do uživatelského prostoru
- Použití neplatné adresy musí být zaznamenáno: `_ASM_EXTABLE(1b, 4b)`
    - (číselné labely jsou lokální, postfix směr skoku `b`)
    - trik: vlastní sekce pro spravení použítí neplatné adresy: `section .fixup`
    - `less uaccess.h`
    - musí porovnat, že nepíši do paměti jádra

# Paralelní stroje
- cc-NUMA = aby cache všech procesorů v systému obsahovali aktuální informace
    - všechny NUMA uzly poslouchají, co kdo dělá
    - skoro všechny x86 s více než 1 socketem

## Zamykání kódu

### Semafory
- `down()` - nepřerušitelný Semafor
- `down_interruptible()` - přerušitený semafor

### Spinlock

## R/W zámky
- read zámek - neexkluzivní
- write zámek - exkluzivní
- problémy:
  - inverze priorit
    - nízká priorita drží zámek, více prioritní proces čeká na nižší proces
    - nízko prioritní proces dostane prioritu nejvyššího procesu čekajícího na zámek
  - upgrade r-zámku na w-zámku
    - pustí čtecí zámek, snaží se chytit zápisový zámek, až načte zápisový musí si ověřit, jestli se nezměnili data

### Read-copy-update
- Umožnit uživatelům nechat vidět konzistentní svět, aby nemuseli pro každou operaci sahat atomickou operací
- umožní běžet bez zámků
- Určeno pro datové struktury, které se častěji čtou než zapisují
- Při RCU nesmí být na procesoru přepnut kontext
  - Jestli došlo na všech procesorech k přepnutí kontextu, tak už nikdo nemůže se dívat na starou verzi struktury
- [https://liburcu.org](https://liburcu.org)
- [Perfbook2 - Is parallel programming hard, and, if so, what can you do about it?](https://mirrors.edge.kernel.org/pub/linux/kernel/people/paulmck/perfbook/perfbook.html)

# Alokace paměti

## Problémy
- Stejné zarovnání cache: aby nebylo na stejné adrese v cachi, např na adrese dělitelné 128
- Studené cache-cold objekty
  - naalokovat nedávno uvolněný objekt, který zůstal v cachi procesoru
  - vs nejstarší uvolněný = není v žádné cachi

## SLAB alokátor
- stránku rozsekám na objekty stejného typu objektů
- volné místo - cache coloring = každá instance aby padla na jiné místo cache
- lze udělat i bez zamykání = částečně obsazená stránka je pouze na jednom procesoru
- znám-li strukturu mohu použít konstruktor a destruktor

# Časovače
- *race to idle* - dilema, jak využít spící procesor

# Čekací fronty
- *thundering herd* - všechny procesy se probudí a porvou o zámek
