+++
title = "12 - SystemV/POSIX IPC"
date = 2021-12-06
+++

# 

## Semafory
- proměnná obsahující celé nezáporné číslo (např stav zamčeno a odemčeno)
- nesmí klesnout pod nulu
- **Blok** semaforů - pracuji se skupinou semaforů v rámci bloku
- `semget(2)`, `semop(2)`
- `SEM_UNDO` - při ukončení procesu jádro zaručuje vrácení hodnoty


## Sdílené paměti
- `shmget(2)`, `shmat(2)` - připojení paměti
- změny jsou vidět hned, až na přeuspořádání procesorem (=> paměťové bariéry)

## Message queue
- `msgget(2)`, `msgsnd(2)`, `msgrcv(2)`
