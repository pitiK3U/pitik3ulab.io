+++
title = "02 - Normy API a program v uživatelském prostoru"
date = 2021-09-20
+++

# Normy API
## ANSI C
- izolovaný samostatný program - neřeší se procesy, ...

## IEEE POSIX

## Single UNIX Specification
- obsahuje POSIX, ISO C, hlavičkocé soubory, ...

## Další normy
- **FIPS 151-1 a 151-2** - upřesnění normy POSIX.1 směrem k praktickému použití

## Volitelné vlastnosti v normách
- Volitelné = nemusí dané normy obsahovat
- **Volby při kompilaci** (podporuje systém řízení prací?)

## POSIX

### POSIX.1 - detecke verzí
- Viz též `feature_test_macro(4)`

### Globální limity
- `sysconf(2)`
- runtime globální limity = `sysconf()`

### Souborové limity
- Závislost na souborovém systému.
- Souborové limity = `pathconf(2)`

# Program v uživatelském programu

## Start programu
- Před spuštěním programu jsou *přípravné * - `crt1.o`,

## Start uživatelského programu
- `main()`
- `char ** argv` - typicky čteme, ale lze měnit
- `argv, envp` - *jakoby* za zásobníkem, mezi datovou sekcí a zásobníkem
- při zvětšení se přesunou jakoby na haldu
 
## Ukončení programu v C
- **8 bitové číslo se znamánkem**
- návrat z `main()` nebo `_exit(2)`
- `exit(3)` - knihovní funkce, vylívaní bufferu, destrukce, ...

## Uživatelský úklid v programu
- `atexit(3)`

## Ukončení procesu
- `_exit(2)` - **služba jádra** pro ukočení procesu, žádné vylívaní bufferu, destrukce. Pouze ukončení procesu.

## Násilné ukončení programu
- `abort(3)` - ukončí proces zasláním signálu SIGABRT a uloží obraz adresového prostoru do souboru core.

## Přepínače

## Chybový stav služeb jádra

## Textový popis chyby

## Získání chybové zprávy

## Proměnné prostředí
- chybně nastavené proměnné prostředí nezamezí průběhu
- vs cmdline args ukončí program

## Alokace paměti
- procesor používá adresy např 8x (16x) menší
- načítá tyto řádky (např osmice bajtů)

### `malloc`
- zarovnán pro libovolný typ proměnné -> adresa dělitelná 8

## Nelokální skoky
- například skok z rekurze nebo více vnořených funkcí

## Dynamické linkování
