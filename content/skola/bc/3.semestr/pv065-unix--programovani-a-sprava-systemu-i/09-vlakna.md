+++
date = 2021-12-06
title = "11 - Vlánka"
+++

# Vícevláknové aplikace
- IEEE POSIX 1003.1c (POSIX threads)

## Kontexty vjádra versus vlákna
- jak a jestli jdou v kernelu vidět vlákna
- `1:N` - jedno vlákno v kernelu, více v uživatelském prostředí, nerozloží se zátěž na více procesorů
- `1:1` - cokoliv je viděť uvnitř kernelu
- `M:N` - zvlášť vlákna s kontexty v jádru a zvlášť vlákna v user-space
  - před zablokováním vlákna se pošle zpráva o zablokování a je možnost vykonávat jiné user-space vlákno

### Linux
- `clone(2)` - obecnější fork, vyrobení nové kontextu s příznaky

## Vytvoření vlákna
- `pthread_create(3)`
- předáváme poitner na funkci, které se zavolá, s jejími argumenty
- i struct s popisy vlastností pro thread

## Ukončení vlákna
- `pthread_exit(3)`

## Čekání na ukončení vlákna
- `pthread_join(3)`

## Vlákna a mutexy
- `pthread_mutex_init` - pro alokaci na haldě

## Podmíkové proměnné
- `pthread_cond_*(3)`
  - atomicky se vlákno odemče a čeká se na událost a po události se snaží zamčít a vrátit se
- Podmínková promměná - hlášení o události jinému vláknu

## Soukromá data vlákna
- globální proměnné, ale pro každá vlákno vlastní instance

## Vlákna a signály
- mohu poslat sígnál specifickému vláknu daného procesu

## Vlákna a sobury
- funkce nejsou specifické pro vlákna, lze použít i v jednom vlákně
