+++
title = "01 - Úvod"
date = 2021-09-13
+++

# Vývojové prostředí
- `cc` - program spouští podprogramy
	- `cpp` - C preprecesor; `cpp -E`; generují `*.i`
		- pracuje s `#`, ale po inkluzi, stále `#`
		- označuje řádek z jakého souboru to je, spracovává lexikální ???
		- pomocí nich hlásí syntaktické chyby
- `cc1` (`comp`) - C kompilátor; `cc -S`; generují `*.s`
	- generuje assembly kód
- `as` - Assembler; `as` (`cc -c`); generují `*.o` - objektový soubor
	- objektový soubor = obsahuje strojový kód
		- není spustitelný soubor
- `ld` = Linker (loader);
	- `*.a` - statické knihovny
	- implicitně se přidává std lib jazyka C
	- `ldd` 

- **Start kompilace** se řeší pomocí **koncové přípony**

## Parametry preprocesoru:

## Parametry kompilátoru:
- `-O[číslo]`
	- od dáného čísla (2/3) jsou optimalizace nestandartní/splňují další požadavky, které umožní kompilátoru optimalizovat více, např. `pointer aliasing`
- `-g` lze použít i při optimalizaci, ale optimalizace může lokální proměnou vyoptimalizovat
- `-p` nakonci funkce je instrukce, které zaznamená projití funkce

## Parametry linkeru:

# Program v paměti
- zásobník
	- aktivační záznam funkce

# Další programy
## program `nm`
- výpis tabulky symbolů
	- `T`
	- `t`
	- `d` - data
	- `D` - globální proměná
	- `U` - Undefined, když není, zkusím najít v jiných souborech
	- `W` - weak; (šablony v cpp)

## Program `strip`
- odstraní tabulku symbolů

## Program `size`
- výpis velikostí jednostlivých sekcí
- **`bss` - sekce, kde data nejsou inicializované**
- zvětší datovou sekci o danou velikost, neinicializuje 
- string literal je v sekci `text`

## Program `objdump`
- výpis informací o objektovém souboru
- funguje i jako disassembler

# Knihovny
- před stránkováním se programy linkovali staticky, celý program se natahal do paměti
- sdílené knihovny

## Formát statických knihoven
- `ar` - archivace souborů, obecný archivátor, statické knihovny jsou jen jedno z použití.
- `ranlib` - index všech symbolů ve všech objektových knihoven

## Sdílené knhovny
- dynamické linkování je komplikovaný kód, = dynamický linker
- první knihovna, která se načítá jako první `/lib/ld.so`
- stará se o načtení dalších dynamických proměnných

### Linkování v době kompilace
- binární formát = jak jsou spustitelná data organizována
- nevýhody:
	- problém s verzemi - při nové verzi se mi změní adresy
		- řešení: tabulky nepřímých skoků, pouze pokud má funkce stejné ABI, jinak nebude fungovat
	- problém s lokalizací stringů

### Linkování v době běhu - formát ELF
- Ověřování, že daná funkce v stdlib existuje.
- Dynamické linker projde přiložené knihovny a nedefinované symboly,
  namapuje knihovnu na volné místo v paměti.
- Pokáždé znovu se řeší linkové adresy.
- Kód musí být zkompilovaný, aby mohl být umístěn kdekoliv = **PIC (position independent code)**,
  relativní adresy skoků.
- Mohu zjišťovat aktuální verze symbolů a verze symbolů při sestavování.
- Symboly textově uvedeny jako řetězce.
- Vlastní verze std funkcí = předefinování na naši funkci, např. `malloc()`.

### Použité knihovny
- `ldd [-dr) program`

## Hlavičkový soubor
- Privátní symboly: symboly začinající `_` podtržítkem.

## Ladění programu
- ladění živého programu vs ladění mrtvého programu
- havárie programu: `SIGQUIT` (Ctrl+\)
- `systemd-coredump(8)` - zamezuje implicitnímu vutvoření souboru `core.pid`
- problém vácejádrových programů -> nemohu atomicky zastavit -> ostatní vlánka mohu dále paměť modifikovat