+++
title = "10 - Pokročilé I/O operace"
date = 2021-12-01
+++

# Zamykání souborů
- zamykání celý soubor vs část souboru
- pouze jeden zámek vs vytvoření dalšího zámku
- Má zámek bránit i I/O operaci?
- dva druhy:
    - **advisory** (nepovinné)
    - **mandatory** (povinné) = držíme zámek, I/O operace někoho jiného je zablokována do odemčení zámku

## Vlastnosti zámků
- úkol:

## `fnctl(2)`
- `F_RDLCK` - zamezí pouze vytvoření zapisovacího zámku

## Scatter-gather I/O
- nesouvislá data
- `readv(2)`
- `writev(2)`

## Memory-mapped I/O
- **urychlení I/O** - ušetření kopírování dat (pro velké soubory)
- **nevýhoda** - musí se vylít TLB
- často používají sdílené knihovny

### `mmap(2)`
- může namapovat někam jinak
- `int prot` - protection

### `mprotect(2)` - změna přístupových práv
- POSIX.4 (1b) říká, že mprotect může být použito pouze na regiony získané pomocí mmap

## Zákaz swapování
- `mlock(2)`
- `munlock(2)`
- úkol:

## Zamčení celé virtuální paměti
- `mlockall(2)`
- `munlockall(2)`
