+++
title = "07 - Komunikace mezi procesy"
date = 2021-11-12
+++

# Roura
- **Datový kanál** - zasílání dat mezi procesy
- **Implementace** - kruhový buffer velikosti `PIPE_BUF` nebo pomocí `conf`
  - Je garantováno, že takový zápis je atomický
- Při zapisování do otevřené roury, která byla zavřená pro čtené, obdrží program signál `SIGPIPE`

# Signály

## Čekání na signál
- úkol:
  - vrací `-1`
  - `errno = EINTR`


### Tabulka signálů
- SIGHUP = pro daemony slouží jako signál pro znovu načtení konfigurace

## Vlastnosti signálů
- `ps s` - Pending, blocked, ignored & caught - každý jeden bit

```
  UID     PID          PENDING          BLOCKED          IGNORED           CAUGHT STAT TTY        TIME COMMAND
 1000     716 0000000000000000 0000000000010000 0000000000384004 000000004b813efb Ss   pts/0      0:00 /bin/bash
 1000     756 0000000000000000 0000000000000000 0000000000381004 0000000188034a03 S+   pts/0      0:00 tmux
 1000     759 0000000000000000 0000000000010000 0000000000384004 000000004b813efb Ss   pts/1      0:00 -bash
 1000     819 0000000000000000 0000000000000000 0000000000000000 00000001f3d1fef9 R+   pts/1      0:00 ps s
```

- Problém, signál přišel ještě jednou -> pouze jeden bit na uložení -> bere se jako jeden
- Při více zaslání stejného signálu -> nemusí se vrátit stejný počet
- Zablokovaný - Přijde signál a program ho aktuálně nechce zpracovat, pak si řekne, že ho chce zpracovat (vs ignorování = nebude zpracován)

## Spolehlivé signály
- při nastavování handleru přerušitelné služby jádra, lze nastavit, že se služba restartuje místo `EINTR`

## Množiny signálů
- Nedostatek atomických operací se signály -> Množiny signálů

### Čekání na signály
- `sigsuspend(2)` - lepší než `pause` - lze vyhnout obdržení signálu před `pause`
- **atomicky** dočasně nahraní masku a po skončení vrátí

### Reakce na signál


### `SIGQUEUE`
- realtime signály
- lze posílat i data (buď `int` nebo pointer)

### `SA_SIGINFO`
- handleru chceme poslat další údaje - např `SIGSEGV` - použití adresy, zápis/čtení

# I/O multiplexing
- kopírování dat mezi fd, problém - operace jsou blokující
  - *staré řešení*: `fork` jeden čte jedním směrem, druhý proces opačným
  - *špatné řešení!! (polling)*: děláme čtení pomocí non-blocking operací -> cyklí proces, zbytečná zátěž!
  - **asynchronní I/O**: `aio.h` - necháme kernel počítat, až skončí, dá nám vědět
  - **vlákna**
  - **selektivní čekání (událostně řízené programy)**:
- řešení:
  - `select(2)` - původně z BSD
  - `poll(2)` - Systém V
  - `kqueue(2)` - FreeBSD
  - `/dev/poll` - Solaris
  - `/dev/epoll`, `eventfd(2)` - Linux
  - pro C - knihovna `libev`

## Množiny deskriptorů

### Selektivní čekání
- `rdset` - soubory připravených na čtení
- `wrset` - soubory na psaní
- `exset` - soubory čekání na výjimku

### `poll(2)`
- `struct pollfd` 
  - `events` - maska eventů
  - `revents` - události, které se stali

## 
- Při 100 tisíc fd, musíme procházet strašně moc položek, abychom zjistili, kdo odepsal -> značně neefektivní
  - Rozložení na 2 procesy
    - 1. zpracovává více fd, které posílají zprávy po delší době (většinu času čeká)
    - 2. zpracovává fd, které posílají často zprávy

## Linux
- `signalfd` - popisuje čekací signály + `siginfo`
- `pidfd`