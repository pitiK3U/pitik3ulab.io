+++
title = "04 - Procesy"
date = 2021-10-18
+++

# Procesy
## Paměť procesoru
- zásobník = nemusí růst vždy dolů, některé platformy nemají zásobník (MIPS)
- **Zero page** = často první stránka nenamapována = odchycení `NULL` pointerů - page fault
  - U 64-bit systémů nenamapované 0 a 4GB, kvůli historickým důvodům.

## Služba jádra
- glue function = napojuje C linkování a přepínání do jádra
  - při vrácení z jádra vrácenou hodnotu z registru naplní `errno` (hodnota `[-1, -4096]`)
- nepřerušitelné funkce = chovají se jako atomické operace, buď se provedou celé nebo nic

### Exec
- pouze `execve` je systémová služba, ostatní jen přeskládávají argumenty a zalovají `execve`
- `char *arg` je včetně 0. agrumentu (cesta programu), může být jiný než `char *path`

## Knihovní funkce
- vypadají jako C funkce, implementované v User Space

## Systémy se stránkováním
- **page cache** = sdílení diskových bufferů s virtuální pamětí, cache 4KB úseků jednotlivých souborů

## Stránkování na žádost
- **on demand paging** = nenačítá se do paměti, dokud není potřeba
- kernel se snaží zabránit změně vykonávaného programu, např. rekompilace + načtení nové stránky
  - **Text file busy**

## I/O operace
- otázka:
  - Mapování komplikovanější - může být více procesorový přístup -> vylití vice procesorové
  - Kopírování může vylít cache
  - možnost u kopírování nevylít cache - `load + store`

## Memory overcommitment
- nastavit míru overcommit memory = `cat /proc/sys/vm/overcommit_ratio`
- otázka:
  - Potřeba cow kopie, ale není kam namapovat stránka
  - Rušíme read-only stránky - můžeme načíst později
  - Uložíme datovou stránku na swap a pokračujeme
  - Out of memory killer: Kernel se snaží podle situace přizpůsobit: kolik jsme naalokovali paměti v poslední době, ...

## Přístupová práva procesu
- Na moderních systémech alespoň 3 UID a GID
- **reálné*, **efektivní** a **uložené**
- spuštěno z cmdline = reálné
- efektivní = číslo vůči kterému se ověřují příst. práva
- většinou reálné == efektivní
- bit `set uid` = reálné se zachová, efektivní se změní na root

## Další atributy procesu
- `pid_t getppid()` se může změnit, když skončí rodič, přepne dětem číslo rodiče na `1`

## Systémové zdroje
- otázka:
  - U+S je max reálný čas * počet procesorů
- zjištění tiků časovače = `sysconf`
  - když není spuštěný proces - čekám pouze na hw přerušení = není potřeba využívat systémový časovač
  - když ve frontě procesů je pouze jeden proces = také není potřeba využívat systémový časovač

### Struktura rusage
- minor fault = nastane vpadek stránky a obshluha je bez I/O operace
- major fault = nastane výpadek stránky a musí proces uspat, namapovat stránku a proces probudit
- dobrovolný context switch = šlápl na stránku, která není v paměti
- nedobrovolný context switch = vypršel časový limit

### Omezení systémových zdrojů
- zdroj pod číslem v `<sys/resource.h>`
- běžný uživatel smí hard limit pouze snižovat

### Typy systémových limitů
- RSS = sdílené knihovny - má se to počítat procesu nebo ne?

## Priorita procesu
- NI (nice) - uživatelská
- PRI
- nemůže nastat, že by méně prioritní proces nedostane žádný čas - kvůli držení zámku
- linux se snaží méně prioritním procesům dát delší výpočetní čas - často jsou výpočetní programy
- na linux poměr dát rozdílem mezi prioritami, né, kde leží na škále (na linuxu [-20,19])

## Skupiny procesů
- několik procesů dohromady
- problém při odkazování na proces pomocí pid - nevíme, jestli je to původní proces nebo nový proces s daným pid
  - OpenBSD přiřazuje implicitně náhodně
  - Linux pidfd - file descritor procesu, jako fd souboru

## Sessions
- větší skupina procesů

## Démoni
- daemon (duchovní bytost) vs demon (biblický démon)
