+++
title = "Přehled"
date = "2021-09-12"
+++

# MB152 - Diferenciální a integrální počet

## Vnitrosemestrální testy
- 5 průběžných testů, max 10 bodů
- Bodovací schodek je 1/4 bodu.
- **Potřeba získat v součtu 5 a více bodů.**
- Body se přenáší ke zkoušce.
- Ohlášení týden dopředu.

### Oprava
- Na konci semestru, v případě, že by celkový součet dosahovat více jak 5 bodů, pak je přeneseno pouze 5 bodů.

## Zkouška
- písemná
- Max počet bodů: 45 (5 - 10 z vnitrosem. testů)
- písemka za 35 bodů, bodovací krok 1/2 bodu

### Hodnocení
- A: [36,45]
- B: [32,36)
- C: [28,32)
- D: [24,28)
- E: [20,24)
- F: < 20

# MB154 - Diskrétní matematika
- stránky [http://www.math.muni.cz/~hasil/Data/CZ/TeachMU.htm](http://www.math.muni.cz/~hasil/Data/CZ/TeachMU.htm#Diferenci%C3%A1ln%C3%AD-a-integr%C3%A1ln%C3%AD-po%C4%8Det-(MB152---FI))
- učební text - [Matematika drsně a svižně](http://www.math.muni.cz/~naca/ucebnice/stranka.html)

## Domácí úkoly
- max 26 bodů (13 sad po 2 bodech, nutno odevzdat do týdne po zveřejnění sady, od 2. týdne, poslední za 4 body)
- První vnitro max 10 bodů
- Druhé vnitro max 10 bodů
- -> 46 ze semestru
- Závěrečná zkouška max 54 bodů

## Cvičení
- účast povinná, max 3 absence
- nedostatečná účast vede k `X`

## Závěrečná zkouška
- Méně než 20 bodů ze semestru (dú + vnitra) je hodnocení `F` pro řádný termín
- se získanými body lze pokračovat na termíny opravné.

## Hodnocení
- A: >= 84
- B: [76,84)
- C: [68,76)
- D: [60,68)
- E: [50, 60)
- F: < 50

# PB006 - Principy programovacích jazyků a OOP

## Domácí úkoly 
- **6x za semestr** dostanete domácí úkol (každý za **3 body**).
- Na řešení bude týden.
- Formou odpovědníků v ISu.
- Často ve formě kratších C# programů.
- Nutno získat alespoň 2/3 bodů. 
- Body nad tento limit se budou přičítat k bodům z úspěšně složené zkoušky. Mohou tedy vylepšit známku, ale nepomohou vám změnit známku z F na E.
- **Úkoly vypracovávejte samostatně!**
- Není povoleno přebírat řešení z jiného zdroje (fórum, tutoriál, kniha, GitHub Copilot...).
- Zadání a řešení jednotlivých úkolů je zakázáno jakýmkoliv způsobem sdílet a zveřejňovat.

## Záverečná zkouška
- písemná 
- dvě části - praktická (krátké prográmky) a teoretická
- nutno >= 50% bodů z každé části

# PB007 - Software Engineering I

## Seminars
- 12 teaching weeks + 1 final-consultation (or backup) week
- Team project on UML modeling, teams of 3 students
- Obligatory attendance (one absence ok) and weekly task delivery
- Simple test at the beginning of each seminar (starting in Week 03)
- Penalty for extra absence (-5 points) and late task delivery (-5 points)

## Evaluation
- Seminar = project YES/NO, test points, activity points and penalty recorded in IS notebook
- Exam = test (35 points) + modelling (35 points)
- Grades: F<50, 50<=E<58, 58<=D<66, 66<=C<74, 74<=B<82, 82<=A
	- A: >= 82
	- B: [74,82)
	- C: [66,74)
	- D: [58,66)
	- E: [50,58)
	- F: < 50

## Exam
- Students may register to an exam term before their project is finalized, but they can only attend the exam if their project has been - approved by their seminar tutor as accepted, which must be recorded in the student notebook Seminar completion at least 24 hours before - the exam.
- Information and instructions related to the exam are in the last sheet of the interactive syllabus.

## Final exam
- The exam consists of a test (7 test questions, 5 points each, 35 points in total) + on-site UML modelling (35 points). 
Time: 90 minutes
- The test will consist of multiple-choice questions of two types, i.e. "exact fit" and "sum of options" explained below.
	- In case of "exact fit" the question will get 5 points only when all the correct options and no incorrect options are selected. Otherwise, the question will get 0 points. 
    - In case of "sum of options" the 5 points will be distributed among the correct options, and at the same time -5 points will be distributed among incorrect options for compensation. A detailed explanation in Czech follows. If needed, ask the lecturer for clarification


# PV005 - Služby počítačových sítí
- po každé přednášce zveřejněn odpovědník
- odpovědník na 3 týdny
- odpovědník lze skládat víckrát
- za nesplněný odpovědník `N`
- max 2x `N`, při třetím `N` => Failed

# PV065 - UNIX -- programování a správa systému I
- prezentace: [www.fi.muni.cz/~kas/pv065](www.fi.muni.cz/~kas/pv065)
- [stratus.fi.muni.cz](stratus.fi.muni.cz)
- Průběžné odpovědníky
	- 4 otázky, omezený čas
	- nejvýše tři nesplněné
	- +2 body/-1 bod
- Závěrečný test:
	- výběr právě jedné správné možnosti
	- doplňovací otázky
	- body ze semestru se přenáší na zkoušku 
- Bodové hranice
	- výborné výsledky stačí na zápočet
	- špatné výsledky nebrání splnění zkoušky na E

# VB005 - Panorama fyziky I
- při každé přednášce úloha (na konci)
	- na konci semestru poslat úlohy na adresu:
	- jedno PDF se všemi úlohy (úloha 1-12)
	- snadné

# VB001 - English Exam
- online due to COVID
- **sumbit certificate to CJF FI head** one month prior to the exam
- check in with ISIC before the exam starts

## The written part
- Use of English - max 58 points 
- **formal writing test** - max 12 points, compulsory 
- **at least 60%** to get to oral part
- via odpovědníky in IS

## The Oral examination
- five-minute presentation with visuals
- follow-up discussion
- prepared handout
- active participation in discussion
- **must enroll the oral exam of the examinar who graded my formal email**

## The final asseessment
- A: [92,100]
- B: [84,91]
- C: [76,83]
- D: [68,75]
- E: [60,67]