+++
title = "10 - Procesor x86-64"
date = 2021-02-12
+++

# x86-64 architektura

## IA-64
- od firmy Intel a Hewlett-Packard
- zdola kompatibilní s x86, ale 64bitová sada úplně jiná --> neuplatnili se
- jiná instrukční sada v 64bitovém režimu: 
	- EPIC (Explicitly Parallel Instruction Computing) - možnost vkládat instrukce paralelně úkolující více jednotek
	- VLIW (Very Long Instruction Word) - tzv. instrukční paket, součástí instrukce jsou samostatné pokyny všem jednotkám procesoru
	- procesory Itanium

## x86-64 tzv. AMD64
- plnohodnotně 64bitová architektura
- firma AMD (Advanced Micro Devices) od roku 1999
- první procesor AMD Opteron 2003
- zpětně kompatibilní s x86
- procesory Opteron, Athlon, Turion, Sempron

## EM64T Extended Memory 64 technology, tzv. Intel 4
- firma Intel převzala architektura AMD64 (pod označením Intel 4)
- procesory Pentium 4, Celeron D, Xeon, Core 2
- jsou drobné rozdíly mezi AMD64 a Intel 4

# Architektura
- **long mode**
	- **64 bitový režim (Long Mode)** - plně 64bitový režim
	- **kompatibilní režim** - 32/16bitový režim, omezená kompatibilita s x86 (pouze chráněný režim, žádný reálný, žádný V86 režim)
- **x86 mode** - plná kompatibilita s x86 32/16bitovým režimem (vč. reálného režimu, ...)

## 64 bitový režim (Long mode)
- 64 bitová virtuální adresa, 52bitová fyzická adresa (4 petabyte)
- šířka adresy implicitně 64 bitů
- virtuální adresa je pouze 64bitový offset
- blízké skoky rozšířeny na 64 bitů
- flat 64bit virtuální adresový prostor
- potlačen význam segmentace jen na určování typů a práv segmentů
- místo segmentace zaveden non-executable bit u každé stránky

## Stránkování
- podporují se 4KB nebo 2MB stránky
- velikost stránky určuje bit 7 stránkového adresáře
- položky stránkových tabulek jsou 64bitové
- formát položky stránkového adresáře pro 2MB stránky

<!--stackedit_data:
eyJoaXN0b3J5IjpbMTk4OTIwODEyNSwtMTA5MzAxNjc2OCwtNT
U1MjI1NjAzLDI3NzI5NDk3NV19
-->