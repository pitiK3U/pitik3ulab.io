+++
title = "01 - Základní pojmy"
date = 2020-10-08
+++

# Základní pojmy
- **Hardware**
*[Hardware]: Technické vybavení
- **Software**
*[Software]: Programové vybavení počítače
- **Firmware**
*[Firmware]: programy součástí technického hardwaru

<!-- markdown cheats -->

- **1 bit** 
*[bit]: (BInary digiT) [1 b]= základní jednotka informace, {0,1}
- **1 byte** - pro zobrazení znaku
*[byte]: [1 B] = skupina 8 bitů

<!-- markdown cheats -->

Osmistopá děrná páska
- ASCII pokrývá 7 bitů
- vodící stopa = 3 pod ní, 5 nad
- 8\. bit je tzv. **paritní bit** k jednoduché detekci chyby v bajtu
- **sudá parita** = doplní počet děr na sudé číslo
- parita pozná pouze jednu chybu, sloužila k detekování chyby z důvodu zaseknutí děrovače, v případě vice chyb nepozná

Jak číst děrnou pásku?

5stopá děrná páska
- použití v tzv. **dálnopisech**
- 2^5^ = 32 neumožňuje současně zobrazit ani velká písmena a číslice
- řešení pomocí číslicové/písmenové změny = určují význam následujících znaků

Děrné štítky
- nahoře je vytisknutý daný příkaz
- není ASCII

1 byte je málo, pro větší čísla:
- **1 slovo** [word] = několik (2, 4, 6, 8, 10 - není přesně dáno) bajtů


- **Paměť** = zařízení pro uchovávání informace (binárně kódovaná data)
- **Operační paměť** = paměť uvnitř zařízení pro aktuálně procesorem zpracované instrukce a data
- **Adresa v paměti** = číselné označení místa v paměti
- **Nejmenší adresovatelná jednotka** = kapacita místa v paměti, které má vlastní adresu (bajt, slovo).

**Adresový registr** = n-tice bitů, která může uchovat číslo, max. 2^n^ - 1.
**Kapacita paměti** = 2^n^, kde n je počet bitů v adresovém registru

- **RAM** = paměť pro čtení i zápis
- **ROM** (data uložená i po odpojení napájení) = paměť pouze pro čtení
- **Paměť s přímým přístupem** = dostanu obsah adresy v konstantním čase
- **Paměť se sekvenčním přístupem**
- Vnitřní (operační) paměť
- Vnější (periferní) paměť
- **Registr** = n-tice bitů, velmi rychlý, součástí procesoru
- V/V zařízení (I/O devices)
- **Řadič** (**Controller**) - zařízení převádějící příkazy v symbolické formě (instrukce) na posloupnost signálů ovládajících připojené zařízení

## Architektura ,,von Neumann"
1. Počítač obsahuje operační paměť, ALU, řadič, V/V zařízení.
2. Předpis pro řešení úlohy je převeden do posloupnosti instrukcí.
3. Údaje a instrukce jsou vyjádřeny binárně.
4. Údaje a instrukce se uchovávají v paměti na místech označených adresami.
5. Ke změně pořadí provádění instrukcí se používají instrukce podmíněného a nepodmíněného skoku.
6. Programem řízené zpracování dat probíhá v počítači samočinně.

datové toky, instrukce, stavové hlášení
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTk0MjE3MDYxNywtODEyNTgyMzM0XX0=
-->