+++
title = "12 - Připojování V/V zařízení"
date = 2021-02-15
+++

# Rozhraní Centronics (EPSON)

## Paralelní rozhraní
1. **Mechanická úroveň**: Konektor Cannon 25kolíkový, na počítači je zásuvka.
2. **Elektronická úroveň**:
	- ,,0" ... 0V až 0.4V - úroveň TTL
	- ,,1" ... 2.4V až 5V - úroveň TTL

https://en.wikipedia.org/wiki/Parallel_port
https://en.wikipedia.org/wiki/IEEE_1284

# Rozhraní V.24 I. = RS-232C
- Sériové rozhraní
- Modem = modulator, demodulátor

![](https://upload.wikimedia.org/wikipedia/commons/2/2a/EIA_232_DTE_DCE_DCE_DTE.png)
Hardwarový handshake: modem: DSR (Data Set Ready) --> pc: DTR (Data Transmit Ready)

## Formát přenosu dat
- **start bit** - vždy = 0 --> zapnutí hodin, musí být domluvená perioda intervalu
- datové bity
- [možné zabezpečení paritou]
- **stop bit** - vždy = 1, 

## Parametry přenosu dat
- musíme se domluvit předem
- **rychlost**: (v bitech za sekundu) 110, 150, 300, 600, 1200, 2400, 4800, 9600, 19200
- **počet datových bitů**: 5, 6, 7, 8
- **zabezpečení**: sudá parita (Even), lichá parita (Odd), žadné
- **délka stop bitu**: 1, 1.5, 2 bity dlouhý

## Nulmodem
- Zapojení dvou počítačů bez modemů

# USB Universal Serial Bus
Idea:
- Všechna typická zařízení se stejně připojují na společnou sběrnici.
- Náhrada připojení klávesnice, myši, RS232 zařízení, Centronics.
- Snadnost použití.
- Možnost připojování/odpojování bez vypnutí.
- Současně napájí a současně přenáší data.

**Master/Slave protokol** - Komunikace je řízena/vyvolána počítačem (host)

**Maximálně 127 zařízení**

**Ochrana proti zkratu a přepětí** - Dovoluje se připojení/odpojení zařízení bez vypnutí počítače. Nutnost ochrany proti elstat. výboji - člověk až 15kV na koberci.

**Napájení z USB** - Maximální proud je omezován dle typu připojeného zařízení. Při překročení proudu je port odpojen.

Počítač se opakovaně dotazuje rozbočovačů na jejich stav a nová zařízení. (Každé připojené zařízení má vlastní adresu i Hub)
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTIwNjM3MDc4MjksLTE0MDY2MDQzNTgsMj
A3NTY0Njc2NywtMTQzODkwNDIzOSw1NTY1MTc1MDcsNzE0MTQ2
NDU3XX0=
-->