+++
title = "06 - Virtuální paměť"
date = 2020-11-21
+++

# Virtuální paměť
- Cílem: navýšit kapacity operační paměti, abych mohl mít současně spuštěno více procesů.
- Slouží k poskytování více operační paměti aplikacím, než kolik jí je fyzicky instalováno.
- přerušení + časovač (sleduje, jak dlouho proces počítá; proces počítá do přerušení časovače, po té OS předá řízení jinému procesu, atd.)
- V UNIXU jsou úrovně priorit, jsou dynamické:
	- čím déle proces počítá, tím má nižší prioritu; jakmile klesne priorita na minimum, tak se nastaví na maximum,
	- vyšší prioritu získá proces, používá-li V/V zařízení. (interaktivní programy, např. textový editor)
- Obsah celé virtuální paměti si uložím na disk a beru pouze jednotlivé části do operační paměti.
- Jak OP tak VP, je rozdělena na pevné stejně velké prostory, v paměti se nazývají Rámec (**Frame**) a na disku Stránka (**Page**).
- Základní velikost stránky (**Page**) = 4KB
- Do volných rámců se libovolně kopírují jednotlivé stránky.
- Musíme mít způsob, jak najít rámec s hledanou adresou.
- Každý odkaz na paměť obsahuje virtuální adresu:
	- S virtuální adresou pracuje programátor.
	- Virtualizace nám musí 
	- Virtuální adresa se skládá ze 2 částí:
		- 12 dolních bitů: adresa na stránce = je stejná jako v reálné adrese
		- nad tím: pořadí stránky na virtuální adrese
	- Tabulka stránek/Tabulka rámců: určuje zda daná stránka je v reálné paměti; adresa, kde na disku je; číslo, ve kterém rámci je načtena
	- Strefili jsme se (**hit**) = paměť je v reálné adrese.
	- Nestrefili jsme se (**miss**) = paměť není v reálné adrese, musíme stránku zkopírovat do rámce.
		- Generuje přerušení výpadek stránky (**page fault**) = program ukazuje na adresu, která není v paměti. (Vnitřní přerušení = procesor generuje sám, dělení nulou, ...)
		- Není-li žádný rámec volný, musíme vybrat jeden rámec za "obět". Musím zkopírovat danou stránku v rámci na adresu na disku. (Lze se vyhnout kopírování stránky zpět, v případě, že jsem stránku nijak nezměnil. = V tabulce se definuje další příznak (bit) "špinavosti", určuje, zda-li jsem danou stránku upravil.)

## Algoritmus výběru oběti

### Algoritmus LRU - Least Recently Used
- Výběr nejdéle nepoužívané položky:
	1. Ve VP vybavit každý blok čítačem, který se při:
		- volání daného bloku nuluje
		- volání jiného blok inkrementuje o jedničku
		- V případě potřeby se vyřadí blok s nejvyšší hodnotou
		- Funguje pouze, že mám všechny bloky obsazené. (Špatně se provozuje na obvodové úrovni.)
	2. Pomocí neúplné matice s prvky nad hlavní diagonálou
		- každá prvek je jednobitová paměť
		- při volání bloku i se:
			- jedničkuje i-tý řádek
			- nuluje i-tý sloupec
		- nejdéle nepoužitelné paměťové místo má:
			- v řádku nuly
			- ve sloupci jedničky

## vs Segmentace
- Virtualizace paměti = docílení většího prostoru OP pro spouštění aplikací
- Segmentace = (mechanismus), rozdělení segment s kódem (instrukce), segment s daty, segment se zásobníkem; pro každý segment jsme si **stanovili** určité **pravidla**, omezení odkud pokud je daný segment

## Vyrovnávací (cache) paměť, použití LRU
- Není nutné vždy přepisovat blok z VP zpět do OP.
- Který blok při zaplnění Vp vyhodit? (použití LRU)
- V cache může být nevalidní informace, pokud je do paměti přístup jinou cestou, než přes cache:
	- Napojení OP na VP a na kanál: kanálové procesory = jsem mezi V/V zařízeními a OP
	- V multiprocesorových systémech při sdílení jedné paměti více procesory.
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTUxMDI3OTE0MSwtODExOTQwMjkxLDU2Nj
Y2NzE0OCwtMTk1OTU5NjIzN119
-->