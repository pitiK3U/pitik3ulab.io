+++
title = "08 - Procesor 80486"
date = 2020-12-18
+++

# Procesor 80486
- 32bitový procesor,
- od 1989 do 1993
- 25 MHz až 126 MHz
- obsahuje jednotku operací v v pohyblivé řádové čárce (koprocesor)
- obsahuje interní vyrovnávací paměť (cache)
- obsahuje novou technologii blízkou RISC

- vnitřní vyrovnávací paměť
	- **Internal cache** - Je společná pro data i instrukce, má kapacitu 8 KB

## Příznakový registr i486
- **AC** Aligment Check - generuje INT 17, přistupuje-li program na adresu, která není dělitelná 4

## Schéma činnosti vyrovnávavací paměti
- Mezi vyrovnávací pamětí a operační paměti se čte po 16 B
- Mezi procesorem a Cache se čte po 4 B, procesor se ptá, zda jsou 4 B ve vyrovnávací paměti
	- Jsou-li předání 4 B je rychlé,
	- Není-li musí cache sáhnout do op. paměti
- Zapisování
	- Chci zapsat něco, co není v cache paměti, můžu zapsat rovnou do op. paměti
	- Je-li ve vyrovnávací paměti, zapíši do cache
		- Otázka, jestli se data musí dostat do op. paměti hned

- **Write-Through** = současně zapíši, jak do cache tak do op. paměti
	- Pokud budeme zapisovat do části paměti, kterou může používat více procesorů
- **Write-Back** = zapisujeme do cache a data se do op. paměti dostanou až při vyrovnávání
![50030.gif](https://is.muni.cz/el/1433/podzim2004/PB151/um/50030.gif)

## Řídící registry
- **CD** Cache Disable
	- Je-li CD=1, cache je vypnutá
	- Po RESET je CD=1
- **NW** Not Write-Through
- Ve stránkovém adresáři a tabulce přibyly 2 bity- Má-li být cache zapnuta a má-li být Not Writetrough

<!--stackedit_data:
eyJoaXN0b3J5IjpbMTUzOTcyOTE0MSwtMTg0MDg1OTM3MSw3Nj
U2ODUwNjAsMTIyOTU0MTA4MF19
-->