+++
title = "03 - Obvody"
date = 2020-10-22
+++

# Booleova algebra
- nauka o operacích na množině {0, 1}
## Základní operace
- Logický (Booleův) součin `AND ∧`
- Logický (Booleův) součet `OR ∨`
- Negace `NOT ¬  ˜ `

- Způsoby popisů
	- Pravdivostní tabulka
	- Graficky v rovině = Vennovy diagramy
	- Matematický aparát 

## Využití Booleovy algebry
- 1938 = využito pro kontaktní z
- Základní prvek: **relé** (**Relay**)
	- Relé **rozepnuto**. = Cívkou neprotéká proud, pružina kotvičku odtahuje od cívky. 
	- Relé **sepnuto**. = Cívkou protéká proud a vzniklé magnetické pole přitáhne kotvičku k cívce.
	- kontakt ovládaný dvouhodnotovou proměnnou a
		1. spínací kontakt (a)
		2. rozpínací kontakt (¬a)
### Zapojení kontaktů
- sériové `a . b`
- paralelní `a + b`

Nevýhody použití:
- First real bug (moth) found in 1947.

### Obvodové znázornění Booleovy algebry:
![Značení Booleovy algebry](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmaecellesther.files.wordpress.com%2F2017%2F10%2Fboolean.jpg%3Fw%3D1400%26h%3D9999&f=1&nofb=1)
### Minimalizace počtu operací B-algebry
1. Matematické výrazy: `!(xy)z + !(x)yz = !xz(!y + y) = !xz`
2. Využitím jednotkové krychle
3. **Karnaughova mapa** - normalizací Vennova diagramy
![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse4.mm.bing.net%2Fth%3Fid%3DOIP.adMToedhUjHzkogcXqDq1wAAAA%26pid%3DApi&f=1)![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2F3%2F35%2FKarnaugh_map_KV_2mal4_Gruppe_9.svg%2F1280px-Karnaugh_map_KV_2mal4_Gruppe_9.svg.png&f=1&nofb=1)- B-algebra je nevhodná pro technickou realizaci - příliš velký počet.

# Shefferova algebra
-  Založena na **negace logického součinu** `NAND`
- Lze převést z Booleovy algebry

# Peirceova algebra
- Založena na **negaci logického součtu** `NOR`

# Fyzikální podstata signálů
![](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAVUAAACUCAMAAAAUEUq5AAAAjVBMVEX////+/v77+/vm5uaKioqZmZmPj4/h4eHExMS4uLj5+fmlpaXs7OzPz8+enp6Tk5NxcXF+fn6EhITx8fGrq6vX19dvb2+7u7vd3d0AAACysrLKysrBwcHT09NmZmZfX19RUVF4eHhaWlo2NjZGRkY6OjpMTExCQkIrKysUFBQMDAwcHBwlJSUaGhowMDC7DlOUAAAgAElEQVR4nO2dh2KkuNKoJZFFRiSTuulx2HTm/R/vKhAUoN22d/67e45r19NJCOmjKJVSAcC3fMu/QCD/F8+ufS73fltS3D3+3yyfqlkg2EbDPfJt++7FuXv8v1qaTxxTipcouJcojre3iAnwgEUQlFKg8sNnhogelrAiENCzewbSHHt6dQjNKxjZJ5aC/tA0EbDpe9CPNCGiB4b0/4SwX7uI5TSu9xyVmRWYZUePRyHL40tCs/iw4OWYd6iO21tR+CdQIvBFqghUHahBSAqrecIdKdvI9yOLfpnHJKB3x9iBqgQDbtufyOqwV5KnoPFH1IPKxX5eoSYGgz1AHwyTFUQhaYOoK6uYlOQP4jfAL2lCC8L3i3K3mF+mGhKqB6wYrFIe1YvS4r9KVMEfYXEZb3FWf5UqsC5lWczja3CNJj90h2xILkMyd9WzEzZvBPzp91PvXEl4wUkyJC99e+sv3bUvwYyvcTGH5VyGczK8Ai/N+qSa6+FGiuo2NeMUu045lf0P4oJfQBVCiBD7Y/ct/2K/I9gtrFK9RQCWEWzakcS+V/YgJ/xXmeozfE7LInNqrJz7E1SrKfaKuvK7idT+OGF76PM+KSPv4jhNg0FnpwnIMhJmuKrdxHfCyc+H8DcEMjyBvHBImY7J+JwAz83I7BQzufUBuFahbQ9xZdvoOUS/hirjyrQPAbx8ln6EGtVLBC7j5Pa3pLTd+Jom3jP/Vab6BG5J/JokBfyqroL54tn9JXFatx587+IOozsM9iVy0njKAKDE7ZtlZ00ze7PrhPUwl/4lutFbye/qxPYulO6NXCxAJpcUtmNH9iXrMn8qnXGqPRtcSed/olyKHFoA38vpvxMCkc2Bdv36C74AXVcnDF7AnIO5dAaveg3CwKQaAWzRfyxFVT9H9VOSsDZ52j+T26k2klc54SeFUW1SO/eHzB8Lp3HDqO6KoGunIa+6wLGAlSXx7HQ4aSavsfPSKqFC1a2zYU49d+qqOquaLElFqSrf8lax+P+6WOXBl5+S9/KptARte5oR/eXsxwdPxpJ0AKTRFeR+XF/ofWrlrvPSWXMN4vqN3OwcXEgx9F3YhTWYZ0ztfYsOfIDCLrTrVSVN8I68m+BBaZy/KacmfT+jxpbTNIeVaBhV7y9qj8Y5sy6pNduR07Xz7OW5MxRVA7K29sui+t264ryupuDPM88KqTeVbAF+tSQPpnu3EXIeODSVvxyhd5SaEopJB/w+HP3+MuIB2aM1+NglPu57h/oCbhx1A+0HtQV1/2gLrFKFkXCdEeEtHOQuBPuVUoWPCOhdEDEvHOBrspc9Rw8dLbJIzk61unlcml0L7Og4ubM01O1FpCt9qnWgz1k/MV9+S/eTgTiDNsCYtuBALu/SkeCuE9VIAJd+BVz/mAZC0V3hXzgRVKjy9ose46XsXwyua8fkYV2t3movcWa/ywqnmjJS552Xj/ndLoYm57oa9pfZToPJ9qu8H7JrNNd97MT5yRGrrlquHUz53IWh3U19X/tNFv9uiZpJugoS13HD5LXLbcVv+HIvAP8oqGMY53ZwdTs3DV7jfI7Yrw9TjcNgSOIun0BR+H5RWkMy2Le+/kCJzqhCkI9hGdVhm7rt5A/Ebbo2sJ2suR0bg5Uq+aMJ2gzkeVtO1P0F7fOQFsLPlKhC2hULgprM5EW9TDLVh/ppei8AZ5Wdgskh3Y3aXTC7JM24qXmYau+GQ3IL5jqmHR0/60K7+n28xB9xu86pzoPjRLeurGnPi3YdcFyEw6Wb+xMDun7dureyesZz7icTbWRGu+6Hy3ITylQD59pNL3F1SRvZaH9dV1+b8tolU9r95eV1U8yle+E8pdGV+xK1beSNfUW6aoyiEQ49GEFUkg+U6NwCUJ+qwqMTtqDzo7F0ongAHkGld9xyrVTx6I1RD8cKD6TEpERWiUZPtwDUgGIcoaiif0pj/SWqfLyLDQO07IUAAmL6WgEOpKo+nvNn5T29Hqx3EqzyCI1zP2GXz/RvlmPInLqum7ppyv9SNxUfUva169ri5VxS570UVB5KkzvvnCl9LKv0vYxYmvn9NE7xQKl16QTV+6PW1qEjp0j3wBV8JM2Dw98PZPXIaPMjJfrMSMJK9dFR6zNJ303xWJqHegHcGX0v0SN39wMl+j8ZtT6TR4g9Us9vqrJ8U5Xkm+p78k31w2f7pirJN1VJvqkq8i+miox0COnjq8hMJCU3qIq5VfOYfezlv54qGPRlFpH0fqF6L7MDqhCAo8Ub6H+I6oS1712JiCj1pGWmfOJU+RErNXr233rg6OMRMf5foZrUia1TlU+QAtjXTa4fKsNYqE5zup4SgdxJPCfSDgr3L75CVRnD/WdSpZLrVBGU7Srq80SjCoE8Gyqo4inN1lNiUNyyyKDa/j26CmWD/c+kSks461QLYzYw03PvpU+LXcVt6e1jgpUFDKrV30RV/vCPpArBFRlUe41q+RBVahU8qd3ykEE1iT5DFUJ9mYAtvU/Fmi4QaQNnljSyx+tZFh6oIpUJH4levkrAYltgC0aZCJYXJgqqvQeIghdLVXVEukynOkK1tarRrCaACmZGlVQAzGSnSm1ElehU0Sc8q9wZjeU1ugVo6YkiW02CpabS4QUCudUpug+hBTO01oRTBcM8wBz0MhGya5CgCkFVV5lSuUgaMHVEsQyqoaariaGrUHabUDfipPDiUNHVARu6Ks0jSVTvTCAyXaUF1qnKdzTTVS+9tpF2m0saJrQnrC9eoK3d9vDNW3ksVB2bGsBWqd5+0KqrSU4KpXJImiFzRG4GVQJVqrZOFSitFe5+uPPkl85CFfL/OsKpQikvay+sRPXOomRGtUcGVdk/Xu0q+m26yPIqWYQFeASaq5Lm8gKuWbCYCk6VrWBEBUjkaUSyW9yFKsIpUKlaUpF4awUEVVlhck1XI9OuytN0qLTINbMhBq3dFfVbyjsNHlx0dc9LurEkqvgcK6NqVwZVWQs41TEDkYsjSUgkJeJUoXUFQUyURBbAeF1kuFD9DaN3dZVNXapUJSvB7wyITF2NtF4AMnVVnpwRrdWQZPWceha9Y/nKe7j4AM5tS9iRI7t6Z+6SUS2BQVVrrWilfEpVTWNJBnvR1RB02pxt4O2XfNHIkulqo9jVva6rBYiuOlXV3jDJqLY4NylVec+zQiDxmDpL34TMNj5HCFgUQbvdMJQqRHh2t9YFH/ZY0X1ddbBBVW7h0iUXg6p03Z01qUoVHlF1TAsQSkcsupq8YwEAtwARW5u9nSG566+mw/Wgx8qTMLuqUgWFNW+m9bhvpbdW0mdG1YIGVVm7BdVepwplcy3qCXudKr2n9lSyrp51zzeqLninteJUawIuu9ajc6oIoWdADbzc4t6hCqIJDJvNqQ7tqiJx2gGVKjD6IFqPeslI01VP93WY6Bagk2bG10wRpaoseiF712ahCoEPctWzUr1jXmgELlSR9lniO30rxNJFhTJ7fo8qdb37ZtW+B6iGT7NJ9ZbVisgAeflYL0DT1QMLoFOlFkDav7MUG0FGVbGrnXSE0FX650aYMBHKbvirlAfsfADJ7izJHSeD6o02wRmQJ+rvUMXUWOCN0wOjK+WTbLEXqlofRFkrky7V/YSuUu/PtKuAWYCQ6up0uzLxDuwqe5NkfLGILTYwWAdUwZWlu2ytiuwr63bVY5snQs8YXTmmSjtGcB9pPG6tFDmkOqkLdk3PChgWAELds2Ki21UMj6myNWbPzFMj4QjQbiakcYB0+VaQIZKWLWebo4wdYG97Ge+NrjgtrVerrKy6Q/WVNQfDSi449KwUOaGqimEBgGlXLdPSmbo6WMdUmWP9Yz0Cx9IRG1V3UT2xRFbvH1MpRPd43Bwqcocq1xuQy0NFp1RJKFZkPi+XqzvssSryEFXDswKmrnoP2tXtk0yVeVYL1RFERt+KiUr1wLNKmW5SVrv9O6dqicIXspE4pQqF0QHt4rXhffjgS1TlNW0nVBE0PCtg+ABITiVTZbJRleSUqu5ZUcUrclpf+lovq/3X/QLcz1SownXWpDRHAg+o+sJQIFAW3Ot7YCTwIaqyUT+zALKROtXVsj1vrTaqpmfFRKWqe1a0Z8N2V/CNFdGywwJKOyw0qrXwm6O9G3pOtfux6Xw1seb/gVHrh6gavQAgURWngES6m06ptuRYVxssUzV7rMCwAKoPACEhuScGG4ZOjD2QchuDwEup16qitRKyZgiqzP1hPdbdxPjyfgHHLiPpNvmVdpXXW2q5z3sB0QlV9vVGVbq7T6lq9gaC3skSh0t4E2+SafnCoe4DK3WXPeeZkOvMX+bbvHwxVQCVke0kb/Qge1r+KgDb9JI6uyRp/cfP9Yt0gH+3D0BqUo7NUCXEJm6UePNRaxV04xh6SRS25Rj4/nCLji2Ao7RWez/yXFc1e8PmrdbP60aa3W6tFqBYrle/2lPJjrD9VlSrJ+rfjVTLW6bpmG1JC61IERxvX+CjHZe8SE/yJPkJ1aM5Vt2umuMAwNRVYI4DcKqWoquH/qpKVfNXxUjgYkNztqUfgmrZ1MZHr1Ou3tkgMi2ipW9B1rohTnWZc5HsKoTbbKBo9ljjQJbdbACeWYBo9KSanlA1RgLBJ/3VRrLQJ/7qCLA+w8JF81dNe7ONrw4DP8YmwCnsPG2crdQ5V18It/plgBBstS3r4+utVWuHT0y3txkWSITdEf4q7bNXp3YVQQjf1VV5cPfMX31gHIBm9JC/etBjfcBfXamy/j07ZuaXGUOn3Uqdi72cZPNbY9AEQVkGzJ7cG11hUvSLM0CpprZdO0n5sZnrX+Sv0vtUWlogU2XHblSlnB72V4Gkq6Dm+1HzZeY6tLZSzx7vGfibAixvZnaxTapVzMaDnYgblNhPOqaD1G1deqw5+hhVfXTlaCSQ6BbgaHQl0OcCjiwApP6q3GN9xF8l5p2xUkUi4ERMzyXsINxKPYMbu4D7oCKvKHXLaLN0QDWYnmnyVFzAYKgEHCjGAajhJqd2VRVGdbDdVy1e1ry8oq18XVlm9E8WR7yw2UZRTxx2dnKYJhx3ql41zl7Qx9WrV1VVm5Zj3LUVk1YaXwV8QoTJRM1g247m6MpKFbLNzxC4ZBldUfpWs8V7X8uVWmwfX99wQPXGl7uNU86koMLf1DlchtE+ssoCY5wirIgvXvhFE+XzvDaN1FATaxiMlSqErVXGa6CIiAWbsHwRcqJlDtZCtenKKSi7rgv4H/s37ITs46vWNF8WX9PmVcuFo1nbJlVGHiMOxBwHqGhib/fc5GbYpMo2Th7OSH9i7Qq9Ga7a1+LkmGz3Er262iqLfXwVrlTRagEgGEbh5mwWAK1UeWu9jFpHHs8Ti5zYVLCkq8aSv01Mqk0HWge4B1TBKzW0+yCvPCVvUKWNACuCeFlFbRs+ZFdjbSIJ24U9dcWwl2/dELufDXss6kfljkBvrSC1lJ2wj03LonXgt50q4K0VpQoxcIYL+4EEReHe2h9EsauFsrgIERYaJLoRcEAVgTfgesA7opr0QJoFlGdtznyAe/Kx1Wuq2vNrg8Es2X1jwTjVTMuKRjxL9aRURRPjdologXyLJorBBHWq7IRJ6XCGmBWR8PopVJXTQWpURjSxYhhU6b1RzObqNYYM4efR3dPJ8Qx+OVWN2NKtcNFePlOWY7hT5qxf+XXCDGQTNA2zmnbGFQ7niq7Sj40T+30TjH7f+30S8MwKoFA9PNssnU1ZE8guwSFVFr9ja6wO560w6w5HneQ0ixEwfu+DdWhxKfrfsdZa0tUTEbPFK1Xq0LGuNBL/siaQx1kUwfR2XaW/MiVepUdrbc6pbsfuuirNWS9rDY3dFhNfCyEFpZMz33QV+dPs9JpPqIwZbu8+RpVfFN6dZssK11oineoSBmYJ87J8A3YLsBt6Nl7N7T7cOtQ7VX4OuX7b2WSq3BfnqOE2ZoWksxmNtUm15eeWqGpRF+gvtzLP6c2yOxAQtNkY2ROYg8ZNB693pQv9QV3lysWdGlaXXlJhZapnu/LKmkVzVTBeCijr3U5VjCTqxyCkUeWNL78269U7OduByBFCTtZEMl11J+alWIprAH4D2Qzyon/Dzlg7Yzx9dl8ARKTvqR/U97j1SOVIMxEKVTDNKLDqqFQth1lPZwazi5VgN3vfCudBOeqrRAEYFaoVD95KT9kzKpF0g35wF1tzEq9jibxEr5m8fpWe/geYcjznUdWEILw0ye392UBVdrsa104wBY7bu6Gf24dUUYD70R/fLNqXUwOuyPVsu+EC88SubFsdOlj91WqA1VA5XVSCKiAdrAJ6DBgH70X1AVr4xka30TMiGAxHY+QLBvK7h6Cm+mupqRO8Tmhz02vYVSaWGiXBm+OoCHHelLYTDaQo93N/mGrYl2EY941TDoF/rKuXoMzSMLWDqlRX/yr1zO0mcPKgzQNylb9fdfWtmrP2GgyX3nmtXhP/2nXT4Ewdm9KUqCZumXfJ1BQvfX31nduOTdfVuQNEX026lRo7l5VAE4ay+7RRNVf3wa3hEFb50z5Ada2tBE8TmaZ2vsai5dFaq+AtTtphjimIe1QL8uQliV3mwRUYY1aQOuW0Z30dehvkGckbP6+cFBR+Uc2KroY/QV651xROfZxH6csdqqj/Sw9WtJV6GsYZBEnieKTpC9kZ23XVXN63zSpCtbn7KNV+uVu3E6DFzO9UMQlBmkd5OzW9Gq9FqWdk0W5iXLLxt96cC4DUaPk5muIupZcvSv2+6Pu8rvK8vYqslhLkzZyl7tsw/9H4WZA976NfOtX/gH7SG79tpG0KuwAwH4+agqY/tgCPywepQqHs1AOgXSHEx+ZIp/qrarnlmXW9ntyAwCjtR5ICTVcB+MPIa5d9nRXcvxL/nuoq9AGuz6jCbEpANGVZDsownY519XH5YI+VtKDyaEMbtRbtUccxDnrP03Q1DvA4RB1tlKtouMjZ7PVEuAloy4P9AEYh+iG3A4uu0ur3y+6GRbxic382qos1E4tNqpBv41sPMH2AweiqbF/0wwqcUOtr9gI+Jh+j6r3Y48++Tvu6qC5Zn+Z99TIWWt/qyqILXkf7BuY5lvseUj1h9Bx0YVlOozv5RRZNhq7SW9GLy6LMiWNHZdWmdpUmqFy6ApsF6NzYq8ditN0oL+P/RPkeyt6gCvVBCqnU/jL5BHkQcPx3UTV2yMqyUXXawO6GaKL+auhYedGmpEo1qjWL4XwB9GbKZ3Ciq/Rua4PJDagb/epVGZHmbvb21w/rNimHog1nf5j6Knxd3Z+dak1dY1CX3Zj19ljOFfW7znXVdPPT/Zd9xFpN9vWIdmeyUrXeJmvGl9qbLlXo9PVsX9zY1ajeaAVBchnLLAxq5WJJFoDYVV/b/pRZ4TQW1YFdZVTnKgn9Ii7nzu/S1LlGBlV6QWY8hY490+Ysqbvwdq6rB/LrY1mYgWH3gK+x6KWzyOPrZRXrV9lEwLJiaU18A7JWwN0DkWLIrjXfDMR+ri2qLfD6IPJ77xICp7RKxxr6pF8Hh9aQtVk4RkU1eGlZNVZfVmlIoJrknshRbc+E72H5oKxUyd24bd7qIZ0OvIO7dvkDaYyIdvjwLlqzuqNIj5ztV0W0WyYuvTlxziV11/VEZynCOrxz/ONpnHDWEiVHB21Zlecl0jM6kCR7oNT5PTIn+a5xAu/a1QcihT4yRv7QOPpjgTkfyOqRs/2iSKFnq9dU+ZuiL35HXlLkXxt35T35pirJN1VJvqkq8k31Y/JN9T35J1C9W9dvqop8U/2Y/O1U71X2m6oi31Q/Jt9U35N/AlVzhZv0/v8H1TvRZMC/h+o9+dZVRf6lVNlkCdHzU9bTiDmI5l5mIlIoi7ejaj1staPg+3FXVPmXUgVWpe+pgXJ4lHXd/fwuVQgihCWsZdaSTDsq2ocM/7upkmHq9JPGmq7GWIlxZ4igii65HEuscIqo0JT3f4YqMPd/gVaLFhPfLD0CqiKLrs7XmzT5U2dZdOeo/3aqlkE10aIa2Pb9WXu0Lt3wpL2RqGuRfpS0I/RRqtpGGFNSvlL4rmPFd0EjxayLNErGjOqynFs8jBNiwheWSJlLVA+WTuxnA16tU4VETi9aq/ep0nOTfI/PQs2sbVCVIj9+SFf18LipZKLSZS1OpT1grFfX6ULguh4IFK5Q7FVZRMS0pAlQxfdcI+D4P/mTTA/3XAN9SapyNhgautqprRWk9//7VNkKBkehWhpU0X4THFOF2tJYSjWYQ6SHyJfrw7aAVGkFuwqsoU2EZ6PGs2Jr2y9gxuLHdQeBnK+IahtPDczByNyZvk8ddsXwfn3ktdaH7uS6LwClBtVKtasQJ49RRQnf6yIyR6QzLcB7rRVE6q3Fd1xWlk41kQrIddXqBn3jr6eufkYgzW5AN/SDdFcusYJzl6ayWGb9INalt/vZJarRq9hGahdC+LbLfjkbhL1BddRaKxg+THWm12S0uQkhpq6+v341KkM5+Ct7IjNujMc5dBpVBCoQOFEZB0ObkIJFCLGuqgVgXh8BedtZNnatzveDMfTCq5Qpp8p8Q1SAbirDTTIjTiATSo3fF3MwUOnEZDBa9sxBI8IeNFor7DxElV6O4adbp5fV+cf6Udbezh5QpcyrJyrSbDXTVa8wqMqGX0S1vQJtebViJoQP0N83ZEvZ/iCUqmrHD3V11cVXfoVxVeTFNPANC+JslKo4DoOWB/xQslxjBLH78mRdjqCaToXfsltsfxbDx+wqPfHvT9n89KRGCKF3ik7VVXUVMCR6mIrB2FtuxSwwgSKO1loBtp6H6aoSKfRMV7lceVLeDmNQ7PutoqR8Eim8J+tIV+lXtBeApzP3c4m8xErIWqttv41B9b4PAEH79DsAPyRl5XY1Naj6BlXboBobe3W8QaNq+gA8rR4r+DhCiErV4kvNxOVeqJb2Uuyw4c2J1rcSVEFhdSerdxaqLNDNfar3WysIgidql4unvRZcVyODqhzMW1AdDarYsADE0qki+eZbqHZrhJC9WIcRQlSqa4QQSVfLbGz4NxlvX6CnR+JkVMnFjH+yyEb1PV19b8dlyIJZOU/7PcGpIoNqqusqVZN3LQDrTei6emABAFwi2m1yHCFEpSqqITosC9XQbsW24isQu1+OLEDoA3Dy4Pi/SVc3qnv8hKUXYHhWhq6a4X/6o916OtVE86xEdfRYwQdUIdKoGlFeoqJbRk8yTkTvWwmqF3odmv6wl/YwVWmx6uO6CkyqjmFXTapmawVMqumBrgJDV4/sqqfZVSPWI/ltAHxDEL3/+ab2xrSrBaHGBmI9OvlSjEep4vs+ABieatOuAoOqUoYTqlCeyDqjqrg7MtVI/gHtftZOtcCqD2DcGfhJxFphu1RcVn5jfBWKwLfQeCTbctpHqVp3R1cgpO4qAL897SU80VX7AV19zAKc6OqgWIBBOmKl6qhUsXE29CKiKrCIWGWlz6sJqrawdfbhcygeporu77lGlOjrjXlXq5xQ7Q27arZW8VGkUJ1qh/Zhh3MLYNpVGE1I96y0s+HX5UrnBLThib8qCl8ejnfepcoKvdbl3TErj/WtJDwnVCudKu1p6mGA8VG0cN0CEB5jR3zSY69tqSQ1W2MEIdCrumrGJmdbj4OYJn6mThV7rIXhrwLwU1TveJz1HV2dNmv8wFzAMCCpC3dmAdSRQMBGdo0QgOrzZoTouuqeWID7zwyifm6se1aGD0BowbjDSiHgmRXRjBz/x/Lp0Le6769eWvdgHfqHxleNcQCjfFdGVdl7E6lPR2MCNaoItqDpMiyGxWWqvmJXjfFVhPyfa3zfm+SvSmcrg3kIyizomqkJhsvQNU6wSskjheJgmIaGyZA3QoZheRP0IuoCLzHv2bJ9dFDsYC/ifiwo0lvccxn8fePTV6iWemuFi7KJ1QBt1iFVLf+KtuX5covr8Vc3ktgYXYHYTfBv9USl/pOXRY8VjEjkYIIbQtqAEBQigmZEFonEmFUf12PMZLQH/hp3qfgi5tHRQuDmxVuR58Vrwf9yNvZWFFNq2yn9lC5x/tx8leLB3RbHVEOdqtfX8fNVkb/MCP70jq8zRW4grMscGLrqKOMA5lPDgBVbTsQjCImLp7dWy0gg7Y/2bNI/IwiIuJ58TB1tY1ZChmUooLPA2nqu8Vf580dnIEWgNJ6iKEV+/JKuHj3bwhgJlGSjejQ+hDxTV9/1VzFRw3Zhw+PgVGmapKKYAgLwbSn6uMaM3G4diJYp7JUcH3DXWqvMy9OfrCSrZwXXO2RD+JGYlsCkao4DIMOu+uZzvI78VbREszJjsG+pjvxVyAL/SWL2AjhV2sSkET1D6oG4A5GdJlfrlQCNKgIzV2OavE3K7k/0BNChD8CnJgVVqsuDQ1aqse06OXn7Wmtl+qtmL+ABfxWB4GQc4J2nhjFRqZr+qtBVDxTslFUJgnVKkw8/K1SB6CbAJRMouJlUrXakBl/4qwj4SS02vkciGEYbpV+jOj5AlTzgrypj/vqzLfZU8vsTqvJkm0SVForf+NRhncXQJIL9AVUWA4reOS2PpIIEG5NqX7PH2mBRvD60l3KS5cFrtERfmrk2nhdAM9UfCmr0WNmZdbu6k5RXWRRKc/iQrh6MWTGqfSdmnUkB5t5aSwEMqixmMLMDSzQ3USSDas0fgkMuCXPPmB8mHDV3C4fxAbsaTdlVa7qXj1O/lA+gJCzsUBEn4S+Jv9XTcspLqSZajklYBNSFqt8F2ZB0kgRJwF+bO1TNJzFxqtgma0TAWlm3rFPtGqA+OfiIasO2YSMQxZYSv1eKo/ohXQ01XZU3nYtSW1Y3KOey+srymOezRQqFyIsKJZ6wZQXcN7I8vOtq7MdzvPjXi6yf7lA1I9szqhDWyxBhDebwaNR6Jch8MF9FYVA9itUnvvkc1UL3AUTOUgRm/eE+S6A4LC7ABiDTZwO9cexLy1UsgOEQCrHghy0ABHMjNMBuHf3Co4sAAAZwSURBVHRHVxF7cKP6JMLTcYB78iGqnjq5C1kE8TQTT9hZqbbqiiCIx8J1ngkfAFv9VThoVCMW6rsfQz1WsJITImlS/tRjBav+qjm6wqgiYM/Cmxim+L6uRrMS/BOcjwPcky+vCUTR2K3lOxeP//zOmsCQ69MZ1VW0WMHVA54VBOOb+A5OgMhXVKNK87SvWl03quIZw7Pyo7lcj8vXqYI4WMt3LuIZ4vep6iOB+IDq8kgqZaXlvTWBC1UM1uCt5kpLdViCZabekZKu5m1aP6snac2Ym+ATVHkUWrEOUgl4ekgVrnUW11SlCpfouGjJA2l9K37UEhxXzpEnVKLaqpmu21aWkUC+MGh9AgAtM9btKoJLR5UfB/Wlm2zMis0KR2H2o7MALtdOICC0/+sMiICIvYPRJ32AtVqbjDunQ6oobuHRbKD4EZMII1qgSMlUtpNVywYSdWUUF2j/Fst3EqTuvUz1XJeXUosAfecpWYwg3Fxre+Q67Th9Itr/arpU0zTW1AH8bSKTbX9lBfs8kzlwM+J6vZPvCOSou4TechF7BhRwovaUKmgHvwB235Ya1f2T54C8N+P4TBLV1gIjW1sHcAT4FYrWHSnrfiuFlQZO6Oqy4HnsjxIxqh5bcMt8AJwVbrcEgEsqPxvLsE6rxKvmPik+vtZ6f59FydyWrV17ZR7vLPd31vMlf23dKZ5eB8etpKcyyFTj27VNihc/myOlFWdUo9crmWvr5WJdgzkM02tfZPOr93qriks7X/rfN6oYhz/KdGpfLtVlsn5v3uphLnrzbCwpVUhSaTskJF1A+yJnBNq9hyjtt2KB5OZ8XfzlXa/t7Wf8OpXPBX7CL/Zunj9O9dJSqs7o1n43+UdUvS5pi66w+z63inSUTINcz7Ep4pTYVhYgdS0kpUrScrhOnY3DHPwchomFyJvbLq+6FGSpwwbztjGrJiT2xUutubEcGyfN4Ga+eTZexRi86LWU7zDnZbuFu+S6P7thpSqeyiIdvLQaUIxRynP0H6d6c1AHSht0oTewRxJDYeAlqo4dT707+UMW2FMvteRyPf1gHuzXdqSXxlYcFErVssMg79rnssvAU9Nd4iKD176b+3AaXGfIr7sF8H4OiX1tn+t4mquaFGGazZ15NlbDPwv/qi9nlS656wcTaH1/oC3SM7hsbbsU1VawW02KeBApf1SACJ7+hecFQBdA9YqxuLZIKh9tgGh76PHH3njk4IlT4oRRS7y+JHHLn1ynUMWehSsfTBWowIhH0iD6jkQVsaIBWA3s8aaruBpBC6uCsEVxLMgn9BCBxtkAmwxIe2e8qiv/JKp/DfTyRrQ3EoEyAD/36vxf7rhEwh/nCy1yApV7SZYX6b1zkKat2x5nSqzgNf5/xx+HIJ4cwZcNIu5ryT4Av6IQYtru02aJcKcHIqSdjYtd3YIOhEqYXZnq5WbTxmieZxbpsNlt/S+mGjhtZLe+M46uV1RR4c1pVeVtmfodlKmCsCiDurkMiZV7+ZuUjVTPLrVG22rSCrveLZbDm+31keM3x25k7d45lKhyufJ//dYG+xXTLQB1KyHQYjBLpWbxipdD63aPjPmrqV4su+5f52hKrBus56gscNZXF1BbNZAtAChA1nkZuNjWlIMTqrfKDqjtjea5v4XRVUq0Uy3jn5fi0r9OY2P7Tncbs375UYoV/KOOaZrcr+v+mpZTOW9dSo2qmPCxgTG6skhvIbTMCuFOSvWLqU4wvY3NDOoE/wBTBsoM5PlwCXz+EDOpfLZ3CdsLuRXV1W3/I2Uj1TOzHOpB1DSHqkmIPKAhUfUTv8+Hvux8O3aiAs4GVQieaRZgLqLkR993ftg6m8nRqWqvXNS+yz5YLSX6xVQdEFZZn9vDgKu69yYvAV1gh4XdlUjSVQiK0uoJcS0c+iiUx2CleoZRENrNnPdVFvixDGCnGg5zOcz9XPfFdbBfhujV1qkC8KMLL4E9O93b4DtDcfX/Ojjbqfzq+AB3Y1qOe2tVnHQApTqc7kQ16jkdZLYfTXBE/w/CCFQeJtZIqNtgZPXisR6rhSqL+hzYI5VlZnQuj+y+/UpMS7v3z6Ust19PkvV57L+ThKbRf4kPEskFoW/7IOjZ4634I672UuT9IN6kI0u2/ial6Yt7NfpAmvT9NLoMq35H1tfkkeMfOsdjBYmMN5/K6G8rtSb45I7+lm/5J8r/A/AlfNoP3ZSsAAAAAElFTkSuQmCC)
## Průběh signálu
- Změny signálu v praxi nejsou diskrétní, nýbrž spojité.
- Takt = 
- Bity přenášíme zvlášť v časovém taktu.
- Nejen, že potřebujeme začátek a konec taktu, ale i prostředek, kdy budeme měřit hodnotu.
- Hodnotu nelze číst (vzorkovat), když se hodnota mění. (Zakázané pásmo = signál se nesmí v takovém stavu nacházet při čtení.)
- Pozitivní logika/Negativní logika

# Technologie TTL (transistor-transistor logic)
- Základní stavební prvek je tranzistor NPN.
- Parametry TTL:
	- napájecí napětí +5V
	- L < 0,8V
	- H > 2,0V
- Tři vývody: báze (řídící vývod), kolektor, emitor
- Malou změnu proudu mezi bází a emitorem ovlivňujeme velké změny proudu mezi kolektorem a emitorem.
- 
## Invertor v TTL
- Implementace operace negace v Booleově algebře.
![](https://is.muni.cz/el/1433/podzim2004/PB151/um/1509a.gif)

NAND pomocí dvou tranzistorů
NOR pomocí dvou tranzistorů

# Kombinační logické obvody
- Základní logické členy:
	- Invertor
	- AND
	- OR
	- NAND
	- NOR
- Ostatní logické členy
	- `XOR ⊕ =1 mod2` = Nonekvivalence
	- `XNOR NOXOR ` = ekvivalence 

## Logické obvody
- **Multiplexor**
	- přepínač, datové a adresové vstupy, `Z = A.X + !A.Y`
- **Dekodér**
	- `n` adresových vstupů a `2^n` datových vstupů
	- pracuje s pamětí

## Sčítačky
### Sčítačka MODULO 2
- `x + y = z`
- přenos neřeší
- `z = !x.y + x.!y`
### Polosčítačka
- Má `S` součet a `P` přenos do vyššího řádu.
- `S = !x.y + x.!y`
- `P = x.y`
### Úplná sčítačka pro jeden binární řád
- vstup `xi`, `yi` a přenos z nižšího řádu `pi-1`
- výstup `si` a `pi`
### Vícemístná sčítačka
- Používá x-krát sčítačku pro jeden řád.
- Nemůžeme spustit naráz x řádů.
- Nelze ani sestavit sčítačku pro 2×32 vstupů. (2^64 možností vstupů)

## Sekvenční logické obvody
- Mají výstup závislý na hodnotě vstupů a na posloupnosti změn, které předcházely.
- Některé výstupy jsou 'zpětná vazba'.
- `Qk` = vnitřní paměť (Pamatují si výstup předchozího vyhodnocení.)

### Základní paměťový člen - Klopný obvod RS
- R = RESET (nulování)
- S = SET (nastavení)
- `R=1 a zároveň S=1` je **zakázaný stav**
- Obvod řízený jedničkami vs obvod řízený nulami.
![](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2F2.bp.blogspot.com%2F_xzA3dAIYcxY%2FSYCkaRi659I%2FAAAAAAAAA5U%2Fv3PYwFizwqk%2Fs200%2FRS-FlipFlop.gif&f=1&nofb=1)- Klopný obvod řízený:
	- hladinou
		- horní
		- dolní
	- hranou
		- čelem impulsu (nástupní hrana = z 0 na 1)
		- týlem impulsu (sestupná hrana = z 1 na 0)

### Klopný obvod D
- `D` = Delay (vzorkovací k. o.)
![](https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fimage.slidesharecdn.com%2Fdcsppt-150919142146-lva1-app6892%2F95%2Fd-flip-flop-7-638.jpg%3Fcb%3D1442672591&f=1&nofb=1 =400x)
### Klopný obvod JK
- Funguje jako RS k.o., ale nemá zakázaný stav. Při zakázaném stavu pro RS vrací negaci Q.
![](https://circuitglobe.com/wp-content/uploads/2015/12/JK-FLIP-FLOP-FIG-2-compressor.jpg)
- U složitějších obvodů nastává problém se resetováním stavu celé sítě, proto se zavádí **globální reset**, který je u všech připojený k **lokálnímu resetu**.

## Typické sekvenční obvody v počítačích
- Sériová sčítačka = Sumátor (kombinační obvod) + D k.o.
- Paralelní registr = střádač (**accumulator**); n-bitová paměť, součástí procesoru
- Sériový přenos:
	- Informaci přenášíme po bitech. (Dvoustavová komunikace)
	- Čtyřstavová komunikace (Přenos informace po dvou bitech. 2^2 = 4)
	- Přenosová rychlost:
		- v bitech za sekundu (neznamená počtu taktů)
		- v počtu změn stavu za sekundu (**baud rate**, **Bd**)
- Uvnitř počítače přenos paralelně pomocí **sběrnice**.
	- Do prvního registru zapíše procesor, to se zapíše i do druhého registru a až může op. paměť zapsat data z registru, aktivuje druhý registr a zapíše data.
- Sériový registr (posuvný registr)
	- Jedním taktem signálu CLK se informace posune o **jeden** D-KO.
	- Sériově poslaná data potřebujeme převést do paralelního tvaru.

### Sčítačka v BCD kódu
- Součet dvou čísel:
	- Budeme přičítat 6:
		- Je-li přenos v binárním součtu.
		- Je-li číslo `11xx` nebo `101x`

### Násobičky
- Sekvenční násobení (bez znaménka)
	- Střádač má dvojnásobnou velikost násobence a násobitele.
	- Násobenec přičítáme ke střádači, když i-tý řád násobitele se rovná 1.
	- Po každém řádu posouváme střádač o jeden bit doprava.

## Bitové manipulace
### Rotace bitů
- Operace nad n-tici bitů. Obsah n-tice se rotuje.
- **Bity se neztrácí.**
- Doleva = Bity nižšího řádu se posunují směrem k vyššímu řádu. Když by došlo k přetečení, přičte se 1 k uvolněnému řádu (významově nejnižšímu).
- Doprava = Bity nejvyššího řádu k nejnižšímu.

### Logický posun
- Podobně jako rotace, ale bit který se uvolňuje se **ztrácí**.
- Doleva = do nejnižšího bitu se vkládá 0
- Doprava = do nejvyššího bitu se vkládá 0

### Aritmetický posun (Arithmetic shift)
- Zachovává znaménko.
- Doleva = násobení 2; **Znaménkový bit se nemění!** (V procesoru žádný aritmetický posun doleva není, používá se logickým posunem doleva. Když by došlo k ovlivnění znaménka dojde k přeplnění.)
- Doprava = dělení 2; **Znaménkový bit se kopíruje do nižšího řádu!** (viz doplňkový kód)
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTQ1OTQyNjk1OSwtMTgyOTQwNjE4NywtNT
I2ODA5MDk3LDg3OTY2MTU2NiwxNjQ2NzkxNDQ2LC01OTg4MzE5
NzIsMzkzMTUxNzcxLDUyMTQ4MDAzOCw1MTAwMDI0NzYsLTM3OT
MxMzE1OSwtMTgzODIzNzU4Miw5MzM4NDY1NCwtMjM1MTYxOTc2
LDIwNzI4ODIzODksMjAyODE3ODEzMSwtMTE1MTgxOTMyMiwtMT
UzNjk1NzUxOCwtMTQzMTA1MTU5NywtMzQzMDg0NDY2LDMxMjE4
ODE1Nl19
-->