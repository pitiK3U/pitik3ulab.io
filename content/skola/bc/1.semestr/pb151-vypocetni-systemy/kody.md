+++
title = "02 - Kódy"
date = 2020-10-16
+++

# Zobrazení celého čísla
## Převod z 10 do 2
Celou část čísla dělíme 2. Zbytky po dělení dvou nám udává cifry čísla v dvojkové soustavě od nejméně významné cifry.
Desetinou část násobíme 2. Číslo cifry udává celá část daného čísla.
[Viz pv170](@/skola/bc/1.semestr/pv170-konstrukce-digitalnich-systemu/uvod.md#zobrazovani-cisel-ciselne-soustavy-kodovani)

## Zobrazení celého čísla v počítači v binárním tvaru
### Čísla bez znaménkem
Velikost je v rozsahu **< 0; 2ⁿ - 1 >**.
### Číslo se znaménkem
Nejvyšší bit je často znaménkový.

#### Přímý kód
V absolutní hodnotě je rozsah zobrazení **< -2<sup>n-1</sup> + 1; -0 >**.
Jednoduché zobrazení, ale složitá aritmetika.

### Inverzní kód
Inverze bitů (jedničkový doplněk).
Rozsah zobrazení je **< -2<sup>n-1</sup> + 1; -0 >**.
Zase problém dvou nul.

### Doplňkovým kód
Dvojkový doplněk = inverze všech bitů a přičtení jedničky.
Rozsah zobrazení je **< -2<sup>n-1</sup>;2<sup>n-1</sup> - 1>**.
**Nemá dvě nuly.**
Pravidla:
1. se všemi bity pracujeme stejně
2. přenos ze znaménkového bitu se ignoruje

## Aritmetika ve dvojkových kódech
Základní operace = **součet**.
**Přetečení (přeplnění)** = výsledek spadá mimo rozsah zobrazení.

### Součet v doplňkovém kódu
Přetečení může nastat pouze pokud sčítáme čísla se stejným znaménkem. Po přetečení má výsledek znaménko opačné.
**Čísla bez znaménka:** přenos indikuje přetečení.
Procesory nastavují příznaky, jeden příznak je **overflow** a je na programátorech řešit přetečení, ne na procesoru

### Součet v inverzním kódu
Nutnost provádět tzv. **kruhový přenos** = přičtení přenosu z nejvyššího řádu k výsledku.

# BCD kód (Binary Coded Decimal)
[viz pv170](../pv170-konstrukce-digitalnich-systemu/uvod.md#bcd)
Řeší problém jednoduchého převodu mezi dvojkovou a desítkovou soustavou.
**Nibble**
*[Nibble]: půlbajt, buď spodní nebo horní 4 bity bajtu.
BCD hodiny
Původní vznik kódu byl pro hromadné zpracování dat. Např. projekt sčítání lidu - hodně vstupních a výstupních operací.
## Rozvinutý tvar (unpacked decimal)
= zónový var desítkového čísla
mezitvar, nepoužívá se k výpočtům
zóna = horní nibble
	- standardně F₁₆
	- + C₁₆ (značí kladné znaménko)
	- - D₁₆ (značí záporné znaménko)
Např. -71346₁₀ -> F7F1F3F4D6₁₆

## Zhuštěný tvar
základní tvar pro výpočty
vypouští se všechny zóny kromě nejpravější (znaménko) 

# Vnější kódy
= uložení písem, číslic aj. znaků
Každý znak má svoji **ordinální (numerickou) hodnotu**.
Jednobajtová kódování.
	- lexikální uspořádání (písmeno dříve v abecedě má nižší hodnotu)
	- snadný převod desítkových číslic na numerickou hodnotu
Vícebajtová kódování.

**ASCII** 
*[ASCII]: American Standard Code for Information Interchange 7bitové kódování, z roku 1963

Prvních 32 znaků v ASCII jsou **řídící znaky** (netisknutelné).
Nejnižší 4 bity znaků čísel udává hodnotu čísla.

## Řídící znaky
Slouží k oznámení akce vzhledem k přenosu dat.

0 = NUL = null
7 = BEL = bell (zazvonil pár znaků před koncem, aby upozornil, při psaní na stroji)
**Carriage return** = ,,návrat vozíku", posun na začátek (téhož) řádku
**Line Feed** = posun o řádek (bez změny sloupce)

**Windows** ukončují řádek dvojicí **CR LF**.
**UNIXové** systémy ukončují řádek **LF**.

`0o033` = ESC = escape - 

# 8bitová zobrazení
Prvních 128 znaků stejné jako ASCII. Zbytek je jiný vzhledem k implementaci.
- IBM PC (neobsahovalo cs a sk znaky)
- Kameničtí (snaží se využít kódování IBM PC s cs a sk znaky)
- PC-Latin2 (používalo naše znaky a bylo formalizováno, ale nepřežilo)
- Windows-1250
- ISO-8859-2 (ISO-Latin2)

Semigrafické symboly = na tvoření tabulky, problém byl, že rozměr obrazovky byl 80x25 

EBCDIC - starší jak ASCII, používá BCD kódování


# Endianita
- Řeší, jak se slova ukládají do paměti.
- Název z románu Gulliverovy cesty
- Uložení 32bit slova 123456
## Big-Endian
Bajt nejvyššího řádu je uložen na nejnižší adrese.
| adresa | 00 | 01 | 02 | 03 |
|--------|----|----|----|----|
|        | 00 | 12 | 34 | 56 | 
## Little-Endian
Bajt nejvyššího řádu je uložen na nejvyšší adresy.
| adresa | 00 | 01 | 02 | 03 |
|--------|----|----|----|----|
|        | 56 | 34 | 12 | 00 | 

# Více bajtová kódování
- Jednobajtové kódování neumožňuje ukazovat více kódování/upravených znaků zároveň.
- Už nemáme více kódování, ale jeden standardizovaný -- UNICODE (unicode.org)
## UNICODE
- `us-ascii` = 1 bajt (7 bitů)
- ostaní = více bajtů
- nevýhody
	- není přesně daná velikost znaků
	- složité zpracování znaků
### USC-2
- znak = 2 bajty
- nelze zobrazit znak, který potřebuje 3 a více bajtů na zobrazení
- zastaralé, používá se UTF-16
## UTF-8
- Nejpoužívanější více bajtové zobrazení.
- Znaky jsou různé délky
- ASCII znak = 1 bajt (7 bitů, nejvyšší je vždy 0)
- Na ostatní znaky používá 2 - 4 bajty (až 21 bitů ~ 2 mil. znaků)
	- nejen písmena mezinárodních abeced, ale různé grafické znaky, smajlíky, ...
	- 1. bajt: počet jedniček `1` zleva vyjadřuje délku sekvence; následuje nula `0`, která je oddělovač
	- další bajty začínají: `10`
	- Ne všechny bity jsou použity na zobrazení hodnoty čísla, 
- hodnoty značíme `U+0159`, kde `0x0159` je hexadecimální
- ordinální hodnota 'ř': `0000 0001 0101 1001`
- `110x xxxx 10xx xxxx`
- `1100 0101 1001 1001`
- Hexadecimálně: `0xC599`

**BOM** (Byte-Order-Mark) = `U+FEFF`; označuje, že daný text je v UNICODE; řeší Endianitu (na LE je značka `0xFFFE`)

# Detekční a opravné kódy
- 1 chyba = inverze jednoho bitu
- Pokud nastala chyba, řekne pokud někde nastala chyba.
- **parita** = informace součástí dat
	- sudá
	- lichá
- redundance = zavedení nadbytečnosti = nenese hodnotu informace; uživatel nevidí, slouží pouze pro firmware
- Kód 2 z 5: (kolik bytů z kolika je rovných 1), Zachytí všechny 1násobné chyby a 40% 2násobných chyb
## Opravné kódy
**Ztrojení**
	- na 3 bitech může být pouze `000` nebo `111`
	- Dokáže detekovat 2 chyby a opravit 1 chybu (jestli je 1 bit = 1 -> 000; jestli jsou 2 bity = 1 --> 111)

Kódová (Hammnigova) vzdálenost
**Kódová vzdálenost d** = počet bitů, v nichž se liší dvě sousední platné kódové kombinace
- trojrozměrná: d == 3, dvě platné kombinace 000 a 111; Ztrojení
- dvojrozměrná = schopni detekovat 1 chybu, ale ne opravit
- jednorozměrné
- Vztahy
	- Detekce `k` chyb: `d >= k+1`
	- Oprava `k` chyb: `d >= 2k+1`
<!--stackedit_data:
eyJoaXN0b3J5IjpbNjk2MTMzODg0LDE4NzQ3NjM2MTIsMTcxMz
U0NTgwOSwtOTg1NjIyMjA4XX0=
-->