+++
title = "05 - Procesor"
date = 2020-11-06
+++

# Procesor
- **Instrukce** = sada mikroinstrukcí (fáze); pouze číslo
- **Assembler** = jazyk symbolických adres/instrukcí;
	- instrukce zapisujeme zkratky
	- adresu nepíšeme přímo, ale můžeme si ji pojmenovat

<!-- markdown cheats -->

- Procesor je synchronní stroj řízený řadičem.
- Základní frekvence = **takt procesoru**
- **Strojový cyklus** = čas potřebný k zápisu (čtení) slova z paměti (např. 3 takty); jedna mikroinstrukce
- **Instrukční cyklus** = čas potřebný pro výběr a provedení instrukce; potřeba několik mikroinstrukcí
- Příklad formátu instrukce: (1 až 3 bajty)
`Operační kód (operační znak) [ Adresa operandu / operand, [ Adresa 2. operandu / 2. operand ] ]`

<!-- markdown cheats -->

- Fáze procesoru:
	- výběr
		- operačního kódu z paměti
		- operandu / adresy operandu z paměti
	- provedení instrukce
	- přerušení, ...
- Výběr instrukcí je řízen registrem:
	- čítač instrukcí (adres)
	- **PC** (Program Counter)
	- IP (Instructiom Pointer, alternativní název k PC)
- Po provedení instrukce zvyšuje PC o délku instrukce. Plní se např. instrukcí skoku, ...

## Počítač (náš pro vysvětlení)
- pracují ve dvojkovém doplňkovém kódu
- registry
	- **A** (Accumulator) = střádač - 8 bitový
	-  **PC** = čítač instrukcí - 16 bitový (= slovo)
- paměť
	- 2^16 bitů = 64KB
	- adresovatelná jednotka = bajt
	- data = 8 bitová
- náš procesor je podobný **8 bitovému** procesoru Intel 8080 (1974)
![Know about Architecture of the Intel 8080 Microprocessor](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.elprocus.com%2Fwp-content%2Fuploads%2F2014%2F06%2F314.jpg&f=1&nofb=1)
- Dn = datový bit
- An = adresový bit
- WR = Write, řízení zápisu do paměti
- Vss = GND; Vbb = -5V; Vcc = +5V; Vdd = +12V
- Phase1, Phase2 = impulsy vnějších hodin

# Instrukce
- **LDA adresa** - Load A Direct
	- naplní registr A obsahem bajtu z paměti
	- uložení instrukce v paměti

| operační znak | 16 bitová adresa paměti | |
|:-------------:|:-----------------------:|-|
| 3Ah | nižší bajt adresy | vyšší bajt adresy|
(Little Endian)
- **STA adresa** - Store A Direct
	- Uloží reg. A do paměti
	- uložení instrukce v paměti

| operační znak | 16 bitová adresa paměti | |
|:-------------:|:-----------------------:|-|
| 32h | nižší bajt adresy | vyšší bajt adresy|

- **JMP adresa** - Jump Unconditional
	- nepodmíněný skok na adresu
	- uložení instrukce v paměti:

| operační znak | 16 bitová adresa paměti | |
|:-------------:|:-----------------------:|-|
| 0DAh | nižší bajt adresy | vyšší bajt adresy|

- **HLT**
	- **Musí být na konci našeho programu!**
	- Přeruší vykonávání příkazů.
- další registry: B, C, D, E, H, L (8bitové)
- **MOV** = přesun mezi registry
	- `MOV r1, r2`
	- r1 <-- r2
	- ri = {A,B,C,D,E,H,L}
	- kódování = 1 bajt (kombinace registrů je součástí operačního znaku)

(
**db** = define byte, pseudo instrukce pro assembler
)

# Interní registry
 **IR**
 - instrukční registr (8bitový)
 - je napojen na dekodér instrukcí (řadič)
 
 **DR**
 - datový registr (8bitový)
 - registr pro čtení/zápis dat z/do paměti
 
 **AR**
 - adresový registr (16bitový)
 - adresa pro čtení/zápis z/do paměti

 **TA = (TA<sub>H</sub>, TA<sub>L</sub>)** 
 - Temporary Address Register (16bitový):
 - TAH (TA High - 8 bitů), TAL (TA Low - 8 bitů)

# Aritmetické instrukce
- `INR r` = increment register (r <-- r + 1)
- `ADD r` = Add Register to A
- `CMA`     = Complement A (inverze všech bitů)

## Příznakový registr F (Flag) procesoru I8080
**Z** (Zero)
= 1 při nenulovém výsledku
= 0 při nulovém výsledku
**S** (Sign) = Kopie znaménkového bitu výsledku operace
**CY** (Carry) = Kopie bitu přenášeného z nejvyššího řádu výsledku operace
**P** (Parity)
= 1 při sudé paritě výsledku
= 0 při liché paritě výsledku
**AC** (Auxilary Carry) = přenos mezi 3 a 4 výsledku

- příznaky nastavují: `ADD`, `INR`
- příznaky nemění: `LDA`, `STR`, `JMP`,  `MOV`, `CMA`
- `CMP r` = Compare Register with A (nastavení příznaků), A - r

## Podmíněné skoky
`Jpodmínka adresa`, skoky podle obsahu příznakového registru
- `JC	CY=1`
- `JNC	CY=0`
- `JZ	Z=1`
- `JNZ	Z=0`
- `JP	S=0` (Jump if positive)
- `JM	S=1` (Jump if minus) 

# Stack
- **Zásobník roste v paměti od vyšších adres k nižším.**
- **Registr SP** (Stack pointer 16bitový)
- Plnění SP instrukcí `LXI SP, hodnota` = Load immediate
- Instrukce
	- PSW -- Processor Status Word = dvojice registrů A, Fl (Flags)
	- `PUSH B | D | H | PSW` -- z dvojice registrů načte 2 bajty do zásobníků
		- B | D | H | A --> SP - 1
		- C | E | L | Fl -- > SP - 2
		- SP - 2 --> SP
	- `POP B | D | H | PSW` -- do dvojice registrů načte 2 bajty ze zásobníků
		- SP --> C | E | L | Fl
		- SP + 1 --> B | D | H | A
		- SP + 2 --> SP
- **Žádná kontrola podtečení!**

## Volání podprogramu
- `CALL adresa` - vloží návratovou adresu do zásobníku a pak skočí na `adresa`
- `RET` - vyskočení z dané funkce, pop adresy ze zásobníků a na danou adresu skočí

# Instrukce nepřímá
- `LDAX 100h` - na adrese 100h je adresa, kam se má uložit, (STAX, JMPX, TAX)

# Programování V/V operací (I/O)
- pro V/V zařízení se používá virtuální paměť, která je podobná operační paměti
- příkazy posíláme na V/V adresu
- možnost, jak zvolit, jestli používáme operační paměť nebo V/V brány
- na procesoru je to pin M/IO
- **time-out** = maximální doba, kterou jsme schopni čekat; zamezení čekání do nekonečna na signál HOTOVO
- instrukce
	- `OUT` = zapíše obsah A na V/V sběrnici
	- `IN` = přečte obsah V/V sběrnice do A
	- `START` = zahájí V/V operaci
	- `FLAG adresa` = skok na adresu, není-li operace hotova

# Přerušení
- Umožňuje multiprogramové zpracování
- Interrupt = Signál žádosti o přerušení je vytvořen vně procesoru (V/V zařízení)
- Procesor si může vybrat, jestli přerušení akceptuje nebo ne
	- Pokud ano, musí uložit nějaký stav; instrukci, která se měla provést a použité registry

## Přerušovací systém
- program (statický) vs. proces (dynamický)
- umožňuje přerušení běžícího procesu a aktivuje rutinu pro obsluhu přerušení
- činnost při přerušení:
	1. přerušení provádění procesu
	2. Úklid PC, A, ...
	3. Provedení obslužné rutiny
	4. Obnovení PC, A, ... a tím pokračování přerušeného procesu
- Proces můžeme přerušit:
	- pouze po provedení instrukce (ne během ní; musí dokončit všechny své fáze; Je-li fáze == výběr instrukce)
	- je-li to povoleno (příznak, který přerušení povoluje a zakazuje)
		`IF (Interrupt FLAG)`
		Instrukce `STI` (přerušení povoleno, tj. `IF := 1`; privilegovaná instrukce)
		Instrukce `CLI` (přerušení zakázáno, tj. `IF := 0`; privilegovaná instrukce)
- procesor nelze přerušit bezprostředně po zahájení obsluhy předchozího přerušení
- **overrun** = dvě přerušení od stejného zařízení --> přepíšou se data z prvního přerušení
- Při přerušení se uplatní uložení `PC` do zásobníku, vynuluje se `IF` a do `PC` se načte adresa programu přerušení (adresa ukazuje do OS)
- `STI` = nepovoluje přerušení hned, ale až po vykonání následující instrukce

## Signál RESET
- nastavení počítače do počátečních podmínek a předání řízení zaváděcímu programu v permanentní paměti
- Signál Reset se uplatní kdykoli - tj. i uvnitř fází instrukce
- Fáze Reset
	- 0 --> IF (zakázání přerušení)
	- 64512 --> PC (Skok do ROM)
- činnosti po zapnutí počítače:
	1. vyčkání asi 1s (doba náběhu a ustálení zdroje)
	2. generování signálu RESET
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE5MDc2MjgxNSw1Mzg5MDIxOTcsLTEzNj
cyMzgyLDE2ODIzNjA1MjQsMTEzNzE3ODE2NCwtMTY3MDE4Nzkw
MSwzMTgzMjA3OTYsNzQ3NDUwNDUyLC0zMjMxNjIyMjMsLTEzNz
IwODYzNDEsLTI0NTY5MTc3OSwxODc2NzMwNjE2LDE5NzU2NDI1
MDYsLTE4NzExODE0NTksLTEyNjY4OTg3NjIsLTExMTYyNzI3Mz
QsLTEyNTgyMTEwNiwxODY1Mjg3MzQ2LDE4NzA1ODg2MjQsMTc5
NzA3NzA0NV19
-->