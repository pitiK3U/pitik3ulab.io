+++
title = "11 - Procesory RISC"
date = 2021-02-12
+++

# Mikroprocesor i860
- **8086 - i486 = CISC** - Complex Instrustion Set Computer
- **i860 = RISC** - Reduced Instruction Set Computer
- 1989
- Není kompatibilní na řadu x86
- Výkonem odpovídá počítači Cray I. "Cray on a Chip"
- 64 bitový procesor
- velký počet registrů

# Jednotky i860
## Prováděcí jednotka
- Registry r0-r31 32bitové
- r0 je vždy = 0
- operace zápisu se ignoruje
- pro 64bitové operace se sdružují do dvojic

## Techniky
### Register Bypassing
- Je-li výsledek předchozí operace vstupem do další bere se ze sběrnice
### Delayed Branch
- Před skokem se provede ještě následující instrukce
```asm
BR návěští
OR r0,r0,r0
```
### 2 varianty podmíněného skoku
- jestli je pravděpodobnější, že se skok provede nebo ne
- 2 druhy podmíněných skoků

## FPU
- Registry f0-f31 32 bitové
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE4ODk0NjYwNTAsMzE2NTE2NjEsLTExMj
I3MzY3MTYsLTEyMTU3MjQ0MTVdfQ==
-->