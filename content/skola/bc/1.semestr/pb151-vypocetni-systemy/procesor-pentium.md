+++
title = "09 - Procesor Intel Pentium"
date = 2020-12-18
+++

# Intel Pentium
- Z řeckého penta = 5 (Čísla nelze ověřit ochranou známkou)
- 32 bitový procesor
- od 1993 do 1999
- 60 MHz až 300 MHz
- 64 bitová sběrnice
- Dvě ALU = umožňuje zpracovat instrukce paralelně
	- U-pipe
	- V-pipe
	- určité instrukce mohou projít pouze přes danou pipe

## Párování instrukcí
- Zpracování instrukcí paralelně
- Obě instrukce musí být "jednoduché"

## Superskalární architektura
- Zdvojení některých jednotek
- Důsledně realizovaná superskalární architektura
	- Samostatné komponenty:
		- Výběr instrukce (Prefetch)
		- Dekódování instrukce (Instruction Decode)
		- Generování adresy (Address Generate)
		- Provedení instrukce (Execute)
		- Dokončení instrukce (Write Back)
	- Dokáží pracovat paralelně, ale jsou navazující na sebe
	- Nejhorší je instrukce skoku a podmíněného skoku (musí se přejít na jinou adresu = načetli jsme instrukce zbytečně)

## Předvídání podmíněných skoků
**Branch Target Buffer - BTB**
- Při výběru instrukce se testuje obsah BTB na shodu s adresou vybírané instrukce. Pokud se adresa v BTB najde, zkoumá se obsah bitů historie.

![](https://is.muni.cz/el/fi/podzim2004/PB151/um/55025.gif)
## Režim správy systému
**System Management Mode - SMM**
- Je podobný reálnému režimu. Nejsou v něm úrovně oprávnění, provilegované instrukce nebo mapování adres.
- V SMM lze provádět V/V operace a adresovat celou 4GB kapacitu fyzické operační paměti.
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTMxNjc5MDM1MiwyODk1ODA4MSwtMzUzMD
QyOTQ1LC0xNzIzNTQzOTgwLDEwMzA4NTU5NTYsNzc1MjAwMzg5
XX0=
-->