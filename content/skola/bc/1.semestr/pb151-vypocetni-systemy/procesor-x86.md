+++
title = "07 - Procesor x86"
date = 2020-11-27
+++

# Procesor Intel 8086 a 8088
- 16 bitový procesor
- 1978 - 1982
- základní procesor řady INTEL x86
- frekvence max. 10 MHz

Procesor 8088
- 16 bitový procesor určený do 8 bitového prostředí

## Typy dat zpracovávané procesory
- Little-Endian (na nižší adrese nižší řád)
- Umíme pracovat s 32 bitovými daty (ale ne nativně)

## Adresace paměti procesor 8086
- Procesor má 20 bitovou adresu --> max paměť = 1MB
- Adresa, kterou programátor používá je **offset**, je maximálně 16 bitový.
- Program rozdělen na **instrukce**, **data**, **zásobník**; každý má max. 64KB.
- Pro určení se kterou částí pracujeme, používáme segmenty.
- Segment = adresa, kde v paměti je uložena naše část, 20 bitová, ale funkčních pouze 16 vrchních bitů (dolní 4 bity jsou 0; musí být dělitelná 16; zvedneme-li segmentovou adresu o 1, zvýšíme reálnou adresu o 16).
- Offset = začíná nulou, funkčních pouze nižších 16 bitů (vrchní 4 bity jsou 0).
- Procesor má vlastní 20 bitovou adresovou aritmetiku. Sečte segment a offset, (segment má dolní 4 bity nula). Kdybychom sečetli 2 čísla, jejich výsledek by měl být větší jak 20 bitů, dojde k přetečení a číslo se vrátí k nule.
- Zapisujeme ve tvaru `segment : offset`, zápis `01A5:0012` představuje 20 bitovou adresu `01A62`
- Nové **segmentové registry**:
	- **CS** (Code Segment) je určen pro výpočet adresy instrukce (je vždy nastaven při spuštění našeho procesu)
	- **DS** (Data Segment) slouží pro výpočet adresy dat
	- **SS** (Stack Segment) se použije při přístupu k zásobníku
	- **ES** (Extra Segment) je může obsahovat pomocný datový segment

# Příznakový registr
![](https://cdncontribute.geeksforgeeks.org/wp-content/uploads/flag-registor-8086-e1524066619626.png)
- **CF** (CY; Carry Flag) = přenos z nejvyššího bitu (jak pro 16bitové operace, tak i pro 8bitové)
- **PF** (Parity Flag) = sudá parita výsledku dolní osmice bitů
- **AF** (AC, Auxiliary Carru flag) = příznak pro přenos přes hranici nejnižšího půlbajtu operandu (vždy z bitu 3 do 4). Význam v BCD aritmetice.
- **ZF** (Zero Flag) = nulový výsledek dokončené operace
- **SF** (Sign Flag) = kopie znaménkového bitu operace
- **OF** (Overflow Flag) = došlo k aritmetickému přetečení (význam pouze při práci se znaménkem)
- **TF** (Trap Flag) = uvádí procesor do krokovacího režimu, ve kterém je po provedení první instrukce generováno přerušení (INT 1). Příznak lze nastavit pouze přes zásobník instrukcí `IRET`.
- **IF** (Interrupt Enable Flag) = nulový zabrání uplatnění vnějších maskovatelných přerušení (generovaných signálem `INTR`).
- **DF** (Direction Flag) = řídí směr zpracování řetězcových operací. (Jestli od nejnižší k nejvyšší = inkrementace, nebo od nejvyšší k nejnižší = dekrementace)

## Zásobník
- všechny přístupy přes registr **`SS`**, `SS:SP`
- 8086 nemá žádný prostředek, který by hlídal maximální mez naplnění zásobníku.

# Přerušení v 8086
- **Vnější** (gen. technickými prostředky)
	- nemaskovatelná (signál NMI) (Zjištění chyby při čtení z paměti)
	- maskovatelná (signál INTR)
- **Vnitřní** (gen. programově)
	- instrukcí `INT n`
	- chybou při běhu programu

## Vektor adres rutin obsluhující přerušení
- Procesor nám nabízí různých 256 adres přerušení, aby když vznikne přerušení se mohlo přerušení hned obsloužit přeskočením na adresu pro daný kód přerušení.
- Pro každé přerušení máme 4 bajty (2 bajty offsetu, 2 bajty segmentů)
- Do těchto adres by neměl zasahovat nikdo jiný, než OS! (Na 8086 nešlo zabezpečit, kdokoliv mohl zapsat kamkoli)
- Akce když se uplatní přerušení:
	1. do zásobníků se uloží registr příznaků (`F`)
	2. vynulují se `IF` a `TF`
	3. do zásobníku se uloží registr `CS`
	4. registr `CS` se naplní 16bitovým obsahem adresy `n × 4 + 2`
	5. do zásobníku se uloží registr `IP`
	6. registr `IP` se naplní 16bitovým obsahem `n × 4`
- Návrat do přerušeného procesu - speciální instrukce `IRET` (INTerrupt RETurn):
	1. ze zásobníku se obnoví registr `IP`
	2. ze zásobníku se obnoví `CS`
	3. ze zásobníku se obnoví příznakový registr (`F`)

## Rezervovaná přerušení 8086
| `INT n`| Význam                                    |
|-------:|-------------------------------------------|
|      0 | Celočíselné dělení nulou (Divide by Zero) |
|      1 | Krokovací režim (Single-step)             |
|      2 | Nemaskovatelná přerušení (NMI)            |
|      3 | Ladící bod (Breakpoint trap)              |
|      4 | Přeplnění (Overflow Trap)                 |
- `INTO` je-li nastaven příznak `OF=1`; zavolá se přerušení INT 4

### Krokovací režim (TF=1)
- procesor přeruší po provedení každé instrukce
- Musíme mít jiný program, který umí ladit. Ladící systém si načte do paměti laděný program.
- Počáteční spuštění krokovacího režimu:
```nasm
MOV AX, 0x0100 ;nastavení TF=1
PUSH AX
MOX AX, segmentová část adresy 1. instrukce
PUSH AX
MOV AX, 0 ;offsetová část adresy 1.instrukce
PUSH AX
IRET ;obnoví se TF=1
```

## Počáteční nastavení procesoru
- Procesor je inicializován aktivní úrovní signálu RESET.

| Registr          | Obsah                                   |
|------------------|-----------------------------------------|
|Příznakový registr| 0 (často bývá 2. bit 1, ale nemá význam)|
| IP               | 0                                       |
| DS, ES, SS       | 0                                       |
| CS               | 0FFFFh                                  |
- První instrukce, kterou bude procesor po inicializaci signálem RESET vypracovávat, je umístěn na adrese `0FFFF:0000h` tj. `0FFFF0h` (15 bajtů od konce).

## Adresovací techniky
(MOV nikdy nepovolí z paměti do paměti)
Operandy:
- Registr = 		`	MOV AH,BL` (Nesmím použít na dva registry různé velikosti.)
- Přímý operand =	`	MOV AH,50`
- Přímá adresa =	`PROM	DB ?`
				`	MOV AH, [PROM]`
				`	MOV AH, PROM`
				`	MOV AH, DS:[101]`
- Nepřímá adresa =`	MOV AH, [BX]` (`SI`, `DI`, `SP`, `BP`, `BX`)
- Bázovaná adresa=`	MOV AH, [BP+PROM]` (`BP`,`BX`)
- Indexovaná adresa=`	MOV AH, [PROM+SI]` (`SI`,`DI`)
				`	MOV AH, PROM[SI]`
- Báze + Index =	`	MOV AH, [BX][DI]`
				`	MOV AH, [BX+DI]`
- Přímá + Báze + Index = `	MOV AH, PROM[BX][DI]`
				`	MOV AH, [PROM+BX+DI]`
				`	MOV AH, [PROM+BX+DI+1]`

**Segment = přímá adresa + báze + index**

# Instrukce MOV
- Instrukce MOV nemění příznaky!
- Nikdy nemohu z paměti přímo do paměti.
- Nelze `MOV CS,...`! - musíme upravit i offset
- Po instrukci `MOV SS, ...` je po dobu trvání následující instrukce (`MOV SP,...`) zakázáno přerušení

# Aritmetické instrukce
## Rozšíření s respektováním znaménka
- Číslo z 8bitového registru rozšířit do 16bitového:
- Znaménkový bit objektu, který se má rozšířit, se zkopíruje do všech bitů objekt, o který se rozšiřuje.

## Celočíselné
- `ADD`: nastavuje příznaky: O, S, Z, A, P, C
- `ADC` - ADD WITH CARRY
	- Instrukce ADC se používá na sčítání objektů, jejichž šířka je větší než běžně zpracovávaná šířka objektů.
- `SUB` - SUBSTRACTION
- `SBB` - SUBSTRACTION WITH BORROW
- `CMP` - COMPARE; CMP Al, CL --> F:=AL-CL
- `INC` - INCREMENT; nastavuje příznaky: O, S, Z, A, P
- `DEC` - DECREMENT
- `NEG` - dvojkový doplněk; nastavuje příznaky: O, S, Z, A, P, C
- `CBW` - CONVERT BYTE TO WORD; nenastavuje příznaky; AX = AL se zachováním znaménka
- `CWD` - CONVERT WORD TO DOUBLEWORD
	- DX&AX:=AX
	- DX - vyšší bity
	- AX - nižší bity
- `IMUL` - SIGNED MULTIPLICATION **respektuje znaménka!**
	- nastavuje příznaky: O, C; S,Z,A,P jsou nedefinované
	- pro 8bit: `IMUL BL` AX:=AL*BL
	- pro 16bit: `IMUL CX` DX&AX:=AX*CX
- `MUL` - UNSIGNED MULTIPLICATION **neuvažuje znaménkový bit**
- `IDIV` - SIGNED DIVIDE
	- příznaky jsou nedefinované
	- pro 8 bit `IDIV BL`
		- AL:=AX/BL
		- AH:=AX modulo BL
	- Je-li podíl větší než maximální rozsah zobrazení --> INT 0
	- Zbytek má stejné znaménko jako dělenec.
- `DIV` - UNSIGNED DIVIDE - **neznaménkové**

# Logické instrukce
- `AND` - logický součin po bitech
	- příznaky: O = 0, C = 0; nastavuje: S,Z,P; A je nedefinováno
- `OR` - logický součet
	- `OR AL,7` AL:=AL∨7
- `XOR` - nonekvivalence
	- `XOR AL,7` AL:=AL⊕7
	- `XOR AL,AL` = vynulování registru AL
- `NOT` - inverze bitů (jedničkový doplněk)
	- příznaky nemění
- `TEST` - logical compare
	- nastavuje příznaky: S,Z,P
	- udělá `AND` na dva operandy, výsledek nikam neuloží
	- pouze podle výsledku nastaví příznaky

# Rotace, posuvy
- `ROL` - rotate left
	- `ROL r/m8,CL` - můžeme použít pouze CL, nebo immediate (max 5)
	- může nastavit OF
- `ROR` - rotate right
	- může nastavit CF
- `RCL` rotate left through carry
	- rotuje se přes carry flag
- `RCR` rotate right through carry
- `SAL` shift arithmetic left (`SHL` shift logical left)
	- obě provedou tutéž akci
- `SAR` - shift arithmetic right
	- nejvyšší bit se kopíruje znovu do nejvyššího
- `SHR` - shift logical right
	- do nejvyššího (uvolněného) bit se dává 0

# Větvení programu
- `JMP` - jump, nepodmíněný skok
	- přímý skok (cílová adresa je v instrukci)
		- mění CS
			- **vzdálený skok (far jump)**
			- plní CS:IP
		- nemění CS - IP := IP + vzdálenost
			- vzdálenost ∈ <0; 65535> sčítá neznaménkově!! = **blízký skok (near jump)**
				- **v případě, že bychom skočili na adresu za segmentem, bude adresa od začátku segmentu**
			- vzdálenost ∈ <-128; +127> sčítá znaménkově!! = **krátký skok (shot jump)**
	- nepřímý skok: v instrukci je odkaz na:
		- Registr SI, DI, SP, BP nebo BX (obsahuje offset cílové adresy
		- Paměť
			- segment : offset
			- offset
```
JMP rel8	JMP SHORT navesti	krátký skok
JMP rel16	JMP návěští		blízský skok
JMP ptr16:16	JMP FAR_PTR návěští	vzdálený skok
JMP r/m16	JMP [BX]		nepřímý blízký skok
JMP m16:16	JMP [dvojslovo]		nepřímý vzdálený skok
```

## Podmíněné skoky
- Pouze krátké skoky: vzdálenost <-128,+127>
- Neuvádí se ,,short"
```nasm
JE	EQUAL		ZF=1	roven
JZ	ZERO		ZF=1	nulový
JNE	NOT EQUAL	ZF=0	různý
JNZ	NOT ZERO	ZF=0	nenulový
JP	PARITY		PF=1	sudá parita
JPE	PARITY EVEN	PF=1	sudá parita
JNP 	NOT PARITY	PF=0	lichá parita
JPO	PARITY ODD	PF=0	lichá parita
JS	SIGNUM		SF=1	záporný
JNS	NOT SIGNUM 	SF=0	kladný nebo nenulový
JC	CARRY		CF=1	nastal přenos
JNC	NOT CARRY	CF=0	nenastal přenos
JO	OVERFLOW	OF=1	nastalo přeteční
JNO 	NOT OVERFLOW 	OF=0	nenastalo přeteční
...
ABOVE/BELOW - pro neznaménková
GREATER/LESS - pro znaménková
```

# Zásobník
- `PUSH` Uložení 16bitového objektu do zásobníku
	- SP:=SP-2
	- [SS:SP] := operand_16bitový
- `POP` Výběr 16bitového objekty ze zásobníku:
	1. operand_16bitový := [SS:SP]
	2. SP:=SP+2

# Volání a návrat z podprogramu
- `CALL`
	1. `PUSH CS` - pouze ,,FAR" varianta
	2. `PUSH IP+délka_instrukce`
	3. (CS):IP := operand
- `RET` - Return
```
RET		POP IP	blízký návrat
RETF		POP IP	vzdálený návrat
		POP CS	vzdálený návrat
RET imm16	POP IP
		SP:=SP+imm16
RETF imm16	POP IP
		POP CS
		SP:=SP+imm16
```

# Příznakový registr
- `PUSHF` push flag registr
- `POPF` pop flag registr

# Přerušení
- `INT` Interrupt
	- `INT imm8` - délka 2 bajty
	1. `PUSHF`
	2. IF:=0, TF:=0
	3. `PUSH CS`
	4. CS:=[imm8x4 + 2]
	5. `PUSH IP + délka instrukce` (obsah zásobníku ukazuje za ,,INT imm8")
	6. IP:=[imm8x4]
- `INT 3`
- `INTO` Interrupt if oveflow
- `IRET` interrupt return
	1. `POP IP`
	2. `POP CS`
	3. `POPF`

# Cykly
- `LOOP` - unconditional loop
	- nemění příznaky
	- registr CX ... čítač průchodů
```nasm
	MOV CX, pocet_pruchodu
	OPAKUJ:
	...
	LOOP OPAKUJ	; DEC CX
			; pokud CX != 0 SHORT skok na OPAKUJ
```
- `LOOPE`

# Ovládání V/V zařízení
- Na adresovou sběrnici ,,číslo" v/v zařízení == V/V brána == port v intervalu 0 - 65535
- Na datovou sběrnici data 
- V/V brány jsou 8bitové - lze pracovat i s dvojicí bran na po sobě jdoucích adresách
- `IN` Input from port
	- Přenos bajtu nebo slova ze V/V brány do registru AL nebo AX
- `OUT` Output to port
	- Přenos bajtu nebo slova z registru AL nebo AX do V/V brány

# Další instrukce přenosů dat
- `XCHG` cílový, zdrojový ... zamění obsahy
- ...
- Instrukce `NOP`
	- `NOP` No operation = operační kód `90h`; jednobajtová; ekv. `XCHG AX, AX`
	- Využití např. pro optimalizace umístění začátku těl cyklů

## Řídící instrukce
- `HLT` HALT - zastavení procesoru
	- obnova NMI nebo RESET
- `ESC` - Vzorek uvádějící instrukce 8087
- `WAIT` - Čekání na dokončení akce 8087
- `LOCK` - Instrukční prefix ,,zamykající" sběrnici po dobu trvání instrukce

## Řetězcové instrukce
...

# Procesor Intel 80286
- 16 bitový procesor
- od 1982, cca do 1990
- frekvence 6 - 16 MHz
- nové počítače PC AT
- 24bitová adresová sběrnice, tj. 16MB RAM
- (Procesor 80186 se v pc moc neuplatnil)

![80286 pin diagram](https://image.slidesharecdn.com/architectureof80286microprocessor-141202115351-conversion-gate01/95/architecture-of-80286-microprocessor-15-638.jpg?cb=1425054100)
# Režimy
## Reálný režim
- Kompatibilní s procesorem 8086.
- Je nastaven po inicializaci procesoru.
- Rozdíl oproti 8086 - adresová aritmetika - není přetečení

## Chráněný režim
- Zapíná se programově z reálného režimu.
- Adresuje 16 MB reálné paměti a 1 GB virtuální paměti.
- Poskytuje prostředky 4 úrovňové ochrany.
- Nelze se vrátit z chráněného režimu zpět do reálného. (Používal se RESET procesoru pomocí V/V instrukcí)

## Příznakový registr
- Stejný jako v 8086
- v chráněném režimu rozšířen o `NT` a `IOPL`
	- `NT` Nested Task
	- `IOPL` I/O Privilege Level
		- Určuje úroveň oprávnění, při kterém může proces ještě provádět V/V instrukce.
		- Vyšší hodnota představuje nižší úroveň oprávnění.

## Registr MSW (Machine Status Word)
![Machine Status Word Register](https://i.ytimg.com/vi/CZyVU694d8c/maxresdefault.jpg)
- slouží k zapínání chráněného režimu
- 16bitový registr
- `PE` Protected Mode Enable 
	- zapíná chráněný režim
- `MP` Monitor Processor Extension
	- indikuje přítomnost koprocesoru
- `EM` Emulate Processor Extension
	- zapíná emulaci koprocesoru tehdy, není-li koprocesor instalován (Je-li `EM=1`, způsobí instrukce koprocesoru přerušení `INT 7`).
- `TS` Task Switch
	- se nastavuje vždy přepnutím procesoru. Je používán koprocesorem zjištění, že v procesoru se vyměnil zadavatel úkolu.

# Adresace paměti v chráněném režimu
- Proces = dynamická veličina, zkopírovaný program v paměti
- Segment (oproti v 8086 snaha segmenty rozdělovat)
	- Ochrana instrukcí před přepsáním.
	- Více uživatelů chce spustit stejný program = každý uživatel má různý datový segment, ale stejný jeden instrukční segment.
	1. Báze segmentu (adresa začátku segmentu)
	2. Limit segmentu (délka segmentu ve slabikách - 1)
	3. Přístupovými právy a typem segmentu.
- Globální adresový prostor - Pouze jeden; pokryta celá paměť; může používat OS
- Lokální adresový prostor - Je jich více; každý proces má vlastní lokální adresový prostor
- Virtuální adresa (selektor:offset) - zpracovávají se jinak než v 8086; selektor ukazuje do tabulky

## Segment selector
![https://nixhacker.com/content/images/2020/02/segment-selector1.PNG](https://nixhacker.com/content/images/2020/02/segment-selector1.PNG)
- Selektor obsahuje 13 bitů (8 192 kombinací) indexu do **tabulky popisovačů segmentů** lokálního nebo globálního adresového prostoru a další 3 informační bity.
- `TI` **Table Indicator**
	- vybírá jestli ukazuji do globálního adresovacího prostoru (`TI=0`) nebo lokálního (`TI=1`)

## Tabulky popisovačů segmentů
![40035a.gif](https://is.muni.cz/el/1433/podzim2004/PB151/um/40035a.gif)
### Transformace virtuální adresy
- Transformace virtuální adresy na reálnou pomocí tabulek popisovačů segmentů v procesoru 80286
- Virtuální adresový prostor 1GB: 14b. Selektor + 16b. Offset

### Popisovač segmentu
Typ popisovaného segmentu je definován obsahem slabiky **přístupová práva**. Podle typu segmentu rozlišujeme v 80286 tyto 4 základní třídy popisovačů:
1. popisovač segmenty obsahujícího data (datový segment),
2. popisovač segmentu obsahujícího instrukce (instrukční segment),
3. popisovač segmentu obsahujícího informace pro systém (systémový segment),
4. popisovač brány. (Virtuální)

#### Popisovač datového segmentu
- `bit 4 = 1`  a `bit 3 = 0` určuje, že jde o datový segment
- `P` Segment Present
	- je nastaven na jedničku tehdy, je-li obsah segmentu uložen v reálné paměti. Není-li, je nulový (A při použití se generuje INT)
- `DPL` Descriptor Privilege Level
- `W` Writable
	- je nastaven na 1, pokud je povoleno čtení i zápis do segmentu. Zásobník musí mít vždy W=1. 
- `A` Accessed 
	- nastavuje procesor na jedničku při každém přístupu k této položce v tabulce popisovačů segmentů .
- `ED` Expansion Direction
	- indikuje, kterým směrem se bude obsah segmentu rozšiřovat. Datové segmenty mohou obsahovat klasická data `ED=0` nebo zásobníky `ED=1`.
![Popisovač datového segmentu](https://is.muni.cz/el/1433/podzim2004/PB151/um/40040a.gif)
#### Popisovač instrukčního segmentu
- `bit 4 = 1`  a `bit 3 = 1` určuje, že jde o instrukční segment
- `DPL` úroveň oprávnění instrukcí procesu
- `R` Readable
	- nulový zakazuje čtení obsahu segmentu. (Můžu jen spouštět)
- `C` Conforming
	- nulový sděluje, že podprogramy volané v tomto segmentu budou mít nastavenou úroveň oprávnění odpovídající úrovni segmentu, v nemže se nacházejí. Je-li C=1, bude volanému podprogramu v tomto segmentu přidělena úroveň oprávnění segmentu z něhož je volán. (Zvýšení oprávnění.)

#### Popisovač systémového segmentu
Smí být umístěn pouze v GDT.
- `bit 4,3 a 2 = 0`  určuje, že jde o systémový segment
- Typ (nejnižší 2 bity)
	1. označuje segment stavu procesu (TSS - Task State Segment) pro právě neaktivní proces.
	2. označuje segment stavu procesu pro právě aktivní proces.
	3. označuje segment stavu procesu pro právě aktivní proces.

### Segmentové registry
- kromě viditelného selektoru, jsou v registrech načteny i přístupová práva, báze segmentu a limit segmentu

#### Registry GDTR a LDTR
![Mikroprocesory II. Literatura Přehled mikroprocesorů Intel](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAeoAAABnCAMAAAD46vG6AAAAk1BMVEX///8AAADl5eXo6Oj09PT6+vrf39/R0dHt7e329vbq6urU1NTc3Nzw8PDOzs7MzMy+vr62travr6+oqKjGxsa3t7d/f3/BwcFlZWWMjIxeXl6dnZ13d3eqqqpZWVmUlJRsbGyHh4czMzNLS0s8PDxPT08cHBxqampEREQlJSV8fHwsLCwNDQ03NzeYmJggICAWFhYrO2YyAAAX5ElEQVR4nO1dCXejug6WDTaLzRKWAAECZGmztGn//697kkk7vdNkbqc3s73hOycQvGNZsoyFAJgwYcKECRMmTJgwYcKECRMmTJgwYcKECRMmTJgwYcKECRMmTJgw4f8FwSyOY0epWRyFUoZ4EYe8iOMoy0I8caU1pdAyjgueRTyaFbKQEqMKqXUcKymlwmilZRZKLaVLIXgu8Ce5pAMXLp1936IQDZhWcs+jhL6gMO6C1pL7wsYQzsGTUrhCuBzjheDcD4QnpMU9ITzK+qs77Y+Ey/5EWL+62/5EaAa24JYV2LZrWZYHgYdnzw7oKrAIPv21kNfoLD3A1Fz5FKZ9m05SepzOHgVKjHIx9ZgCc5sEmo66n+GV1soUp7QpQklThOLmwkQpisL/GIUyQCmOsoFTFNcU5kcs+tXd9iciZz+xsia8STEukzcp5y+DYuLnVVYlNylGsOIm5fxlkD+V1OltymH5bcr5wyF4ooED8CjkktvnUBdcIqnjaNcP3ibXP1OA1/VtyplITRjYqlpskVXbO8bWd+wJldXKaK0PbAdbxlY79vxGjuoLXG0HYXabWfUrVN8QvN8hXIKzWua7lMt/DX5TmP3614fL+OMXbAtGZEyIfh0bkJt3qMLsBojZGlpk4CXTAHdv1JpLXC3ZMBsW9O+kP9EGN7sWM1ybq+3VysGTWL4Jy7D5+cUB4BtSW03VzfcBZ2dRkbEvtJ6PdxU/H5f3C36xkc+vf9+t3LZV/3Aox7Zoxl9rPOfcsEXfu/9o0B7GsXptXP0AROzenOk+KtYBNXULSPgZm6Pc82BDpF4z9ZrDuUBqQWG0dn38lMDtqmsx86uLpMdxeHxFWffy7OJT+8c4JG//Is13X0jtnTPe55Ta/rqAf6D4urluCmKFy4VzywxbzN8MX+KOrxYuLyQ+frOmm2Ik78v/1LTrSDO3ITU2esNSPWOnLzniC72ptyBOPaY/xgLck+zAq/uzwEsr7LgWh4A8xLZf66ZQA91o3vhwStNOQHq3BF0DvzCfXie1IYddB1DHUefFyP06B754UJcS+zRe5z39bT24n6EYSzH/OuswXCdYiX2+qzLGwxr7pKgpBq/UKYNMtZaOwa1VjWoNW2JwlNhgzbghaMDBRTZVIiaZ9sgzLPuAE5oTj4UqRiIE/4SUPPezCDChH7kw3NENagk+9340gycMx2KYJmlM1D1giMXMZKxQgBMa1ldH9qYZmr1vU8SGNUoA785nGZx6FsOijXsTlfUVXtVxLxh/aJsls1jj4FgeKlXynuVDgzJBw+Myr5v3zWu792EGliFNvV2KNTukrC1nUD7FcLi/nBxbBIuX8lGIDENwjGBZ+k0Li2jeIKlHTu6owmEF/Uk1/sZaZmovd6JjVe8zYR172bfQNy4k6+wx9+4Wm/OEZaRC17dYf7MtlgWkKeRVvjORNjslR9Q68mWyhbqZryrJtLst1lnMMgHr+j6L2Ja575p9U0iiKI8ZMXRCjIykNDQ+n6BE2Yfj+EsOzd7rJyXmntc4sxrmwGGj2f0wtjxE3S5i5Vw0J8BZbBHj7KCWSN5y7gv1AIsZcOQI9wFWFxgyvbbYSrFPUw73Gd4B1D0slNEXHy8y9UjqCpUJXuI9zlOacGYb2FqU2z4tVq9cbUhd7QLWDc8u287gSOKYU+xKwFJTwmowU5s+wuK1eeMEkHfYhlKSxG5TYM0w6jg24/YRuZydZszdS40D4KgtNo8FVZs+Zc0SnuQPX8OWZhpm1NITqWXQj/MyDjMT3yPLSbaH6KUll9Qy0kTuMtjxBGXbkPKkGM7TqN1y5sYl/tspJCx2UbaFeTyzzUCq7wVz3GSeC7kBNns/RXbXSL1Faf9kNOukhHUBm1DEVYYzcXwxOZHa0EsgjZoTkTrdws6m8cFC/ei+zNUnIvU+9kmyuUF8V+yoQAdbbkjNcS6Auis8jFeP0L9OyCb/+uCjKoBaHfZRkmCvCHGOtKGpaNLxBAx9j0L7KIWsHxzxALJbcjF24Y/Ggc27+xW2tMblVXU4PpNmC9UjY72iqZotHXhiw8M3SJ2yKF+i8K+6x32XP6yYKzZZ2VKUYnmDFGmbWT2/OyCP01J5cczhWAz4J/PZ1kuXDcinOXt837jhCqltFullh6U/QZnAsxdslsgdNbDy4upMmHV1UaLgRVpsB1gM+lnDptbrzGNWx0o9CvBgMXgOrj9gkasmWbpNrEvZccoVoIDFbMsc0rKHugoWETy/aKFCMy3gIZds4y4GF1WMvoaq9ktDwASZu97xNE29FA5pgfKOqbiCagYsmXl7P6qAXZFHN4WcxYo0QtQWwqLIRpLGWZhRkzIeFahZR7PX1dYFUhdZXhhltkBFLgBNz2PycwZNA9+N8Sp4iLFLAAe2jaPJizEVdq8KQNBdahldWLjO28ttDvOkRQ0QMhcs5C+s2QOBzBJd5g1SIaiOmrrZTRIPnBwbLJ0c08tEKzebmecCUZIkMzP95rkIwgT/8lZBkmtwkhCndcogMjzEbQg8mb10Slu0yPFtoULQOo0gaGcCisQ0x2tnMxApqpBFHYoySRaJleZuTPqRQn3SrROI0it3+isRfe5pWb6ry+/PNZ/9e5oPwL/Rdsfd1QcAH4aFiklyQQH9HZF9jtRe6Hwi17eeln0H7NuIR1l85h6+Ah+uiarfDprloVJSOlEYOn6ooywMQ63w4HDt4F+H+2HoKxXw0CDKIkyqlBNGIbcoV+RyCh5zhcpycOoIM42HCEsOsUClZahC5XUN7UIrLNBRocUjzKK5xIRuGDiho33pa4zhgRWGXAeWxPp1EKhQ88B1Q9fRAccoNT0D/wz4rzUn+ST+u+T9GyE827a1kuakOR4DrujCR77Ck801BmGcSRFoH4+eVBTlaY1hHm2gUZSiKG4p13Z9VypThG9yUREIqV0qQVGU9l3MhRyrKLOWmMv2xygUIdrzuZIURWLANWHK5S5KAayS/4SVzIQJEyZMmDBhwoQJEyZMmDBhwoQJEyZM+C2g5k2Cx6b3ErOfFm068N8YhYLcXTPf+16IgUwdvF0P0LwxkxOLzdtEXxklBPAvaKa37T6KwbxvvM9gOJjr/dxsQ4tXAs9Xb1I//Yd35PhodJgfAXYc4NVM/B/W5OthPKeeJlvZV/Nj/7KhGLbovG8sbmO58P8Ml8zqgi9WIauRxvGrdUT3xkS0H/57hdnZtH33EvCW1PVLBfpxhwPDWr3wbPpvLxBkf4g5x68E2be2CryqArC76q6D/AQt2yPJ1VDZ0JVwmic9sjNvyg9bcIRVOm9Q+kZlY/Oy5Nk2nDU2eKem30C4kO6R9S6WeODg7GBWZXMyLB3mL7aCarlUEJf0joDTLMKEPY5DMOtOBxvyKhHA5+mpceYSeHWIi4enCpIqq0VxwvMPtqP+Q0FylTii2eBfLVgLMxZA09M0Dc6SSO2zpWAK8gY+vK8vnnZwmIN+Ar2G0wPYR9A4VWxm0D0aw/7smSQKDxiRWqx2sKwhLGE96gnqQSQVrKx4T4a3qEZsk9Gm0WLDWs9Ku6mBycMgBUsEs1UDQyOwvPuNjpht3uCY8B4betvGmMGTfN5Uxkq6xJl7sTnVDLp78JgDu4RYlX34RaxtCt0BykU3Z0haO4nAZm7Ixrl670C2RXF9rCvGQ1TLdhHcYyNk9zQy72hODHXJhGIHnGK2L5ZlR7xgdT5sbQbxAufqVrBDYcOJ3p6i1+TJdH4i9WUkrCfVZraH+RyInQXjUO1sPdwLGUCMpGEJlBWk+6DpP/riAY6VdgNDL2zMcdrRu5lMBygvJJJi3dJ4KnLmSsuYGPcpnEoI91k6NxWMb+ktO59ZwhlwMplX9jh53OOQO6YZtng5b5CyixPo+aqHegkOLGgkMh8erylxfznE+J5OuzLGvnt6ccpH4mQtOUWoRboypN4MgMuuVdmL/YdYZjlA/Qhqj4PHQ2qS5s4kbGeQPAGJCJdBTyFZTLTva5oohgGae6MF6jscH4pxxfLFAedeGE7ZqGKTdGlxIPSwU7R22NViR+8Etmt9gj2Jf+bC5NrmChpjir9GVphth8Mmun8uQfQ96UZ9zLfPutuUarP33UXZ7nPv+SMLbXW3Vg3OzEk/J5GBhcFwHEAcyuGxPm2WAua44nKXKCyWmzrbLOV6l7nbMl6OBp35ch7CaVF3ZbTdIgH5tjTs3rItcne1Xbtit2esyB939rbvA3C3S7vdbDCyXQzl8l/X4xM+gPj3kI4H1Cayu1/dit8dViQty5eO40g/UI4Tch44keMEgcYwrbgMlZTc1o7CMNuxMNDmIcaFvnZ2mENHWYY5/BRPFBzFjqOSBGMwS0RBcZs4Y5QTY9JTPosdrVvjvDAuTEwbOSqLKIETqlke4aU0ebU7cyidomK1pkaFlh1lhVM4ypSqlXN6KLK+z0InDngcUSaZRZnSlo8NVcrSsRMpbmmK4VbmFDwg43D+7Xfj/89g/2JD7F+Mv+lJasAi4SrFPY8jxwSW4shAtsdd39HC9siFHxlgK+VjCoryyZOf8jw/QI7Ho1TK9byAF8/CC6TrkEU39ymXp7V06UqrwOOBJ5UpgkvPc13ksdFuPPAwuSaDcc93JFaJ0gTrMk5GqQwlbWMirrESSVV6rtZNjQmxLNsNFBl7vzRUa2pNYFoo6f0PTk3DEzVTOZZLYSi3KIHmOv67VLabLTt/pjOr5FbOrG7j6u4PAbuRtynxM0ndXnWI831gv4dW+ZNwK662fyapT7fyRvjHk5oHENFi0vXIe/aXXV9BnhkElwH/8pzrVqT2fyapqxu9n/rHk5ofGVvS86SSsefyjh0EuZTb759R5azpjcnNHWvOA8C9QGoRvbjY888aqvh3TdW9sKwVqM79kCfQ86tc7XrvN7KuPr8VRGrb8y4lONtFYJR9bUn2zefCyFUv8d6b4/X0346+CnZmsQNryVvRgwcshYFV0C/HSMXOW8QXHAcUy3h93pduzuUkxkGf+61tS33Bq5pkzak3bnLSz7yzbl99Yf7qW+f205HGlv3WC2aCNzO74o0wpmdu60t6eDJ6QSlWd9Wwuewh/ksl7/x47ZYN6/sn81+O/okf3qiS87tm3v+DCQQ7m1V876O9xzOJ5oz0l5r15Lotw4sAm3ekyKezOyzvPVdjktPZzCg+u5wZLQbm31KGgqcLgXsLjOOWYfmZTePkcC3mdLUlzdih/2AhzzgbvADPaOC7yzYrZz9ss/LVZ8rXeH1l155/HbUxvonmY9+WpoL2rb0EkkP+03nRSwO/16pieR5wqdloVOwO3LPbOYfcReZF+upt8B2pBQuFjRUngwcDpsoOPqS93dkDWyDzHlB2plnzjq0uaeDkyinZ0sZy7YLowoH8fZylSELGCjke+JBwex43teppMTBbRhhXlxrax6PHBzu4YO1yXQM3Li9EynE95gw8rjzglbD7K6sqE7wcZ4PiRBN3Ps+BpxWSeT+SOibx1z7gTQ82xrQagjSHKA2LIBN2mqUFeOtdTm5yChCJHl1vhaZjLV1gS+BwypGvaZc1P53lwx77g+SppOROEdUBJuQzhyiGwWkC3sz/0NroifpditETLLIzeSnFi3HE7FndPbya673n6pxR2C4cThTZpLMGDoduHdjMA3dnP0b5jsl3hL1E6vihWuCgEyxASVnN8bBMst5EZYdDhJ1M3giDfdI0d8BKvQK4r3TJG5YMlTFa2Gzz9JI3wmvrateM8fZ+IRYPQ8JmTQqLuxkcrvCKUcuWhmWrg4s0b+Z6B31G2+b7kc+M58CMQZfqo3i2q0SveKkclm89XKfeb92ugtlCwGyjqwrKXf+iRlAnZqX7IGC+Das5pAeI5/5x5MFjP5BNXV7SPlxTsNZ9TIKjbhybaQ9blFZwt7v7CK3NXI2jpjPuQeORxumZ1BSJvTCmFJeeDWLv5qy8p31kzXrkvBVJZ7r/RQFlasfb91qE9/C+nENnNKg6Na4/8xop0SzGoeXgINRsWHrDHB60vSTPbXKNjesxgXyC3mxtgv8gthdm0vr0PswgQdaJLePLkEG9BRQOCkfopTJMV7yQmgcor9VqnOPFbHtnGM+UuDU9KNi8Y/wO73+dwoujXIdcjNksIM97u4IWIacv0s7wkO5QzBwS2k1FUu9OyfN4+0wKclv6dMp2vCzo8RP5auuUIAe+zio+beC94LyIB+OTVpw9wa7Geblm44AjUhP1zULD/drdncDxEHdA1h4iXYXkYEnwXTVE0KPgwjkP+6S80ArvggZOBqi7DNY83yRwSvykOJynUQ9XAm5ONgo7RRKCkT3o0ObuhnTRrvFY5LfNjCyMWHLBG+E1rl7G4O1BPmSQLmBdiGPiFU0Cz/7lfjNub4jUjY90Kx7JmyD2duYdXTiOXN3SPLyozSAQPFx3SxL6gqQiduswQwJ7SR0Fa0WO9k6vqzeSSXBYwpMWKL8E87qOCPxSsYeigKYbP8DBVGILtqHgyT7CnsjiLS2K1x95kif3jO1W7BmVS/bUlMwokEGPoQuy1mOs9FErH60/7a+5OmPSWguIFs7WSVnis9k2TqrsroT7poJtRx1zyRui914Dr1mUL1CrGU7bY1WsNphtly8NlVAA9jjMZmVe98c5OEfyRlg+JvCYz2tYxC7bBfXTHPR+YLt3BV9dbGmWSFx0hM8L8kZ4tF22RmGGQmVxcc0naK5WD7VVMEiX0TGGdpetBYs6tjzbpHnzhYyW2FVd6TTZPqhTa8WTWOGAp9yHUt+fQJVNkJY+ypJl+qUpOD+XVcZ2ate7mOgwh/bg14brJDJBjNRv70Ud1B150HtKUCJ1MxQVHaykP3zM9loorbnUPkhtqZeVrYiUox0BXmtOuhu3/d9Z2XFVGXN8J0W1RJGP5QCXnhAH4DlUNDX1kreYC1w9021mWCPEHAFggyAIz3mNN0JvRq4HH3IQAWmgQpMfM8swvrSNL2qw/EtL+vkVJ1Raz1Ka4DSc3fabJuO/y0sYYwyt4rZtI3L9TE3TrQcy9bkbJWYKd9okGVeKuhVe0ZH1NFbRxgVoHJ1NaurLsZ4C9W27rs8iKOhmqMGJIdMFhBKjvLYNQKVjbw8t6rz1nMMMOf4eV3OVrBPekkd7ib0f1Ilwhhs9/H3Fxbn6M7gkwD8Azart9+e6Old/H8THLSGvoLxBQx4jeK/l/gjc6sHoZ7c7/M/Un9xqu+M/uqgTyS22xuqu+ikfAGGF9ISwOX0zUnBpBUJ4vufjyaZHhsJzpbRFIHzXFYEtuBY+fVxSctsSliuEZdu+9DEFw2hP0scoue/ZruQSy+Da5XSSUpqvUEp3vPA8z6cNaSrKBTugT1hihQF9BDMALNCyKSoIuCcwK6YDGVATAx54KPsq8ASWYXFsEZYbjI2nI7gmtXCp8Xger8wR5wnjntnD+6Ar4f1d3gjLX2sG8ovxd9mIC5fTXpfPOc+CwCcTDY5BXEcWJ/gUw+m7sciOoQniDl3w0BLmygvoMgwoFwbSUavAnG1zVCaZ1kCFaG6bMN9EORZlVubrl3gOxgIxnaWQjU2gT23T2h3LGIuS42kMi1zTHHMLnIuxJLqwnMxcyLEBinLpMDOZssjnwfSOz4QJEyZMmDDhd4JP36uI2k++7kIPptr6yyZ9ACqJrnwr4z/CfMwDspelLD1cA8f9rH2vk/zxX7P8XiSL6AlO987nPvNDDjQWTvbyxCug58KP5Q8xurV68yWjYjE+4vK39NSdsU8+r4r7T35t5A8G8/0ItCU+RWrv6QHgOPNenGpsWQ4x6Bv4zriAgD4sWDr6vESKdlj/9rIt0L9j58HxZ3zY6DeCxwayNdHsU86LluH4Ja6zFdg8WpNAv/BlrFuAjFJPm/j8hVMoUJSIQ7r+nAEelrX5q97tGJ9dM88T7vMnnhy1i4Q5sNTDuNEoj9lz7UHxic8pfQT8yXwisThbcoVYKZew/JwEZwLWP+Tb2r8x7jg8e83w4s7ouxDoiM2IQR7N3p3Qzqai7xzeuo0jyCwlWEHRj5fku6MazKcGP4Ehg6e/67EoMsdynkJY9otPCcLu2EPab1/UsOKxR17/MZsI1nJVCujKxTg9883qAF6zKD8nwN31/W12Qv8kjMbt/2nl8cO/xvpbVj1hwoQJEyZMmDBhwoQJEyZMmDBhwoRfhv8BQud9sslE66sAAAAASUVORK5CYII=)  
Plnění:  
	GDTR: LGDT operand obsahující bázi (24b) a limit (16b)  
	LDTR: LLDT selektor (16b) (nesmí se použít v reálném režimu)  

### Naplnění instrukčního segmentu
- jádro OS si vytvoří datový segment, pro nikoho jiný nedostupný
- do tohoto nahraje instrukce jako data, po té přes datový segment "přeplácne" instrukční segment a tento pak dá LDT

## Úroveň oprávnění
- 0 až 3 (nejvyšší až nejnižší
0. jádro operačního systému (řízení procesoru, V/V operací)
1. služby poskytované OS (plánování procesů, organizace V/V, přidělování prostředků0
2. systémové programy a podprogramy z knihoven (systém obsluhy souborů, správa knihoven)
3. uživatelské aplikace

`DPL`

`CPL` Current Privilege Level
- dva nejnižší bity **selektoru CS**
- momentální oprávnění procesu
<!-- markdown cheats -->
`RPL` Requested Privelege Level
- bity 0 a 1 **selektory segmentového registru**
- nastavuje se
<!-- markdown cheats -->
`EPL`
- vyhodnotí se pomocí `RPL` a `DPL`

## Brány pro předání řízení
- Zamezení přeskočit kontroly volání více privilegovaných funkcí např OS
- Označuje vstupní body 
- Každá úroveň oprávnění má vlastní zásobník
- brána překopíruje počet parametrů ze zásobníku vyšší úrovně do nového zásobníku

## Privilegované instrukce
- CPL = 0
```nasm
LGDT
LIDT	naplnění datiblu pro přerušení
LLDT
LTR	plnění tabulky při přerušení
LMSW	plnění registru MSW
CLTS	nulování TS bitu v MSW
HLT
```
`POPF` a `IRET` smí měnit zásobník pouze pro CPL = 0

## Segment stavu procesu (TSS) Task State switch
![40105a.gif](https://is.muni.cz/el/1433/podzim2004/PB151/um/40105a.gif)
- registr TR ukazuje na aktivní proces TSS

# Přerušení
- **Interrupt Descriptor Table (IDT)** obsahuje až 256 popisovačl rutin obsluhujících přerušení.
- **IDTR** obsahuje adresu IDT (like GDTR)
- **Popisovače v IDT** pouze tyto tři:
	1. brána zpřístupňující TSS,
	2. brána pro maskující přerušení (Interrupt Gate)
	3. brána pro nemaskující přerušení (Trap Gate)
- Do zásobníku navíc může být uloženo chybové slovo.

## Přerušení generovaná procesorem
- **Fault** do zásobníku uloží CS:IP ukazující **na instrukci**, která způsobila přerušení.
- **Trap** do zásobníku uloží CS:IP ukazující **za instrukci** (na následující instrukci), která přerušení způsobila.
- **Abort** v procesu nelze pokračovat a musí být násilně ukončen.

## Rezervovaná přerušení

# Zapnutí chráněného režimu
1. do paměti zavést programy a odpovídající tabulky popisovačů,
2. nastavit GDTR a IDTR,
3. zapnout chráněný režim nastavením bitu PE:=1 registru MSW,
4. provést blízký skok JMP proto, aby se zrušil obsah interních front procesoru, ve kterých jsou uloženy předvybrané instrukce (výběr instrukcí totiž závisí na zvoleném režimu procesoru),
5. vytvořit TSS inicializačního procesu a nastavit obsah TR,
6. naplnit LDTR,
7. inicializovat ukazatel vrcholu zásobníku SS:SP,
8. všechny segmenty v paměti označit P:=0,
9. nastavit příznakový registr F a registr stavu procesoru MSW,
10. inicializovat externí zařízení,
11. zabezpečit obsluhu všech možných přerušení,
12. povolit přerušení (IF:=1),
13. zahájit provádění prvního programu

# Procesor Intel 80386 - i386
- 32bitový procesor
- u vyšších frekvencí se začali poprvé používat chladiče
- od 1986 do 1994
- alternativní název i386DX
- varianta pro 16bitové prostředí: 386SX
- matematický koprocesor zvlášť i387 (velmi drahý)
- 32bitová datová sběrnice
- 32bitová adresovatelná sběrnice, tj. max. 4GB RAM

## Registry
- 32bitové začínají s `E`, např. `EAX`, `EBX`
## EFlags
- **VM** (Virtual 8086 Mode) - zapíná režim virtuální 8086 pro proces, jemuž obsah příznakového registru náleží. Příznak VM smí programátor nastavovat pouze v chráněném režimu, a to instrukcí IRET, a to jenom na úrovni oprávnění 0. Příznak je také modifikován mechanismem přepnutí procesu.
	- Program MONITOR, kontroluje daný segment v VM. V případě použití privilegovaných instrukcí, musí MONITOR filtrovat akce a zpracovat je vhodným způsobem.
- **RF** (Resume Flag) - maskuje opakování ladícího přerušení

## Popis signálů a registry
- Registry pro uložení selektoru datových segmentů: **DS**, **ES**, **FS** a **GS**.
- Velikost viditelných částí registrů se nezměnila (selektor je stále 16-bitový), ale zvětšila se neviditelná část tak, že báze segmentu je 32bitová.

## Adresace
- logická adresa - dříve virtuální, je převedena na lineární
- lineární adresa - 
- fyzická adresa - je transformováná činností stránkovací jednotky z lineární. V případě, že není stránkování je totožná s lineární
  ![Adresace](https://is.muni.cz/el/1433/podzim2004/PB151/um/45002.gif)
## Řídicí registry 80386
- **PE** - nejnižší bit; zapíná chráněný režim
- **ET** - Extension Type - sděluje typ koprocesoru (80237=0, 80387=1)
- **PG** - Paging - zapíná stránkování
- Rozšířená podpora ladění:
	- Ladící registry: DR0, DR1, DR2, DR3, DR6 a DR7
	- Testovací registry: TR6 a TR7 (

## Popisovače segmentů
- 8 bajtů, obsahující bázi segmentu(32 bitů), limit segmentu (20bitů), přístupová práva
- **G** Granularita
	= 0 - jednotka limitu je 1 B (max. 1 MB)
	= 1 - jednotka limitu je 4 KB (ma. 4 GB)
- **AVL** (Available for Programmer Use)
- **s** závisí na typu popisovače

### Popisovač datového segmentu
- s = **B** (Big)
	= 0 - segment podle pravidel 80286 (max. 64KB), implicitní velikost položky ukládané do zásobníku je 16bitů
	= 1 - segment podle pravidel 80386 (max. 4GB), zásobník lze plnit od adresy FFFFFFFFh, implicitní velikost položky ukládané do zásobníku je 32 bitů

### Popisovač instrukčního segmentu
- s = **D** (default)
	= 0 - implicitní velikost adres a operandů je 16 bitů
	= 1 - implicitní velikost adres a operandů je 32 bitů

Explicitní určení velikosti zajišťují prefixy:
**`66h`** mění implicitní velikost **operandu** a
**`67h`** mění implicitní velikost **adresy** 

###  Popisovač systémového segmentu
- bit s není použit
- v 80386 se v přístupových právem rozšířil počet typů na 16 kombinací
	- 0..7 označení stejná jako pro 80286
	- 8..F vlastnosti podobné jako u 0..7, ale pro 80386

## Stránkování
- umožňuje použít více operační paměti než reálně máme
- od 80386 prvně přístupné stránkování
- **logická adresa**
	- používá programátor `selektor 16:offset 32`
- **lineární adresa**
	- vznikne procesem stránkování
- **fyzická adresa**
	- je-li zapnuté stránkování, vzniká z lineární adresy
	- není-li, pak vzniká z logické adresy
	- vystupuje ven

* Rámec a stránka kapacity 4KB

![Stránkování](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fis.muni.cz%2Fel%2F1433%2Fpodzim2004%2FPB151%2Fum%2F45004.gif&f=1&nofb=1)
### Položka stránkové tabulky a adresáře
- **P** Present - Je-li P=0, není obsah stránky ve fyzické paměti. Zpřístupnění takové stránky zalová `INT 14` a v CR2 je adresa stránky
- **D** Dirty - obsah rámce byl změněn. Stránka je potřeba uložit na disk.
- **A** Accessed - nastavuje procesor při každém použití tohoto specifikátoru. Použití při výběru rámce, který z paměti odstraníme.
- **U** User Accesible 
- **W** Writeable

### TLB - Translation Look-aside Buffer
- 4 cestná asociativní paměť (tabulka klíčů + data)
- asociativní paměť má 8 položek (řádků)
- dolní 3 bity lineární adresy (12-14) určuje řádek tabulky
- 4\. bit v cestě daného řádku, určuje, jestli je daná cesta volná k použití
- každý proces má jednu stránkový adresář a stránkovou tabulku (max 1 stránkový adresář + 1024 stránkových tabulek

### Vyprázdnění TLB
- Vyprázdnění TLB je nastavení V:=0 do všech položek.
- Automaticky vždy při naplnění **CR2**.

### Mapa přístupných V/V bran
- Pro kontrolování V/V instrukcí pouze tehdy, je-li CPL>IOPL.
- Je-li bit mapy =0 - V/V operace se povolí
- je-li bit mapy =1 - generuje se INT 13
- Pracuje-li V/V instrukce se slovem nebo dvojslovem - testují se všechny odpovídající bity.
- uložena ve stejném segmentu jako je TSS; systém smí nad strukturou TSS uložit vlastní data
- offset této mapy uložen na konci TSS struktury
- Za konec mapy se doporučuje vyplnil samými 1 (aby se případně generovalo INT 13)

![Mapa bran](https://is.muni.cz/el/1433/podzim2004/PB151/um/45006.gif)
## Rezervovaná přerušení
INT 1 - ladění
INT 14 - Výpadek stránky (Page Fault)
	- nemáme oprávnění na přístup
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTEzMzA2NTExMywtMTM2OTA2MjUzMiw1NT
E1NTYxMjYsNzg2MDY2MTc4LC0xODE3NjU0NzQyLDE2NzY2NzM2
NDQsLTE4NTYwMjc5MCwtMTkzMDA2MDkyMCwtMTI0MTgwMzc1LC
0xNzQ5OTgzNDkzLC0yMDI3NTk1MDQyLDg1NjIwODAzMywtNTE3
NTU4MTc4LC0xODQzOTk5MDUxLDEzNzYxMjY4NTAsNTg3Njk1Mj
Q5LDY4NDY5MDY5OCwxMDk1MDA0NjE3LDg2MjYxMDg0NCwyMDI0
MzM1OTI3XX0=
-->