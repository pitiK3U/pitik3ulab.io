+++
title = "11 - Reálná čísla IEEE 754"
date = 2021-02-12
+++

# IEEE 754
## Matematický koprocesor
- Nyní máme oba procesory u sebe
- Liší se v příkazech, v op. kódu je bit, který určuje pro který procesor daná instrukce je

### Typy dat pro koprocesor
- integer - až 64 bitů
- BCD
- Reálné čísla - až do 80 bitů (existuje až 128 bitová reprezentace čísla)

![IEEE Standard 754 Floating Point Numbers - Tutorialspoint.dev](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftutorialspoint.dev%2Fimage%2FSingle-Precision-IEEE-754-Floating-Point-Standard.jpg&f=1&nofb=1)
![IEEE Standard 754 Floating Point Numbers - GeeksforGeeks](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmedia.geeksforgeeks.org%2Fwp-content%2Fuploads%2FDouble-Precision-IEEE-754-Floating-Point-Standard-1024x266.jpg&f=1&nofb=1)
## Nenormalizované číslo
- **Musíme vědět, že číslo je nenormalizované!**
- máme větší rozsah zobrazení
- Ke každéme registru uschovávajícímu číslo IEEE je 2 bitový příznak
	- 11 .. registr je prázdný
	- 01 .. registr = 0
	- 10 ... registr = 
		- nekonečno
		- nenormalizované číslo
		- atd.
	- 00 ... ,,normální" obsah registru

## Formát čísel
### Rozsah zobrazení:
(-1.0 × 2<sup>2<sup>n-1</sup></sup>; +1.0 × 2<sup>2<sup>n-1</sup></sup>) n ... počet bitů

### Přesnost zobrazení:
- na kolik bitů lze zobrazit Mantisu
- počet bitů Mantisy +1 = m + 1

### Rozlišitelnost
= nejmenší kladné nenulové zobrazitelné číslo

|  |  | |
|--|--|--|
| normalizované: | +1. 00...000 |× 2<sup>2<sup>n-1</sup></sup>+1 |
| nenormalizované: | +0. 00...001 |× 2<sup>2<sup>n-1</sup></sup> + 1 - m|


<!--stackedit_data:
eyJoaXN0b3J5IjpbLTEwNzAzMTAzNTcsLTMxNjg3Nzk5MCwxMz
E5OTU0OTk2XX0=
-->