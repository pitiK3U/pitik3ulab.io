+++
title = "04 - Paměti"
date = 2020-10-30
+++

# Paměti
- vnější paměti = umístěné mimo, typicky disk, **udrží data i když odpojíme**
- vnitřní (operační) paměti  = paměť, kde jsou uložené data programu po spuštění
- registry = **malé**, ale za to **velmi rychlé**

<!-- markdown cheats -->

- parametry pamětí:
	- vybavovací doba - 10ns...100ms = **čas přístupu k záznamu v paměti**
	- rychlost toku dat = počet přenesených bitů za sekundu do paměti
	- kapacita paměti = počet bitů, bajtů, slov
	- cena za 'bit' (v praxi spíše GiB)
	- doba uchování dat
	- přístup
		- přímý (nejčastěji operační paměť)
		- sekvenční (magnetická páska)
	- destruktivnost při čtení
	- energetická závislost a nezávislost (jestli paměť udrží data i bez napájení)
	- statika (nejsou potřeba 'krmit' a udrží svůj obsah po celou dobu napájení) a dynamika (náboj se po určité době ztrácí = musí se po určité době znovu nabýt)
	- spolehlivost - definujeme v rozmezí teplot (jak dlouho bude fungovat, kolik dat přeneseme než nastane chyba)

<!-- markdown cheats -->

- **cache** = vyrovnávací paměť 
	- slouží k vyrovnávání rozdílů toků (mezi procesorem a pamětí, mezi procesorem a V/V zařízením); 
	- cache tipuje, co bude dále čteno a požádá si o data dopředu

# Vnitřní paměť
- adresovatelná jednotka = nejmenší velikost dat, kterou jsme schopni zapsat/přečíst; nejčastěji bajt
- adresový registr = dáváme adresu paměti, kterou chceme číst nebo zapsat
- datový registr =čteme data z paměti nebo zapisujeme
- třístavový signál = čti/piš/nic nedělej
![Vnitřní paměti](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fis.muni.cz%2Fel%2F1433%2Fpodzim2004%2FPB151%2Fum%2F20015b.gif&f=1&nofb=1)
- Dekodér 1 z N: má N vstupů a 2^N výstupů (výběrové vodiče)
- Čtecí/Zápisový vodič

<!-- markdown cheats -->

- Paměť pro čtení a zápis:
	- RWM (Read-Write Memory)
	- RAM (Random Access Memory)
- operační paměť počítačů
- nejrozšířenější - polovodičové paměti:
	- Bipolární TTl
	- Unipolární NMOS, CMOS
	- SRAM, DRAM
	- energeticky závislé
	- nedestruktivní

<!-- markdown cheats -->

## Archaický typ - Feritové paměti
- Feritový kroužek
	- navlečen na místech křížení
	- magnetický materiál --> 
	- Pokud teče proud jen v jednou vodiči, elektromagnetické pole okolo vodiče není dostatečně silné, aby změnil vlastnosti kroužku; Pokud ale proud teče ve dvou vodičích, el-mag. pole se sečte a je dostatečně silný, aby změnil vlastnosti kroužku. 
	- Čtení se provádí **destruktivním** způsobem:
		- Do kroužku zapíšeme nulu.
		- Když tam byla 0, nic se nezmění.
		- Pokud, ale kroužek změní své el-mag. vlastnosti, tak tam byla 1.
	- Zachování dat paměti má na starost řadič!
	- (9. bit je paritní bit desky)

# Permanentní paměti
- paměť určené pouze pro čtení
- základem je **ROM** (read only memory)
- data jsou zapisují při tvorbě
![Paměť počítače](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fmamut.spseol.cz%2Fnozka%2Felektro%2Fepo%2Fpameti%2FROMa.gif&f=1&nofb=1)
- **PROM** (programmable ROM) = **OTP ROM** (One Time Programmable) 
	- Tavná spojka NiCr - kde má být 1, tak se nechá; kde 0, tak se spojka vypálí
	- pustíme krátký velký impuls --> vypálí tu součástku
![Hradla, klopné obvody RS, Polovodičové paměti](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.itnetwork.cz%2Fimages%2Fimg%2Ftvy%2F20.png&f=1&nofb=1)
- **EPROM**(erasable PROM)
	- mazání = Působením UV záření se obsah maže tím, že se elektrony rozptýlí.
	- programování - elektricky; Elektrony se shromáždí na jedné straně přechodu.
![EPROM](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fs3-ap-southeast-1.amazonaws.com%2Fsawaal.com%2Fqaimg%2Feprom_can_be_used_for1563542842.jpg&f=1&nofb=1 =250x)
- **EEPROM** (electrically EPROM)
	- mazání elektrickým proudem = **RMM** (Read Mostly Memory)

# Asociativní paměti
= **CAM** (contens addressable memory)
- paměť není adresovatelná číslem (adresou), ale **klíčem** (řetězcem)
- máme paměť klíčů a obsah ke klíči
- paměť klíčů obsahuje adresu kde začínají data ke klíči
![Asociativní paměť](https://is.muni.cz/el/fi/podzim2004/PB151/um/20025b.gif)![Architektura počítačů](https://is.muni.cz/el/1433/podzim2004/PB151/um/20025a.gif)

<!--stackedit_data:
eyJoaXN0b3J5IjpbMTQ5NDU0NjYyMSwxMTUzNjIxODczLDI1ND
gyOTkyOCw1MDIwODQxMTksLTEyMDMzMzc1OTgsMTEyNjAxNTc2
OV19
-->