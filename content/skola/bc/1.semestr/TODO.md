+++
draft = true
+++
<!--
- [ ] co; do kdy (reset)
-->
- [ ] [PB151] Zkouškové příklady
	- [x] E
	- [x] D
	- [ ] C
	- [ ] B
	- [ ] A
- [ ] [PB170] Projekt
- [ ] [IB015] Zkouška
- [ ] [PV170] Ústní

Zkoušky
IB000
- Pátek 22.1.2021 10:00, [Pá 5. 2. 2021 10:00], [Pá 12. 2. 2021 10:00]
IB015
- Středa 3.2.2021 10:00, [Středa 10.2.2021 10:00], [Středa 17.2.2021 10:00]
IB111
PB151
PB154
- Zápočtový test: Pondělí 18.1.2021 10:00, [Pondělí 25.1.2021 10:00], [Pátek 29.1.2021 10:00]
- Pondělí 1.2.2021 10:00, []
PV170

IB000
- [x] přednáška
- [ ] týdenní odpovědník; čtvrtek 13:59 (od pondělí 00:01)
- [x] **malý domácí úkol 17. - 18.11.**; Náplní bude podobný procvičovacím odpovědníkům z Lekcí 1 až 5.
- [x] domácí test 15.-.16.12. 18:00 Lekcí 1 až 8, zaměřeno hlavně na relace.
- [x] velký domácí úkol; od 2.11. do 7.11.
- [x] **školní test 30.11. 7:30-10:00**;
- [x] **školní test 4.1. 7:30-10:00** (75min) Učivo musíte znát až po isomorfismus grafů. 
	- Celkem je za tento školní test až 16 bodů, z nichž o 10 bojujete v tomto odpovědníku a dalších 6 bodů můžete volitelně zkusit získat za písemný důkaz popsaný dole.

IB015
- [x] přednáška
- [x] odpovědník; neděle 18.10.2020
- [x] DÚ
	- [x] 1
	- [x] 2
	- [x] 3

IB111
- [x] přednáška
- [x] první dva/bonusový př., kap. 5; čtvrtek (neděle )
- [x] třetí příklady, kap. 5; sobota (neděle )
- [x] domácí úkol,
	- [x] hw1
	- [x] hw2
	- [x] hw3
	- [x] hw4
	- [ ] hw5
		- [x] implementace
		- [ ] kvalita
	- [ ] hw6
		- [ ] implementace
		- [ ] kvalita
- [x] Vnitrosemetrálka, 3. listopadu 2020

PB151
- [ ] přednáška
- [x] milníky 
	- [x] 1. odpovědník 12.11.2020
	- [x] 2. odp.
	- [x] 3. odp.
	- [x] laboratoř

PB154
- [x] přednáška
- [x] relační algebra
- [ ] odpovědník
- [ ] democvičení

PB170
- [ ] závěrečný projekt - konec semestru cca + konec zkouškového
	- jlcpcb.com

PV170
- [x] přednáška
- [ ] samostatné úkoly (4)
- [ ] 2 písemné testy
	- [x] 10. listopadu od 16:00 do 18:00
	- [x] 15.12. kombinační obvody - minimalizace, až 20 bodů
		- [x] Bude dána funkce
	- [ ] Sekvenční obvody
- [ ] závěrečná zkouška písemná (nebo písemné testy) + ústní

VB035
- [x] writing several formal emails 6.12.
- [x] giving feedback on classmates emails 13.12.
- [x] presenting final project 11.1.
- [x] IBM lecture 17.12.

VV028
- [x] sledovat přednášky, každý týden
- [ ] Týden 4 - slidy, otázky
- [x] úkol - kompenzace studia informatiky
- [x] úkol - poslechnutí nesmyslného rozkazu

## [IB015]
IB015 -- Ukázka Testu Minimálních Znalostí (Podzim 2020)  
=============================================  
  
[1] S použitím základních konstrukcí jazyka Haskell (pouze to, co je k dispozici  
po zavedení modulu Prelude) naprogramujte funkci, která pro zadaný seznam znaků  
a číslo, vrátí seznam znaků, ve kterém jsou znaky z původního seznamu zopakovány  
tolikrát, kolik uvádí zadané číslo.  
  
Příklad:  
f ['a','2','b'] 3 = ['a','a','a','2','2','2','b','b','b']  
  
f :: [Char] -> Int -> [Char]  
f ZDE-DOPLNÍ-STUDENT  
  
[2] Uveďte jakýkoliv monomorfní typ (typ bez typových proměnných) funkce foo  
tak, aby funkce foo mohla být korektně použita v následujícím výrazu.  
  
 
map (foo even) . filter (>3)  
  
foo :: ZDE-DOPLNÍ-STUDENT  
foo :: (Int -> Bool) -> Bool
  
[3] Je definován následující datový typ  
  
data Kdetoje = Věc String | Krabice Kdetoje | Skříň [Kdetoje] | Šuplík  
Kdetoje  
  
a) Uveďte libovolnou platnou hodnotu typu Kdetoje  
  
ZDE-DOPLNÍ-STUDENT  
Věc "a"
  
b) naprogramujte funkci  
  
kolik_krabic :: Kdetoje -> Int  
  
která udává, kolik krabic musí zloděj otevřít, aby se dostal k  
věci.  
  
ZDE-DOPLNÍ-STUDENT  
  
[4] Doplňte instensionální definici seznamu tak, aby platilo:  
  
take 4 seznam = [(1,1),(2,1),(1,2),(3,1)]  
take 4 (drop 3 seznam) = [(3,1),(2,2),(1,3),(4,1)]  
  
seznam = [ (a,b) | a <- [1..], ZDE-DOPLNÍ-STUDENT  
  
  
[5] Jak definujete x, aby platilo  
  
concat (fst x) = "Ahoj"  
  
x = ZDE-DOPLNÍ-STUDENT
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTEyNTU4OTA0MjIsLTIwOTU0MzEzMDUsLT
EzNDM1NjI5MDEsNjQ2NjE5NzU2LC0xNTQ2OTQ3MjEwLDI2OTYz
MTg3NCwtMTQzNzcxODM2OSwxNzI3NTAyMDUzLC02MzM1NzM3ND
EsLTQ0MTgzNDUxNywtMjA4OTEyNTg0OCw1MzEwMTI0NzIsLTk4
NDMxNjMyNiwtNjczMDgxOTgwLDEwODc5MjQ5NTYsMjA4NDg2Nz
IwNCwxMzQ2OTA5NjExLC0xMDY0MjI1MDY1LDE5OTg4MjczMzgs
MjA1NTU0ODA2XX0=
-->