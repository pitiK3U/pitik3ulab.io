+++
draft = true
+++
Na Jaře - Soutěž pro šikovné studenty FI na 24h

https://www.fi.muni.cz/for-partners/Theses-info.html

Předmět s projektem, kde se pracuje v více studenty.

Zjistit si info o firmě.
Připravit se na pohovor. Slušné oblečení.

Linky:
https://www.fi.muni.cz/for-partners/annual-reports.html

Stránky spolupráce konkrétních firem
https://www.fi.muni.cz/for-partners/lexicalcomputing.html
https://www.fi.muni.cz/for-partners/invasys.html
https://www.fi.muni.cz/for-partners/honeywell.html
https://www.fi.muni.cz/for-partners/inqool.html
https://www.fi.muni.cz/for-partners/kentico.html
https://www.fi.muni.cz/for-partners/ysoft.html.en
https://www.fi.muni.cz/for-partners/bluesoft.html
https://www.fi.muni.cz/for-partners/flowmon.html.en
https://www.fi.muni.cz/for-partners/logex.html.en
https://www.fi.muni.cz/for-partners/tescan.html


<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE3MDcxODQwMzAsMTU2NzY4OTk2NywtOT
A4MDYxNzI0LDIyMTQ1MTI0OV19
-->