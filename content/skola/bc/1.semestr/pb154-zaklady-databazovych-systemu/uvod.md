+++
title = "01 - Základy databázových systémů"
date = 2020-10-05
+++

# PB154 - Základy databázových systémů
* učebnice Database system concepts
* [slides](https://is.muni.cz/auth/el/fi/podzim2020/PB154/um/01-ch1-introduction.pdf?predmet=1324138)

# Úvod
* **Database Management System** (**DBMS**)
	* kolekce propojených dat
	* množina programů, která tyto data zpracovávají
	* program musí odrážet skutečnost, kterou modeluje a musí mít jistý výkon
* Využití:
	* Bankovnictví: zlepšení transakčního zpracování
	* Letectví
	* Univerzity
	* Prodej
* osobní (malé) × velké databáze

<!-- markdown cheats -->

* první systémy = **souborové zpracování**
	* data v různých formátech
	* duplicitní data
	* špatný přístup k datům (pro každý přístup byl potřeba nový program = časově náročné)
	* data izolována a uložena v různých formátech
	* **integritní omezení** :? (den v týdnu pouze Po, ..., Ne; ne nic jiného)
		* změna omezení složitá
		* přidání nových omezení nebylo jednoduché
	* problém s **Atomicity of updates** (Atomicitou aktualizace) 
		* systém v konzistentním stavu
		* buď se stane celé změna nebo žádná
	* nebyla možnost zápisu více uživatelů zároveň (potřebný k největší výkonnosti)
	* bezpečností problémy
		* těžko nastavit přístup jen pro určité uživatele

-> databázové systémy, aby odstranili dané problémy
# Datový model
* relační datový model
* ER model (Entity-Relationship data model)
* Object-based (Object-oriented and Object-re)

<!-- markdown cheats -->

* **relace** = tabulka
* **schéma** = hlavička tabulky
* **n-tice** = řádky 

<!-- markdown cheats -->

* prostředky pro manipulaci s daty = jazyky -> **DML** (**Data Manipulation Language**)
* typy:
	* procedulární - jak to dostat
	* neprocedulární - co, ale ne jak
* nejpoužívanější = **SQL**

<!-- markdown cheats -->

**DDL** (Data Definition Language)
* definuje schéma tabulky
```sql
create tableinstructor(
	IDchar(5),
	name varchar(20),
	dept_name varchar(20),
	salarynumeric(8,2))
```
* data dictionary
* metadata
	* schéma
	* integritní omezení
		* Primary key
		* referential integruty
	* authorization - 

<!-- markdown cheats -->

SQL
* příklad:
```sql
select name
from instructor
where instructor.ID = '22222'
```

* použití:
	* samostatné jazyky
	* **API** (ODBC/JDBC = open/java database connectivity)

## Návrh databáze
* problém s redundancí - duplikace dat && komplikace změny dat (musí změnit všechny výskyty)
* normalizací
	* funkční závislostí, jak umožňují redundanci zmírnit
* ER model
	* entity: 'objekt', který je rozdílný od jiných objektů
		* popisuje vlastnosti
	* vztahy: spojení několik entit
entity = obdélníky
vztah = kosočtverec

### Storage management
* data musí být uložená na diskovém médiu (HDD)
* proces mapování  je zajišťován systémem **Storage management**
* co dělá
	* přístup k datům
	* uspořádání dat
	* označování a 

### Zpracování požadavků
1. parsování a překlad
2. optimalizace
3. vykonání

### Transaction management
* odpovědi na otázky 
1. co se stane když zapsání transakce selže
2. co se stane když více uživatelů aktualizuje data
* transakce
* Transaction-management component =
* Concurrence-control manager = kontroluje ... a zajišťuje konsistenci databáze

naivní uživatelé
aplikační programátoři

Administrátoři

* všichni používají query processor --> storage manager --> disk storage

Architektura databází
* centralizované
* client-server
* paralelní
* distribuované

## historie
* začátek v 50s and 60s
	* magnetické pásky a štítky
* 70s a 80s
	* velký rozvoj
	* síťová a hiararchální datový model
	* revoluční byl relační datový model
		* jednoduchý --> získal popilarity
* 80s
	* SQL, standardy
	* paralelní a distribuoané systémy
	* object-oriented db
* 90s
* brzké 2000s
	* XML a XQuery nové standardy (očekavní že překoná relační model --> nenaplněno)
* pozdní 2000s
	* 
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE4MzAxMzQ5MzQsLTE4MTAyMjAyODcsLT
EzMzMxNzQyMzBdfQ==
-->