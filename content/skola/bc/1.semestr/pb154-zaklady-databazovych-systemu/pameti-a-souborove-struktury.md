+++
title = "06 - Paměti a souborové systémy"
date = 2020-11-27
+++

```c
// Kradeno z https://github.com/Krejdom/school_notes/blob/master/PB154.md 
```

# Úložiště a souborové systémy
## Klasifikace fyzických úložných médií
* rychlost přístupu, cena za jednotku dat, spolehlivost
* **volatilní** (pomíjivé) -- ztratí obsah po vypnutí
* **non-volatilní** (nepomíjivá/perzistentní) -- obsah zůstává i po vypnutí
* spolehlivost zvýšíme redundancí
* výkonnost zvýšíme paralelismem

### Cache
* nejrychlejší, nejdražší, volatilní
* spravovaná HW sytému

### Hlavní paměť
* rychlý přístup, **volatilní**
* malá nebo drahá pro uložení celé databáze

### Flash paměť
* přístup srovnatelný s hlavní pamětí, zápis a mazání je pomalé
* data přežijí i při výpadku elektřiny
* přežije pouze omezený počet přepisů
* **solid state disks**
* velmi rozšířené u různých zařízení (USB, fotoaparáty, mobily...)

### Magnetické disky
* dokáží uchovat celou databázi
* pomalá, protože se data nejdřív přepíší na hlavní paměť
* přímý přístup (nikoli sekvenční jako na magnetických páskách)
* přežije selhání systému i elektřiny
* čtecí hlava, **sektory** (512 bajtů) - základní jednotka, cylindry, plotny
* **blok** = souvislá sekvence sektorů z jednoho tracku; použití pro předávání dat mezi pamětí a diskem
* standardy diskového rozhraní: ATA (AT adaptor), SATA, SCSI, SAS
* Mohou být připojeny k vysokorychlostním sítím: SAN, NAS
* parametry: přístupový čas (=access time; seek time, rotation latency), data-transfer rate, MTTF (Mean Time To Failure) -- čas, který by měl disk běžet bez selhání
* výtahový algoritmus (rozvrhování; minimalizace přejezdu čtecí hlavy)
* Diskový řadič - interface mezi hlavní pamětí a diskem
	* zajištuje přenos z hlavní paměti na disk a naopak; zajišťuje i správnost
	* **checksum** = hodnota pro kontrolu správnosti zápisu; spočítá se kontrolní suma před přenosem a ověří se na zapsaných datech 
		* Jestli jsou data porušená, je velká šance, že checksum před a po zápisu nebude stejný
	- Řadič přemapuje špatné sektory

#### Organizace souborů
* data se ukládají do určitých bloků, jsou aktualizovaná, mazána, ...
* --> může dojít k **fragmentaci** = "data jsou uložená daleko od sebe, čtecí hlava musí hodně přejíždět"
* --> **defragmetace** = odstranění fragmentace

### Optické disky
* non-volatilní, zápis a čtení je pomalejší než u magnetických disků
* data se čtou opticky z točícího se disku pomocí laseru
* CD-ROM (640 MB), DVD (4.7--17 G), Blu-ray (27--54 GB)
* CD-R, DVD-R, DVD+R, CD-RW, DVD-RW, DVD+RW, DVD-RAM)
* juke-box

### Pásková úložiště
* non-volatilní, velmi pomalé (sekvenční přístup), velká kapacita (40--300 GB)
* využití: zálohování, archivace dat

## Hierarchie úložišť
1. **primární úložiště** -- nejrychlejší, volatilní (cache, hlavní paměť)
2. **sekundární úložiště** (on-line úložiště) -- non-volatilní, mírně rychlejší přístup (flash paměť, magnetické disky)
3. **terciální úložiště** (off-line úložiště) -- non-volatilní, pomalý přístup (magnetické pásky, optická úložiště)

### RAID úrovně
- Redundant Arrays of Independent Disks
- Více disků zapojené do jednoho systému, velká kapacita a rychlost užíváním více disků zároveň, vysoká spolehlivost = obnovení dat v případě failu
- HW Raid vs SW Raid (SW je jednodušší)

* Level 0: bez redundance
* Level 1: zrcadlení disků
* Level 2: Memory-Style Error-Correction Codes
* Level 3: Bit-Interleaved Parity
* Level 4: Block-Interleaved Parity
* Level 5: Block-Interleaved Distributed Parity
* Level 6: P+Q Redundance

## Organizace souborů a záznamů
* databáze = kolekce souborů
* soubor = sekvence záznamů
* záznam = sekvence polí
* Heap = halda (uložení, kde je volné místo)
* sekvenční 
* hashování
* multitable clustering file organization
* Datový adresář = metadata

## Přístup v úložišti
* buffer = část hlavní paměti k ukládání kopií diskových bloků
* LRU strategy (Least Recently Used)
* Pinned block
* Toss-immediate strategy
* MRU strategy (Most recently used)
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTc0ODYyMDY5MSwtMTg3Mjk5NTU2MCwxMD
M4MTA3NDQ2LC0xMDI2NDA4NDU1LC03NTU4MjYzMCwtMTU0Njg2
NDE1Miw4MTAyMzczMzgsLTE5NDM4MzkzNzUsLTEyNDk5ODM2Nj
ksLTE4NDMyNDAwNDEsLTQzMzQ2MDksLTEzOTMwNTA2MDgsLTI0
NDU5ODk5MV19
-->