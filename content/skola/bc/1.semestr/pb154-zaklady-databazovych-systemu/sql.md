+++
title = "03 - SQL"
date = 2020-10-30
+++
```c
// Kradeno z https://github.com/Krejdom/school_notes/blob/master/PB154.md 
// protože na zápisy z toho předmětu už asi nemám
```

## SQL (Structure Query Language)

-   jedná se o tzv. DDL (Data Definition Language)
-   popisuje schéma relací, doménu hodnot atributů, integritní omezení a další
-   ! nevrací množinu !
-   jazyk pro práci s daty v relačních databázích
-   postaven nad relační algebrou
-   příkazy lze zanořovat
- nezáleží na velikosti písmen - **case insensitive**

**skalární subquery** -- je očekávána jedna hodnota

### Doménové typy v SQL

-   char(n) -- přesně daná délka
-   varchar(n) -- omezenou pouze maximální délkou
-   int
-   smallint
-   numeric(p,d) -- p = přesnost, d = počet číslic za desetinnou čárkou
-   real, double precision -- přesnost závisí na stroji
-   float(n) -- přesnost (na n číslic) stanoví uživatel

### Základní struktura dotazů

-   `SELECT (projekce)` -- `SELECT *` znamená vyber vše
    -   `AVG (thing)` -- průměr
    -   `COUNT (počet)`, `MIN`, `MAX`, `SUM`
-   `FROM (kartézský součin)`
-   `WHERE (selekci)` -- využívá: and, or, not
    -   `BETWEEN 900 AND 1000`
-   relace `GROUP BY atribut1, atribut2...` -- atributy jsou nepovinné
-   `HAVING podminka_s_agregacni_funkci` -- aplikuje se po vytvoření skupin! (WHERE se aplikuje před)
-   `ORDER BY` -- následuje atribut a `decs` nebo `asc` (výchozí)
-   `DISTINCT` -- klíčové slovo, které je hned za SELECT, **odstraní duplicity**
-   `ALL` -- klíčové slovo, které je hned za SELECT, **ponechá duplicity**
-   `SOME`
-   `LIKE` -- `WHERE name LIKE '%dar%'` (pokud jméno obsahuje podřetězec _dar_)
-   `ESCAPE` -- `LIKE '100 %' ECSAPE '' (obsahuje _100 %_)
    -   `'slovo%'` -- začíná na _slovo_
    -   `'%slovo%'` -- obsahuje podřetězec _slovo_
    -   `'_ _ _'` -- slovo délky 3
    -   `'_ _ _ %'` -- slovo alespoň délky 3
-   `IS NULL`
-   `IN` -- množina obsahuje zvolený prvek (`pes IN zvirata`)
-   `NOT IN`
-   `EXISTS`
-   `ANY()`
-   `EXCEPT` = rozdíl, `UNION` = spojení, `INTERSECT` = průnik; chci-li
-   `CREATE TYPE` -- vytvoření vlastního datového typu
-   `CREATE DOMAIN` -- vytvoření vlastní domény
-   `CREATE INDEX` -- vytvoření indexu
-   ...



#### Manipulace s tabulkou

-   `CREATE TABLE r (A1 D1, A2 D2, ... , An Dn, (integritní-omezení1), ..., (integritní-omezeník))` -- vytvoří relaci `r`, kde `A` je jméno atributu, `D` je datový typ hodnot v doméně atributu
-   `puvodni_jmeno AS nove_jmeno` - přejmenování
-   `INSERT INTO tabulka VALUES('obsahsloupce1', 'obsah_sloupce2',...)` -- vloží nový záznam do tabulky
-   `DELETE FROM tabulka` -- smaže veškerý obsah tabulky
-   `DELETE FROM tabulka WHERE podminka` -- smaže položky splňující podmínku
-   `UPDATE r SET A1=e ... WHERE`
-   `DROP TABLE tabulka` -- smaže tabulku a veškerý její obsah
-   `ALTER TABLE` -- změna tabulky:
    -   `ALTER TABLE r ADD a d` -- přidá nový atribut `a` a jeho doménu `d`
    -   `ALTER TABLE r DROP a` -- smaže atribut `a` z tabulky

#### Spojení
-   používají se jako subquery ve FROM části
-   `(NATURAL) INNER JOIN` - přirozené vnitřní spojení = **spojí se pouze to, co je v obou relacích** (Spojovací sloupec je pouze jednou)
-   `LEFT OUTER JOIN` - levé vnější spojení = **levá relace se vezme celá**
-   `RIGHT OUTER JOIN` - pravé vnější spojení = **pravá relace se vezme celá**
-   `FULL OUTER JOIN` - úplné vnější spojení = **obě relace jsou obsaženy, to co chybí je doplněno NULL hodnotami**
-   `INNER JOIN ON podmínka` (Spojovací sloupec je dvakrát)
-   `INNER JOIN USING (seznam atributů)`

**view** -- relace, která je "virtuálně" zprostředkována uživateli, `CREATE VIEW v AS <query>`

**blob** (binary large object), **clob** (character large object) -- vrací pouze pointer, nikoli samotné data

**trigger** -- příkaz, který je vykonán automaticky systémem, jako vedlejší efekt modifikace databáze (`INSERT`, `DELETE`, `UPDATE`), může být omezen na určitý atribut, může se zreplikovat provedenou akci na další data, např. při změně výše DPH se automaticky přepočítá cena

#### Integritní omezení
-   `UNIQUE` -- unikátní záznamy z množiny
-   `CHECK(p)` -- p je predikát
-   `NOT NULL`
-   `PRIMARY KEY`

<!-- markdown cheats -->
- kandidátský klíč může být `NULL`, ale primární ne

##### Referenční integrita
- Úzce spjaté s cizím klíčem.
- Zajišťuje, že příslušná hodnota existuje i v jiné relaci.
- `FOREIGN KEY father REFERENCES person`

#### Datové typy -- datum a čas
-   `DATE` - `'YYYY-m-d'`
-   `TIME` - `'HH:MM:SS.MS'`
-   `TIMESTAMP` - `'DATE TIME'`
-   `INTERVAL` - `'1' day`
##### Uživatelské typy
- `CREATE TYPE name AS NUMERIC (12,2) FINAL`

##### Domény
- `CREATE DOMAIN person_name CHAR(20) NOT NULL`

##### Indexy
- Rychlý způsob, jak se dostat k příslušným hodnotám atributů.
- `CREATE INDEX studentID_index ON student(ID)`
<!--stackedit_data:
eyJoaXN0b3J5IjpbMjAwODI0MTk3OSwxNjA3ODIwMjcyLDU5Nz
c2ODUyMiwtMjMxNzI3MzIwLDk4MTE2OTExMSwxMzQzMzE5NTY2
LDEzMjA1NzAwODcsNjU1MzgxODQ5LDQzODc5MTgyMywtMjQ4Mz
A4MTg2LDEzMjgyODA4NTIsMTMyODI4MDg1MiwxODc0NTU5ODYz
LC0xMjM4MTM3NTQ0LDY0NDA0NDgzNCwxMTczODg5MDc4LC0yNz
E4NjE4NDhdfQ==
-->