+++
title = "04 - ER model"
date = 2020-11-13
+++

```c
// Kradeno z https://github.com/Krejdom/school_notes/blob/master/PB154.md 
```

# RE model

(Entity-Relationship Model)

-   databáze může být reprezentována pomocí _kolekce entit_ nebo _vztahů mezi entitami_

**entita** -- existující objekt rozlišitelný od ostatních, má atributy ("vlastnosti")

**entitní množina** -- množina entit stejného typu, které sdílejí stejné vlastnosti

**vztah** -- propojení více entit

**množina vztahů** -- matematická relace mezi 2 a více entitami, může k ní patřit nějaká vlastnost

**stupeň množiny vztahů** -- většinou binární (vztah mezi dvěma entitami)

**kardinalita** -- one to one, one to many, many to one, many to many

## Atribut

-   **jednoduchý** (např. město) vs. **složený** (např. adresa)
-   **jednohodnotový** (např. jméno) vs. **vícehodnotový** (např. telefonní číslo)
-   **odvozený** -- vypočítá se z jiného atributu (např. věk)

## Klíče
- **super klíč** -- množina atributů, kterou každou entitu jednoznačně identifikují
- **kandidátský klíč** -- super klíč, u kterého nemůžeme už žádný atribut odstranit
- **primární klíč** -- kandidátský klíč, může být ale jenom jeden
- Nemůžeme mít více vztahů mezi dvěma primárními klíči. (Chceme-li zaznamenávat datum setkání mezi studentem a profesorem, nemůže být více vztahů, ale musíme použít více hodnotový atribut.)

## E-R diagram

-   entitní množina = obdélník
-   množina vztahů = diamant
-   atributy jsou vypsány uvnitř obdélníku
-   primární klíč je podtržen
-   to, co je _one_ má šipku; to, co je _many_ šipku nemá
- Total participation:
	-   každá entita z entitní množiny musí mít alespoň jeden vztah z množiny vztahů = dvojtá čára kolem diamantu a vztahové čáry od dané entitní množiny
- **role** -- při vztahu na stejné entitní množině lze danou roli uvést u čáry a označit tím její význam

### Ternární vztah
- Může mít nanejvýš jednu šipečku. (Kdybychom měli více šipeček, existuje více interpretací.)

## Slabá entitní množina

-   **nemá primární klíč**
-   její existence závisí na existenci identifikační entitní množiny
-   je propojena pomocí totálního one-to-many vztahu z identifikační do slabé entitní množiny
-   diamant je dvojitý
-   parciální klíč (discriminator) -- atributy rozlišující jednotlivé entity v slabé entitní množině, podtržení přerušovanou čarou

### Reprezentace v relačním schématu
-   primární klíč je tvořen primárním klíčem silné entitní množiny a parciálním klíčem
- vztahové množiny:
	- Many to many: primární klíč z první a druhé entitní množiny
	- One to many & many to one: přidáním atributu na stranu 'many', obsahující primární klíč ze strany 'one'
- Vícehodnotové atributy: v relačním schématu se neuvádí
	- vytvoří se nová relace `(ID, phone_number)` a `ID` bude stejné pro danou osobu. 
	- Pokud množina obsahuje pouze `ID` a vícehodnotový atribut, nemusíme vytvářet novou relaci.

**specializace** (top-down postup) -- rozklad jedné velké entitní množiny na menší

**dědičnost atributů** -- podřazená entitní množina dědí všechny atributy a vztahy ze své nadřazené entitní množiny, se kterou je propojena; OR -- šipky do nadřazené se po cestě spojí, XOR -- do nadřazené vedou dvě šipky

**generalizace** (bottom-up postup) -- kombinace více entitních množin, které sdílí stejné vlastnosti do nadřazené entitní množiny

**agregace** -- uzavření ER-diagramu do obdélníčku a napojení na další entitní množiny

**UML** (Unified Modeling Language) -- podobné ER diagramům
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTgzMDA3MjIzMCwzOTQxOTUwNjAsMTUyMz
c2NjE3NSwtMTg2MzQwMjY3MywxNzk4NzAxNDE0LDE1ODU1NTI0
MCwtNTMzMjQwNDRdfQ==
-->