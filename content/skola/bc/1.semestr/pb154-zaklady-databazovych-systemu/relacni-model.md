+++
title = "02 - Relační model a Formální dotazovací jazyky"
date = 2020-10-14
+++

# Relační model
- [slides](https://is.muni.cz/auth/el/fi/podzim2020/PB154/um/02-ch2-relational-model.pdf?predmet=1324138)
- základem = relace

<!-- markdown cheats -->

- **Atribut** = Sloupec v tabulce
- **Doména** = Množina povolených hodnot
- **Relační schéma** = záhlaví tabulky; 
	- uspořádaná n-tice atributů, 
	- prázdné relační schéma není povoleno!, 
	- př. `r(R1)` relace `r` mám relační schéma `R1`, `R1 = jmeno, vek, adresa`
- **Relace** = tabulka, skládá se z n-tic (řádků) a atribut (sloupců)
- **Uspořádaná n-tice** = Záznam/řádek v tabulce

## Atributové typy
- **doména** = Každý sloupec má definován jednoznačný název, typ a rozsah.
- hodnoty jsou **atomické** = dále nedělitelné
- hodnota **null** = značí žádnou hodnotu, je u každého atributu

**relační instance** = aktuální hodnoty relace v tabulce
na pořadí prvků relace nezáleží

Databáze = množina relací

## Klíče
- **superklíč** = podmnožina libovolných atributů jednoznačně určující danou **n-tici**
- **kandidátský klíč** = superklíč, kterému nemůžeme už žádný atribut odebrat, je minimální
- **primární klíč** = jeden z kandidátských klíčů, slouží pro praktické účely
- **cizí klíč** = (množina/jeden) atribut, který se objevuje v jedné relaci a je primárním klíčem v druhé

<!-- markdown cheats -->

- Procedurální = co chceme udělat a jakým způsobem
- Neprocedurální (deklarativní)
- Čisté (teoretické) jazyky
	- relační algebra
	- n-ticový relační kalkul
	- doménový relační kalkul
- Používané uživateli - SQL

# Formální dotazovací jazyky (Formal Query Languages)
- [slides](https://is.muni.cz/auth/el/fi/podzim2020/PB154/um/03-ch6-formal-query-languages.pdf?predmet=1324138)
- 3 typy:
	- relační algebra
	- n-ticový relační kalkul
	- doménový relační kalkul
    
## Relační algebra
- Procedurální jazyk
- 6 základních operátorů:
	- selekce σ
	- projekce Π
	- sjednocení ∪
	- rozdíl –
	- kartézský součin ×
	- přejmenování ρ
- Operace mají vstup jednu/dvě relace a výstup jednu novou relaci
- Operace můžeme skládat dohromady

### Selekce σ
- proces odstraňování n-tic, které nesplňují podmínku `p`
- podmínky můžeme 
	- atributy, konstanty + porovnávání
    - výrazy, závorková logika
    - spojovat: konjunkcí ∧, disjunkcí ∨ a negací ¬
`σ p (relace)`
   
### Projekce Π {#projekce}
- proces zanechání sloupců
- vrací množinu bez duplikátů
`Π a1,a2,...,ak (relace)`

### Sjednocení ∪
- sjednocení dvou relací `r` a `s`
- vrací množinu bez duplikací!
- relace musí mít stejnou **aritu** = **stejný počet atributů a každý atribut stejnou doménu**

### Rozdíl –
- vrací n-tice, které v obou relacích nejsou
- relace musí mít stejnou **aritu**

### Kartézský součin ×
- vše se vším
- relační schéma = relační schéma 1. relace + relační schéma 2. relace
- předpokládáme, že relace jsou disjunktní (nemají společný prvek)
- v případě, že nejsou disjunktní, musíme použít přejmenování

### Přejmenování ρ
- Musíme použít v případě kolize jmen.
- Umožňuje více pojmenování pro jednu relaci.
- změna názvu relace: `ρ nove_jmeno_relace (stare_jmeno_relace)`
- změna relačního schéma: `ρ nove_jmeno_relace(a1, a2) (stare_jmeno_relace)`

## Rozšiřující operátory
### Průnik (Set-Intersection) ∩
- relace mají stejnou **aritu**
- `r ∩ s = r – (r – s) = s – (s – r)`

### Přirozené spojení (Natural-Join) ⋈
- Zavedeno, protože kartézský součin produkuje obrovské množství dat --> 
- asociativní, komutativní
- relace `r` a `s` na jejich schématech `R`, `S`
- výsledné schéma `r ⋈ s` je `R ∪ S`
- např.
	- `r(R)`, kde `R = (A, B, C, D)`
	- `s(S)`, kde `S = (E, B, D)`
	- `r ⋈ s` je definováno jako 
	`Π r.A,r.B,r.C,r.D,s.E (σ r.B = s.B ∧ r.D = s.D (r × s))`

#### Theta join
- r ⋈<sub>θ</sub> s = σ<sub>θ</sub>(r × s)

### Vnější spojení
- Ve výsledku mohou být i n-tice, které nejsou v podmínce
- Chybějící hodnoty se doplní `null` hodnotou
- `LEFT` vs `RIGHT` vs `FULL`

### `NULL` hodnoty
- Znamená, že hodnota je neznámá nebo neexistuje
- Výsledek jakékoliv aritmetické operace je `null`
- Agregační funkce `null` ignorují

#### Porovnávání s null
| výraz                 |  vyhodnocení  |
|:---------------------:|---------------|
| `true AND unknown`    | `unknown`     |
| `false AND unknown`   | `false`       |
| `unknown AND unknown` | `unknown`     |
| `true OR unknown`     | `true`        |
| `false OR unknown`    | `unknown`     |
| `unknown OR unknown`  | `unknown`     |
| `NOT unknown`         | `unknown`     |

### Zobecněná projekce (Generalized projection) Π
- stejná jako [projekce](#projekce), ale argumenty mohou být aritmetické výrazy
`Π ID, name, dept_name, salary/12 (instructor)` 

### Agregační funkce (Agregate function) G
- Agregační funkce
	- `avg` 
	- `min`
	- `max`
	- `sum`
	- `count`
- Výsledek nemá jméno: můžu použít přejmenování, nebo `as`

`podle_ceho_seskupit G agregacni_funkce (relace)`

### Konstantní relace
- ,,Neměnná" relace
- Schéma lze určit přejmenováním
`ρ moje_predmety(uco,kod) {(123, 'PB123'), (456, 'VV001')}`

## Modifikace databáze
- **Přiřazení `r ← E`**
	- `E` libovolný výraz relační algebry.

<!-- markdown cheats -->

- **Smazání `r ← r – E`**
- **Přidání `r ← r ∪ E`**
- **Aktualizace `r ← Π E1,E2, ... (relace)`**

### Vice-množinová relační algebra
- Čistá relační algebra odstraňuje všechny duplikáty (po projekci, atd.)
- Z praktických důvodů se, ale často duplikáty nechávají.
	- selekce: má stejný počet duplikovaných n-tic jako ve vstupu
	- projekce: všechny n-tice co byly duplikované jsou i ve výsledku, může se zvýšit velikost duplikace, tím, že jsem zrušil nějaký sloupec
	- kartézský součin: m je počet kopií v t1, n kopií v t2. pak je `m × n` kopií t1.t2 v `r × s`
	- průnik: min(m, n)
	- rozdíl: max(0, m - n)

## N-ticový relační kalkul
- Jazyky neprocedurální, vysoce teoretické --> v praxi se moc neuplatňují.
- Dotazy jsou ve tvaru: `{t|P(t)}`
- Chci vybrat množinu všech n-tic `t`, pro které je predikát `P(t)` pravdivý.
### Predikát
1. množina atributů a konstant
2. množina porovnávacích operátorů (`<, >, <=, ...`)
3. množina logických spojek (`and ∧, or ∨ a not ¬`)
4. Implikace `⇒`
5. Množina kvantifikátorů
	- Existenční `∃`
	- Všeobecný `∀`

`{t|t ∈ instruktor ∧ t[salary] > 80000}`
`{t| ∃ s ∈ instruktor (t[ID] = s[ID] ∧ s[salary] > 80000}`

### Bezpečnost výrazů
- Můžeme vygenerovat nekonečný výraz: `{t|¬t∈r}`
- Každá součást výrazu, musí být v relaci, n-tici nebo konstantě v P

## Doménový relační kalkul
-   Neprocedurální dotazový jazyk, silou ekvivalentní n-ticovému relačnímu kalkulu.
-   Dotaz je ve tvaru `{< x1, x2, ..., xn > | P(x1, x2,..., xn)}`, kde `x1, x2,..., xn` jsou doménové proměnné, P je formule.
<!--stackedit_data:
eyJoaXN0b3J5IjpbMzY1NDk5NTEsLTMzMzg3MjM0NywtMTYyMz
E0MjA0OSwtMTY3MzQxMjAwMCwtMTcwMzkzNDUxMiwtMTY1MTIz
NjI0NywxNzE5NDg5Nzc4LDQ0NzIzNTU2NiwtMTA4NDAwNzU5NC
w1ODc3MDU4OTYsMTE4NDYyMTkyNiwxODE2MDYwMDcwLC0xMDg3
NzEzMzc4LDE3MzU3OTc3OTksMjM4ODg3NzEzLC0xNDA2NDk1Mz
AwLDc0MjE4ODU2Ml19
-->