+++
title = "12 - Transakce"
date = 2021-01-08
+++
```c
// Kradeno z https://github.com/Krejdom/school_notes/blob/master/PB154.md 
```

# Transakce
= jednotka vyhodnocování programu, který přistupuje a případně mění různé datové položky.

**ACID** (Atomicity, Consistency, Isolation, Durability)

**atomičnost** = Buď jsou provedeny správně všechny operace nebo žádná, <br />
**konzistence** = Izolované zpracování transakcí zachová databázi konzistentní (Suma hodnot účtů A a B je zachována), <br />
**izolace** = Nezávislé transakce se provádí po sobě, ne zároveň, \
**trvanlivost** = Změny po úspěšném provedení jsou zachovány i v případě chyby systému <br />

## Status transakce
-   aktivní
-   částečně committed = všechno vykonáno
-   failed
-   zrušená
-   committed

**rozvrhy** = posloupnost instrukcí, které určují pořadím, jakým jsou instrukce vykonávány

**serializovatelnost** -- jaké procesy mohou běžet současně, aniž by porušily ACID, proces je konfliktně serializovatelný právě tehdy, když je precendenční graf acyklický

zotavitelnost rozvrhu = Čte-li Tj data zapsaná transakcí Ti, musí být operace `commit` nejdříve od Ti a poté od Tj

kaskádovému zpětnému zotavení = Selže-li transakce Ti, musí selhat i transakce na ní závislé -- **nežádoucí**

bezkaskádový rozvrh = pro každé transakce Ti a Tj, kde Tj čte data zapsaná Ti, se `commit` operace Ti nachází před čtením Tj

## Úrovně konzistence v SQL-92
-   serializovatelné
-   repeatable read
-   read commited
-   read uncommited
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE4MTMzNjYxNDgsLTE1MjQ2NDQ3MDksLT
E3MjE1MzgxOTUsLTc2ODEzNzMsLTI2OTAwMjU3OSwtMTYxOTY4
MzAzNiwtMTUyOTQ5MzUwNywxMjQ1OTQ2MTQxLC03OTk0MzIzMj
BdfQ==
-->