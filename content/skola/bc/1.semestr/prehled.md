+++
title = "Prehled"
date = 2020-10-05
+++

# IB000 - Matematické základy programování
* [Předmětové aktuality](https://is.muni.cz/auth/df/aktuib000/)
* [Učebnice](https://is.muni.cz/auth/el/fi/podzim2020/IB000/um/UInf-text20.pdf?predmet=1324037;qurl=%2Fel%2Ffi%2Fpodzim2020%2FIB000%2Findex.qwarp)
* [Termíny a pokyny testů](https://is.muni.cz/auth/df/aktuib000/)

## Osnova předmětu
1. Základní formalismy matematiky
2. Matematická logika a důkazy
3. Množiny a množinové operace
4. Techniky matematických důkazů
5. Rekurze, Strukturální indukce
6. Relace a jejich vlastnosti
7. Ekvivalence, Uspořádané množiny
8. Skládání relací a funkcí
9. Pojem grafu
10. Stromy a kostry grafů
11. Formalizace a důkazy pro algoritmy
12. Nekonečné množiny a zastavení algoritmu
## Testy + zkouška
### Semestrální průběžné
* každý týden test (= 1 bod)
  * **max. 10** (12. lekcí)
  * **min. 7 bodů**
* **3x domácí online test** = 8 bodů
* **3x prioritní**
	* **semestrální test** = 12 bodů
	* druhý **semestrální test** = 16 bodů
	* **domácí písemný úkol** = 16 bodů
* mimořádné bonusy 

<!-- markdown cheats -->

Hodnocení:
* **požadován zisk aspoň 14 prioritních bodů** z testů plus domácího úkolu
&& **požadován zisk aspoň 7 bodů** v celkovém součtu testíků přípravy na cvičení
* semestrální výsledek = **součet tří lepších** nezáporných z uvedených sedmi (testíky jsou jako jedna)

### Závěrečné (zkouškové)
* Závěrečná povinná **zkouška testem na počítači** na 36 bodů. Požadavkem pro úspěšné složení zkoušky je zisk **aspoň 51 bodů v součtu** s celkovým semestrálním výsledkem
* -> (nepovinná zkouška) **písemná zkouška** za až 30 bodů
* -> Celkový součet pak určí známku E-A.
* S povinným ziskem prioritních bodů pak k udělení zápočtu stačí dosáhnout semestrálního výsledku **aspoň 26 bodů**.
* -Zkouškové období se už blíží (i když je opožděné proti běžným zvyklostem) a je na čase se začít zabývat praktickými otázkami provedení zkoušek...

Tento semestr i zkoušky budou muset být převážně vzdáleně (není ale vyloučeno dělat prezenční v malých počtech!). Proto se připravte na to, že budete mít FIXNÍ tři termíny zkoušek - řádný a dva opravné. Nebudete si moci nic volit a prostě v určený den a hodinu budete všichni řešit zkoušku IB000. (Pokud vám to přijde příliš přísné na obvyklou volnost FI v minulosti, tak vězte, že u "sousedů" na FIT VUT měli takto fixní termíny všech velkých zkoušek už před koronou!) Samozřejmě si to my na FI zkoordinujeme, aby vám nekolidovalo více takových fixních zkoušek krátce za sebou. Tím pádem si také opět, jako u semestrálních testů, budete muset zajistit spolehlivé i záložní spojení.  
Zkoušky budou jinak vypadat jako v minulých letech, až na ne-prezenční přítomnost budete řešit na počítači stejný druh zkouškového odpovědníku vycházejícího převážně z procvičovacích odpovědníků lekcí. V každém bude 6 příkladů po 6 bodů v úrovních klasicky 2x lehké, 2x střední, 1x těžký, 1x zabiják. Následná volitelná písemná zkouška na důkazy pak bude odevzdávaná z daných zadání podobně, jako byl domácí úkol, jen v krátkém časovém limitu. Termín volitelné písemné zkoušky bude vyhlášen zvlášť od té počítačové s odpovědníkem.

# IB015 - Neimperativní programovaní
* [sbírka úloh](https://is.muni.cz/auth/el/fi/podzim2020/IB015/um/seminars/exercises/student.pdf?predmet=1324038)
## Osnova předmětu
## Testy + zkouška
*   [00,10) bodů = známka F  
*   [10,12) bodů = známka E  
*   [12,14) bodů = známka D  
*   [14,17) bodů = známka C
*   [17,20) bodů = známka B
*   [20,25] bodů = známka A

<!-- markdown cheats -->

* 12 domácích úkolů = celkem 15 bodů
	* na úlohy bude jeden týden
	* https://is.muni.cz/auth/el/fi/podzim2020/IB015/index.qwarp?prejit=5521811

<!-- markdown cheats -->

* závěrečná písemka
	1. povinný test minimálních znalostí, bude tvořen úkoly/příklady, o nichž se předpokládá, že je úspěšný absolvent kurzu vyřeší zcela správně (bude prominuta nejvýše jedna drobná chyba) (Hodnocení: **Splněno/Nesplněno**) (v případě 12 bodů z DÚ, promine se jedno F)
	2. příklady = **max. 10 bodů** (pouze funkc. paradigm a Haskell)
* požadavky splnění:
	* test minimálních znalostí hodnocen jako "Splněno",
	* za řešení domácích úloh získáno alespoň 8 bodů.

# IB111 - Základy programování
* [official web](https://www.fi.muni.cz/IB111/)
* [sbírka](https://is.muni.cz/auth/el/fi/podzim2020/IB111/um/ib111.seminar.pdf)
## Osnova předmětu
| blok | Téma |
|:----:|------|
| 1 | 1. if, cykly, proměnné, funkce |
|   | 2. funkce, typy, ladění |
|   | 3. seznamy, n-tice, náhodnost |
|   | 4. řetězce, I/O (print) |
| 2 | 5. typy, asserty, korektnost |
|   | 6. ADT, list, dict, set, ... |
|   | 7. paměť, objekty, linked list |
|   | 8. složitost, hledání, řazení |
| 3 | 9. rekurze 1 |
|   | 10. rekurze 2 |
|   | 11. interakce s prostředím |
|   | 12. interpret mini-pythonu |
| - | 13. bonusy, opakování |

## Testy + zkouška
* Získání zápočtu (X):
	* v každém bloku **odevzdat 3 ze 4** příprav do cvičení,
	* získat **2 body za funkčnost** každého z:
		* **5 domácích úloh** a **vnitrosemestrálního testu**, nebo,
		* **6 domácích úloh**,
	* získat **36 tvrdých bodů**.
* (F):
	* uspět v pass/fail části zkoušky,
	* získat aspoň 18 bodů za funkčnost programovací zkoušky,
	* získat v součtu (semestr + zkouška) aspoň 60 tvrdých bodů.

<!-- markdown cheats -->

* V semestru bude zadáno **6 domácích úloh**, které tvoří významnou část hodnocení předmětu. Na úspěšné odevzdání každé domácí úlohy budete mít **8 pokusů** rozložených do **dvou týdnů**.

<!-- markdown cheats -->

* Vnitrosemetrálka 
	* 3. listopadu od 12:00 do 14:00 (max. 6 tvrdých bodů + max 2. měkké body) 

<!-- markdown cheats -->

* Zkouška
	* teoretická část (**pass/fail**)
	* praktická část 

# PB151 - Výpočetní systémy
* [notes](./PB154/_index.md)
## Osnova předmětu
## Testy + zkouška
* zkouška - upřesnění 3 týdny před zkouškou
	
### průběžná:
* 4 milníky - nesplnění = X
	* po probrání každé ze 4 kapitol
	* Odpovědníky - potřeba min. 90 bodů
	* Laboratoř [(link)](https://is.muni.cz/auth/edutools/brandejs/compsyslab)
### finální: 
* #todo

# PB154 - Základy databázových systémů
* [notes](@/skola/bc/1.semestr/pb154-zaklady-databazovych-systemu/uvod.md)

Na vypracování zápočtového testu máte 40 minut. Zápočtový test se skládá z 8 otázek. Každá otázka má 4 možné odpovědi, z nichž právě jedna je správná. Otázky je nutné řešit postupně, k již _zodpovězené otázce se nelze vracet_. Za správnou odpověď jsou +3 body, za špatnou je -1 bod a za žádnou odpověď je 0 bodů. Pro udělení zápočtu je třeba získat 10 bodů. Bodové hodnocení ze zápočtového testu (tedy minimálně 10, maximálně 24 bodů) se přičítá k výsledku zkouškové písemky.
## Testy + zkouška
* **zkouška**
	* zápočtový test (min. 10 bodů)
		* 40 min, 8 ot., celk. 24 bodů
	* zkoušková písemka (součet zápočtový test + zkouška min. 35 bodů = E)
		* 16 ot.,  
		* A-62, B-55, C-48, D-41, E-35

# PV170 - Konstrukce digitálních systémů
## Osnova předmětu
## Testy + zkouška
* v průběhu: 2 až 3 písemné testy (s využitím odevzdáváren v ISu)
* zkouška: písemné (s úkoly 70) + ústní (max. 30)

# PB170 - Seminar on digital system design
## Osnova předmětu
1. Binární, osmičková, desítková, šestnáctková soustava, převody mezi nimi,  
proč jsou pro nás důležité všechny, kódování čísel v digitální elektronice  
(doplňkový, inverzní, BCD kód, floating point, little endian, big endian,  
vícebytové kódování čísel, ASCII kódování písmen, Unicode)  
2. Booleova logika, logické funkce, logické obvody (hradla), lehce o jejich  
fyzikálních limitech a funkčnosti, návrh logických obvodů  
3. pravdivostní tabulka, Karnaughova mapa, minimalizace logické funkce, sestavení  
funkce pouze pomocí NAND nebo NOR, normální formy  
4. navrhování logických obvodů na simulátoru (ideálně i něco o navrhování  
reálných obvodů), VHDL, Verilog  
5. Klopné obvody, paměť, registr, sekvenční obvody, Mealyho a Moorův automat  
6. práce na závěrečném projektu do konce semestru  
Bonus: Jak se navrhují, vyvíjí a vyrábí reálné čipy
## Testy + zkouška
* závěrečný projekt
<!--stackedit_data:
eyJoaXN0b3J5IjpbMzI4MjgwNzksLTE0MTU0NjAyMSwyMTA3Mj
I1NTgxLDQ1NjQ1NjU1MywtNDAzODg2NzU5XX0=
-->