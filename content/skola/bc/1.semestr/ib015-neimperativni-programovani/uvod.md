+++
title = "01 - Úvod"
date = 2020-10-05
+++


# IB015
* korektnost
* elegance
* efektivita
<!-- markdown cheats -->

* proměnné jsou neměnné
* definice fce 
`název :: typ_vstupu -> typ_vystupu`
`název :: typ_vstupu -> typ_vstupu -> typ_vstupu -> typ_vystupu`
* skládání fcí `.` (,,čteme po")
	`(f1 . f2) x = f1 ( f2 x )`
* lokální definice
	`let ... in`
	```haskell
	let fcube x = x * x * x in fcube 12
	let fcube x = x * x * x
	```
* Typy
	* `Integer` = libovolně velká čísla
	* `Int` = celá čísla do velikosti slova procesoru
	* `Rational` = racionální čísla
	* ...
* Větvění výpočtu
	* víceřádkové definice
	```haskell
	jedna_nebo_dva :: Interger -> Bool
	jedna_nebo_dva 1 = True
	jedna_nebo_dva 2 = True
	jedna_nebo_dva _ = False -- Vždy uspěje
	```
		* prochází od shora dolů, poté uspěje na podtržítku
	* podmíněný výraz `if podmínka then výraz1 else výraz2`
<!-- markdown cheats -->

* Zápis binárních funkcí
	* Infixový zápis bin. fcí: `3+4`, `4*5`
	* Prefixový zápis bin. fcí: `(+) 3 4`, `(*) 4 5`
	```haskell
	min 5 7
	-- Při použití infoxového zápisu musíme obalit v ``
	5 `min` 7 
	```

<!--stackedit_data:
eyJoaXN0b3J5IjpbNjcxOTExOTc1LDU4MjUwODE0NywxNDUwNj
kzNzY2XX0=
-->