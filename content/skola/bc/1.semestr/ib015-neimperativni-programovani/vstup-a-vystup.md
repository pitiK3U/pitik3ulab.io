+++
title = "08 - Vstup a výstup"
date = 2020-11-24
+++

# Vstup a výstup
- samostatně spustitelný program = **posloupnost akcí**, při které odchází k interakci programu s okolním světem
Typování
- Unární typový konstruktor `IO a`
- Jedna jediná hodnota, a to **vstup-výstupní akce**.
- Tato hodnota nemá textovou reprezentaci.

Výstupní akce
- Výstupní akce mají typ `IO ()`
```haskell
putStrLn "Ahoj!" :: IO ()
```
Vstupní akce
- Vstupní akce mají typ `IO a`, která typová proměnná a nabývá hodnoty (typu) podle typu objektu, který vstupuje.
```haskell
getLine :: IO String
```

## Základní funkce pro výstup
- `putChar :: Char -> IO ()` = Zapíše znak na stdout
- `putStr :: String -> IO ()` = Zapíše řetězec na stdout
- `putStrLn :: String -> IO ()` = to co `putStr` a přidá znak konce řádku
- `print :: Show a => a -> IO ()` = vypíše jakoukoli hodnotu tisknutelného typu

## Základní funkce pro vstup
- `getChar :: IO Char` = Načte znak ze stdin
- `getLine :: IO String` = Načte řádek ze stdin
- `getContents :: IO String` = Čte líně veškerý obsah ze stdin jako řetězec
- `interact :: (String -> String) -> IO ()`
	- Argumentem je funkce, která zpracovává řetězec a vrací řetězec
	- Veškerý obsah stdin je předán této fci a vytisknut na stdout.

## Refereční transparentnost
- Daný výraz se vždy vyhodnotí na stejnou hodnotu, bez ohled na na okolí (kontext), ve kterém je použit.
- Haskell je referečně transparentní.

### Dopady na vstup-výstupní chování
- **Nelze napsat program, který by zpracoval vstup uživatele a vyhodnotil se podle zadaného vstupu na různé hodnoty.**
- Lze napsat program, který zpracuje vstup a podle vstupu vypíše na výstup různé výsledky.
- Hodnoty předávané skrze vstup-výstupní akce nesouvisí s hodnotou výrazu, který tuto vstup-výstupní akci realizuje.

## Vnitřní výsledek a vedlejší efekt
- Načtený řetězec se uchová jako tzv. **vnitřní výsledek** provedení této vstupní akce.
- Skutečné načtení řetězce a zapamatování si vnitřního výsledku je realizováno jako **vedlejší efekt** vyhodnocení výrazu `getLine`.

## Operátor >>=
### Přístup k hodnotě vnitřního výsledku
- Pomocí binárního operátoru `>>=`.
- Vy výrazu `f >>= g` funguje operátor `>>=` tak, že vezme vnitřní výsledek vstupní akce `f` a na tento aplikuje unární funkci `g`, **jejímž výsledkem je ovšem vstup-výstupní akce**.
```haskell
f :: IO a
g :: a -> IO b
f >>= g :: IO b
```

### Operátor >>=
```haskell
(>>=) :: IO a -> ( a -> IO b ) -> IO b

-- ekvivalentní zápis
getLine >>= putStr
getLine >>= (\x -> putStr x)
```

**"Jakmile jste v `IO`, nelze utéct."** (Jakmile jste v krabici, zůstanete v krabici.)

## Funkce `return`
### Jako vstupní akce
- Prázdná akce, jejíž provedení má cíl pouze naplnit hodnotu vnitřního výsledku.
```haskell
return :: a -> IO a
return ['A', 'h', 'o', 'j'] :: IO string
```

### Jako výstupní akce
- Funkce `return` může sloužit i jako výstupní akce, která nemá žádný efekt.
```haskell
return :: a -> IO a
return () :: IO ()
```

### Funkce `pure`
- Alternativa k funkci `return`
- Použití funkce `return` je vzhledem ke kontextu známého z imperativních programovacích funkcí částečně matoucí.

## Řazení akcí, operátor `>>`
- Binární operátor, který řadí vstup-výstupní akce.
- Zapomíná/ničí hodnotu vnitřního výsledku.
- Výraz má hodnoty poslední (druhé) vstup-výstupní akce.
```haskell
(>>) :: IO a -> IO b -> IO b
```

## Funkce `sequence`
- Máme-li seznam vstup-výstupních akcí **stejného typu**, lze funkce z tohoto seznamenu spustit a provést jako dávku.
```haskell
sequence :: [IO a] -> IO [a]
sequence [] = return []
sequence (a:s) = do x<-a
                    t <- sequence s
                    return (x:t)
```
```haskell
-- Použití:
sequence [ putStr "Ahoj", putStr " ", putStr "světe!" ]
sequence [ getLine, getLine, getLine ] >>= print
```

## Funkce `mapM`
- Aplikuje unární vstup-výstupní akci na seznam hodnot a vzniklý **seznam vstup-výstupních akcí provede**.
```haskell
mapM :: (a -> IO b) -> [a] -> IO [b]
mapM f = sequence . map f
```

## Jednoduchá práce se soubory
- `type FilePath = String`
- `readFile :: FilePath -> IO String` = líně načte obsah souboru jako řetězec
- `writeFile :: FilePath -> String -> IO ()` = zapíše řetězec do daného souboru (existující obsah smaže).
- `appendFile :: FilePath -> String -> IO ()` = Připíše řetězec do daného souboru.

## `do` notace
- Syntaktická konstrukce `do` slouží k alternativnímu zápisu s operátory `>>=` a `>>`.
```haskell
putStr "vstup?"  >>
getLine          >>= \ x ->
putStr "výstup?" >>
getLine          >>= \ y ->
readFile x       >>= \ z ->
writeFile y (map toUpper z)

-- ekvivalentně pomocí do
do putStr "vstup?"
  x <- getLine
  putStr "výstup?"
  y <- getLine
  z <- readFile x
  writeFile y (map toUpper z)
```

## Další IO funkce
- Jsou definovány v tzv. **modulech**. `System.IO`, `System.Directory`, `System.Time`

## Monáda a typ IO
- typová třída `Monad` je zobecněním typu `IO`
```haskell
(>>) :: Monad m => m a -> m b -> m b
```

# Moduly
- Sadu funkcí, typů a typových tříd, které mají něco společného.
- Program v Haskellu je tvořen hlavním modulem `Mian`, která následně importuje a používá funkce z jiných modulů.
- `Prelude` je implicitně zavedený modul, který obsahuje definice funkcí, které jsme doposud běžně používali.
- Moduly umožňují **strukturovat kód projektu**, oddělit a případně později znovu používat ucelené části kódu.

## Použití modulu
- Před použitím funkcí a typů z modulu je třeba explicitně požádat o zavedení tohoto modulu, tzv. **importovat modul**. `import`
- Lze importovat pouze vybrané, nebo skrýt některé funkce modulu.
- Vynutit při použití funkce povinnou identifikaci modulu, v podobě `jmeno_modulu.jmeno_funkce` (kvalifikace).
- Přejmenovat kvalifikaci.

`import qualified Data.Char as C hiding (toUpper)`
- Kvalifikace funkcí z modulů zavede nový význam znaku tečka. `C.toUpper.head`

## Obecná definice
```haskell
[module Jmeno [(export1, ..., exportn)] where ]
[ import M1[spec1] ]
[ ... ]
[ import Mm[specm] ]
[ globalni_deklarace ]
```
- Není-li uvedena hlavička, doplní se `module Main (main) where`.
- Není-li importován `Prelude`, automaticky se doplní.

# Kompilace programu
- Přeložený kód je rychlejší než interpretovaný.
- Překlad pomocí `ghc`.
- Musí obsahovat funkci `main :: IO ()`. Je automaticky spuštěna pod zavedení programu do paměti.
```
hello.hs // Zdrojový kód s funkcí `main`
ghc hello.hs -o hello // Překlad
hello . // Spustitelný soubor
hello.hi a hello.o // Pomocné soubory pro zrychlení překladu více souborů
```

## Funkce `show` a `read`
Změna typu
- Vstup/výstup je obvykle realizován v objektech typu `String`.
- Převést objekty na řetězce a zpět je možné pomocí funkcí"
```haskell
show :: Show a => a -> String
read :: Read a => String -> a
```
- Asymetrické použití, při použití funkce `read` **musíme uvést typ**, do kterého chceme transformovat.
```haskell
show 123 ~>* "123"
read "123" ~>* ERROR
(read "123")::Int ~>* 123
```

## Parsování vstupu
- Převedou se pouze 1005 správně strukturované řetězce.
```haskell
(read "[1,2,3]")::[Char] ~>* ERROR
(read "[1,2,3]")::[Float] ~>* [1.0,2.0,3.0]
```

```haskell
main = do putStrLn "Napis cele cislo:"
          x <- getLine
          print (1 + read x::Int)
```

# Syntactic sugar
## `case`
- Více řádkovou definice lze ekvivalentně přepsat s využitím `case`.
```haskell
case expression of
    pattern1 -> expression1
    ...
    patternn -> expressionn
```
- Výrazy na pravých stranách musí být stejného typu.

## Strážené definice funkcí
- Stráže je výraz typu `Bool` přidružený k definičnímu přiřazení.
- Při výpočtu bude tato definice funkce realizována, pouze pokud bude asociovaný výraz vyhodcnoecn na `True`.
```haskell
function args
   | guard1 = expression1
   ...
   | guardn = expressionn
   | otherwise = default expression 
```
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE5Njc1MDY0LDc3MjUwMDc1MCwtMzc0OD
Y3OTY4LC0xMzI5ODgyMzIyLDc0OTI5ODE5NiwxNDQwMjgzNTEy
LC0xNTQ5MzUwNDQzLDIxNDM4NDI5MTZdfQ==
-->