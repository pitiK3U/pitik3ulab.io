+++
title = "02 - Hodnoty a typy"
date = 2020-10-13
+++

# Programování a data
- programování se zabývá zpracováváním dat
- důležité vědět jak jsou data uložena a jak s nimi pracovat

Dekompozice dat
- rozdělení dat do základních datových elementů

Základní datové elementy
- čísla
- znaky
- pravdivostní hodnoty

Základní způsoby kompozice dat
- uspořádaná n-tice
- seznamy

### Uspořádaná n-tice (Tuple)
- pevně daný počet hodnot v pevně daném pořadí (prvek kartézského součinu nosných množin.)
- `(11, "březen", 1977)`, `("xbarnat", "majen10cm")`
- použít v případě: 
	- **Počet prvků n-tice je znám předem.** (compile-time)
	- Počet prvků v n-tici je malý

### Seznamy (Array)
- posloupnost hodnot stejného typu
- posloupnost - prázdná, konečná i nekonečná
- `[]`, `[12,43,-3,15,29]`, `[1,2,...]`, `[("Fero", 12), ("Nero", 7), ("Pero",5)]`
- použít v případě:
	- **Data vznikají nebo se zpracovávají postupně.**
	- Počet prvků není předem znám.
	- Počet prvků je znám předem a je velmi velký. (nehodilo by se použití n-tice)

# Hodnoty a typy
## Typ
- Označení množiny všech hodnot dané kvality.
- Pomáhá nám  pochopit, jak jednotlivé funkce fungují a jak je použít.
- Každá hodnota, nebo výraz má svůj typ.
- Definice signatury funkcí.
- Typy se komponují stejně jako data.

Základní datové typy
- `Int` = malé celé číslo (32/64 bit)
- `Integer` = libovolně velké celé číslo (záleží na velikosti dostupné paměti)
- `Float` = reálná čísla s plovoucí čárkou
- `Char` = jeden znak
- `Bool` = pravdivostní hodnota

Složené typy
- Uspořádané n-tice" `(Bool,Int)`
- Seznamy: `[Int]`, `[Char]` == `String`, `[[Char]]`

Funkcionální typy
- `Integer -> Bool`, `Float -> Float -> Float`
- **Arita funkce** označuje počet parametrů.
- konstanty (0), unární (1), binární, ternární, ... funkce.
- Typ výrazu lze odvodit z typu použité funkce bez **nutnosti výpočtu**.

Polymorfní typy
- typ není jednoznačně daný
- Místo konkrétního typu se použije **typová proměnná**
- **Typová proměnná zastupuje i složené typy.**
- **Specializace** typové proměnné. = Za typovou proměnnou se při aplikaci dosadí daný typ parametru. 

Typové třídy
- Některé fce nevyžadují konkrétní typ, ale nemohou použít ani libovolný typ. Je potřeba specializace typové proměnné omezit na vybranou podtřídu typů.
- Například algebraické funkce.
Základní typové třídy
- `Integral` - celočíselné
- `Num` - numerické
- `Ord` - uspořadatelné
- `Eq` - porovnatelné na rovnost

```haskell
odd :: Integral a => a -> Bool
(+) :: Num a => a -> a -> a
``` 

Hodnotové konstruktory
- funkce, který vytvářejí hodnoty nějakého typu
- `(,, ... ,)` - hodnotový konstruktor uspořádané n-tice
- `(,)` - hodnotový konstruktor uspořádané dvojice
```haskell
(,) :: a -> b -> (a,b)
(,) x y = (x,y)
```
- Projekce
```haskell
fst :: (a,b) -> a
fst (x,y) = x

snd :: (a,b) -> b
snd (x,y) = y
```

Hodnotové konstruktory seznamů
- Prázdný seznam: `[] :: [a]`
- Operátor připojení prvku na začátku seznamu: `(:)`
```haskell
(:) :: a -> [a] -> [a]
```
Spojení seznamů
- Seznamy musí být **stejného typu**: `(++)`
```haskell
(++) :: [a] -> [a] -> [a]
```

Hodnotové konstruktory ve víceřádkových definicích
- Fungují jako vzory na levých stranách definice.
- Mapují se vždy na **nejvnějšnější výskyt

použití symbolu `@`
- Pokud vzor je korektně vytvořený datový vzor, pak zápisem `jmeno@vzor` získáme proměnnou `jmeno`, která bude po úspěšném použití vzoru odkazovat na celý mapovaný obsah.
```haskell
f (a@(x:y)) = x:a++y
f [1,2,3]   [1,1,2,3,2,3]
```
<!--stackedit_data:
eyJoaXN0b3J5IjpbMjkxMDk2NjY4LDE1OTIwNTA0MjgsLTEzOD
Q4MjQwODUsOTUwNDM2NjkzXX0=
-->