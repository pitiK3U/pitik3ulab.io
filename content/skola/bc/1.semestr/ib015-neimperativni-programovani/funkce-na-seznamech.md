+++
title = "03 - Funkce na seznamech"
date = 2020-10-20
+++

# Funkce na seznamech
- `head :: [a] -> a` = vrací první prvek seznamu
- `tail :: [a] -> a` = vrací seznam bez prvního prvku
- `last :: [a] -> a` = vrací poslední prvek seznamu
- `init :: [a] -> a` = vrací seznam bez posledního

<!-- markdown cheats -->
- `length :: [a] -> Int` = vrací počet prvků na nejvyšší úrovni
- `take :: Int -> [a] -> [a]` = vrátí prvních n prvků seznamu jako seznam
- `drop :: Int -> [a] -> [a]` = vrátí původní seznam bez prvních n prvků
- `(!!) :: [a] -> Int -> a` = vrátí prvek na dané pozici (začíná 0)

<!-- markdown cheats -->
- `reverse :: [a] -> [a]` = vrátí seznam prvků v opačném pořadí

<!-- markdown cheats -->
- `minimum :: Ord a => [a] -> a`
- `maximum :: Ord a => [a] -> a`
- `sum :: Num a => [a] -> a`
- `product :: Num a => [a] -> a` = součin
- `or :: [Bool] -> Bool`
- `and :: [Bool] -> Bool`

<!-- markdown cheats -->
> !! Tyto funkce nefungují na seznamech:
> - `min :: Ord a => a -> a -> a`
> - `max :: Ord a => a -> a -> a`
> - `(&&) :: Bool -> Bool -> Bool`
> - `(||) :: Bool -> Bool -> Bool`
 
 <!-- markdown cheats -->
 - `map :: (a -> b) -> [a] -> [b]` = aplikace funkce na prvky v seznamu
 - `filter :: (a -> Bool) -> [a] -> [a]` = vrací výběr prvků seznamu podle dané podmínky

 <!-- markdown cheats -->
- `takeWhile :: (a -> Bool) -> [a] -> [a]`
- `dropWhile :: (a -> Bool) -> [a] -> [a]`

 <!-- markdown cheats -->
- `concat :: [[a]] -> [a]` - spojení seznamu seznamů do jednoho seznamu
- `zip :: [a] -> [b] -> [(a,b)]` - spojení dvou seznamu do seznamu dvojic (výsledná délka seznamu je délka kratšího seznamu
- `zipWith :: (a -> b -> c) -> [a] -> [b] -> [c]`  - jako `zip`, ale pomocí funkce
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE4NTM4MzM5OSwtNTM2OTA5NTM1LDE5Mj
gyODE4NzMsLTEyMDgwMjM5MywtMTg3OTM4NzUyMywtMzA3Njcx
MzIzLC03ODU5MjExNTMsLTUzMDI1ODM5NiwtMTAxMjYxODgzOF
19
-->