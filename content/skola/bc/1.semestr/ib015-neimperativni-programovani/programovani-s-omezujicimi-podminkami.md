+++
title = "11 - Programování s omezujícími podmínkami"
date = 2021-01-05
+++

# Constraint programming
- Obecné neimperativní programovací paradigma.
- V množině možných řešení problému je hledané řešení popsáno pouze omezujícími podmínkami, které musí splňovat.

## Obecný postup řešení a domény proměnných
### Různé instance paradigmatu
- Podle typu proměnných, vystupujících v popisu problému.
- Pravdivostní hodnoty, Celočíselné hodnoty, Konečné množiny, Doména lineárních funkcí, ...

### Postup řešení úloh
- Myšlenka
	- Modelování problému v dané doméně.
- Program
	- Specifikace proměnných a jejich rozsahů.
	- Specifikace omezujících podmínek.
	- Vymezení cíle.
- Výpočet
	- Zjednodušení zadání, propagace omezení.
	- Systematické procházení možných valuací a hledání vyhovujícího řešení.

## Hostitelské jazyky
- Obvykle součástí nějakého programovacího jazyka/systému.
- Constraint Logic Programming (CLP)
- clpdf v Prologu - CLP over Finite Domains
	- Výrazy jsou v celočíselné doméně
	- Porovnávání se přeřazuje '#'
	- Určení, které proměnné patří do jakých domén
	- Určení hodnot domén - počet, horní a dolní hranice, sjednocení domén
	- 
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTIyODAwMTk0LC0xNzAyNzc5MzI1LDEyMT
Q2NTQ5MTldfQ==
-->