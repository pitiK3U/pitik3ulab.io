+++
title = "09 - Prohledávání a prořezávání"
date = 2020-12-01
+++

# Prohledávání a prořezávání
## Problém n dam
- https://en.wikipedia.org/wiki/Eight_queens_puzzle
- Jak rozestavit šachové dámy na šachovnici n × n tak aby se nemohly vzít?
- Řešení budeme kódovat seznamy čísel a to tak, že čísla v seznamu určují pozice dam v odpovídajících sloupcích.
### Postupné rozšiřování šachovnice
- Zavedeme funkci `da m n :: Int -> Int -> [[Int]]`, která bude počítat všechna řešení pro obdélníkovou matice s `m` sloupky a `n` řádky (m < n).
- Nová funkce bude řešení počítat rekurzivně, a to s využitím řešení pro šachovnici s (m-1) sloupky a `n` řádky.
- Šachovnice s nula sloupky, má jedno triviální řešení: `[]`, tj. `da 0 n = [[]]`

### Pomocná funkce
- Rozhodnout, které pozice při rozšíření o jeden sloupek jsou při stávajícím rozložení dam nevyhovující.
```haskell
hrozba :: [Int] -> [Int]
hrozba p = p ++ zipWith (+) p [1..] ++ zipWith (-) p [1..]
```

### Řešení
```haskell
type Reseni = [Int]

damy :: Int -> [Reseni]
damy n = da n n

da :: Int -> Int -> [Reseni]
da 0 _ = [[]]
da m n = [ k:p | p <- da (m-1) n, k <- [1..n], nh k p] -- nh = neni hrozba
	 where nh k p = k `notElem` ( p
				      ++ zipWith (+) p [1..]
				      ++ zipWith (-) p [1..]
```

### Backtracking, prořezávání
- **Backtracking** - rekurzivní generování všech možných řešení.
- **Prořezávání** - časná eliminace neplatných řešení (zde realizováno funkcí `nh k p`)

# Pole
- Tabulka adresovatelných míst pro uložení dat.
- Klíčovou vlastností je časově konstantní operace pro přístup k hodnotě na zadané adrese.
- Důležitá datová struktura v imperativním světě.
- `import Data.Array`

## Typový konstruktor `Array`
- Binární typový konstruktor: `Array :: *->*->*`
- `Array I A` je typ všech polí, která jsou indexovaná hodnotami typu `I` a obsahují prvky `A`.
- Typ použitý pro indexaci musí být instancí typové třídy `Ix`.
```haskell
array (1,4) [(1,'a'),(2,'b'),(3,'a'),(4,'b')] :: (Num i, Ix i) => Array i Char
```

## Indexace
- Instance typové třídy `Ix` umožňují efektivní implementaci pole.
```haskell
class (Ord) => Ix a where
    range :: (a,a) -> [a]
    index :: (a,a) -> a -> Int
    inRange :: (a,a) -> a -> Bool
```
- Uspořádaná dvojice indexů tvoří meze pole.

## Přístup k prvkům pole
```haskell
(!) :: Ix i => Array i e -> i -> e

array (5,6) [(5,"ANO!"),(6,"NE1")] ! 5 ~>* "ANO!"
```

## Aktualizace hodnot v poli
- Operace `//` zkopíruje původní pole a při kopírování aktualizuje uvedený seznam dvojic `(index, hodnota)`.
- --> **Není konstantní operace!!!**
```haskell
(//) :: Ix i => Array i e -> [(i,e)] -> Array i e
array (1,2) [(1,1),(2,2)] // [(1,3),(2,3)] ~>* array (1,2) [(1,3),(2,3)]
```

## Operace s poli
```haskell
-- Meze pole
bounds :: Ix i => Array i e -> (i,i)

-- Seznam indeců pole
indices :: Ix i => Array i e -> [i]

-- Převody mezi poli a seznamy
elems :: Ix i => Array i e -> [e]
listArray :: Ix i => (i,) -> [e] -> Array i e

-- Převody mezi poli a seznamy dvojic
assocs :: Ix i => Array i e -> [(i,e)]
array :: Ix i => (i,i) -> [(i,e)] -> Array i e
```

## `accumArray`
- Vytváří pole ze seznamu dvojic `(index,hodnota)` a navíc akumuluje (funkcí foldl) prvky seznamu se stejným indexem.
```haskell
accumArray :: Ix i => (e -> a -> e) -> e
		       -> (i, i) -> [(i, a)] -> Array i e
```
- Je-li akumulační funkce konstantní, pracuje v lineárním čase.
```haskell
accumArray (+) 0 (1,2) [(1,1),(1,1),(1,1)]
	~>* (1,2) [(1,3),(2,0)]
```

# Lineární řazení čísel z omezeného rozsahu
- Jsou-li čísla v seznamu z omezeného rozsahu, je možné tento seznam setřídit v **lineárním čase**.
```haskell
max = 1000

countlist :: [Int] -> Array Int Int
countlist s = accumArray (+) 0 (0,max) (zip s (repeat 1))

sortBoundedList s = concant [ replicate k x | (x,k) <- assocs (countlist s) ]
```
<!--stackedit_data:
eyJoaXN0b3J5IjpbMzI4ODM3NTUzLDEwMDY0MTU5ODYsMTAzMz
Y2MTczMCwtNzkwNDIyMjEzLDE3MTcyNzk4MywtMTMyOTg2NTI2
NiwtNjU1ODc1MTMxLC0xMTMzMTcxMTgyXX0=
-->