+++
title = "04 - Funkce"
date = 2020-10-27
+++

# Funkce vyššího řádu
- Funkce která mají funkci jako parametr nebo výsledek.
- Funkcionály
- `(.) :: (a -> b) -> (c -> a) -> c -> b`

 <!-- markdown cheats -->
- Každá funkce, které bere alespoň dva argumenty, lze chápat jako fci vyššího řádu
```haskell
(*) :: Num a => a -> a -> a
(*) :: Num a => a -> (a -> a)
```

 <!-- markdown cheats -->
- **Částečná aplikace** = vyhodnotí funkci i pro neúplný výčet argumentů
- Specializací typové proměnné mohou -> přibýt
```haskell
id :: a -> a
id x = x

id (+) :: a
id (+) :: Num a => a -> a -> a
```
 
 <!-- markdown cheats -->
Funkce `flip`
- Bere binární funkce a vrací binární funkci, akorát parametry volá opačně.
- **modifikátor funkce**
```haskell
flip :: ( a -> b -> c ) -> b -> a -> c
flip f y x = f x y
```
- **Point free** = Funkce bez formálních parametrů

 <!-- markdown cheats -->
- Zabránit částečné aplikaci místo více argumentů = n-tice argumentů. <br>
`curry` = rozdrobuje parametry, bere n-tici a vrátí jednotlivé parametry <br>
`uncurry` = bere funkci a jednotlivé parametry a zalová funkci na n-tici 

 <!-- markdown cheats -->
**Operátorová sekce** = speciální zápis binární funkce, která je částečně aplikována
```haskell
(p+) = (+) p
(+q) = flip (+) q
```

 <!-- markdown cheats -->
Kombinátory
- **bezparametrové funkce** = není použito formálních parametrů
```haskell
f x = (not.odd) x
f = (not.odd)
```

# Nepojmenované funkce (λ-funkce)
- Funkce která je definována v místě použití bez jména.
- Pochází z lambda kalkulu. Princip vytváření lambda funkcí, se říká lambda abstrakce
```haskell
\ formální parametry -> tělo funkce
map (\ x -> x*x+1) [1,2,3,4,5]
```

Aplikační operátor `$`
- Aplikuje funkci na argument --> můžeme se vyhnout složitému závorkování.
```haskell
f(g x) = ($) f (g x) = f $ (g x) = f $ g x
```
<!--stackedit_data:
eyJoaXN0b3J5IjpbMjM0ODY3MzY4LC0yOTg3MzEwMzAsLTE3MD
cxOTYxNzldfQ==
-->