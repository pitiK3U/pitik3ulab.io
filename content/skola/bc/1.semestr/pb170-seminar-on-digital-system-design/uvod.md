+++
title = "01 - Úvod"
date = 2020-10-15
+++

# 
- Pochopit, jak funguje procesor
- Simulátory:
	- Protheus (Kvalitní, ale velmi drahý)
	- circuitverse.com
	- Quartus (FPGA, VHDL, ...)
	- Crocodile technology (Drahá licence)

# Teorie
```
ASCII
' ' -> 0x20
'0' -> 0x30
'A' -> 0x41
'a' -> 0x61
```

Kladné napájení - `+` `+5V`
Záporné napájení - `-` `GND`

#### Obecný postup, jak z libovolné pravdivostní tabulky udělat libovolný obvod.
Na řádku mezi vstupy použiji **`AND`**, mezi řádky použiji **`OR`**.

##### Karnaughova mapa

# Základní poznatky
- Proud je líný == jde cestou s nejmenším napětím.
- --> Pro každou větev zapojovat rezistory, protože snad nikdy nedostaneme stejné 2 součástky a pak proud půjde pouze přes jednu větev.

- Drát, který je bez proudu, reaguje jako anténa a přijímá signály z okolí. Logická hradla tyto výkyvy může rozpoznat a tím zapříčiní nedefinované chování.
- --> Musím použít `pull-down` nebo `pull-up` rezistor.  (od 100k ohmu až po 100 ohmu)

- zesilovače/výkonové tranzistory
- Ochrana proti studentům = na výstupy použít ochranné odpory, přepólování - diody nebo tranzistory

**Klopné obvody** = paměť, dokáží si zapamatovat jeden bit

Sekvenční obvody = obvody, které mají paměť

## Sekvenční obvody
Flipflop R-S
Flipflop D = jednobitová paměť (registr)
Clk =
- NE555 = oscilátor + kondenzátor, záleží na velikosti odporu, připojení k odporu nebo potenciometru

Převod ze sériového obvodu do paralelního = několik D flipflopů za sebou se stejným clk.
Sčítač (74HC393) 

### Paměti
- Adresové piny, datové piny, Write, Read, napájení


### navrhování sekvenčních obvodů
#### Moore machine (automat)
- pouze závislý na vnitřních stavech

#### Mealy machine (automat)
- zobrazení je závislé na vstupní hodnoty a vnitřních stavech
<!--stackedit_data:
eyJoaXN0b3J5IjpbMzYwODEwNjMxLDk1ODI3MDE5NiwxNjk5ND
c3NDM2LDQxMjI2NjgyOSw5OTY4NTA4MjcsMTY0MDU5NzkwOSwt
MzY4MzgzMzg4LC0yMDgzMDU0ODEzLC0xMTY3Mzg3NzQxLC01Nj
kwMzc4MzAsNTQ2ODg2MTcyLDEwNzkzODE2MTcsMTU4MzYyMDMx
MCw2MDE5ODg3NzcsOTg5MzQ3NzE3LDQyMTI3MjQ0LDUyMDIyNT
M1NywxMDU5MDIxMjIzLDM0MjI4MjM2Nl19
-->