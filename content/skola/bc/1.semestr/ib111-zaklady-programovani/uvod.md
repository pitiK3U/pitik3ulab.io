+++
title = "IB111"
date = 2020-10-05
+++

# IB111
* [stránka](https://www.fi.muni.cz/IB111)
# Úvod
* **korektní** = dělat, co chceme
* **efektivní** = rychlé
* **čitelné** = aby ostatní prog. tomu porozuměli
---
* **Algoritmus** = postup pro vyřešení určitého typu problémů
* vlastnosti algoritmů
	* jasný vstup a výstup
	* **obecnost** = pro řešení typové úlohy ne konkrétní
	* **determinovanost**
		* každý krok je jasně a přesně definován
	* **konečnost** = algoritmus někdy skončí
	* **elementárnost** = skládá se z jednoduchých kroků
	* (efektivita)
* programování = zápis algoritmu pro počítač
	* musí být opravdu přesný

# Základy pythonu
* **REPL** (**read-eval-print loop**)
* `python -i filename.py` interaktivní načtení programu do interpretu
* **PEP8** [(link)](https://www.python.org/dev/peps/pep-0008/)
	* `snake_case`
* umocňování `**`
* celočíselné dělení `//` (NE `int(x / 10)`!!)
* `range(10) == [0, ..., 10]`
* **procedura** = fce, která má vedlejší efekty
* **čistá fce** = nemá vedlejší efekty (změna globální proměnné, I/O funkce, random, změna proměnných mimo scope funkce) 
* **predikát** = čistá fce, která vrací `True`/`False`
* **DRY** (don't repeat yourself)

Doporučení pro psaní funkcí
nejprve ujasnit **specifikaci** (vstupně/výstupní chování) 
- jaké potřebuje vstupy?
- co bude výstupem funkce?
funkce by měly být **krátké**
- jedna myšlenka
- pár úrovní zanoření

Největší společný dělitel
Euklidův algoritmus
- Jestliže `a > b`, pak platí, že `gcd(a, b) = gcd(a - b, b)`
Lepší algoritmus
- `gcd(a, b) = gcd(a mod b, b)`

list comprehension = `s = [2 * x for x in range(10)]`

**!!!Nikdy nepoužívat implicitní parametry s měnitelnými proměnnými - listy !!!**

Řetězce jsou neměnné! <br>
Formátování řetězců: pyformat.info <br>
`"{:d}".format(123)` <br>
`.split()` <br>
`.join()` <br>

## Testování
**precodnition** = Vstupní podmínka algoritmu <br>
**postconditoin** = Výstupní podmínka algoritmu

**Algoritmus je korektní, pokud pro všechny stupy splňující vstupní podmínku skončí a jeho efekt splňuje výstupní podmínku.**

- `assert` slouží pro ověření chyby programátora, **ne vstupu uživatele**!

### Typové anotace

# Datové struktury
Abstraktní datový typ (uživatelský pohled na data)
- rozhraní
- popis operací, které chceme provádět

Konkrétní datová struktura (implementační pohled na data)
- implementace
- přesný popis uložení dat v paměti
- definice funkcí pro práci s těmito daty

## ADT
- seznam
	- Jednosměrný zřetězený seznam: data + další prvek
	- Obousměrně zřetězený seznam: data + další prvek + předchozí prvek
	- Dynamické pole: Mám danou velikost, avšak můžu tuto velikost měnit.
- [zásobník](#stack)
- [fronta](#queue); prioritní fronta
- [množina](#set)
- [slovník (asociativní pole)](#hashtable)

#### Statické pole - array
- souvislý úsek paměti
- všechny prvky mají stejnou velikost
- indexování je velmi rychlé
- nedá se snadno rozšiřovat
---
v pythonu

konstatní:
- indexování `list[i]`
- délka `len(list)`
- odstranění prvku z konce `list.pop()`
- přidávání prvků na konec `seznam.append(prvek)`

Lineární:
- kopie seznamu (i části `list[x:y]`)
- hledání prvku v seznamu `in` nebo `.index`
- mazání nebo přidávání prvků uprostřed nebo na začátku
- spojování seznamů pomocí `+`
---

## Stack (Zásobník) {#stack}
- LIFO (Last In First Out)
- operace:
	- push (vložení)
	- pop (odstranění)
	- top (náhled na vrchní prvek)
	- empty

## Queue (Fronta) {#queue}
- FIFO (First In First Out)
- operace
	- enqueue (vložení)
	- dequeue (odstranění)
	- front (náhled na přední prvek)
	- empty
- použití knihovny `collections`
	- datový typ `deque` (oboustranná fronta)
	- vložení append
	- odebírání `popleft`

## Množina {#set}
- neuspořádaná kolekce dat bez vícenásobných prvků
- operace
	- insert
	- find
	- delete

## Hashtable (dictionary, map, slovník) {#hashtable}
- neuspořádaná množina dvojic (klíč, hodnota)
- klíče jsou unikání
- operace jako u množiny (insert, find, delete)
- navíc přístup k hodnotě pomocí klíče
- klíče jsou neměnné, ale hodnoty se smí měnit

# Proměnné, paměť
## Složené datový typy a objekty
### Záznamy, struktury
- datový typ složený z více položek různých typů
- typicky fixní počet

### Objekty
- často rozšíření struktur
- kombinují data funkce (metody)
- dědičnost, polymorfismus

# Vyhledávání, řazení, základy složitosti
## Vyhledávání
V obecném seznamu
- vstup: seznam čísel (objektů) + dotaz(číslo)
- výstup: odpověď `True`/`False`, jestli je číslo v seznamu
- alt. výstup: index čísla v seznamu
- časová složitost: cca délka seznamu
- `in`, `seznam.index()`

V seřazeném seznamu:
- vstup: **seřazený** seznam čísel (objektů) + dotaz(číslo)
- výstup: odpověď `True`/`False`, jestli je číslo v seznamu, případně index prvku

* rozumné řešení: **půlení intervalů** (`log N`)

### Binární vyhledávání (Binary search)
- půlení intervalu
- podíváme se na prostřední prvek seznamu
	- podle jeho hodnoty buď skončíme, nebo pokračujeme doleva nebo doprava
	- udržujeme si ,,dolní" a ,,horní" mez

## Časová složitost
- funkce, která délce vstupu přiřazuje počet kroků
- typicky nejhorší případ
- co jsou
	- základní kroky, které počítáme
	- velikost vstupu

Asymptotická časová složitost: [viz haskell (Big O notation)](@/skola/bc/1.semestr/ib015-neimperativni-programovani/akumulacni-funkce.md#casova-slozitost)

## Řazení
Select sort: nejmenší prvek na správné místo, nejmenší prvek zbytku na správné místo, ...
- best case = worst case = O(n^2)

Insert sort: seřazená část a neseřazená část, vezmu jeden prvek z neseřazené části a zařadím do seřazené části
- best case = O(n)
- worst case = O(n^2)

Bubble sort: very bad

**Quick sort**: pivot (jeden prvek) a rozdělím seznam na menší než pivot, pivot a větší než pivot
- worst case = O(n^2)
- avg case = O(n log n) (závisí na výběru pivota

**Merge sort**: rozdělíme seznam na dvě poloviny, rekurzivně seřadíme vzniklé menší seznamy, spojíme do jednoho (merge)
- worst case = O(n log n) = nejlepší, co jde

`my_list.sorted()` = modifikuje list <br>
`sorted(my_list)` = vytvoří nový list

# Rekurze
- odkaz sám na sebe
- při vymýšlení algoritmu můžeme předpokládat, že menší instanci někdo vyřeší za nás

## Pravidla pro správné použití rekurze
1. Báze = Dostatečně jednoduché vstupy musíme vyřešit přímo.
2. Rekurzivní volání musí být v nějakém smyslu jednodušší než aktuální problém.
<br>(Korekurze (corecursion) - něco jiného pro pokročilé)

## Nepřímá rekurze
- rekurzivní funkce se nemusí vola přímo, ale i skrz jinou funkci

## Rekurzivní stromeček
- nakreslit kmen
- nakreslit dva menší stromečky (pootočené)
- vrátit se zpátky

## RecursionError
- velikost zásobníku volání je nějak omezena (stack overflow)

## Vztah rekurze a iterace
- každý rekurzivní program je možno přepsat jako iterativní pomocí zásobníku

### Koncová rekurze (tail recursion)
- rekurzivní volání je to poslední, co se ve funkci stane
- lze snadno nahradit cyklem, bez použití zásobníku
- některé kompilátory/interprety toto optimalizující samy

## Fraktály
- Hánosjké věže
- Sierpińský trojúhelník

## Rekurzivní datová struktura

## Sebereplikace
 1. Program, který vypíše sám sebe
 2. List item

## Součinové typy A × B
- hodnota má 2 složky: jedna typu A, druhá typu B
- ntice, záznamy (`record`,`struct`)
## Součtové typy A + B
- hodnota je buď typu A nebo typu B
- (tagged) Union, variant
- (disjuktní) sjednocení
- `Union[X, Y]`
	- buď hodnota typu X nebo typu Y
	- možno i více typů: `Union[X, Y, Z]` 
## Backtracking
### Problém dam
- problém splnění omezení (constraint satisfaction problem)

# Interakce s prostředím
## Práce se soubory
 - otevřít
 - práce 
 - zavřít !

## Práce s bitmapovými obrázky
- knihovna: pillow

## Práce s archívy
- knihovna: zipfile

## Knihovna `sys`
- komunikace se systémem
- `sys.argv` = seznam parametrů z příkazové řádky
- `sys.stdin`, `sys.stdout`, `sys.stderr`
- `sys.exit` = ukončí program, jestli program skončil v pořádku nebo ne

## Knihovna `glob`
- vrací seznam souborů odpovídající danému POSIXovému vzoru

## Knihovna `os`
- práce se souborovým systémem

## Knihovna `time`
- `time.time` = aktuální čas `float` v sekundách od 1.1.1970 0.00 UTC

## Knihovna `datetime`

## Knihovna `tkinter`
- jednoduchý modul pro vytváření grafických uživatelských rozhraní - GUI

## Knihovna `fractions`

## Knihovna `itertools`

# Interpret jednoduchého programovacího jazyka
- source code 
- -> lexing (lexical analysis) & parsing (syntatical analysis)
- -> AST (Abstract Syntax Tree)
- -> Intermediate code
	- -> bytecode (sortof readable) -> interpret
	- -> machine code
<!--stackedit_data:
eyJoaXN0b3J5IjpbODIxNTI2MzYwLC01Mjk2OTIzMzcsNjQ0Nz
gyMjI5LC0xNDQ4ODExOTUsMTUyOTM1OTQ2NiwyMTc0NTQwNTcs
LTEwNDAzNDY3NzIsLTE3Njk3MTAwNTgsLTU4NjMwODU2MCwxNz
QxMTMxMzUzLDEwOTY4MDQ5MTYsLTE0MjMwNjEzMDAsLTcyMTY4
OTA4NywtMTUxMzA2MzgzOSwxNDAwNzUwOTI3LDIwMTA1ODk3OD
csNjAwNDAxNTczLDc3NDQ0Njk3Nyw4MTg2Nzc4NCw5ODg0Mzkz
OTRdfQ==
-->