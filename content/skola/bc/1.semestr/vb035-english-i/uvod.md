+++
title = "English I"
+++

English
 fail/pass
 no test, kredit based on our work
 several assignments - decent effort

report from IBM lecture

**Need to complete**:
1. writing several formal emails
2. giving feedback on classmates emails
3. presenting final project **11.1.**
4. IBM lecture


# Presentation
- [https://www.eapfoundation.com/speaking/presentations/language/](https://www.eapfoundation.com/speaking/presentations/language/)
- [https://hbr.org/2013/06/how-to-give-a-killer-presentation](https://hbr.org/2013/06/how-to-give-a-killer-presentation)
- sign posting language
	- transition between speakers, topics, ...
	- greeting the audience
	- giving the structure
- Focus on clear goal and message.
- Simple keywords on slide

Format
- name, title, date
- introduction
- outline
- conclusion
- sources
- Q&A - must have question

newspaper = something for my filed of interest
- frenzy = a state or period of uncontrolled excitement or wild behavior
- co-opetition = You have brutal competition, but at the same time, you have necessary cooperation.
- jeopardy = in a dangerous position
- scrutiny = careful and complete examination
- prominent = important or well known
grammer that i have difficulty with
- a/the pecking order = the order of importance in relation to one another among the members of a group
# Formal emails
- Greeting:
	- Dear Sir/Madam,
	- Dear Mr./Mrs./Ms. Smith,
	- To Whom It May Concern: (Capital letters)
- Starting email
	- Ms. if I am unaware of marital status
	- Dr., Prof., ...
	- After salutation colon or comma
- The email format
	- Can start with introduction.
	- Start with reason of writing.
	- Polite structure.
- Closing
	- "I look forward to your reply,"; 
	- "Best regards,"; "Sincerely,"; "Warm regards,"; "Kind regards,"; "Thank you,"
	- Signature 
- Vocab
	- regard
## Feedback
- information content
- structure, style
- details (vocab, grammar)
# Final project
- something more practical, part-time job, ...
- study abroad, even outside Europe
- Czechitas 
- Spring semestr - film festival, involvement
- school Labs - 
- IT startups
- final presentations should be around 10 mins

# IBM Lecture
- William Carbone
- Music, piano
- Think; dress for the job you want not the job you have
## AI
- Deep blue = chess-playing computer
- Watson ???
- Project Debater
- Augmented intelligence
	- Three levels
	- 1. Machine Learning
	- 2. Artificial Intelligence
	- 3. Cognitive Computing - not yet
- Ethical challenges = Trust, Privacy
- Human + machine
- Journey to wisdom
	- data -> information -> knowledge -> wisdom
- Technology shift
	- Automating the World -> Understanding the World
	- H-factor = how we put humans into the revolution
- Project CIMON = Crew Interactive Mobile CompanioN; AI in space
- Use the Force
- https://cognitiveclass.ai

## Quantum computing
- Simulating Quantum Systems
- AI
- Optimization / Monte Carlo
- Quantu bit = qubit is a controllable quantum object that is the unit of information
- A quantum circuit is a set of quantum gate operations on qubits and is the unit of computation
- super position
- entanglement
- Will not replace normal computing; Complementory to current structure
- Qiskit = for students; open access, open source; building quantum programs
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTExMjg1MTYyMTQsLTEyMzI2NTA1NTEsMT
E5NzI5NDk4OCw5NDYxNDE2LC0xMDI3NTIzNDc2LDI2ODgwMjk3
LC04NjUwNzU3NDksMTk5OTA4ODU5OCwzNDQzMjkxMzcsLTE3Nj
EwMDIwOTQsOTExNjgxNTYsLTg0NjQxMTE4OSwxMzgxODk1Nzk3
XX0=
-->