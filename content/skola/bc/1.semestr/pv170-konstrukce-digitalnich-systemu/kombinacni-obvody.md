+++
title = "05 - Kombinační obvody"
date = 2020-11-03
+++

# Minimalizační metody
- Víc než 8 vstupů nelze zrealizovat. Musí se obcházet minimalizací.
- především v disjunktivním tvaru
- Účel minimalizace:
	- **minimální počet operací** (počet logických členů)
	- minimalizace spojů/obvodů
	- minimalizace počtu proměnných (počet vstupů logických členů)
	- minimalizace zpoždění
	- minimalizace spotřeby obvodu

- **Implikant booleovské funkce f** = výraz ve tvaru součinového termu, který definuje funkci f1, f1 --> f, tj. funkce se skládá z implikantů
- **Přímý implikant booleovské funkce f** (primterm) = každý implikant booleovské funkce f2, neexistuje f2 --> f1, ttj. tento implikant se nedá zjednodušit.

## Minimalizační metody
- Odstraňování implikantů, které jsou v opačné podobě (**liší se v jedné proměnné**).

### Minimalizace s využitím Booleovy Algebry
Využívají se především asociativní zákon, distributivní zákon, zákon vyloučeného třetího, zákon absorpce a zákon absorpce negace, De Morganovy zákony, Shannonův teorém o rozkladu.

### Minimalizace s využitím map
Hledáme **minimalizační smyčku** - co největší oblast, kterou můžeme vyjádřit, co nejjednodušším implikantem.

### Minimalizace skupiny logických výrazů
Jestliže máme více funkcí, můžeme najít oblast, kterou mají tyto funkce společnou.

### Minimalizace Quine-McCluskey
- algebraická minimalizace => lze naprogramovat
- Mintermy vždy do skupin po stejném počtu jedniček.
- Spojuji sousední skupiny, tak abych našel implikanty, které se liší pouze na jednom místě.
- 

# Kombinační obvody
- n vstupů a m výstupů

## Syntéza
- v zadání určitý počet vstupních a výstupních log. proměnných, počet log. fcí
- sestavíme prav. tabulku, mapu nebo ..
- popis
- algebraické vyjádření fčních vztahů převedeme na tvar, vyhovuje
- nakreslíme strukturu log. sítě a optimalizace
- analýza navržené log. sítě a korekce návrhu
- konečné sestavení log. obvodu

<!-- markdown cheats -->
- **Pokud je málo 1, je vhodné použít `NAND`, naopak je-li hodně 1, je vhodné použít `NOR`.**
## Faktorizace
- zjednodušení obvodu, za cenu většího zpoždění
### Rottova mřížka - Aplikace De Morganova pravidla
- Rozdělíme obvod na část, kterou realizuji pomocí `NAND` a pomocí `NOR`.
- Pravidla:
	- 
## aritmetické obvody
## komparátory
## dekodéry a multiplexery
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE0MDU5OTgyMjMsLTY5OTc2MTQ1Niw4OD
EyMDA1MjksNzA4NjY4NjA4LC0xNzcxMDk1MTAxLC0xOTM1NzE4
OTc4LC05MzczNzYxNywtOTA5NjY4OTc0XX0=
-->