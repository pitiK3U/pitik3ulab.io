+++
title = "01 - Zobrazování čísel, číselné soustavy - kódování"
date = 2020-10-06
template = "latex-skola-page.html"
+++

# Zobrazování čísel, číselné soustavy - kódování
## Zobrazení hodnot čísel
* spojité = muselo být nekonečné
* nespojité - používáme nejčastěji
	* zobrazovací kódy
	* číselné soustavy
		* nepolyadické (např. římská číselná soustava)
		* polyadické (poziční)
			* dekadická
			* binární
			* hexadecimální
			* osmičková

Pozice číslice vyjadřuje **váhu**, uvedenou v tzv. **úplném tvaru čísla**

Vyjádřete číslo $A = 48 901,16_{10}$ v úplném tvaru:

$ A = 4 * 10^4 + 8  *  10^3 + 9 * 10^2 + 0 * 10^1 + 1 * 10^0 + 1 * 10^-1 + 6 * 10^-2 $

Obecně lze zápis libovolného čísla v jakékoliv číselné soustavě vyjádřit v následujícím tvaru:
$Z$ = základ 
$A = a[n-1]*Z^n-1 + a[n-2]*Z^n-2 + a[n-3]*Z^n-3 + ... + a[i]*Z^i + ... + a[0]*Z^0 + a[-1]*Z^-1 + ...$
$$ \sum_{i = -r}^{n-1} a_iZ^i $$

## Převod do jiné soustavy
### Do desítkové číselné soustavy
Číslo rozepíšeme do úplného tvaru a vyčíslíme:
Převeďte číslo $A = 432,2_8$ z osmičkové do desítkové soustavy.

$A = 4 * 8^2 + 3 * 8^1 + 2 * 8^0+ 2 * 8^{-1}$

$A = 4 * 64 + 3 * 8 + 2 * 1+ 2 * 0,125 = 282,25_{10}$

### Z desítkové číselné soustavy
#### Převod celé části
Dané číslo dělíme základem, na který převádíme. Zbytek po dělení udává hodnotu cifry a váhá cifry je po kolikáté dělíme - 1. Dělení opakuje do té doby, než zbytek po dělení se rovná nule.

Převeďte čísla $A = 47_{10}$ desítkové do binární soustavy.

| A  | 47 |    2   |
|----|:--:|--------|
| Q1 | 23 | a0 = 1 |
| Q2 | 11 | a1 = 1 |
| Q3 | 5  | a2 = 1 |
| Q4 | 2  | a3 = 1 |
| Q5 | 1  | a4 = 0 |
| Q6 | 0  | a5 = 1 |

$A = 47_{10} = 101111_2$

### Převod zlomkové části
Oproti převodu celé části, zde dané číslo násobíme základem, na který převádíme. Jednotlivé cifry jsou celá čísla, která po násobení dílčích zbytků dostaneme. Váha cifry je počet dělení krát - 1. 

Převeďte číslo $N = 0,3125_{10}$ desítkové do osmičkové soustavy.

| N | 0,3125 | 8 |
|---|--------|---|
| S-1 | 2,5 | a-1 = 2 |
| s-2 | 4,0 | a-2 = 4 |

$N = 0,3125_{10} = 0,24_8$

## Zobrazování reálných čísel v číslicových systémech
**rozsah zobrazitelnosti**: $Z^n - 1$
**optimální polyadická soustava**
#TODO

### Zobrazení záporných čísel
#TODO
* přirozený kód
* inverzní kód
* doplňkový kód
* modifikovaný doplňkový kód

významově nejnižší bit je v obraze čísla umístěn napravo
znaménkový bit se nachází nalevo od významově nejvyššího bitu

doplnkovy: zprava opisuji nuly, narazim na 1 opisu, zbytek inverzuji

# Formáty čísel
### pevná řádová čára
### pohyblivá řádová čárka
normalizovaný tvar = aby mělo co nejvíce platných řádů
### **BCD** 
*[BCD]: Binary Coded Decimal (binárně dekadický tvar)
Vznik = potřeba zpracovávat znaky a čísla v alfanumerickém tvaru.
**Čísla** mají ve vyšší čtveřici bitů **vždy** stejnou kombinaci.
Nejnižší čtveřici bitů je v běžném binárním tvaru s váhami **8421**.
Nabývá hodnot od $0_{10} = 0000_2$ do $9_{10} = 1001_2$.

**Komplementární kódy** = součet všech 4 vah čísel == 9; Jsou vůči ose opačné = přetočíme podle osy, tak se žádné 1 nepřekrývají. (Osa je mezi 4 a 5.)
![Tabulka 1.2]

**Neváhové kódy** jsou schopny plnit speciální požadavky jako, aby nula nebyla $0000_2$ nebo aby kód byl **cyklický**.
**Cyklicé neváhové BCD kódy** = **sousední hodnoty se liší pouze v jednom bitu** = kvůli harazdním situacím
* tento kód se používá pro kódování vnitřních stavů asynchronních sekvenčních obvodů nebo pro potřeby analog-číslo převodníků

**Reflexní kód** = souměrný kolem osy mezi čísly **4** a **5** mimo nejvyšší řád
*binární cyklický kód* = váhový kód který je cyklický
**Grayův kód** = symetrický pro daný řád, používá se pro osy

#### Převody mezi BCD a binárním kódem
hardwarove/programově
* přímým převodem
* posuny a korekce - Hornerovo schéma
##### z binární soustavy do váhové 8421 BCD soustavy pomocí programově realizovaného čítače
#TODO: simplify
Jedná se o jednoduchý, ale z hlediska času diskutabilní postup.

Převod se uskutečňuje po krocích, v nichž se opakovaně přičítá jednička do programově realizovaného
dekadického čítače a zároveň odečítá jednička od programově realizovaného binárního čítače. Počet
cyklů, potřebných pro převod je dán hodnotou binárního čísla.

Desítkový čítač reprezentuje programovatelný registr, jenž je programově rozdělen na tetrády bitů.
Pokud tetráda obsahuje číslo 9 (binárně 1001), pak se po přičtení provede dekadická korekce –
příslušná tetráda se vynuluje a přičte se jednička do vyšší tetrády.

Binární odečítací čítač lze realizovat pomocí dekrementace obsahu univerzálního registru.

V této konfiguraci převod končí v okamžiku, kdy binární čítač je vynulován.
##### Z binární do váhové 8421 BCD soustavy pomocí posuvů a korekcí (Hornerovo schéma)
Číslo posouváme doleva, pokud je v tetrádě číslo >= 5, musíme provést korekci = přičteme 3, provádíme dokud celé číslo není obsaženo v D části.

##### Z váhového 8421 BCD kódy do binární soutavy s použitím postupného násobení
 Dílčí součet čísla v binárním tvaru získáme násobením příslušné tetrády mocninou deseti podle vztahu $d_{m-1}* 10^{m-1}$. Konečnou hodnotu pak dostaneme jako výsledek součtu všech dílčích součtů, což lze vyjádřit vztahem:
 $$ B = \sum_{j=1}^{m-1} d_j * 10^j $$
V binární číselné soustavě se výpočet desetinásobku dílčího součtu nahrazuje součtem dvojnásobku a osminásobku tohoto dílčího součtu.

##### Z váhového8421 BCD kódu do binární soustavy s použitím posuvů a korekcí
Číslo posouváme doprava, pokud je v tetrádě číslo >= 5, musíme provést korekci = odečteme 3 (nebo přičteme 13, ekvivalentní úprava), provádíme dokud celé číslo není obsaženo v B části.

##### Převod obvodově realizovaným čítačem
Používá se především, je-li použit neváhový BCD kód nebo jiný než váhový 8421 BCD kód
![obrazek 1.1]





# Hazardy kombinačních obvodů
# Sekvenční obvody
## syntéza
## návrh
## čítače
## registry
## řadiče a sekvencéry
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTg2OTgxNzU4LC0xNDE1NjE0NjMzLDIxMD
IwNTc5ODYsMTYwNjk4MDY4NywtMTkyOTM0NDU2OSwxMzAxNzk1
NDAzLDEwNDk4MDc1NTRdfQ==
-->