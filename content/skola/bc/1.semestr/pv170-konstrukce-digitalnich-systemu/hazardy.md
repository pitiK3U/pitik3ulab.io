+++
title = "06 - Hazardy komb. obvodu"
date = 2020-11-24
+++

# Hazardy
- Každé součástky mají různé dlouhé zpoždění
- Hazardy (glitches) = reakce obvodu na změny vektoru vstupních proměnných; nadbytečné nevyžadované změny, neodpovídají logickému modelu, ale fyzikálnímu
- dělíme na:
	- funkční hazardy - hazard nevhodné změny vstupních proměnných
	- logické hazardy - hazard realizace navržené struktury s reálnými prvky
- Například přechod z 0111 na 1000 se mění 4 proměnné, v průběhu změny může nastat nevyžádaná změna
- Jak eliminovat:
	- instalace filtru na výstupech logického obvody
	- eliminace vícenásobných změn vstupů obvodů - aplikace tzv. fundamentálního režimu
	- synchronizace kombinačního obvodu - zavedení vzorkovacího signálu

logické hazardy
- statický hazard - při změně vstupního vektoru, se má zachovat výstupní stav, ale v průběhu se změní: 1 -> 0 -> 1 místo 1 -> 1
	- přidáme implikant, který překryje dobu toho statického hazardu = maskování hazardů
- dynamický hazard - při změně vstupního vektory, se má změnit výstupní stav, ale změní se víckrat: 1 -> 0 -> 1 -> 0 místo 1 -> 0
	- 
<!--stackedit_data:
eyJoaXN0b3J5IjpbNzU2NDc4ODE2LDEzMTM1ODE2ODUsLTY4Mz
Y1MjA2Nl19
-->