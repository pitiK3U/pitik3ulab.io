+++
title = "03 - Logické systémy a logické funkce"
date = 2020-10-20
+++

# Logické systémy
Vybavení, která slouží k 
## Řízení systému
- technické vybavení
- programové vybavení
- operátor (lidská obsluha)

Cílem řízení procesu je ze vstupních zdrojů získat výstupní produkty.
Řídící systém je řízený programem/algoritmem a řídí proces.
Jsou zde **vnější vlivy** a mohou nastat **poruchy**(vnitřní změna).

Postup návrhu řídícího systému:
- identifikace procesu (analýza)
- vytvoření matematického modelu procesu (myšlenkový postup)
- návrh algoritmu logického řízení (model řídícího automatu)
- dekompozice řízení, dekompozice řídícího automatu
- syntéza automatu, obvodová realizace automatu

Řadič
Řídící blok

Teorie řízení je založeno na teorii automatů:
- abstraktní teorii automatů
- aplikovanou teorii automatů (tento předmět)

## Teorie automatů

## Logické systémy
= dynamické systémy s diskrétním parametrem, kde v/v proměnné mohou být pouze 0 nebo 1.
= binární nebo boolovské systémy
### Charakteristika
### Čas
Mění svůj stav v čase, čas vnímáme spojeně.
U logických systému se používá diskrétní čas.
sychronnní 
asynchronní
semiasynchronní 

U synchronních
### Struktura
### Dělení
- skvenční
- kombinační obvody = sekvenční + kus paměti
#### Sekvenční
funkce generuje jednak výstup, tak následující stav

logický člen = **hradlo**

# Logické funkce - velmi důležité
= funkční předpis pro kombinační zobrazení množiny vstupních vektorů na množinu výstupních vektorů.

**f : X → Z** nebo **z = f (x)**

Pro logický obvod s **n** vstupy a **m** výstupy je maximální počet funkcí **(2^(2^n))^m**.

## Booleovské funkce
**f : X → {0, 1}**
Pro logický obvod s **n** vstupy je tedy maximální počet funkcí **(2^(2^n))**.

Pro 2 vstupní proměnné je jich tedy 16. Některé důležité:
| | log. výraz | popis|
|-|-|-|
|`AND`|Z = x1*x2|logický součin| 
|`XOR`|Z = !x1 * x2+ x1 * !x2|nonekvivalence|
|`OR`|Z = x1 + x2|logický součet|
|**`NOR`**|Z = !(x1 + x2)|Pierceova funkce|
|`NXOR`|Z = x1 * x2 + !x1 * !x2|ekvivalence
|`NAND`|Z = !(x1 * x2)| Schefferova funkce|
 
**Minterm** = neobsahuje booleovský součet
**Maxterm** = neobsahuje booleovský součin 

### Úplná normální disjunktivní forma
= vyjadřuje situaci, kdy logická funkce nabývá hodnotu **logické 1**.
Je to tedy suma všech mintermů.

Implikanty nabývající hodnotu logické 0 můžeme vynechat.

### Úplná normální konjunktivní forma
= vyjadřuje situaci, kdy logická funkce nabývá hodnotu **logické 0**.
Je to tedy součet všech maxtermů.

Implikanty nabývající hodnotu logické 1 můžeme vynechat.

**Převod mezi úplnými normálními formami lze uskutečnit pomocí De Morganových pravidel.**
<!--stackedit_data:
eyJoaXN0b3J5IjpbOTAxMDEwNTc2LC0zMTc3ODEzODEsODAwOD
k4MjAxLC0zMTA4NDMyMTIsLTEwODIxMjAzODUsMTU5NzU3MzI2
OCwtOTM1ODk3MzgxLC03ODkwMDc2MDMsLTExMTY5NTEyNTUsLT
EwMzA2MTI1NDIsNTI0Nzg2NjEwLC00Mjc0Njk4OTcsLTE3NDY3
OTY3NDQsMTAzMzI4NjUwXX0=
-->