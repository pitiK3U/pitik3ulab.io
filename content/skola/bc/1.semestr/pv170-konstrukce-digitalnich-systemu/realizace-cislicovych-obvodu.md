+++
title = "04 - Realizace číslicových obvodů"
date = 2020-10-27
+++

# Realizační prvky logických obvodů
V digitálním světě stačí 2 hodnoty, které udává nějaká fyzikální veličina.
Používáme zatím dvě veličiny, buď napětí, nebo náboj.
## Značky číslicových obvodů
- Značky `AND`, `OR` a `NEGACE` se lehce realizují.
- **Hradlo** = obvod realizující Booleovskou operaci.
### Realizační prvky
#### Kontaktní logika
- Relé.
- Diody = polovodičový prvek = vede proud jedním směrem. 
- **DTL** = diodově tranzistorová logika
- Pomocí `NOR` nebo `NAND` lze realizovat všechny logické obvody.
#### Tranzistorová logika
- TTL = tranzistor-tranzistor logic
- ECL = emitor ? logic; v moderní době se tolik nepoužívají, byly velice rychlé až GHz, ale měli velkou spotřebu
##### CMOS
- CMOS = komplementární MOS; nejpoužívanější obvody
	- tranzistor se otevírá/zavírá vzhledem k napětí
	- tranzistor P = otevřený když je vstup pozitivní 1
	- tranzistor N = otevřený když je vstup negativní 0
	- Není zde odpor == jsou úsporné.
	- Mají malý odběr, když se proud nemění.

Prahová úroveň = mění se napětí
lavinový průraz = poškození, velké napájecí napětí

#### TTL
- Při změně **z 0 do 1** je **velký odběr** proudu.
- Důležité je zpoždění = doba mezi změnou napětí na vstupu a na výstupu.
- Šumová imunita = poruchy a magnetická indukce ovlivňují výstupní proud => rozmezí pro která jsou vysoké a nízké hodnoty garantované
- Obvody s třístavovým obvodem: 
- Rušení = při dlouhém drátě se dějí divné věci.
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTIwOTI3NDcyOThdfQ==
-->