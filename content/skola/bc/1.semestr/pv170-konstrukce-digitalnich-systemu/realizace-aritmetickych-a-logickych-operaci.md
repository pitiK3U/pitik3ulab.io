+++
title = "02 - Realizace aritmetických a logických operací"
date = 2020-10-13
+++

# Realizace aritmetických a logických operací

- 4 registry
- sumátor = sečte 2 binární čísla aritmeticky
- C?? -> přetečení
- výsledek se zapíše do registru Z

## Sumátor/ALU
- logické operace nad čísly
- aritmetické operace nad čísly
- převody nad jednotlivými obrazy čísel

## Logické  Operace
- porovnání (compare) - pomocí komparačního obvodu = komparátor, **Komparaci lze také provést pomocí operace odečítání.**
- nastavování/nulování jednotlivých bitů = operace **OR**/operace **AND**
- operace **INV** = inverze hodnoty

## Aritmetické operace
### Sčítání
- musí se respektovat řádová čárka
- přenos může nastat i z řádu nejvyššího
- |A| + |B| = Sum

### Odčítání
- zápůjčka (delivery) = musím si zapůjčit z vyššího řádu
- obchází se slučováním (merging)
<!-- markdown cheats -->
Chyby, které můžou nastat
- součet dvou bin. čís. v doplňk. kódu
	- v případě dvou přenosů, přes znaménkový bit -> vpohodě
	- v případě přetečení ze znamenkového bitu -> overflow
	- v případě přetečení do znamenkového bitu -> overflow

### Násobení
- běžně naprogramované jako makro, které násobí přes sumátor
- moderní procesory mají násobičku
- se znaménkem stejně jako v desítkové soustavě
- výsledek násobení má 2x větší velikost
- násobenec, násobitel
- **Jdeme po bitech jednoho čísla a děláme bitový součin s bity druhého a posuneme podle indexu doleva. Všechny tyto čísla sečteme.**

### Dělení (dividing)
- `A` dělenec (dividend), `B` dělitel (divisor), `R` podíl, `D` zbytek
- Od nejvyššího řádu:
	- Pro úsporu místa může být `D-R` spojené.
	- Dělenec `A` posouváme od nejvyššího bitu dělence do nejnižšího bitu slova `D`. (`<<`)
	- `-B` je dělitel převeden na záporné číslo.
	- `D := D + (-B)`
	- Je-li `D` záporné, `R := R << 1` a obnovíme zbytek `D := D + B`.
	- Jinak, `R := R + 1 << 1` a zbytek se neobnovuje.
	- Proces končí přesunutím celého `A` do `D`. 
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTc0OTI1OTMwNywtODQ1MDQ0MDAzLC0xNz
kwNjQzMDEzLDgxMTE4NzEzLC0yMDIzMTIxNDEyLC02MDAwNzk4
OTcsLTIxMTQ1NjQ4MDgsLTEyMjQ5MDkzNTUsLTczMjgwMjcyNi
wtMTUwNTM4NjEzLC0xNzUzNzgxMjMxLC0xNTM4NDgxODE1LC0y
MzkxMTQyMTgsODEzNzEwNTUyLDE4MDk0OTQ1NzAsLTE1NTk4MT
Y2NjYsLTE3NDY3NTQwNTZdfQ==
-->