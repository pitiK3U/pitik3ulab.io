#!/bin/env bash

cd public

filename="sw.js"

htmls=$(find . -name '*.html' -printf "'%p',")
#echo "${htmls}"

sed -i "s|'./index.html',|${htmls}|" $filename
